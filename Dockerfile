FROM registry.gitlab.com/hreysenbach/cocotb_ghdl_container/cocotb:v1.0.0

COPY requirements.txt ./

RUN pip install --user -r requirements.txt

ENTRYPOINT [ "/bin/bash", "-l", "-c" ]