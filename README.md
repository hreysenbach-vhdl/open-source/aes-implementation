# AES Implementation

Implementation of AES in VHDL.

To run the simulation you can build the docker container with 
`docker build -t <tag_name> .`

Then run the container with 
`docker run -it -v $(pwd):/home/cocotb/workdir/ <tag_name> /bin/bash`

From the command prompt navigate to `sim/cocotb` and type `make`. This will 
compile the VHDL with GHDL and run the CocoTB simulation.

The different tests can be enabled as required, by default the only enabled 
tests are those which test complete encryption and decryption with PyCryptoDome 
for verification.

The VHDL is fully synthesisable using VHDL 1993.

I have done basic compliation tests on a Zynq-7020 device. 

For the high throughput mode, the design meet timing at 200MHz and consumed 
approximately 13000 LUTs, the results can be seen here:
![Implementation results on a Zynq-7020 device for high throughput](readme_pics/zynq_7020_key_256_bit_high_throughput.png)

For the low throughput mode, the design meet timing at 200MHz and consumed 
approximately 3300 LUTs, the results can be seen here:
![Implementation results on a Zynq-7020 device for low throughput](readme_pics/zynq_7020_key_256_bit_low_throughput.png)




