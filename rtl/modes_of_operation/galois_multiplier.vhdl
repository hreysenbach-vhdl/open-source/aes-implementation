-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;

entity galois_multiplier is
    port (
        a                   : in    std_logic_vector(127 downto 0);
        b                   : in    std_logic_vector(127 downto 0);
        c                   : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of galois_multiplier is
begin
    c(  0) <= 
        (b(  0) and (a(  0))) xor
        (b(  1) and (a(127))) xor
        (b(  2) and (a(126))) xor
        (b(  3) and (a(125))) xor
        (b(  4) and (a(124))) xor
        (b(  5) and (a(123))) xor
        (b(  6) and (a(122))) xor
        (b(  7) and (a(121))) xor
        (b(  8) and (a(120))) xor
        (b(  9) and (a(119))) xor
        (b( 10) and (a(118))) xor
        (b( 11) and (a(117))) xor
        (b( 12) and (a(116))) xor
        (b( 13) and (a(115))) xor
        (b( 14) and (a(114))) xor
        (b( 15) and (a(113))) xor
        (b( 16) and (a(112))) xor
        (b( 17) and (a(111))) xor
        (b( 18) and (a(110))) xor
        (b( 19) and (a(109))) xor
        (b( 20) and (a(108))) xor
        (b( 21) and (a(107))) xor
        (b( 22) and (a(106))) xor
        (b( 23) and (a(105))) xor
        (b( 24) and (a(104))) xor
        (b( 25) and (a(103))) xor
        (b( 26) and (a(102))) xor
        (b( 27) and (a(101))) xor
        (b( 28) and (a(100))) xor
        (b( 29) and (a( 99))) xor
        (b( 30) and (a( 98))) xor
        (b( 31) and (a( 97))) xor
        (b( 32) and (a( 96))) xor
        (b( 33) and (a( 95))) xor
        (b( 34) and (a( 94))) xor
        (b( 35) and (a( 93))) xor
        (b( 36) and (a( 92))) xor
        (b( 37) and (a( 91))) xor
        (b( 38) and (a( 90))) xor
        (b( 39) and (a( 89))) xor
        (b( 40) and (a( 88))) xor
        (b( 41) and (a( 87))) xor
        (b( 42) and (a( 86))) xor
        (b( 43) and (a( 85))) xor
        (b( 44) and (a( 84))) xor
        (b( 45) and (a( 83))) xor
        (b( 46) and (a( 82))) xor
        (b( 47) and (a( 81))) xor
        (b( 48) and (a( 80))) xor
        (b( 49) and (a( 79))) xor
        (b( 50) and (a( 78))) xor
        (b( 51) and (a( 77))) xor
        (b( 52) and (a( 76))) xor
        (b( 53) and (a( 75))) xor
        (b( 54) and (a( 74))) xor
        (b( 55) and (a( 73))) xor
        (b( 56) and (a( 72))) xor
        (b( 57) and (a( 71))) xor
        (b( 58) and (a( 70))) xor
        (b( 59) and (a( 69))) xor
        (b( 60) and (a( 68))) xor
        (b( 61) and (a( 67))) xor
        (b( 62) and (a( 66))) xor
        (b( 63) and (a( 65))) xor
        (b( 64) and (a( 64))) xor
        (b( 65) and (a( 63))) xor
        (b( 66) and (a( 62))) xor
        (b( 67) and (a( 61))) xor
        (b( 68) and (a( 60))) xor
        (b( 69) and (a( 59))) xor
        (b( 70) and (a( 58))) xor
        (b( 71) and (a( 57))) xor
        (b( 72) and (a( 56))) xor
        (b( 73) and (a( 55))) xor
        (b( 74) and (a( 54))) xor
        (b( 75) and (a( 53))) xor
        (b( 76) and (a( 52))) xor
        (b( 77) and (a( 51))) xor
        (b( 78) and (a( 50))) xor
        (b( 79) and (a( 49))) xor
        (b( 80) and (a( 48))) xor
        (b( 81) and (a( 47))) xor
        (b( 82) and (a( 46))) xor
        (b( 83) and (a( 45))) xor
        (b( 84) and (a( 44))) xor
        (b( 85) and (a( 43))) xor
        (b( 86) and (a( 42))) xor
        (b( 87) and (a( 41))) xor
        (b( 88) and (a( 40))) xor
        (b( 89) and (a( 39))) xor
        (b( 90) and (a( 38))) xor
        (b( 91) and (a( 37))) xor
        (b( 92) and (a( 36))) xor
        (b( 93) and (a( 35))) xor
        (b( 94) and (a( 34))) xor
        (b( 95) and (a( 33))) xor
        (b( 96) and (a( 32))) xor
        (b( 97) and (a( 31))) xor
        (b( 98) and (a( 30))) xor
        (b( 99) and (a( 29))) xor
        (b(100) and (a( 28))) xor
        (b(101) and (a( 27))) xor
        (b(102) and (a( 26))) xor
        (b(103) and (a( 25))) xor
        (b(104) and (a( 24))) xor
        (b(105) and (a( 23))) xor
        (b(106) and (a( 22))) xor
        (b(107) and (a( 21))) xor
        (b(108) and (a( 20))) xor
        (b(109) and (a( 19))) xor
        (b(110) and (a( 18))) xor
        (b(111) and (a( 17))) xor
        (b(112) and (a( 16))) xor
        (b(113) and (a( 15))) xor
        (b(114) and (a( 14))) xor
        (b(115) and (a( 13))) xor
        (b(116) and (a( 12))) xor
        (b(117) and (a( 11))) xor
        (b(118) and (a( 10))) xor
        (b(119) and (a(  9))) xor
        (b(120) and (a(  8))) xor
        (b(121) and (a(  7))) xor
        (b(122) and (a(  6) xor a(127))) xor
        (b(123) and (a(  5) xor a(126))) xor
        (b(124) and (a(  4) xor a(125))) xor
        (b(125) and (a(  3) xor a(124))) xor
        (b(126) and (a(  2) xor a(123))) xor
        (b(127) and (a(  1) xor a(122) xor a(127)));
    c(  1) <= 
        (b(  0) and (a(  1))) xor
        (b(  1) and (a(  0) xor a(127))) xor
        (b(  2) and (a(126) xor a(127))) xor
        (b(  3) and (a(125) xor a(126))) xor
        (b(  4) and (a(124) xor a(125))) xor
        (b(  5) and (a(123) xor a(124))) xor
        (b(  6) and (a(122) xor a(123))) xor
        (b(  7) and (a(121) xor a(122))) xor
        (b(  8) and (a(120) xor a(121))) xor
        (b(  9) and (a(119) xor a(120))) xor
        (b( 10) and (a(118) xor a(119))) xor
        (b( 11) and (a(117) xor a(118))) xor
        (b( 12) and (a(116) xor a(117))) xor
        (b( 13) and (a(115) xor a(116))) xor
        (b( 14) and (a(114) xor a(115))) xor
        (b( 15) and (a(113) xor a(114))) xor
        (b( 16) and (a(112) xor a(113))) xor
        (b( 17) and (a(111) xor a(112))) xor
        (b( 18) and (a(110) xor a(111))) xor
        (b( 19) and (a(109) xor a(110))) xor
        (b( 20) and (a(108) xor a(109))) xor
        (b( 21) and (a(107) xor a(108))) xor
        (b( 22) and (a(106) xor a(107))) xor
        (b( 23) and (a(105) xor a(106))) xor
        (b( 24) and (a(104) xor a(105))) xor
        (b( 25) and (a(103) xor a(104))) xor
        (b( 26) and (a(102) xor a(103))) xor
        (b( 27) and (a(101) xor a(102))) xor
        (b( 28) and (a(100) xor a(101))) xor
        (b( 29) and (a( 99) xor a(100))) xor
        (b( 30) and (a( 98) xor a( 99))) xor
        (b( 31) and (a( 97) xor a( 98))) xor
        (b( 32) and (a( 96) xor a( 97))) xor
        (b( 33) and (a( 95) xor a( 96))) xor
        (b( 34) and (a( 94) xor a( 95))) xor
        (b( 35) and (a( 93) xor a( 94))) xor
        (b( 36) and (a( 92) xor a( 93))) xor
        (b( 37) and (a( 91) xor a( 92))) xor
        (b( 38) and (a( 90) xor a( 91))) xor
        (b( 39) and (a( 89) xor a( 90))) xor
        (b( 40) and (a( 88) xor a( 89))) xor
        (b( 41) and (a( 87) xor a( 88))) xor
        (b( 42) and (a( 86) xor a( 87))) xor
        (b( 43) and (a( 85) xor a( 86))) xor
        (b( 44) and (a( 84) xor a( 85))) xor
        (b( 45) and (a( 83) xor a( 84))) xor
        (b( 46) and (a( 82) xor a( 83))) xor
        (b( 47) and (a( 81) xor a( 82))) xor
        (b( 48) and (a( 80) xor a( 81))) xor
        (b( 49) and (a( 79) xor a( 80))) xor
        (b( 50) and (a( 78) xor a( 79))) xor
        (b( 51) and (a( 77) xor a( 78))) xor
        (b( 52) and (a( 76) xor a( 77))) xor
        (b( 53) and (a( 75) xor a( 76))) xor
        (b( 54) and (a( 74) xor a( 75))) xor
        (b( 55) and (a( 73) xor a( 74))) xor
        (b( 56) and (a( 72) xor a( 73))) xor
        (b( 57) and (a( 71) xor a( 72))) xor
        (b( 58) and (a( 70) xor a( 71))) xor
        (b( 59) and (a( 69) xor a( 70))) xor
        (b( 60) and (a( 68) xor a( 69))) xor
        (b( 61) and (a( 67) xor a( 68))) xor
        (b( 62) and (a( 66) xor a( 67))) xor
        (b( 63) and (a( 65) xor a( 66))) xor
        (b( 64) and (a( 64) xor a( 65))) xor
        (b( 65) and (a( 63) xor a( 64))) xor
        (b( 66) and (a( 62) xor a( 63))) xor
        (b( 67) and (a( 61) xor a( 62))) xor
        (b( 68) and (a( 60) xor a( 61))) xor
        (b( 69) and (a( 59) xor a( 60))) xor
        (b( 70) and (a( 58) xor a( 59))) xor
        (b( 71) and (a( 57) xor a( 58))) xor
        (b( 72) and (a( 56) xor a( 57))) xor
        (b( 73) and (a( 55) xor a( 56))) xor
        (b( 74) and (a( 54) xor a( 55))) xor
        (b( 75) and (a( 53) xor a( 54))) xor
        (b( 76) and (a( 52) xor a( 53))) xor
        (b( 77) and (a( 51) xor a( 52))) xor
        (b( 78) and (a( 50) xor a( 51))) xor
        (b( 79) and (a( 49) xor a( 50))) xor
        (b( 80) and (a( 48) xor a( 49))) xor
        (b( 81) and (a( 47) xor a( 48))) xor
        (b( 82) and (a( 46) xor a( 47))) xor
        (b( 83) and (a( 45) xor a( 46))) xor
        (b( 84) and (a( 44) xor a( 45))) xor
        (b( 85) and (a( 43) xor a( 44))) xor
        (b( 86) and (a( 42) xor a( 43))) xor
        (b( 87) and (a( 41) xor a( 42))) xor
        (b( 88) and (a( 40) xor a( 41))) xor
        (b( 89) and (a( 39) xor a( 40))) xor
        (b( 90) and (a( 38) xor a( 39))) xor
        (b( 91) and (a( 37) xor a( 38))) xor
        (b( 92) and (a( 36) xor a( 37))) xor
        (b( 93) and (a( 35) xor a( 36))) xor
        (b( 94) and (a( 34) xor a( 35))) xor
        (b( 95) and (a( 33) xor a( 34))) xor
        (b( 96) and (a( 32) xor a( 33))) xor
        (b( 97) and (a( 31) xor a( 32))) xor
        (b( 98) and (a( 30) xor a( 31))) xor
        (b( 99) and (a( 29) xor a( 30))) xor
        (b(100) and (a( 28) xor a( 29))) xor
        (b(101) and (a( 27) xor a( 28))) xor
        (b(102) and (a( 26) xor a( 27))) xor
        (b(103) and (a( 25) xor a( 26))) xor
        (b(104) and (a( 24) xor a( 25))) xor
        (b(105) and (a( 23) xor a( 24))) xor
        (b(106) and (a( 22) xor a( 23))) xor
        (b(107) and (a( 21) xor a( 22))) xor
        (b(108) and (a( 20) xor a( 21))) xor
        (b(109) and (a( 19) xor a( 20))) xor
        (b(110) and (a( 18) xor a( 19))) xor
        (b(111) and (a( 17) xor a( 18))) xor
        (b(112) and (a( 16) xor a( 17))) xor
        (b(113) and (a( 15) xor a( 16))) xor
        (b(114) and (a( 14) xor a( 15))) xor
        (b(115) and (a( 13) xor a( 14))) xor
        (b(116) and (a( 12) xor a( 13))) xor
        (b(117) and (a( 11) xor a( 12))) xor
        (b(118) and (a( 10) xor a( 11))) xor
        (b(119) and (a(  9) xor a( 10))) xor
        (b(120) and (a(  8) xor a(  9))) xor
        (b(121) and (a(  7) xor a(  8))) xor
        (b(122) and (a(  6) xor a(  7) xor a(127))) xor
        (b(123) and (a(  5) xor a(  6) xor a(126) xor a(127))) xor
        (b(124) and (a(  4) xor a(  5) xor a(125) xor a(126))) xor
        (b(125) and (a(  3) xor a(  4) xor a(124) xor a(125))) xor
        (b(126) and (a(  2) xor a(  3) xor a(123) xor a(124))) xor
        (b(127) and (a(  1) xor a(  2) xor a(122) xor a(123) xor a(127)));
    c(  2) <= 
        (b(  0) and (a(  2))) xor
        (b(  1) and (a(  1) xor a(127))) xor
        (b(  2) and (a(  0) xor a(126) xor a(127))) xor
        (b(  3) and (a(125) xor a(126) xor a(127))) xor
        (b(  4) and (a(124) xor a(125) xor a(126))) xor
        (b(  5) and (a(123) xor a(124) xor a(125))) xor
        (b(  6) and (a(122) xor a(123) xor a(124))) xor
        (b(  7) and (a(121) xor a(122) xor a(123))) xor
        (b(  8) and (a(120) xor a(121) xor a(122))) xor
        (b(  9) and (a(119) xor a(120) xor a(121))) xor
        (b( 10) and (a(118) xor a(119) xor a(120))) xor
        (b( 11) and (a(117) xor a(118) xor a(119))) xor
        (b( 12) and (a(116) xor a(117) xor a(118))) xor
        (b( 13) and (a(115) xor a(116) xor a(117))) xor
        (b( 14) and (a(114) xor a(115) xor a(116))) xor
        (b( 15) and (a(113) xor a(114) xor a(115))) xor
        (b( 16) and (a(112) xor a(113) xor a(114))) xor
        (b( 17) and (a(111) xor a(112) xor a(113))) xor
        (b( 18) and (a(110) xor a(111) xor a(112))) xor
        (b( 19) and (a(109) xor a(110) xor a(111))) xor
        (b( 20) and (a(108) xor a(109) xor a(110))) xor
        (b( 21) and (a(107) xor a(108) xor a(109))) xor
        (b( 22) and (a(106) xor a(107) xor a(108))) xor
        (b( 23) and (a(105) xor a(106) xor a(107))) xor
        (b( 24) and (a(104) xor a(105) xor a(106))) xor
        (b( 25) and (a(103) xor a(104) xor a(105))) xor
        (b( 26) and (a(102) xor a(103) xor a(104))) xor
        (b( 27) and (a(101) xor a(102) xor a(103))) xor
        (b( 28) and (a(100) xor a(101) xor a(102))) xor
        (b( 29) and (a( 99) xor a(100) xor a(101))) xor
        (b( 30) and (a( 98) xor a( 99) xor a(100))) xor
        (b( 31) and (a( 97) xor a( 98) xor a( 99))) xor
        (b( 32) and (a( 96) xor a( 97) xor a( 98))) xor
        (b( 33) and (a( 95) xor a( 96) xor a( 97))) xor
        (b( 34) and (a( 94) xor a( 95) xor a( 96))) xor
        (b( 35) and (a( 93) xor a( 94) xor a( 95))) xor
        (b( 36) and (a( 92) xor a( 93) xor a( 94))) xor
        (b( 37) and (a( 91) xor a( 92) xor a( 93))) xor
        (b( 38) and (a( 90) xor a( 91) xor a( 92))) xor
        (b( 39) and (a( 89) xor a( 90) xor a( 91))) xor
        (b( 40) and (a( 88) xor a( 89) xor a( 90))) xor
        (b( 41) and (a( 87) xor a( 88) xor a( 89))) xor
        (b( 42) and (a( 86) xor a( 87) xor a( 88))) xor
        (b( 43) and (a( 85) xor a( 86) xor a( 87))) xor
        (b( 44) and (a( 84) xor a( 85) xor a( 86))) xor
        (b( 45) and (a( 83) xor a( 84) xor a( 85))) xor
        (b( 46) and (a( 82) xor a( 83) xor a( 84))) xor
        (b( 47) and (a( 81) xor a( 82) xor a( 83))) xor
        (b( 48) and (a( 80) xor a( 81) xor a( 82))) xor
        (b( 49) and (a( 79) xor a( 80) xor a( 81))) xor
        (b( 50) and (a( 78) xor a( 79) xor a( 80))) xor
        (b( 51) and (a( 77) xor a( 78) xor a( 79))) xor
        (b( 52) and (a( 76) xor a( 77) xor a( 78))) xor
        (b( 53) and (a( 75) xor a( 76) xor a( 77))) xor
        (b( 54) and (a( 74) xor a( 75) xor a( 76))) xor
        (b( 55) and (a( 73) xor a( 74) xor a( 75))) xor
        (b( 56) and (a( 72) xor a( 73) xor a( 74))) xor
        (b( 57) and (a( 71) xor a( 72) xor a( 73))) xor
        (b( 58) and (a( 70) xor a( 71) xor a( 72))) xor
        (b( 59) and (a( 69) xor a( 70) xor a( 71))) xor
        (b( 60) and (a( 68) xor a( 69) xor a( 70))) xor
        (b( 61) and (a( 67) xor a( 68) xor a( 69))) xor
        (b( 62) and (a( 66) xor a( 67) xor a( 68))) xor
        (b( 63) and (a( 65) xor a( 66) xor a( 67))) xor
        (b( 64) and (a( 64) xor a( 65) xor a( 66))) xor
        (b( 65) and (a( 63) xor a( 64) xor a( 65))) xor
        (b( 66) and (a( 62) xor a( 63) xor a( 64))) xor
        (b( 67) and (a( 61) xor a( 62) xor a( 63))) xor
        (b( 68) and (a( 60) xor a( 61) xor a( 62))) xor
        (b( 69) and (a( 59) xor a( 60) xor a( 61))) xor
        (b( 70) and (a( 58) xor a( 59) xor a( 60))) xor
        (b( 71) and (a( 57) xor a( 58) xor a( 59))) xor
        (b( 72) and (a( 56) xor a( 57) xor a( 58))) xor
        (b( 73) and (a( 55) xor a( 56) xor a( 57))) xor
        (b( 74) and (a( 54) xor a( 55) xor a( 56))) xor
        (b( 75) and (a( 53) xor a( 54) xor a( 55))) xor
        (b( 76) and (a( 52) xor a( 53) xor a( 54))) xor
        (b( 77) and (a( 51) xor a( 52) xor a( 53))) xor
        (b( 78) and (a( 50) xor a( 51) xor a( 52))) xor
        (b( 79) and (a( 49) xor a( 50) xor a( 51))) xor
        (b( 80) and (a( 48) xor a( 49) xor a( 50))) xor
        (b( 81) and (a( 47) xor a( 48) xor a( 49))) xor
        (b( 82) and (a( 46) xor a( 47) xor a( 48))) xor
        (b( 83) and (a( 45) xor a( 46) xor a( 47))) xor
        (b( 84) and (a( 44) xor a( 45) xor a( 46))) xor
        (b( 85) and (a( 43) xor a( 44) xor a( 45))) xor
        (b( 86) and (a( 42) xor a( 43) xor a( 44))) xor
        (b( 87) and (a( 41) xor a( 42) xor a( 43))) xor
        (b( 88) and (a( 40) xor a( 41) xor a( 42))) xor
        (b( 89) and (a( 39) xor a( 40) xor a( 41))) xor
        (b( 90) and (a( 38) xor a( 39) xor a( 40))) xor
        (b( 91) and (a( 37) xor a( 38) xor a( 39))) xor
        (b( 92) and (a( 36) xor a( 37) xor a( 38))) xor
        (b( 93) and (a( 35) xor a( 36) xor a( 37))) xor
        (b( 94) and (a( 34) xor a( 35) xor a( 36))) xor
        (b( 95) and (a( 33) xor a( 34) xor a( 35))) xor
        (b( 96) and (a( 32) xor a( 33) xor a( 34))) xor
        (b( 97) and (a( 31) xor a( 32) xor a( 33))) xor
        (b( 98) and (a( 30) xor a( 31) xor a( 32))) xor
        (b( 99) and (a( 29) xor a( 30) xor a( 31))) xor
        (b(100) and (a( 28) xor a( 29) xor a( 30))) xor
        (b(101) and (a( 27) xor a( 28) xor a( 29))) xor
        (b(102) and (a( 26) xor a( 27) xor a( 28))) xor
        (b(103) and (a( 25) xor a( 26) xor a( 27))) xor
        (b(104) and (a( 24) xor a( 25) xor a( 26))) xor
        (b(105) and (a( 23) xor a( 24) xor a( 25))) xor
        (b(106) and (a( 22) xor a( 23) xor a( 24))) xor
        (b(107) and (a( 21) xor a( 22) xor a( 23))) xor
        (b(108) and (a( 20) xor a( 21) xor a( 22))) xor
        (b(109) and (a( 19) xor a( 20) xor a( 21))) xor
        (b(110) and (a( 18) xor a( 19) xor a( 20))) xor
        (b(111) and (a( 17) xor a( 18) xor a( 19))) xor
        (b(112) and (a( 16) xor a( 17) xor a( 18))) xor
        (b(113) and (a( 15) xor a( 16) xor a( 17))) xor
        (b(114) and (a( 14) xor a( 15) xor a( 16))) xor
        (b(115) and (a( 13) xor a( 14) xor a( 15))) xor
        (b(116) and (a( 12) xor a( 13) xor a( 14))) xor
        (b(117) and (a( 11) xor a( 12) xor a( 13))) xor
        (b(118) and (a( 10) xor a( 11) xor a( 12))) xor
        (b(119) and (a(  9) xor a( 10) xor a( 11))) xor
        (b(120) and (a(  8) xor a(  9) xor a( 10))) xor
        (b(121) and (a(  7) xor a(  8) xor a(  9))) xor
        (b(122) and (a(  6) xor a(  7) xor a(  8) xor a(127))) xor
        (b(123) and (a(  5) xor a(  6) xor a(  7) xor a(126) xor a(127))) xor
        (b(124) and (a(  4) xor a(  5) xor a(  6) xor a(125) xor a(126) xor a(127))) xor
        (b(125) and (a(  3) xor a(  4) xor a(  5) xor a(124) xor a(125) xor a(126))) xor
        (b(126) and (a(  2) xor a(  3) xor a(  4) xor a(123) xor a(124) xor a(125))) xor
        (b(127) and (a(  1) xor a(  2) xor a(  3) xor a(122) xor a(123) xor a(124) xor a(127)));
    c(  3) <= 
        (b(  0) and (a(  3))) xor
        (b(  1) and (a(  2))) xor
        (b(  2) and (a(  1) xor a(127))) xor
        (b(  3) and (a(  0) xor a(126) xor a(127))) xor
        (b(  4) and (a(125) xor a(126) xor a(127))) xor
        (b(  5) and (a(124) xor a(125) xor a(126))) xor
        (b(  6) and (a(123) xor a(124) xor a(125))) xor
        (b(  7) and (a(122) xor a(123) xor a(124))) xor
        (b(  8) and (a(121) xor a(122) xor a(123))) xor
        (b(  9) and (a(120) xor a(121) xor a(122))) xor
        (b( 10) and (a(119) xor a(120) xor a(121))) xor
        (b( 11) and (a(118) xor a(119) xor a(120))) xor
        (b( 12) and (a(117) xor a(118) xor a(119))) xor
        (b( 13) and (a(116) xor a(117) xor a(118))) xor
        (b( 14) and (a(115) xor a(116) xor a(117))) xor
        (b( 15) and (a(114) xor a(115) xor a(116))) xor
        (b( 16) and (a(113) xor a(114) xor a(115))) xor
        (b( 17) and (a(112) xor a(113) xor a(114))) xor
        (b( 18) and (a(111) xor a(112) xor a(113))) xor
        (b( 19) and (a(110) xor a(111) xor a(112))) xor
        (b( 20) and (a(109) xor a(110) xor a(111))) xor
        (b( 21) and (a(108) xor a(109) xor a(110))) xor
        (b( 22) and (a(107) xor a(108) xor a(109))) xor
        (b( 23) and (a(106) xor a(107) xor a(108))) xor
        (b( 24) and (a(105) xor a(106) xor a(107))) xor
        (b( 25) and (a(104) xor a(105) xor a(106))) xor
        (b( 26) and (a(103) xor a(104) xor a(105))) xor
        (b( 27) and (a(102) xor a(103) xor a(104))) xor
        (b( 28) and (a(101) xor a(102) xor a(103))) xor
        (b( 29) and (a(100) xor a(101) xor a(102))) xor
        (b( 30) and (a( 99) xor a(100) xor a(101))) xor
        (b( 31) and (a( 98) xor a( 99) xor a(100))) xor
        (b( 32) and (a( 97) xor a( 98) xor a( 99))) xor
        (b( 33) and (a( 96) xor a( 97) xor a( 98))) xor
        (b( 34) and (a( 95) xor a( 96) xor a( 97))) xor
        (b( 35) and (a( 94) xor a( 95) xor a( 96))) xor
        (b( 36) and (a( 93) xor a( 94) xor a( 95))) xor
        (b( 37) and (a( 92) xor a( 93) xor a( 94))) xor
        (b( 38) and (a( 91) xor a( 92) xor a( 93))) xor
        (b( 39) and (a( 90) xor a( 91) xor a( 92))) xor
        (b( 40) and (a( 89) xor a( 90) xor a( 91))) xor
        (b( 41) and (a( 88) xor a( 89) xor a( 90))) xor
        (b( 42) and (a( 87) xor a( 88) xor a( 89))) xor
        (b( 43) and (a( 86) xor a( 87) xor a( 88))) xor
        (b( 44) and (a( 85) xor a( 86) xor a( 87))) xor
        (b( 45) and (a( 84) xor a( 85) xor a( 86))) xor
        (b( 46) and (a( 83) xor a( 84) xor a( 85))) xor
        (b( 47) and (a( 82) xor a( 83) xor a( 84))) xor
        (b( 48) and (a( 81) xor a( 82) xor a( 83))) xor
        (b( 49) and (a( 80) xor a( 81) xor a( 82))) xor
        (b( 50) and (a( 79) xor a( 80) xor a( 81))) xor
        (b( 51) and (a( 78) xor a( 79) xor a( 80))) xor
        (b( 52) and (a( 77) xor a( 78) xor a( 79))) xor
        (b( 53) and (a( 76) xor a( 77) xor a( 78))) xor
        (b( 54) and (a( 75) xor a( 76) xor a( 77))) xor
        (b( 55) and (a( 74) xor a( 75) xor a( 76))) xor
        (b( 56) and (a( 73) xor a( 74) xor a( 75))) xor
        (b( 57) and (a( 72) xor a( 73) xor a( 74))) xor
        (b( 58) and (a( 71) xor a( 72) xor a( 73))) xor
        (b( 59) and (a( 70) xor a( 71) xor a( 72))) xor
        (b( 60) and (a( 69) xor a( 70) xor a( 71))) xor
        (b( 61) and (a( 68) xor a( 69) xor a( 70))) xor
        (b( 62) and (a( 67) xor a( 68) xor a( 69))) xor
        (b( 63) and (a( 66) xor a( 67) xor a( 68))) xor
        (b( 64) and (a( 65) xor a( 66) xor a( 67))) xor
        (b( 65) and (a( 64) xor a( 65) xor a( 66))) xor
        (b( 66) and (a( 63) xor a( 64) xor a( 65))) xor
        (b( 67) and (a( 62) xor a( 63) xor a( 64))) xor
        (b( 68) and (a( 61) xor a( 62) xor a( 63))) xor
        (b( 69) and (a( 60) xor a( 61) xor a( 62))) xor
        (b( 70) and (a( 59) xor a( 60) xor a( 61))) xor
        (b( 71) and (a( 58) xor a( 59) xor a( 60))) xor
        (b( 72) and (a( 57) xor a( 58) xor a( 59))) xor
        (b( 73) and (a( 56) xor a( 57) xor a( 58))) xor
        (b( 74) and (a( 55) xor a( 56) xor a( 57))) xor
        (b( 75) and (a( 54) xor a( 55) xor a( 56))) xor
        (b( 76) and (a( 53) xor a( 54) xor a( 55))) xor
        (b( 77) and (a( 52) xor a( 53) xor a( 54))) xor
        (b( 78) and (a( 51) xor a( 52) xor a( 53))) xor
        (b( 79) and (a( 50) xor a( 51) xor a( 52))) xor
        (b( 80) and (a( 49) xor a( 50) xor a( 51))) xor
        (b( 81) and (a( 48) xor a( 49) xor a( 50))) xor
        (b( 82) and (a( 47) xor a( 48) xor a( 49))) xor
        (b( 83) and (a( 46) xor a( 47) xor a( 48))) xor
        (b( 84) and (a( 45) xor a( 46) xor a( 47))) xor
        (b( 85) and (a( 44) xor a( 45) xor a( 46))) xor
        (b( 86) and (a( 43) xor a( 44) xor a( 45))) xor
        (b( 87) and (a( 42) xor a( 43) xor a( 44))) xor
        (b( 88) and (a( 41) xor a( 42) xor a( 43))) xor
        (b( 89) and (a( 40) xor a( 41) xor a( 42))) xor
        (b( 90) and (a( 39) xor a( 40) xor a( 41))) xor
        (b( 91) and (a( 38) xor a( 39) xor a( 40))) xor
        (b( 92) and (a( 37) xor a( 38) xor a( 39))) xor
        (b( 93) and (a( 36) xor a( 37) xor a( 38))) xor
        (b( 94) and (a( 35) xor a( 36) xor a( 37))) xor
        (b( 95) and (a( 34) xor a( 35) xor a( 36))) xor
        (b( 96) and (a( 33) xor a( 34) xor a( 35))) xor
        (b( 97) and (a( 32) xor a( 33) xor a( 34))) xor
        (b( 98) and (a( 31) xor a( 32) xor a( 33))) xor
        (b( 99) and (a( 30) xor a( 31) xor a( 32))) xor
        (b(100) and (a( 29) xor a( 30) xor a( 31))) xor
        (b(101) and (a( 28) xor a( 29) xor a( 30))) xor
        (b(102) and (a( 27) xor a( 28) xor a( 29))) xor
        (b(103) and (a( 26) xor a( 27) xor a( 28))) xor
        (b(104) and (a( 25) xor a( 26) xor a( 27))) xor
        (b(105) and (a( 24) xor a( 25) xor a( 26))) xor
        (b(106) and (a( 23) xor a( 24) xor a( 25))) xor
        (b(107) and (a( 22) xor a( 23) xor a( 24))) xor
        (b(108) and (a( 21) xor a( 22) xor a( 23))) xor
        (b(109) and (a( 20) xor a( 21) xor a( 22))) xor
        (b(110) and (a( 19) xor a( 20) xor a( 21))) xor
        (b(111) and (a( 18) xor a( 19) xor a( 20))) xor
        (b(112) and (a( 17) xor a( 18) xor a( 19))) xor
        (b(113) and (a( 16) xor a( 17) xor a( 18))) xor
        (b(114) and (a( 15) xor a( 16) xor a( 17))) xor
        (b(115) and (a( 14) xor a( 15) xor a( 16))) xor
        (b(116) and (a( 13) xor a( 14) xor a( 15))) xor
        (b(117) and (a( 12) xor a( 13) xor a( 14))) xor
        (b(118) and (a( 11) xor a( 12) xor a( 13))) xor
        (b(119) and (a( 10) xor a( 11) xor a( 12))) xor
        (b(120) and (a(  9) xor a( 10) xor a( 11))) xor
        (b(121) and (a(  8) xor a(  9) xor a( 10))) xor
        (b(122) and (a(  7) xor a(  8) xor a(  9))) xor
        (b(123) and (a(  6) xor a(  7) xor a(  8) xor a(127))) xor
        (b(124) and (a(  5) xor a(  6) xor a(  7) xor a(126) xor a(127))) xor
        (b(125) and (a(  4) xor a(  5) xor a(  6) xor a(125) xor a(126) xor a(127))) xor
        (b(126) and (a(  3) xor a(  4) xor a(  5) xor a(124) xor a(125) xor a(126))) xor
        (b(127) and (a(  2) xor a(  3) xor a(  4) xor a(123) xor a(124) xor a(125)));
    c(  4) <= 
        (b(  0) and (a(  4))) xor
        (b(  1) and (a(  3))) xor
        (b(  2) and (a(  2))) xor
        (b(  3) and (a(  1) xor a(127))) xor
        (b(  4) and (a(  0) xor a(126) xor a(127))) xor
        (b(  5) and (a(125) xor a(126) xor a(127))) xor
        (b(  6) and (a(124) xor a(125) xor a(126))) xor
        (b(  7) and (a(123) xor a(124) xor a(125))) xor
        (b(  8) and (a(122) xor a(123) xor a(124))) xor
        (b(  9) and (a(121) xor a(122) xor a(123))) xor
        (b( 10) and (a(120) xor a(121) xor a(122))) xor
        (b( 11) and (a(119) xor a(120) xor a(121))) xor
        (b( 12) and (a(118) xor a(119) xor a(120))) xor
        (b( 13) and (a(117) xor a(118) xor a(119))) xor
        (b( 14) and (a(116) xor a(117) xor a(118))) xor
        (b( 15) and (a(115) xor a(116) xor a(117))) xor
        (b( 16) and (a(114) xor a(115) xor a(116))) xor
        (b( 17) and (a(113) xor a(114) xor a(115))) xor
        (b( 18) and (a(112) xor a(113) xor a(114))) xor
        (b( 19) and (a(111) xor a(112) xor a(113))) xor
        (b( 20) and (a(110) xor a(111) xor a(112))) xor
        (b( 21) and (a(109) xor a(110) xor a(111))) xor
        (b( 22) and (a(108) xor a(109) xor a(110))) xor
        (b( 23) and (a(107) xor a(108) xor a(109))) xor
        (b( 24) and (a(106) xor a(107) xor a(108))) xor
        (b( 25) and (a(105) xor a(106) xor a(107))) xor
        (b( 26) and (a(104) xor a(105) xor a(106))) xor
        (b( 27) and (a(103) xor a(104) xor a(105))) xor
        (b( 28) and (a(102) xor a(103) xor a(104))) xor
        (b( 29) and (a(101) xor a(102) xor a(103))) xor
        (b( 30) and (a(100) xor a(101) xor a(102))) xor
        (b( 31) and (a( 99) xor a(100) xor a(101))) xor
        (b( 32) and (a( 98) xor a( 99) xor a(100))) xor
        (b( 33) and (a( 97) xor a( 98) xor a( 99))) xor
        (b( 34) and (a( 96) xor a( 97) xor a( 98))) xor
        (b( 35) and (a( 95) xor a( 96) xor a( 97))) xor
        (b( 36) and (a( 94) xor a( 95) xor a( 96))) xor
        (b( 37) and (a( 93) xor a( 94) xor a( 95))) xor
        (b( 38) and (a( 92) xor a( 93) xor a( 94))) xor
        (b( 39) and (a( 91) xor a( 92) xor a( 93))) xor
        (b( 40) and (a( 90) xor a( 91) xor a( 92))) xor
        (b( 41) and (a( 89) xor a( 90) xor a( 91))) xor
        (b( 42) and (a( 88) xor a( 89) xor a( 90))) xor
        (b( 43) and (a( 87) xor a( 88) xor a( 89))) xor
        (b( 44) and (a( 86) xor a( 87) xor a( 88))) xor
        (b( 45) and (a( 85) xor a( 86) xor a( 87))) xor
        (b( 46) and (a( 84) xor a( 85) xor a( 86))) xor
        (b( 47) and (a( 83) xor a( 84) xor a( 85))) xor
        (b( 48) and (a( 82) xor a( 83) xor a( 84))) xor
        (b( 49) and (a( 81) xor a( 82) xor a( 83))) xor
        (b( 50) and (a( 80) xor a( 81) xor a( 82))) xor
        (b( 51) and (a( 79) xor a( 80) xor a( 81))) xor
        (b( 52) and (a( 78) xor a( 79) xor a( 80))) xor
        (b( 53) and (a( 77) xor a( 78) xor a( 79))) xor
        (b( 54) and (a( 76) xor a( 77) xor a( 78))) xor
        (b( 55) and (a( 75) xor a( 76) xor a( 77))) xor
        (b( 56) and (a( 74) xor a( 75) xor a( 76))) xor
        (b( 57) and (a( 73) xor a( 74) xor a( 75))) xor
        (b( 58) and (a( 72) xor a( 73) xor a( 74))) xor
        (b( 59) and (a( 71) xor a( 72) xor a( 73))) xor
        (b( 60) and (a( 70) xor a( 71) xor a( 72))) xor
        (b( 61) and (a( 69) xor a( 70) xor a( 71))) xor
        (b( 62) and (a( 68) xor a( 69) xor a( 70))) xor
        (b( 63) and (a( 67) xor a( 68) xor a( 69))) xor
        (b( 64) and (a( 66) xor a( 67) xor a( 68))) xor
        (b( 65) and (a( 65) xor a( 66) xor a( 67))) xor
        (b( 66) and (a( 64) xor a( 65) xor a( 66))) xor
        (b( 67) and (a( 63) xor a( 64) xor a( 65))) xor
        (b( 68) and (a( 62) xor a( 63) xor a( 64))) xor
        (b( 69) and (a( 61) xor a( 62) xor a( 63))) xor
        (b( 70) and (a( 60) xor a( 61) xor a( 62))) xor
        (b( 71) and (a( 59) xor a( 60) xor a( 61))) xor
        (b( 72) and (a( 58) xor a( 59) xor a( 60))) xor
        (b( 73) and (a( 57) xor a( 58) xor a( 59))) xor
        (b( 74) and (a( 56) xor a( 57) xor a( 58))) xor
        (b( 75) and (a( 55) xor a( 56) xor a( 57))) xor
        (b( 76) and (a( 54) xor a( 55) xor a( 56))) xor
        (b( 77) and (a( 53) xor a( 54) xor a( 55))) xor
        (b( 78) and (a( 52) xor a( 53) xor a( 54))) xor
        (b( 79) and (a( 51) xor a( 52) xor a( 53))) xor
        (b( 80) and (a( 50) xor a( 51) xor a( 52))) xor
        (b( 81) and (a( 49) xor a( 50) xor a( 51))) xor
        (b( 82) and (a( 48) xor a( 49) xor a( 50))) xor
        (b( 83) and (a( 47) xor a( 48) xor a( 49))) xor
        (b( 84) and (a( 46) xor a( 47) xor a( 48))) xor
        (b( 85) and (a( 45) xor a( 46) xor a( 47))) xor
        (b( 86) and (a( 44) xor a( 45) xor a( 46))) xor
        (b( 87) and (a( 43) xor a( 44) xor a( 45))) xor
        (b( 88) and (a( 42) xor a( 43) xor a( 44))) xor
        (b( 89) and (a( 41) xor a( 42) xor a( 43))) xor
        (b( 90) and (a( 40) xor a( 41) xor a( 42))) xor
        (b( 91) and (a( 39) xor a( 40) xor a( 41))) xor
        (b( 92) and (a( 38) xor a( 39) xor a( 40))) xor
        (b( 93) and (a( 37) xor a( 38) xor a( 39))) xor
        (b( 94) and (a( 36) xor a( 37) xor a( 38))) xor
        (b( 95) and (a( 35) xor a( 36) xor a( 37))) xor
        (b( 96) and (a( 34) xor a( 35) xor a( 36))) xor
        (b( 97) and (a( 33) xor a( 34) xor a( 35))) xor
        (b( 98) and (a( 32) xor a( 33) xor a( 34))) xor
        (b( 99) and (a( 31) xor a( 32) xor a( 33))) xor
        (b(100) and (a( 30) xor a( 31) xor a( 32))) xor
        (b(101) and (a( 29) xor a( 30) xor a( 31))) xor
        (b(102) and (a( 28) xor a( 29) xor a( 30))) xor
        (b(103) and (a( 27) xor a( 28) xor a( 29))) xor
        (b(104) and (a( 26) xor a( 27) xor a( 28))) xor
        (b(105) and (a( 25) xor a( 26) xor a( 27))) xor
        (b(106) and (a( 24) xor a( 25) xor a( 26))) xor
        (b(107) and (a( 23) xor a( 24) xor a( 25))) xor
        (b(108) and (a( 22) xor a( 23) xor a( 24))) xor
        (b(109) and (a( 21) xor a( 22) xor a( 23))) xor
        (b(110) and (a( 20) xor a( 21) xor a( 22))) xor
        (b(111) and (a( 19) xor a( 20) xor a( 21))) xor
        (b(112) and (a( 18) xor a( 19) xor a( 20))) xor
        (b(113) and (a( 17) xor a( 18) xor a( 19))) xor
        (b(114) and (a( 16) xor a( 17) xor a( 18))) xor
        (b(115) and (a( 15) xor a( 16) xor a( 17))) xor
        (b(116) and (a( 14) xor a( 15) xor a( 16))) xor
        (b(117) and (a( 13) xor a( 14) xor a( 15))) xor
        (b(118) and (a( 12) xor a( 13) xor a( 14))) xor
        (b(119) and (a( 11) xor a( 12) xor a( 13))) xor
        (b(120) and (a( 10) xor a( 11) xor a( 12))) xor
        (b(121) and (a(  9) xor a( 10) xor a( 11))) xor
        (b(122) and (a(  8) xor a(  9) xor a( 10))) xor
        (b(123) and (a(  7) xor a(  8) xor a(  9))) xor
        (b(124) and (a(  6) xor a(  7) xor a(  8) xor a(127))) xor
        (b(125) and (a(  5) xor a(  6) xor a(  7) xor a(126) xor a(127))) xor
        (b(126) and (a(  4) xor a(  5) xor a(  6) xor a(125) xor a(126) xor a(127))) xor
        (b(127) and (a(  3) xor a(  4) xor a(  5) xor a(124) xor a(125) xor a(126)));
    c(  5) <= 
        (b(  0) and (a(  5))) xor
        (b(  1) and (a(  4))) xor
        (b(  2) and (a(  3))) xor
        (b(  3) and (a(  2))) xor
        (b(  4) and (a(  1) xor a(127))) xor
        (b(  5) and (a(  0) xor a(126) xor a(127))) xor
        (b(  6) and (a(125) xor a(126) xor a(127))) xor
        (b(  7) and (a(124) xor a(125) xor a(126))) xor
        (b(  8) and (a(123) xor a(124) xor a(125))) xor
        (b(  9) and (a(122) xor a(123) xor a(124))) xor
        (b( 10) and (a(121) xor a(122) xor a(123))) xor
        (b( 11) and (a(120) xor a(121) xor a(122))) xor
        (b( 12) and (a(119) xor a(120) xor a(121))) xor
        (b( 13) and (a(118) xor a(119) xor a(120))) xor
        (b( 14) and (a(117) xor a(118) xor a(119))) xor
        (b( 15) and (a(116) xor a(117) xor a(118))) xor
        (b( 16) and (a(115) xor a(116) xor a(117))) xor
        (b( 17) and (a(114) xor a(115) xor a(116))) xor
        (b( 18) and (a(113) xor a(114) xor a(115))) xor
        (b( 19) and (a(112) xor a(113) xor a(114))) xor
        (b( 20) and (a(111) xor a(112) xor a(113))) xor
        (b( 21) and (a(110) xor a(111) xor a(112))) xor
        (b( 22) and (a(109) xor a(110) xor a(111))) xor
        (b( 23) and (a(108) xor a(109) xor a(110))) xor
        (b( 24) and (a(107) xor a(108) xor a(109))) xor
        (b( 25) and (a(106) xor a(107) xor a(108))) xor
        (b( 26) and (a(105) xor a(106) xor a(107))) xor
        (b( 27) and (a(104) xor a(105) xor a(106))) xor
        (b( 28) and (a(103) xor a(104) xor a(105))) xor
        (b( 29) and (a(102) xor a(103) xor a(104))) xor
        (b( 30) and (a(101) xor a(102) xor a(103))) xor
        (b( 31) and (a(100) xor a(101) xor a(102))) xor
        (b( 32) and (a( 99) xor a(100) xor a(101))) xor
        (b( 33) and (a( 98) xor a( 99) xor a(100))) xor
        (b( 34) and (a( 97) xor a( 98) xor a( 99))) xor
        (b( 35) and (a( 96) xor a( 97) xor a( 98))) xor
        (b( 36) and (a( 95) xor a( 96) xor a( 97))) xor
        (b( 37) and (a( 94) xor a( 95) xor a( 96))) xor
        (b( 38) and (a( 93) xor a( 94) xor a( 95))) xor
        (b( 39) and (a( 92) xor a( 93) xor a( 94))) xor
        (b( 40) and (a( 91) xor a( 92) xor a( 93))) xor
        (b( 41) and (a( 90) xor a( 91) xor a( 92))) xor
        (b( 42) and (a( 89) xor a( 90) xor a( 91))) xor
        (b( 43) and (a( 88) xor a( 89) xor a( 90))) xor
        (b( 44) and (a( 87) xor a( 88) xor a( 89))) xor
        (b( 45) and (a( 86) xor a( 87) xor a( 88))) xor
        (b( 46) and (a( 85) xor a( 86) xor a( 87))) xor
        (b( 47) and (a( 84) xor a( 85) xor a( 86))) xor
        (b( 48) and (a( 83) xor a( 84) xor a( 85))) xor
        (b( 49) and (a( 82) xor a( 83) xor a( 84))) xor
        (b( 50) and (a( 81) xor a( 82) xor a( 83))) xor
        (b( 51) and (a( 80) xor a( 81) xor a( 82))) xor
        (b( 52) and (a( 79) xor a( 80) xor a( 81))) xor
        (b( 53) and (a( 78) xor a( 79) xor a( 80))) xor
        (b( 54) and (a( 77) xor a( 78) xor a( 79))) xor
        (b( 55) and (a( 76) xor a( 77) xor a( 78))) xor
        (b( 56) and (a( 75) xor a( 76) xor a( 77))) xor
        (b( 57) and (a( 74) xor a( 75) xor a( 76))) xor
        (b( 58) and (a( 73) xor a( 74) xor a( 75))) xor
        (b( 59) and (a( 72) xor a( 73) xor a( 74))) xor
        (b( 60) and (a( 71) xor a( 72) xor a( 73))) xor
        (b( 61) and (a( 70) xor a( 71) xor a( 72))) xor
        (b( 62) and (a( 69) xor a( 70) xor a( 71))) xor
        (b( 63) and (a( 68) xor a( 69) xor a( 70))) xor
        (b( 64) and (a( 67) xor a( 68) xor a( 69))) xor
        (b( 65) and (a( 66) xor a( 67) xor a( 68))) xor
        (b( 66) and (a( 65) xor a( 66) xor a( 67))) xor
        (b( 67) and (a( 64) xor a( 65) xor a( 66))) xor
        (b( 68) and (a( 63) xor a( 64) xor a( 65))) xor
        (b( 69) and (a( 62) xor a( 63) xor a( 64))) xor
        (b( 70) and (a( 61) xor a( 62) xor a( 63))) xor
        (b( 71) and (a( 60) xor a( 61) xor a( 62))) xor
        (b( 72) and (a( 59) xor a( 60) xor a( 61))) xor
        (b( 73) and (a( 58) xor a( 59) xor a( 60))) xor
        (b( 74) and (a( 57) xor a( 58) xor a( 59))) xor
        (b( 75) and (a( 56) xor a( 57) xor a( 58))) xor
        (b( 76) and (a( 55) xor a( 56) xor a( 57))) xor
        (b( 77) and (a( 54) xor a( 55) xor a( 56))) xor
        (b( 78) and (a( 53) xor a( 54) xor a( 55))) xor
        (b( 79) and (a( 52) xor a( 53) xor a( 54))) xor
        (b( 80) and (a( 51) xor a( 52) xor a( 53))) xor
        (b( 81) and (a( 50) xor a( 51) xor a( 52))) xor
        (b( 82) and (a( 49) xor a( 50) xor a( 51))) xor
        (b( 83) and (a( 48) xor a( 49) xor a( 50))) xor
        (b( 84) and (a( 47) xor a( 48) xor a( 49))) xor
        (b( 85) and (a( 46) xor a( 47) xor a( 48))) xor
        (b( 86) and (a( 45) xor a( 46) xor a( 47))) xor
        (b( 87) and (a( 44) xor a( 45) xor a( 46))) xor
        (b( 88) and (a( 43) xor a( 44) xor a( 45))) xor
        (b( 89) and (a( 42) xor a( 43) xor a( 44))) xor
        (b( 90) and (a( 41) xor a( 42) xor a( 43))) xor
        (b( 91) and (a( 40) xor a( 41) xor a( 42))) xor
        (b( 92) and (a( 39) xor a( 40) xor a( 41))) xor
        (b( 93) and (a( 38) xor a( 39) xor a( 40))) xor
        (b( 94) and (a( 37) xor a( 38) xor a( 39))) xor
        (b( 95) and (a( 36) xor a( 37) xor a( 38))) xor
        (b( 96) and (a( 35) xor a( 36) xor a( 37))) xor
        (b( 97) and (a( 34) xor a( 35) xor a( 36))) xor
        (b( 98) and (a( 33) xor a( 34) xor a( 35))) xor
        (b( 99) and (a( 32) xor a( 33) xor a( 34))) xor
        (b(100) and (a( 31) xor a( 32) xor a( 33))) xor
        (b(101) and (a( 30) xor a( 31) xor a( 32))) xor
        (b(102) and (a( 29) xor a( 30) xor a( 31))) xor
        (b(103) and (a( 28) xor a( 29) xor a( 30))) xor
        (b(104) and (a( 27) xor a( 28) xor a( 29))) xor
        (b(105) and (a( 26) xor a( 27) xor a( 28))) xor
        (b(106) and (a( 25) xor a( 26) xor a( 27))) xor
        (b(107) and (a( 24) xor a( 25) xor a( 26))) xor
        (b(108) and (a( 23) xor a( 24) xor a( 25))) xor
        (b(109) and (a( 22) xor a( 23) xor a( 24))) xor
        (b(110) and (a( 21) xor a( 22) xor a( 23))) xor
        (b(111) and (a( 20) xor a( 21) xor a( 22))) xor
        (b(112) and (a( 19) xor a( 20) xor a( 21))) xor
        (b(113) and (a( 18) xor a( 19) xor a( 20))) xor
        (b(114) and (a( 17) xor a( 18) xor a( 19))) xor
        (b(115) and (a( 16) xor a( 17) xor a( 18))) xor
        (b(116) and (a( 15) xor a( 16) xor a( 17))) xor
        (b(117) and (a( 14) xor a( 15) xor a( 16))) xor
        (b(118) and (a( 13) xor a( 14) xor a( 15))) xor
        (b(119) and (a( 12) xor a( 13) xor a( 14))) xor
        (b(120) and (a( 11) xor a( 12) xor a( 13))) xor
        (b(121) and (a( 10) xor a( 11) xor a( 12))) xor
        (b(122) and (a(  9) xor a( 10) xor a( 11))) xor
        (b(123) and (a(  8) xor a(  9) xor a( 10))) xor
        (b(124) and (a(  7) xor a(  8) xor a(  9))) xor
        (b(125) and (a(  6) xor a(  7) xor a(  8) xor a(127))) xor
        (b(126) and (a(  5) xor a(  6) xor a(  7) xor a(126) xor a(127))) xor
        (b(127) and (a(  4) xor a(  5) xor a(  6) xor a(125) xor a(126) xor a(127)));
    c(  6) <= 
        (b(  0) and (a(  6))) xor
        (b(  1) and (a(  5))) xor
        (b(  2) and (a(  4))) xor
        (b(  3) and (a(  3))) xor
        (b(  4) and (a(  2))) xor
        (b(  5) and (a(  1) xor a(127))) xor
        (b(  6) and (a(  0) xor a(126) xor a(127))) xor
        (b(  7) and (a(125) xor a(126) xor a(127))) xor
        (b(  8) and (a(124) xor a(125) xor a(126))) xor
        (b(  9) and (a(123) xor a(124) xor a(125))) xor
        (b( 10) and (a(122) xor a(123) xor a(124))) xor
        (b( 11) and (a(121) xor a(122) xor a(123))) xor
        (b( 12) and (a(120) xor a(121) xor a(122))) xor
        (b( 13) and (a(119) xor a(120) xor a(121))) xor
        (b( 14) and (a(118) xor a(119) xor a(120))) xor
        (b( 15) and (a(117) xor a(118) xor a(119))) xor
        (b( 16) and (a(116) xor a(117) xor a(118))) xor
        (b( 17) and (a(115) xor a(116) xor a(117))) xor
        (b( 18) and (a(114) xor a(115) xor a(116))) xor
        (b( 19) and (a(113) xor a(114) xor a(115))) xor
        (b( 20) and (a(112) xor a(113) xor a(114))) xor
        (b( 21) and (a(111) xor a(112) xor a(113))) xor
        (b( 22) and (a(110) xor a(111) xor a(112))) xor
        (b( 23) and (a(109) xor a(110) xor a(111))) xor
        (b( 24) and (a(108) xor a(109) xor a(110))) xor
        (b( 25) and (a(107) xor a(108) xor a(109))) xor
        (b( 26) and (a(106) xor a(107) xor a(108))) xor
        (b( 27) and (a(105) xor a(106) xor a(107))) xor
        (b( 28) and (a(104) xor a(105) xor a(106))) xor
        (b( 29) and (a(103) xor a(104) xor a(105))) xor
        (b( 30) and (a(102) xor a(103) xor a(104))) xor
        (b( 31) and (a(101) xor a(102) xor a(103))) xor
        (b( 32) and (a(100) xor a(101) xor a(102))) xor
        (b( 33) and (a( 99) xor a(100) xor a(101))) xor
        (b( 34) and (a( 98) xor a( 99) xor a(100))) xor
        (b( 35) and (a( 97) xor a( 98) xor a( 99))) xor
        (b( 36) and (a( 96) xor a( 97) xor a( 98))) xor
        (b( 37) and (a( 95) xor a( 96) xor a( 97))) xor
        (b( 38) and (a( 94) xor a( 95) xor a( 96))) xor
        (b( 39) and (a( 93) xor a( 94) xor a( 95))) xor
        (b( 40) and (a( 92) xor a( 93) xor a( 94))) xor
        (b( 41) and (a( 91) xor a( 92) xor a( 93))) xor
        (b( 42) and (a( 90) xor a( 91) xor a( 92))) xor
        (b( 43) and (a( 89) xor a( 90) xor a( 91))) xor
        (b( 44) and (a( 88) xor a( 89) xor a( 90))) xor
        (b( 45) and (a( 87) xor a( 88) xor a( 89))) xor
        (b( 46) and (a( 86) xor a( 87) xor a( 88))) xor
        (b( 47) and (a( 85) xor a( 86) xor a( 87))) xor
        (b( 48) and (a( 84) xor a( 85) xor a( 86))) xor
        (b( 49) and (a( 83) xor a( 84) xor a( 85))) xor
        (b( 50) and (a( 82) xor a( 83) xor a( 84))) xor
        (b( 51) and (a( 81) xor a( 82) xor a( 83))) xor
        (b( 52) and (a( 80) xor a( 81) xor a( 82))) xor
        (b( 53) and (a( 79) xor a( 80) xor a( 81))) xor
        (b( 54) and (a( 78) xor a( 79) xor a( 80))) xor
        (b( 55) and (a( 77) xor a( 78) xor a( 79))) xor
        (b( 56) and (a( 76) xor a( 77) xor a( 78))) xor
        (b( 57) and (a( 75) xor a( 76) xor a( 77))) xor
        (b( 58) and (a( 74) xor a( 75) xor a( 76))) xor
        (b( 59) and (a( 73) xor a( 74) xor a( 75))) xor
        (b( 60) and (a( 72) xor a( 73) xor a( 74))) xor
        (b( 61) and (a( 71) xor a( 72) xor a( 73))) xor
        (b( 62) and (a( 70) xor a( 71) xor a( 72))) xor
        (b( 63) and (a( 69) xor a( 70) xor a( 71))) xor
        (b( 64) and (a( 68) xor a( 69) xor a( 70))) xor
        (b( 65) and (a( 67) xor a( 68) xor a( 69))) xor
        (b( 66) and (a( 66) xor a( 67) xor a( 68))) xor
        (b( 67) and (a( 65) xor a( 66) xor a( 67))) xor
        (b( 68) and (a( 64) xor a( 65) xor a( 66))) xor
        (b( 69) and (a( 63) xor a( 64) xor a( 65))) xor
        (b( 70) and (a( 62) xor a( 63) xor a( 64))) xor
        (b( 71) and (a( 61) xor a( 62) xor a( 63))) xor
        (b( 72) and (a( 60) xor a( 61) xor a( 62))) xor
        (b( 73) and (a( 59) xor a( 60) xor a( 61))) xor
        (b( 74) and (a( 58) xor a( 59) xor a( 60))) xor
        (b( 75) and (a( 57) xor a( 58) xor a( 59))) xor
        (b( 76) and (a( 56) xor a( 57) xor a( 58))) xor
        (b( 77) and (a( 55) xor a( 56) xor a( 57))) xor
        (b( 78) and (a( 54) xor a( 55) xor a( 56))) xor
        (b( 79) and (a( 53) xor a( 54) xor a( 55))) xor
        (b( 80) and (a( 52) xor a( 53) xor a( 54))) xor
        (b( 81) and (a( 51) xor a( 52) xor a( 53))) xor
        (b( 82) and (a( 50) xor a( 51) xor a( 52))) xor
        (b( 83) and (a( 49) xor a( 50) xor a( 51))) xor
        (b( 84) and (a( 48) xor a( 49) xor a( 50))) xor
        (b( 85) and (a( 47) xor a( 48) xor a( 49))) xor
        (b( 86) and (a( 46) xor a( 47) xor a( 48))) xor
        (b( 87) and (a( 45) xor a( 46) xor a( 47))) xor
        (b( 88) and (a( 44) xor a( 45) xor a( 46))) xor
        (b( 89) and (a( 43) xor a( 44) xor a( 45))) xor
        (b( 90) and (a( 42) xor a( 43) xor a( 44))) xor
        (b( 91) and (a( 41) xor a( 42) xor a( 43))) xor
        (b( 92) and (a( 40) xor a( 41) xor a( 42))) xor
        (b( 93) and (a( 39) xor a( 40) xor a( 41))) xor
        (b( 94) and (a( 38) xor a( 39) xor a( 40))) xor
        (b( 95) and (a( 37) xor a( 38) xor a( 39))) xor
        (b( 96) and (a( 36) xor a( 37) xor a( 38))) xor
        (b( 97) and (a( 35) xor a( 36) xor a( 37))) xor
        (b( 98) and (a( 34) xor a( 35) xor a( 36))) xor
        (b( 99) and (a( 33) xor a( 34) xor a( 35))) xor
        (b(100) and (a( 32) xor a( 33) xor a( 34))) xor
        (b(101) and (a( 31) xor a( 32) xor a( 33))) xor
        (b(102) and (a( 30) xor a( 31) xor a( 32))) xor
        (b(103) and (a( 29) xor a( 30) xor a( 31))) xor
        (b(104) and (a( 28) xor a( 29) xor a( 30))) xor
        (b(105) and (a( 27) xor a( 28) xor a( 29))) xor
        (b(106) and (a( 26) xor a( 27) xor a( 28))) xor
        (b(107) and (a( 25) xor a( 26) xor a( 27))) xor
        (b(108) and (a( 24) xor a( 25) xor a( 26))) xor
        (b(109) and (a( 23) xor a( 24) xor a( 25))) xor
        (b(110) and (a( 22) xor a( 23) xor a( 24))) xor
        (b(111) and (a( 21) xor a( 22) xor a( 23))) xor
        (b(112) and (a( 20) xor a( 21) xor a( 22))) xor
        (b(113) and (a( 19) xor a( 20) xor a( 21))) xor
        (b(114) and (a( 18) xor a( 19) xor a( 20))) xor
        (b(115) and (a( 17) xor a( 18) xor a( 19))) xor
        (b(116) and (a( 16) xor a( 17) xor a( 18))) xor
        (b(117) and (a( 15) xor a( 16) xor a( 17))) xor
        (b(118) and (a( 14) xor a( 15) xor a( 16))) xor
        (b(119) and (a( 13) xor a( 14) xor a( 15))) xor
        (b(120) and (a( 12) xor a( 13) xor a( 14))) xor
        (b(121) and (a( 11) xor a( 12) xor a( 13))) xor
        (b(122) and (a( 10) xor a( 11) xor a( 12))) xor
        (b(123) and (a(  9) xor a( 10) xor a( 11))) xor
        (b(124) and (a(  8) xor a(  9) xor a( 10))) xor
        (b(125) and (a(  7) xor a(  8) xor a(  9))) xor
        (b(126) and (a(  6) xor a(  7) xor a(  8) xor a(127))) xor
        (b(127) and (a(  5) xor a(  6) xor a(  7) xor a(126) xor a(127)));
    c(  7) <= 
        (b(  0) and (a(  7))) xor
        (b(  1) and (a(  6) xor a(127))) xor
        (b(  2) and (a(  5) xor a(126))) xor
        (b(  3) and (a(  4) xor a(125))) xor
        (b(  4) and (a(  3) xor a(124))) xor
        (b(  5) and (a(  2) xor a(123))) xor
        (b(  6) and (a(  1) xor a(122) xor a(127))) xor
        (b(  7) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(  8) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(  9) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 10) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 11) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 12) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 13) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 14) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 15) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 16) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 17) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 18) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 19) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 20) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 21) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 22) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 23) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 24) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 25) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 26) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 27) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 28) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 29) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 30) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 31) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 32) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 33) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 34) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 35) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 36) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 37) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 38) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 39) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 40) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 41) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 42) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 43) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 44) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 45) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 46) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 47) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 48) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 49) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 50) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 51) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 52) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 53) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 54) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 55) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 56) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 57) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 58) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 59) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 60) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 61) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 62) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 63) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 64) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 65) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 66) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 67) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 68) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 69) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 70) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 71) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 72) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 73) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 74) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 75) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 76) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 77) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 78) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 79) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 80) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 81) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 82) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 83) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 84) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 85) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 86) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 87) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 88) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 89) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 90) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 91) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 92) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 93) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b( 94) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b( 95) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b( 96) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b( 97) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b( 98) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b( 99) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(100) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(101) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(102) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(103) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(104) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(105) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(106) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(107) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(108) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(109) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(110) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(111) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(112) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(113) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(114) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(115) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(116) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(117) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(118) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(119) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(120) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(121) and (a(  7) xor a( 12) xor a( 13) xor a( 14))) xor
        (b(122) and (a(  6) xor a( 11) xor a( 12) xor a( 13) xor a(127))) xor
        (b(123) and (a(  5) xor a( 10) xor a( 11) xor a( 12) xor a(126))) xor
        (b(124) and (a(  4) xor a(  9) xor a( 10) xor a( 11) xor a(125))) xor
        (b(125) and (a(  3) xor a(  8) xor a(  9) xor a( 10) xor a(124))) xor
        (b(126) and (a(  2) xor a(  7) xor a(  8) xor a(  9) xor a(123))) xor
        (b(127) and (a(  1) xor a(  6) xor a(  7) xor a(  8) xor a(122)));
    c(  8) <= 
        (b(  0) and (a(  8))) xor
        (b(  1) and (a(  7))) xor
        (b(  2) and (a(  6) xor a(127))) xor
        (b(  3) and (a(  5) xor a(126))) xor
        (b(  4) and (a(  4) xor a(125))) xor
        (b(  5) and (a(  3) xor a(124))) xor
        (b(  6) and (a(  2) xor a(123))) xor
        (b(  7) and (a(  1) xor a(122) xor a(127))) xor
        (b(  8) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(  9) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 10) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 11) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 12) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 13) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 14) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 15) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 16) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 17) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 18) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 19) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 20) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 21) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 22) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 23) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 24) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 25) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 26) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 27) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 28) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 29) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 30) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 31) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 32) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 33) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 34) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 35) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 36) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 37) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 38) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 39) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 40) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 41) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 42) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 43) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 44) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 45) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 46) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 47) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 48) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 49) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 50) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 51) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 52) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 53) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 54) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 55) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 56) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 57) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 58) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 59) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 60) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 61) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 62) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 63) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 64) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 65) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 66) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 67) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 68) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 69) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 70) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 71) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 72) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 73) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 74) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 75) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 76) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 77) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 78) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 79) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 80) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 81) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 82) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 83) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 84) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 85) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 86) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 87) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 88) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 89) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 90) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 91) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 92) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 93) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 94) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b( 95) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b( 96) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b( 97) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b( 98) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b( 99) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(100) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(101) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(102) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(103) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(104) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(105) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(106) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(107) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(108) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(109) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(110) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(111) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(112) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(113) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(114) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(115) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(116) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(117) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(118) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(119) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(120) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(121) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(122) and (a(  7) xor a( 12) xor a( 13) xor a( 14))) xor
        (b(123) and (a(  6) xor a( 11) xor a( 12) xor a( 13) xor a(127))) xor
        (b(124) and (a(  5) xor a( 10) xor a( 11) xor a( 12) xor a(126))) xor
        (b(125) and (a(  4) xor a(  9) xor a( 10) xor a( 11) xor a(125))) xor
        (b(126) and (a(  3) xor a(  8) xor a(  9) xor a( 10) xor a(124))) xor
        (b(127) and (a(  2) xor a(  7) xor a(  8) xor a(  9) xor a(123)));
    c(  9) <= 
        (b(  0) and (a(  9))) xor
        (b(  1) and (a(  8))) xor
        (b(  2) and (a(  7))) xor
        (b(  3) and (a(  6) xor a(127))) xor
        (b(  4) and (a(  5) xor a(126))) xor
        (b(  5) and (a(  4) xor a(125))) xor
        (b(  6) and (a(  3) xor a(124))) xor
        (b(  7) and (a(  2) xor a(123))) xor
        (b(  8) and (a(  1) xor a(122) xor a(127))) xor
        (b(  9) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 10) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 11) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 12) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 13) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 14) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 15) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 16) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 17) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 18) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 19) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 20) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 21) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 22) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 23) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 24) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 25) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 26) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 27) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 28) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 29) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 30) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 31) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 32) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 33) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 34) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 35) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 36) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 37) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 38) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 39) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 40) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 41) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 42) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 43) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 44) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 45) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 46) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 47) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 48) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 49) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 50) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 51) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 52) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 53) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 54) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 55) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 56) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 57) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 58) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 59) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 60) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 61) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 62) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 63) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 64) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 65) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 66) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 67) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 68) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 69) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 70) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 71) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 72) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 73) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 74) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 75) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 76) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 77) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 78) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 79) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 80) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 81) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 82) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 83) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 84) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 85) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 86) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 87) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 88) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 89) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 90) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 91) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 92) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 93) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 94) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 95) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b( 96) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b( 97) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b( 98) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b( 99) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(100) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(101) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(102) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(103) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(104) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(105) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(106) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(107) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(108) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(109) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(110) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(111) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(112) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(113) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(114) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(115) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(116) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(117) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(118) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(119) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(120) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(121) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(122) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(123) and (a(  7) xor a( 12) xor a( 13) xor a( 14))) xor
        (b(124) and (a(  6) xor a( 11) xor a( 12) xor a( 13) xor a(127))) xor
        (b(125) and (a(  5) xor a( 10) xor a( 11) xor a( 12) xor a(126))) xor
        (b(126) and (a(  4) xor a(  9) xor a( 10) xor a( 11) xor a(125))) xor
        (b(127) and (a(  3) xor a(  8) xor a(  9) xor a( 10) xor a(124)));
    c( 10) <= 
        (b(  0) and (a( 10))) xor
        (b(  1) and (a(  9))) xor
        (b(  2) and (a(  8))) xor
        (b(  3) and (a(  7))) xor
        (b(  4) and (a(  6) xor a(127))) xor
        (b(  5) and (a(  5) xor a(126))) xor
        (b(  6) and (a(  4) xor a(125))) xor
        (b(  7) and (a(  3) xor a(124))) xor
        (b(  8) and (a(  2) xor a(123))) xor
        (b(  9) and (a(  1) xor a(122) xor a(127))) xor
        (b( 10) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 11) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 12) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 13) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 14) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 15) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 16) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 17) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 18) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 19) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 20) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 21) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 22) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 23) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 24) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 25) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 26) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 27) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 28) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 29) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 30) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 31) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 32) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 33) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 34) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 35) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 36) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 37) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 38) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 39) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 40) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 41) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 42) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 43) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 44) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 45) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 46) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 47) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 48) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 49) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 50) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 51) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 52) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 53) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 54) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 55) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 56) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 57) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 58) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 59) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 60) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 61) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 62) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 63) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 64) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 65) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 66) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 67) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 68) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 69) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 70) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 71) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 72) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 73) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 74) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 75) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 76) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 77) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 78) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 79) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 80) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 81) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 82) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 83) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 84) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 85) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 86) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 87) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 88) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 89) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 90) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 91) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 92) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 93) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 94) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 95) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 96) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b( 97) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b( 98) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b( 99) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(100) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(101) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(102) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(103) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(104) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(105) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(106) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(107) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(108) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(109) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(110) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(111) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(112) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(113) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(114) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(115) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(116) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(117) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(118) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(119) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(120) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(121) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(122) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(123) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(124) and (a(  7) xor a( 12) xor a( 13) xor a( 14))) xor
        (b(125) and (a(  6) xor a( 11) xor a( 12) xor a( 13) xor a(127))) xor
        (b(126) and (a(  5) xor a( 10) xor a( 11) xor a( 12) xor a(126))) xor
        (b(127) and (a(  4) xor a(  9) xor a( 10) xor a( 11) xor a(125)));
    c( 11) <= 
        (b(  0) and (a( 11))) xor
        (b(  1) and (a( 10))) xor
        (b(  2) and (a(  9))) xor
        (b(  3) and (a(  8))) xor
        (b(  4) and (a(  7))) xor
        (b(  5) and (a(  6) xor a(127))) xor
        (b(  6) and (a(  5) xor a(126))) xor
        (b(  7) and (a(  4) xor a(125))) xor
        (b(  8) and (a(  3) xor a(124))) xor
        (b(  9) and (a(  2) xor a(123))) xor
        (b( 10) and (a(  1) xor a(122) xor a(127))) xor
        (b( 11) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 12) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 13) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 14) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 15) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 16) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 17) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 18) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 19) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 20) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 21) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 22) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 23) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 24) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 25) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 26) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 27) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 28) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 29) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 30) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 31) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 32) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 33) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 34) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 35) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 36) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 37) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 38) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 39) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 40) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 41) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 42) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 43) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 44) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 45) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 46) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 47) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 48) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 49) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 50) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 51) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 52) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 53) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 54) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 55) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 56) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 57) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 58) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 59) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 60) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 61) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 62) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 63) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 64) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 65) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 66) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 67) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 68) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 69) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 70) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 71) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 72) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 73) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 74) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 75) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 76) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 77) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 78) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 79) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 80) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 81) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 82) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 83) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 84) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 85) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 86) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 87) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 88) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 89) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 90) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 91) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 92) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 93) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 94) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 95) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 96) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 97) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b( 98) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b( 99) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(100) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(101) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(102) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(103) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(104) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(105) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(106) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(107) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(108) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(109) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(110) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(111) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(112) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(113) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(114) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(115) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(116) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(117) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(118) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(119) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(120) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(121) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(122) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(123) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(124) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(125) and (a(  7) xor a( 12) xor a( 13) xor a( 14))) xor
        (b(126) and (a(  6) xor a( 11) xor a( 12) xor a( 13) xor a(127))) xor
        (b(127) and (a(  5) xor a( 10) xor a( 11) xor a( 12) xor a(126)));
    c( 12) <= 
        (b(  0) and (a( 12))) xor
        (b(  1) and (a( 11))) xor
        (b(  2) and (a( 10))) xor
        (b(  3) and (a(  9))) xor
        (b(  4) and (a(  8))) xor
        (b(  5) and (a(  7))) xor
        (b(  6) and (a(  6) xor a(127))) xor
        (b(  7) and (a(  5) xor a(126))) xor
        (b(  8) and (a(  4) xor a(125))) xor
        (b(  9) and (a(  3) xor a(124))) xor
        (b( 10) and (a(  2) xor a(123))) xor
        (b( 11) and (a(  1) xor a(122) xor a(127))) xor
        (b( 12) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 13) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 14) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 15) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 16) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 17) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 18) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 19) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 20) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 21) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 22) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 23) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 24) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 25) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 26) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 27) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 28) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 29) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 30) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 31) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 32) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 33) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 34) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 35) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 36) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 37) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 38) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 39) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 40) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 41) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 42) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 43) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 44) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 45) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 46) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 47) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 48) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 49) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 50) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 51) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 52) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 53) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 54) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 55) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 56) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 57) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 58) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 59) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 60) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 61) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 62) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 63) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 64) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 65) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 66) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 67) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 68) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 69) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 70) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 71) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 72) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 73) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 74) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 75) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 76) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 77) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 78) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 79) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 80) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 81) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 82) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 83) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 84) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 85) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 86) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 87) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 88) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 89) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 90) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 91) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 92) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 93) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 94) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 95) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 96) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 97) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 98) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b( 99) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(100) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(101) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(102) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(103) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(104) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(105) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(106) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(107) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(108) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(109) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(110) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(111) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(112) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(113) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(114) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(115) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(116) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(117) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(118) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(119) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(120) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(121) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(122) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(123) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(124) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(125) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(126) and (a(  7) xor a( 12) xor a( 13) xor a( 14))) xor
        (b(127) and (a(  6) xor a( 11) xor a( 12) xor a( 13) xor a(127)));
    c( 13) <= 
        (b(  0) and (a( 13))) xor
        (b(  1) and (a( 12))) xor
        (b(  2) and (a( 11))) xor
        (b(  3) and (a( 10))) xor
        (b(  4) and (a(  9))) xor
        (b(  5) and (a(  8))) xor
        (b(  6) and (a(  7))) xor
        (b(  7) and (a(  6) xor a(127))) xor
        (b(  8) and (a(  5) xor a(126))) xor
        (b(  9) and (a(  4) xor a(125))) xor
        (b( 10) and (a(  3) xor a(124))) xor
        (b( 11) and (a(  2) xor a(123))) xor
        (b( 12) and (a(  1) xor a(122) xor a(127))) xor
        (b( 13) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 14) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 15) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 16) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 17) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 18) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 19) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 20) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 21) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 22) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 23) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 24) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 25) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 26) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 27) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 28) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 29) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 30) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 31) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 32) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 33) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 34) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 35) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 36) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 37) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 38) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 39) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 40) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 41) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 42) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 43) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 44) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 45) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 46) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 47) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 48) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 49) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 50) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 51) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 52) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 53) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 54) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 55) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 56) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 57) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 58) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 59) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 60) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 61) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 62) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 63) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 64) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 65) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 66) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 67) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 68) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 69) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 70) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 71) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 72) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 73) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 74) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 75) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 76) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 77) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 78) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 79) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 80) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 81) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 82) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 83) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 84) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 85) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 86) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 87) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 88) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 89) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 90) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 91) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 92) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 93) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 94) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 95) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 96) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 97) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 98) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b( 99) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(100) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(101) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(102) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(103) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(104) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(105) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(106) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(107) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(108) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(109) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(110) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(111) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(112) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(113) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(114) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(115) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(116) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(117) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(118) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(119) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(120) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(121) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(122) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(123) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(124) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(125) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(126) and (a(  8) xor a( 13) xor a( 14) xor a( 15))) xor
        (b(127) and (a(  7) xor a( 12) xor a( 13) xor a( 14)));
    c( 14) <= 
        (b(  0) and (a( 14))) xor
        (b(  1) and (a( 13))) xor
        (b(  2) and (a( 12))) xor
        (b(  3) and (a( 11))) xor
        (b(  4) and (a( 10))) xor
        (b(  5) and (a(  9))) xor
        (b(  6) and (a(  8))) xor
        (b(  7) and (a(  7))) xor
        (b(  8) and (a(  6) xor a(127))) xor
        (b(  9) and (a(  5) xor a(126))) xor
        (b( 10) and (a(  4) xor a(125))) xor
        (b( 11) and (a(  3) xor a(124))) xor
        (b( 12) and (a(  2) xor a(123))) xor
        (b( 13) and (a(  1) xor a(122) xor a(127))) xor
        (b( 14) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 15) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 16) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 17) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 18) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 19) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 20) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 21) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 22) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 23) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 24) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 25) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 26) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 27) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 28) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 29) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 30) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 31) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 32) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 33) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 34) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 35) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 36) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 37) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 38) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 39) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 40) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 41) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 42) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 43) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 44) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 45) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 46) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 47) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 48) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 49) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 50) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 51) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 52) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 53) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 54) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 55) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 56) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 57) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 58) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 59) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 60) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 61) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 62) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 63) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 64) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 65) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 66) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 67) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 68) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 69) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 70) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 71) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 72) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 73) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 74) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 75) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 76) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 77) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 78) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 79) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 80) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 81) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 82) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 83) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 84) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 85) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 86) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 87) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 88) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 89) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 90) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 91) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 92) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 93) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 94) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 95) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 96) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 97) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 98) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b( 99) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(100) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(101) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(102) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(103) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(104) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(105) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(106) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(107) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(108) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(109) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(110) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(111) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(112) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(113) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(114) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(115) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(116) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(117) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(118) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(119) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(120) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(121) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(122) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(123) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(124) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(125) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(126) and (a(  9) xor a( 14) xor a( 15) xor a( 16))) xor
        (b(127) and (a(  8) xor a( 13) xor a( 14) xor a( 15)));
    c( 15) <= 
        (b(  0) and (a( 15))) xor
        (b(  1) and (a( 14))) xor
        (b(  2) and (a( 13))) xor
        (b(  3) and (a( 12))) xor
        (b(  4) and (a( 11))) xor
        (b(  5) and (a( 10))) xor
        (b(  6) and (a(  9))) xor
        (b(  7) and (a(  8))) xor
        (b(  8) and (a(  7))) xor
        (b(  9) and (a(  6) xor a(127))) xor
        (b( 10) and (a(  5) xor a(126))) xor
        (b( 11) and (a(  4) xor a(125))) xor
        (b( 12) and (a(  3) xor a(124))) xor
        (b( 13) and (a(  2) xor a(123))) xor
        (b( 14) and (a(  1) xor a(122) xor a(127))) xor
        (b( 15) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 16) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 17) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 18) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 19) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 20) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 21) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 22) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 23) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 24) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 25) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 26) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 27) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 28) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 29) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 30) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 31) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 32) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 33) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 34) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 35) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 36) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 37) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 38) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 39) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 40) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 41) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 42) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 43) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 44) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 45) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 46) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 47) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 48) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 49) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 50) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 51) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 52) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 53) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 54) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 55) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 56) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 57) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 58) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 59) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 60) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 61) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 62) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 63) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 64) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 65) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 66) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 67) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 68) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 69) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 70) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 71) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 72) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 73) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 74) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 75) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 76) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 77) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 78) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 79) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 80) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 81) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 82) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 83) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 84) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 85) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 86) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 87) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 88) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 89) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 90) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 91) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 92) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 93) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 94) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 95) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 96) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 97) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 98) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b( 99) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(100) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(101) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(102) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(103) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(104) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(105) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(106) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(107) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(108) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(109) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(110) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(111) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(112) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(113) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(114) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(115) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(116) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(117) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(118) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(119) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(120) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(121) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(122) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(123) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(124) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(125) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(126) and (a( 10) xor a( 15) xor a( 16) xor a( 17))) xor
        (b(127) and (a(  9) xor a( 14) xor a( 15) xor a( 16)));
    c( 16) <= 
        (b(  0) and (a( 16))) xor
        (b(  1) and (a( 15))) xor
        (b(  2) and (a( 14))) xor
        (b(  3) and (a( 13))) xor
        (b(  4) and (a( 12))) xor
        (b(  5) and (a( 11))) xor
        (b(  6) and (a( 10))) xor
        (b(  7) and (a(  9))) xor
        (b(  8) and (a(  8))) xor
        (b(  9) and (a(  7))) xor
        (b( 10) and (a(  6) xor a(127))) xor
        (b( 11) and (a(  5) xor a(126))) xor
        (b( 12) and (a(  4) xor a(125))) xor
        (b( 13) and (a(  3) xor a(124))) xor
        (b( 14) and (a(  2) xor a(123))) xor
        (b( 15) and (a(  1) xor a(122) xor a(127))) xor
        (b( 16) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 17) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 18) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 19) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 20) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 21) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 22) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 23) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 24) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 25) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 26) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 27) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 28) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 29) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 30) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 31) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 32) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 33) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 34) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 35) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 36) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 37) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 38) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 39) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 40) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 41) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 42) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 43) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 44) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 45) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 46) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 47) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 48) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 49) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 50) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 51) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 52) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 53) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 54) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 55) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 56) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 57) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 58) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 59) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 60) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 61) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 62) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 63) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 64) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 65) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 66) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 67) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 68) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 69) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 70) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 71) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 72) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 73) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 74) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 75) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 76) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 77) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 78) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 79) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 80) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 81) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 82) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 83) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 84) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 85) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 86) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 87) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 88) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 89) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 90) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 91) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 92) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 93) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 94) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 95) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 96) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 97) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 98) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b( 99) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(100) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(101) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(102) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(103) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(104) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(105) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(106) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(107) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(108) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(109) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(110) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(111) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(112) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(113) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(114) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(115) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(116) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(117) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(118) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(119) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(120) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(121) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(122) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(123) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(124) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(125) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(126) and (a( 11) xor a( 16) xor a( 17) xor a( 18))) xor
        (b(127) and (a( 10) xor a( 15) xor a( 16) xor a( 17)));
    c( 17) <= 
        (b(  0) and (a( 17))) xor
        (b(  1) and (a( 16))) xor
        (b(  2) and (a( 15))) xor
        (b(  3) and (a( 14))) xor
        (b(  4) and (a( 13))) xor
        (b(  5) and (a( 12))) xor
        (b(  6) and (a( 11))) xor
        (b(  7) and (a( 10))) xor
        (b(  8) and (a(  9))) xor
        (b(  9) and (a(  8))) xor
        (b( 10) and (a(  7))) xor
        (b( 11) and (a(  6) xor a(127))) xor
        (b( 12) and (a(  5) xor a(126))) xor
        (b( 13) and (a(  4) xor a(125))) xor
        (b( 14) and (a(  3) xor a(124))) xor
        (b( 15) and (a(  2) xor a(123))) xor
        (b( 16) and (a(  1) xor a(122) xor a(127))) xor
        (b( 17) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 18) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 19) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 20) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 21) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 22) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 23) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 24) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 25) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 26) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 27) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 28) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 29) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 30) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 31) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 32) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 33) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 34) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 35) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 36) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 37) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 38) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 39) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 40) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 41) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 42) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 43) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 44) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 45) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 46) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 47) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 48) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 49) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 50) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 51) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 52) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 53) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 54) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 55) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 56) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 57) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 58) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 59) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 60) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 61) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 62) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 63) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 64) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 65) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 66) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 67) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 68) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 69) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 70) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 71) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 72) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 73) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 74) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 75) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 76) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 77) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 78) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 79) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 80) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 81) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 82) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 83) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 84) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 85) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 86) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 87) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 88) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 89) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 90) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 91) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 92) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 93) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 94) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 95) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 96) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 97) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 98) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b( 99) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(100) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(101) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(102) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(103) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(104) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(105) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(106) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(107) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(108) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(109) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(110) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(111) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(112) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(113) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(114) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(115) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(116) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(117) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(118) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(119) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(120) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(121) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(122) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(123) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(124) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(125) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(126) and (a( 12) xor a( 17) xor a( 18) xor a( 19))) xor
        (b(127) and (a( 11) xor a( 16) xor a( 17) xor a( 18)));
    c( 18) <= 
        (b(  0) and (a( 18))) xor
        (b(  1) and (a( 17))) xor
        (b(  2) and (a( 16))) xor
        (b(  3) and (a( 15))) xor
        (b(  4) and (a( 14))) xor
        (b(  5) and (a( 13))) xor
        (b(  6) and (a( 12))) xor
        (b(  7) and (a( 11))) xor
        (b(  8) and (a( 10))) xor
        (b(  9) and (a(  9))) xor
        (b( 10) and (a(  8))) xor
        (b( 11) and (a(  7))) xor
        (b( 12) and (a(  6) xor a(127))) xor
        (b( 13) and (a(  5) xor a(126))) xor
        (b( 14) and (a(  4) xor a(125))) xor
        (b( 15) and (a(  3) xor a(124))) xor
        (b( 16) and (a(  2) xor a(123))) xor
        (b( 17) and (a(  1) xor a(122) xor a(127))) xor
        (b( 18) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 19) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 20) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 21) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 22) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 23) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 24) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 25) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 26) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 27) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 28) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 29) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 30) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 31) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 32) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 33) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 34) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 35) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 36) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 37) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 38) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 39) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 40) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 41) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 42) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 43) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 44) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 45) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 46) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 47) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 48) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 49) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 50) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 51) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 52) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 53) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 54) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 55) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 56) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 57) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 58) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 59) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 60) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 61) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 62) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 63) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 64) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 65) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 66) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 67) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 68) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 69) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 70) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 71) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 72) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 73) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 74) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 75) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 76) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 77) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 78) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 79) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 80) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 81) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 82) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 83) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 84) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 85) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 86) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 87) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 88) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 89) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 90) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 91) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 92) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 93) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 94) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 95) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 96) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 97) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 98) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b( 99) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(100) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(101) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(102) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(103) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(104) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(105) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(106) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(107) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(108) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(109) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(110) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(111) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(112) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(113) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(114) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(115) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(116) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(117) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(118) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(119) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(120) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(121) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(122) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(123) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(124) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(125) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(126) and (a( 13) xor a( 18) xor a( 19) xor a( 20))) xor
        (b(127) and (a( 12) xor a( 17) xor a( 18) xor a( 19)));
    c( 19) <= 
        (b(  0) and (a( 19))) xor
        (b(  1) and (a( 18))) xor
        (b(  2) and (a( 17))) xor
        (b(  3) and (a( 16))) xor
        (b(  4) and (a( 15))) xor
        (b(  5) and (a( 14))) xor
        (b(  6) and (a( 13))) xor
        (b(  7) and (a( 12))) xor
        (b(  8) and (a( 11))) xor
        (b(  9) and (a( 10))) xor
        (b( 10) and (a(  9))) xor
        (b( 11) and (a(  8))) xor
        (b( 12) and (a(  7))) xor
        (b( 13) and (a(  6) xor a(127))) xor
        (b( 14) and (a(  5) xor a(126))) xor
        (b( 15) and (a(  4) xor a(125))) xor
        (b( 16) and (a(  3) xor a(124))) xor
        (b( 17) and (a(  2) xor a(123))) xor
        (b( 18) and (a(  1) xor a(122) xor a(127))) xor
        (b( 19) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 20) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 21) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 22) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 23) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 24) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 25) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 26) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 27) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 28) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 29) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 30) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 31) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 32) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 33) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 34) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 35) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 36) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 37) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 38) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 39) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 40) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 41) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 42) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 43) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 44) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 45) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 46) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 47) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 48) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 49) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 50) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 51) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 52) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 53) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 54) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 55) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 56) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 57) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 58) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 59) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 60) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 61) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 62) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 63) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 64) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 65) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 66) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 67) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 68) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 69) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 70) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 71) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 72) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 73) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 74) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 75) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 76) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 77) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 78) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 79) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 80) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 81) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 82) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 83) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 84) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 85) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 86) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 87) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 88) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 89) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 90) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 91) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 92) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 93) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 94) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 95) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 96) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 97) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 98) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b( 99) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(100) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(101) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(102) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(103) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(104) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(105) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(106) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(107) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(108) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(109) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(110) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(111) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(112) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(113) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(114) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(115) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(116) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(117) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(118) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(119) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(120) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(121) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(122) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(123) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(124) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(125) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(126) and (a( 14) xor a( 19) xor a( 20) xor a( 21))) xor
        (b(127) and (a( 13) xor a( 18) xor a( 19) xor a( 20)));
    c( 20) <= 
        (b(  0) and (a( 20))) xor
        (b(  1) and (a( 19))) xor
        (b(  2) and (a( 18))) xor
        (b(  3) and (a( 17))) xor
        (b(  4) and (a( 16))) xor
        (b(  5) and (a( 15))) xor
        (b(  6) and (a( 14))) xor
        (b(  7) and (a( 13))) xor
        (b(  8) and (a( 12))) xor
        (b(  9) and (a( 11))) xor
        (b( 10) and (a( 10))) xor
        (b( 11) and (a(  9))) xor
        (b( 12) and (a(  8))) xor
        (b( 13) and (a(  7))) xor
        (b( 14) and (a(  6) xor a(127))) xor
        (b( 15) and (a(  5) xor a(126))) xor
        (b( 16) and (a(  4) xor a(125))) xor
        (b( 17) and (a(  3) xor a(124))) xor
        (b( 18) and (a(  2) xor a(123))) xor
        (b( 19) and (a(  1) xor a(122) xor a(127))) xor
        (b( 20) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 21) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 22) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 23) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 24) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 25) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 26) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 27) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 28) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 29) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 30) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 31) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 32) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 33) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 34) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 35) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 36) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 37) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 38) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 39) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 40) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 41) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 42) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 43) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 44) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 45) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 46) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 47) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 48) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 49) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 50) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 51) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 52) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 53) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 54) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 55) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 56) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 57) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 58) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 59) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 60) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 61) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 62) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 63) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 64) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 65) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 66) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 67) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 68) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 69) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 70) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 71) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 72) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 73) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 74) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 75) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 76) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 77) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 78) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 79) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 80) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 81) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 82) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 83) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 84) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 85) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 86) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 87) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 88) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 89) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 90) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 91) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 92) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 93) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 94) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 95) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 96) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 97) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 98) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b( 99) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(100) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(101) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(102) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(103) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(104) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(105) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(106) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(107) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(108) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(109) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(110) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(111) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(112) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(113) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(114) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(115) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(116) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(117) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(118) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(119) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(120) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(121) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(122) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(123) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(124) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(125) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(126) and (a( 15) xor a( 20) xor a( 21) xor a( 22))) xor
        (b(127) and (a( 14) xor a( 19) xor a( 20) xor a( 21)));
    c( 21) <= 
        (b(  0) and (a( 21))) xor
        (b(  1) and (a( 20))) xor
        (b(  2) and (a( 19))) xor
        (b(  3) and (a( 18))) xor
        (b(  4) and (a( 17))) xor
        (b(  5) and (a( 16))) xor
        (b(  6) and (a( 15))) xor
        (b(  7) and (a( 14))) xor
        (b(  8) and (a( 13))) xor
        (b(  9) and (a( 12))) xor
        (b( 10) and (a( 11))) xor
        (b( 11) and (a( 10))) xor
        (b( 12) and (a(  9))) xor
        (b( 13) and (a(  8))) xor
        (b( 14) and (a(  7))) xor
        (b( 15) and (a(  6) xor a(127))) xor
        (b( 16) and (a(  5) xor a(126))) xor
        (b( 17) and (a(  4) xor a(125))) xor
        (b( 18) and (a(  3) xor a(124))) xor
        (b( 19) and (a(  2) xor a(123))) xor
        (b( 20) and (a(  1) xor a(122) xor a(127))) xor
        (b( 21) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 22) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 23) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 24) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 25) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 26) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 27) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 28) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 29) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 30) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 31) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 32) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 33) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 34) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 35) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 36) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 37) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 38) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 39) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 40) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 41) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 42) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 43) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 44) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 45) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 46) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 47) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 48) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 49) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 50) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 51) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 52) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 53) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 54) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 55) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 56) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 57) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 58) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 59) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 60) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 61) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 62) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 63) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 64) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 65) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 66) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 67) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 68) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 69) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 70) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 71) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 72) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 73) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 74) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 75) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 76) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 77) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 78) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 79) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 80) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 81) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 82) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 83) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 84) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 85) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 86) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 87) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 88) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 89) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 90) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 91) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 92) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 93) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 94) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 95) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 96) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 97) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 98) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b( 99) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(100) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(101) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(102) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(103) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(104) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(105) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(106) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(107) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(108) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(109) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(110) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(111) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(112) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(113) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(114) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(115) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(116) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(117) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(118) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(119) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(120) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(121) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(122) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(123) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(124) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(125) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(126) and (a( 16) xor a( 21) xor a( 22) xor a( 23))) xor
        (b(127) and (a( 15) xor a( 20) xor a( 21) xor a( 22)));
    c( 22) <= 
        (b(  0) and (a( 22))) xor
        (b(  1) and (a( 21))) xor
        (b(  2) and (a( 20))) xor
        (b(  3) and (a( 19))) xor
        (b(  4) and (a( 18))) xor
        (b(  5) and (a( 17))) xor
        (b(  6) and (a( 16))) xor
        (b(  7) and (a( 15))) xor
        (b(  8) and (a( 14))) xor
        (b(  9) and (a( 13))) xor
        (b( 10) and (a( 12))) xor
        (b( 11) and (a( 11))) xor
        (b( 12) and (a( 10))) xor
        (b( 13) and (a(  9))) xor
        (b( 14) and (a(  8))) xor
        (b( 15) and (a(  7))) xor
        (b( 16) and (a(  6) xor a(127))) xor
        (b( 17) and (a(  5) xor a(126))) xor
        (b( 18) and (a(  4) xor a(125))) xor
        (b( 19) and (a(  3) xor a(124))) xor
        (b( 20) and (a(  2) xor a(123))) xor
        (b( 21) and (a(  1) xor a(122) xor a(127))) xor
        (b( 22) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 23) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 24) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 25) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 26) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 27) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 28) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 29) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 30) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 31) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 32) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 33) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 34) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 35) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 36) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 37) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 38) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 39) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 40) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 41) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 42) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 43) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 44) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 45) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 46) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 47) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 48) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 49) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 50) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 51) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 52) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 53) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 54) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 55) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 56) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 57) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 58) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 59) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 60) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 61) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 62) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 63) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 64) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 65) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 66) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 67) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 68) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 69) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 70) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 71) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 72) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 73) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 74) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 75) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 76) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 77) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 78) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 79) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 80) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 81) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 82) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 83) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 84) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 85) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 86) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 87) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 88) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 89) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 90) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 91) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 92) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 93) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 94) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 95) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 96) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 97) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 98) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b( 99) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(100) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(101) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(102) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(103) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(104) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(105) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(106) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(107) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(108) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(109) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(110) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(111) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(112) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(113) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(114) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(115) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(116) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(117) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(118) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(119) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(120) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(121) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(122) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(123) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(124) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(125) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(126) and (a( 17) xor a( 22) xor a( 23) xor a( 24))) xor
        (b(127) and (a( 16) xor a( 21) xor a( 22) xor a( 23)));
    c( 23) <= 
        (b(  0) and (a( 23))) xor
        (b(  1) and (a( 22))) xor
        (b(  2) and (a( 21))) xor
        (b(  3) and (a( 20))) xor
        (b(  4) and (a( 19))) xor
        (b(  5) and (a( 18))) xor
        (b(  6) and (a( 17))) xor
        (b(  7) and (a( 16))) xor
        (b(  8) and (a( 15))) xor
        (b(  9) and (a( 14))) xor
        (b( 10) and (a( 13))) xor
        (b( 11) and (a( 12))) xor
        (b( 12) and (a( 11))) xor
        (b( 13) and (a( 10))) xor
        (b( 14) and (a(  9))) xor
        (b( 15) and (a(  8))) xor
        (b( 16) and (a(  7))) xor
        (b( 17) and (a(  6) xor a(127))) xor
        (b( 18) and (a(  5) xor a(126))) xor
        (b( 19) and (a(  4) xor a(125))) xor
        (b( 20) and (a(  3) xor a(124))) xor
        (b( 21) and (a(  2) xor a(123))) xor
        (b( 22) and (a(  1) xor a(122) xor a(127))) xor
        (b( 23) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 24) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 25) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 26) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 27) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 28) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 29) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 30) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 31) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 32) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 33) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 34) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 35) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 36) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 37) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 38) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 39) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 40) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 41) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 42) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 43) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 44) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 45) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 46) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 47) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 48) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 49) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 50) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 51) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 52) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 53) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 54) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 55) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 56) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 57) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 58) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 59) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 60) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 61) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 62) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 63) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 64) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 65) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 66) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 67) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 68) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 69) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 70) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 71) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 72) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 73) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 74) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 75) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 76) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 77) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 78) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 79) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 80) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 81) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 82) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 83) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 84) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 85) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 86) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 87) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 88) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 89) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 90) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 91) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 92) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 93) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 94) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 95) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 96) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 97) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 98) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b( 99) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(100) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(101) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(102) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(103) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(104) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(105) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(106) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(107) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(108) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(109) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(110) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(111) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(112) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(113) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(114) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(115) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(116) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(117) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(118) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(119) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(120) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(121) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(122) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(123) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(124) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(125) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(126) and (a( 18) xor a( 23) xor a( 24) xor a( 25))) xor
        (b(127) and (a( 17) xor a( 22) xor a( 23) xor a( 24)));
    c( 24) <= 
        (b(  0) and (a( 24))) xor
        (b(  1) and (a( 23))) xor
        (b(  2) and (a( 22))) xor
        (b(  3) and (a( 21))) xor
        (b(  4) and (a( 20))) xor
        (b(  5) and (a( 19))) xor
        (b(  6) and (a( 18))) xor
        (b(  7) and (a( 17))) xor
        (b(  8) and (a( 16))) xor
        (b(  9) and (a( 15))) xor
        (b( 10) and (a( 14))) xor
        (b( 11) and (a( 13))) xor
        (b( 12) and (a( 12))) xor
        (b( 13) and (a( 11))) xor
        (b( 14) and (a( 10))) xor
        (b( 15) and (a(  9))) xor
        (b( 16) and (a(  8))) xor
        (b( 17) and (a(  7))) xor
        (b( 18) and (a(  6) xor a(127))) xor
        (b( 19) and (a(  5) xor a(126))) xor
        (b( 20) and (a(  4) xor a(125))) xor
        (b( 21) and (a(  3) xor a(124))) xor
        (b( 22) and (a(  2) xor a(123))) xor
        (b( 23) and (a(  1) xor a(122) xor a(127))) xor
        (b( 24) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 25) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 26) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 27) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 28) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 29) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 30) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 31) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 32) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 33) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 34) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 35) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 36) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 37) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 38) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 39) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 40) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 41) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 42) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 43) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 44) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 45) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 46) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 47) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 48) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 49) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 50) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 51) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 52) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 53) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 54) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 55) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 56) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 57) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 58) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 59) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 60) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 61) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 62) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 63) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 64) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 65) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 66) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 67) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 68) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 69) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 70) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 71) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 72) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 73) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 74) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 75) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 76) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 77) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 78) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 79) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 80) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 81) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 82) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 83) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 84) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 85) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 86) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 87) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 88) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 89) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 90) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 91) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 92) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 93) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 94) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 95) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 96) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 97) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 98) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b( 99) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(100) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(101) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(102) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(103) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(104) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(105) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(106) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(107) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(108) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(109) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(110) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(111) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(112) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(113) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(114) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(115) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(116) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(117) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(118) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(119) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(120) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(121) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(122) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(123) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(124) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(125) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(126) and (a( 19) xor a( 24) xor a( 25) xor a( 26))) xor
        (b(127) and (a( 18) xor a( 23) xor a( 24) xor a( 25)));
    c( 25) <= 
        (b(  0) and (a( 25))) xor
        (b(  1) and (a( 24))) xor
        (b(  2) and (a( 23))) xor
        (b(  3) and (a( 22))) xor
        (b(  4) and (a( 21))) xor
        (b(  5) and (a( 20))) xor
        (b(  6) and (a( 19))) xor
        (b(  7) and (a( 18))) xor
        (b(  8) and (a( 17))) xor
        (b(  9) and (a( 16))) xor
        (b( 10) and (a( 15))) xor
        (b( 11) and (a( 14))) xor
        (b( 12) and (a( 13))) xor
        (b( 13) and (a( 12))) xor
        (b( 14) and (a( 11))) xor
        (b( 15) and (a( 10))) xor
        (b( 16) and (a(  9))) xor
        (b( 17) and (a(  8))) xor
        (b( 18) and (a(  7))) xor
        (b( 19) and (a(  6) xor a(127))) xor
        (b( 20) and (a(  5) xor a(126))) xor
        (b( 21) and (a(  4) xor a(125))) xor
        (b( 22) and (a(  3) xor a(124))) xor
        (b( 23) and (a(  2) xor a(123))) xor
        (b( 24) and (a(  1) xor a(122) xor a(127))) xor
        (b( 25) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 26) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 27) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 28) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 29) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 30) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 31) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 32) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 33) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 34) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 35) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 36) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 37) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 38) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 39) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 40) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 41) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 42) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 43) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 44) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 45) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 46) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 47) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 48) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 49) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 50) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 51) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 52) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 53) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 54) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 55) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 56) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 57) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 58) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 59) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 60) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 61) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 62) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 63) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 64) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 65) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 66) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 67) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 68) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 69) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 70) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 71) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 72) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 73) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 74) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 75) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 76) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 77) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 78) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 79) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 80) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 81) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 82) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 83) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 84) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 85) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 86) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 87) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 88) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 89) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 90) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 91) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 92) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 93) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 94) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 95) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 96) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 97) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 98) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b( 99) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(100) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(101) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(102) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(103) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(104) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(105) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(106) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(107) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(108) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(109) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(110) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(111) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(112) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(113) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(114) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(115) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(116) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(117) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(118) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(119) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(120) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(121) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(122) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(123) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(124) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(125) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(126) and (a( 20) xor a( 25) xor a( 26) xor a( 27))) xor
        (b(127) and (a( 19) xor a( 24) xor a( 25) xor a( 26)));
    c( 26) <= 
        (b(  0) and (a( 26))) xor
        (b(  1) and (a( 25))) xor
        (b(  2) and (a( 24))) xor
        (b(  3) and (a( 23))) xor
        (b(  4) and (a( 22))) xor
        (b(  5) and (a( 21))) xor
        (b(  6) and (a( 20))) xor
        (b(  7) and (a( 19))) xor
        (b(  8) and (a( 18))) xor
        (b(  9) and (a( 17))) xor
        (b( 10) and (a( 16))) xor
        (b( 11) and (a( 15))) xor
        (b( 12) and (a( 14))) xor
        (b( 13) and (a( 13))) xor
        (b( 14) and (a( 12))) xor
        (b( 15) and (a( 11))) xor
        (b( 16) and (a( 10))) xor
        (b( 17) and (a(  9))) xor
        (b( 18) and (a(  8))) xor
        (b( 19) and (a(  7))) xor
        (b( 20) and (a(  6) xor a(127))) xor
        (b( 21) and (a(  5) xor a(126))) xor
        (b( 22) and (a(  4) xor a(125))) xor
        (b( 23) and (a(  3) xor a(124))) xor
        (b( 24) and (a(  2) xor a(123))) xor
        (b( 25) and (a(  1) xor a(122) xor a(127))) xor
        (b( 26) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 27) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 28) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 29) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 30) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 31) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 32) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 33) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 34) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 35) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 36) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 37) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 38) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 39) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 40) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 41) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 42) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 43) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 44) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 45) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 46) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 47) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 48) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 49) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 50) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 51) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 52) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 53) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 54) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 55) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 56) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 57) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 58) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 59) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 60) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 61) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 62) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 63) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 64) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 65) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 66) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 67) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 68) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 69) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 70) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 71) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 72) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 73) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 74) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 75) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 76) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 77) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 78) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 79) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 80) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 81) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 82) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 83) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 84) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 85) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 86) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 87) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 88) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 89) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 90) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 91) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 92) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 93) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 94) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 95) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 96) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 97) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 98) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b( 99) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(100) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(101) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(102) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(103) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(104) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(105) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(106) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(107) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(108) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(109) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(110) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(111) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(112) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(113) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(114) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(115) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(116) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(117) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(118) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(119) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(120) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(121) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(122) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(123) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(124) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(125) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(126) and (a( 21) xor a( 26) xor a( 27) xor a( 28))) xor
        (b(127) and (a( 20) xor a( 25) xor a( 26) xor a( 27)));
    c( 27) <= 
        (b(  0) and (a( 27))) xor
        (b(  1) and (a( 26))) xor
        (b(  2) and (a( 25))) xor
        (b(  3) and (a( 24))) xor
        (b(  4) and (a( 23))) xor
        (b(  5) and (a( 22))) xor
        (b(  6) and (a( 21))) xor
        (b(  7) and (a( 20))) xor
        (b(  8) and (a( 19))) xor
        (b(  9) and (a( 18))) xor
        (b( 10) and (a( 17))) xor
        (b( 11) and (a( 16))) xor
        (b( 12) and (a( 15))) xor
        (b( 13) and (a( 14))) xor
        (b( 14) and (a( 13))) xor
        (b( 15) and (a( 12))) xor
        (b( 16) and (a( 11))) xor
        (b( 17) and (a( 10))) xor
        (b( 18) and (a(  9))) xor
        (b( 19) and (a(  8))) xor
        (b( 20) and (a(  7))) xor
        (b( 21) and (a(  6) xor a(127))) xor
        (b( 22) and (a(  5) xor a(126))) xor
        (b( 23) and (a(  4) xor a(125))) xor
        (b( 24) and (a(  3) xor a(124))) xor
        (b( 25) and (a(  2) xor a(123))) xor
        (b( 26) and (a(  1) xor a(122) xor a(127))) xor
        (b( 27) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 28) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 29) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 30) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 31) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 32) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 33) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 34) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 35) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 36) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 37) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 38) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 39) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 40) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 41) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 42) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 43) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 44) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 45) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 46) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 47) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 48) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 49) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 50) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 51) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 52) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 53) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 54) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 55) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 56) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 57) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 58) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 59) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 60) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 61) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 62) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 63) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 64) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 65) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 66) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 67) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 68) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 69) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 70) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 71) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 72) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 73) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 74) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 75) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 76) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 77) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 78) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 79) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 80) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 81) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 82) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 83) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 84) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 85) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 86) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 87) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 88) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 89) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 90) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 91) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 92) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 93) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 94) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 95) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 96) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 97) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 98) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b( 99) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(100) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(101) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(102) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(103) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(104) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(105) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(106) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(107) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(108) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(109) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(110) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(111) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(112) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(113) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(114) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(115) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(116) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(117) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(118) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(119) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(120) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(121) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(122) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(123) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(124) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(125) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(126) and (a( 22) xor a( 27) xor a( 28) xor a( 29))) xor
        (b(127) and (a( 21) xor a( 26) xor a( 27) xor a( 28)));
    c( 28) <= 
        (b(  0) and (a( 28))) xor
        (b(  1) and (a( 27))) xor
        (b(  2) and (a( 26))) xor
        (b(  3) and (a( 25))) xor
        (b(  4) and (a( 24))) xor
        (b(  5) and (a( 23))) xor
        (b(  6) and (a( 22))) xor
        (b(  7) and (a( 21))) xor
        (b(  8) and (a( 20))) xor
        (b(  9) and (a( 19))) xor
        (b( 10) and (a( 18))) xor
        (b( 11) and (a( 17))) xor
        (b( 12) and (a( 16))) xor
        (b( 13) and (a( 15))) xor
        (b( 14) and (a( 14))) xor
        (b( 15) and (a( 13))) xor
        (b( 16) and (a( 12))) xor
        (b( 17) and (a( 11))) xor
        (b( 18) and (a( 10))) xor
        (b( 19) and (a(  9))) xor
        (b( 20) and (a(  8))) xor
        (b( 21) and (a(  7))) xor
        (b( 22) and (a(  6) xor a(127))) xor
        (b( 23) and (a(  5) xor a(126))) xor
        (b( 24) and (a(  4) xor a(125))) xor
        (b( 25) and (a(  3) xor a(124))) xor
        (b( 26) and (a(  2) xor a(123))) xor
        (b( 27) and (a(  1) xor a(122) xor a(127))) xor
        (b( 28) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 29) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 30) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 31) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 32) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 33) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 34) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 35) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 36) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 37) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 38) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 39) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 40) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 41) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 42) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 43) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 44) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 45) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 46) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 47) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 48) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 49) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 50) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 51) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 52) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 53) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 54) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 55) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 56) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 57) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 58) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 59) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 60) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 61) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 62) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 63) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 64) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 65) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 66) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 67) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 68) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 69) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 70) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 71) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 72) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 73) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 74) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 75) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 76) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 77) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 78) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 79) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 80) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 81) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 82) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 83) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 84) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 85) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 86) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 87) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 88) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 89) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 90) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 91) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 92) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 93) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 94) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 95) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 96) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 97) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 98) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b( 99) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(100) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(101) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(102) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(103) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(104) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(105) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(106) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(107) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(108) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(109) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(110) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(111) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(112) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(113) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(114) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(115) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(116) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(117) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(118) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(119) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(120) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(121) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(122) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(123) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(124) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(125) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(126) and (a( 23) xor a( 28) xor a( 29) xor a( 30))) xor
        (b(127) and (a( 22) xor a( 27) xor a( 28) xor a( 29)));
    c( 29) <= 
        (b(  0) and (a( 29))) xor
        (b(  1) and (a( 28))) xor
        (b(  2) and (a( 27))) xor
        (b(  3) and (a( 26))) xor
        (b(  4) and (a( 25))) xor
        (b(  5) and (a( 24))) xor
        (b(  6) and (a( 23))) xor
        (b(  7) and (a( 22))) xor
        (b(  8) and (a( 21))) xor
        (b(  9) and (a( 20))) xor
        (b( 10) and (a( 19))) xor
        (b( 11) and (a( 18))) xor
        (b( 12) and (a( 17))) xor
        (b( 13) and (a( 16))) xor
        (b( 14) and (a( 15))) xor
        (b( 15) and (a( 14))) xor
        (b( 16) and (a( 13))) xor
        (b( 17) and (a( 12))) xor
        (b( 18) and (a( 11))) xor
        (b( 19) and (a( 10))) xor
        (b( 20) and (a(  9))) xor
        (b( 21) and (a(  8))) xor
        (b( 22) and (a(  7))) xor
        (b( 23) and (a(  6) xor a(127))) xor
        (b( 24) and (a(  5) xor a(126))) xor
        (b( 25) and (a(  4) xor a(125))) xor
        (b( 26) and (a(  3) xor a(124))) xor
        (b( 27) and (a(  2) xor a(123))) xor
        (b( 28) and (a(  1) xor a(122) xor a(127))) xor
        (b( 29) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 30) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 31) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 32) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 33) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 34) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 35) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 36) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 37) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 38) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 39) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 40) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 41) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 42) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 43) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 44) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 45) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 46) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 47) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 48) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 49) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 50) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 51) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 52) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 53) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 54) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 55) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 56) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 57) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 58) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 59) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 60) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 61) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 62) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 63) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 64) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 65) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 66) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 67) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 68) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 69) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 70) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 71) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 72) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 73) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 74) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 75) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 76) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 77) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 78) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 79) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 80) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 81) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 82) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 83) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 84) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 85) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 86) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 87) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 88) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 89) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 90) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 91) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 92) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 93) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 94) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 95) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 96) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 97) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 98) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b( 99) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(100) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(101) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(102) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(103) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(104) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(105) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(106) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(107) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(108) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(109) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(110) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(111) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(112) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(113) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(114) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(115) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(116) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(117) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(118) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(119) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(120) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(121) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(122) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(123) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(124) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(125) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(126) and (a( 24) xor a( 29) xor a( 30) xor a( 31))) xor
        (b(127) and (a( 23) xor a( 28) xor a( 29) xor a( 30)));
    c( 30) <= 
        (b(  0) and (a( 30))) xor
        (b(  1) and (a( 29))) xor
        (b(  2) and (a( 28))) xor
        (b(  3) and (a( 27))) xor
        (b(  4) and (a( 26))) xor
        (b(  5) and (a( 25))) xor
        (b(  6) and (a( 24))) xor
        (b(  7) and (a( 23))) xor
        (b(  8) and (a( 22))) xor
        (b(  9) and (a( 21))) xor
        (b( 10) and (a( 20))) xor
        (b( 11) and (a( 19))) xor
        (b( 12) and (a( 18))) xor
        (b( 13) and (a( 17))) xor
        (b( 14) and (a( 16))) xor
        (b( 15) and (a( 15))) xor
        (b( 16) and (a( 14))) xor
        (b( 17) and (a( 13))) xor
        (b( 18) and (a( 12))) xor
        (b( 19) and (a( 11))) xor
        (b( 20) and (a( 10))) xor
        (b( 21) and (a(  9))) xor
        (b( 22) and (a(  8))) xor
        (b( 23) and (a(  7))) xor
        (b( 24) and (a(  6) xor a(127))) xor
        (b( 25) and (a(  5) xor a(126))) xor
        (b( 26) and (a(  4) xor a(125))) xor
        (b( 27) and (a(  3) xor a(124))) xor
        (b( 28) and (a(  2) xor a(123))) xor
        (b( 29) and (a(  1) xor a(122) xor a(127))) xor
        (b( 30) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 31) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 32) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 33) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 34) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 35) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 36) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 37) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 38) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 39) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 40) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 41) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 42) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 43) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 44) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 45) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 46) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 47) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 48) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 49) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 50) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 51) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 52) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 53) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 54) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 55) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 56) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 57) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 58) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 59) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 60) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 61) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 62) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 63) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 64) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 65) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 66) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 67) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 68) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 69) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 70) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 71) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 72) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 73) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 74) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 75) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 76) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 77) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 78) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 79) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 80) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 81) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 82) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 83) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 84) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 85) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 86) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 87) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 88) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 89) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 90) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 91) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 92) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 93) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 94) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 95) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 96) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 97) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 98) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b( 99) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(100) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(101) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(102) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(103) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(104) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(105) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(106) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(107) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(108) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(109) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(110) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(111) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(112) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(113) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(114) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(115) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(116) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(117) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(118) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(119) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(120) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(121) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(122) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(123) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(124) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(125) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(126) and (a( 25) xor a( 30) xor a( 31) xor a( 32))) xor
        (b(127) and (a( 24) xor a( 29) xor a( 30) xor a( 31)));
    c( 31) <= 
        (b(  0) and (a( 31))) xor
        (b(  1) and (a( 30))) xor
        (b(  2) and (a( 29))) xor
        (b(  3) and (a( 28))) xor
        (b(  4) and (a( 27))) xor
        (b(  5) and (a( 26))) xor
        (b(  6) and (a( 25))) xor
        (b(  7) and (a( 24))) xor
        (b(  8) and (a( 23))) xor
        (b(  9) and (a( 22))) xor
        (b( 10) and (a( 21))) xor
        (b( 11) and (a( 20))) xor
        (b( 12) and (a( 19))) xor
        (b( 13) and (a( 18))) xor
        (b( 14) and (a( 17))) xor
        (b( 15) and (a( 16))) xor
        (b( 16) and (a( 15))) xor
        (b( 17) and (a( 14))) xor
        (b( 18) and (a( 13))) xor
        (b( 19) and (a( 12))) xor
        (b( 20) and (a( 11))) xor
        (b( 21) and (a( 10))) xor
        (b( 22) and (a(  9))) xor
        (b( 23) and (a(  8))) xor
        (b( 24) and (a(  7))) xor
        (b( 25) and (a(  6) xor a(127))) xor
        (b( 26) and (a(  5) xor a(126))) xor
        (b( 27) and (a(  4) xor a(125))) xor
        (b( 28) and (a(  3) xor a(124))) xor
        (b( 29) and (a(  2) xor a(123))) xor
        (b( 30) and (a(  1) xor a(122) xor a(127))) xor
        (b( 31) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 32) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 33) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 34) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 35) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 36) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 37) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 38) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 39) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 40) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 41) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 42) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 43) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 44) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 45) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 46) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 47) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 48) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 49) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 50) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 51) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 52) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 53) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 54) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 55) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 56) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 57) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 58) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 59) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 60) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 61) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 62) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 63) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 64) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 65) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 66) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 67) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 68) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 69) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 70) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 71) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 72) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 73) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 74) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 75) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 76) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 77) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 78) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 79) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 80) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 81) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 82) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 83) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 84) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 85) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 86) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 87) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 88) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 89) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 90) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 91) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 92) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 93) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 94) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 95) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 96) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 97) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 98) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b( 99) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(100) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(101) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(102) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(103) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(104) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(105) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(106) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(107) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(108) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(109) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(110) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(111) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(112) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(113) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(114) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(115) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(116) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(117) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(118) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(119) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(120) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(121) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(122) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(123) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(124) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(125) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(126) and (a( 26) xor a( 31) xor a( 32) xor a( 33))) xor
        (b(127) and (a( 25) xor a( 30) xor a( 31) xor a( 32)));
    c( 32) <= 
        (b(  0) and (a( 32))) xor
        (b(  1) and (a( 31))) xor
        (b(  2) and (a( 30))) xor
        (b(  3) and (a( 29))) xor
        (b(  4) and (a( 28))) xor
        (b(  5) and (a( 27))) xor
        (b(  6) and (a( 26))) xor
        (b(  7) and (a( 25))) xor
        (b(  8) and (a( 24))) xor
        (b(  9) and (a( 23))) xor
        (b( 10) and (a( 22))) xor
        (b( 11) and (a( 21))) xor
        (b( 12) and (a( 20))) xor
        (b( 13) and (a( 19))) xor
        (b( 14) and (a( 18))) xor
        (b( 15) and (a( 17))) xor
        (b( 16) and (a( 16))) xor
        (b( 17) and (a( 15))) xor
        (b( 18) and (a( 14))) xor
        (b( 19) and (a( 13))) xor
        (b( 20) and (a( 12))) xor
        (b( 21) and (a( 11))) xor
        (b( 22) and (a( 10))) xor
        (b( 23) and (a(  9))) xor
        (b( 24) and (a(  8))) xor
        (b( 25) and (a(  7))) xor
        (b( 26) and (a(  6) xor a(127))) xor
        (b( 27) and (a(  5) xor a(126))) xor
        (b( 28) and (a(  4) xor a(125))) xor
        (b( 29) and (a(  3) xor a(124))) xor
        (b( 30) and (a(  2) xor a(123))) xor
        (b( 31) and (a(  1) xor a(122) xor a(127))) xor
        (b( 32) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 33) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 34) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 35) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 36) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 37) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 38) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 39) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 40) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 41) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 42) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 43) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 44) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 45) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 46) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 47) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 48) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 49) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 50) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 51) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 52) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 53) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 54) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 55) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 56) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 57) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 58) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 59) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 60) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 61) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 62) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 63) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 64) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 65) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 66) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 67) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 68) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 69) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 70) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 71) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 72) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 73) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 74) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 75) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 76) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 77) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 78) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 79) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 80) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 81) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 82) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 83) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 84) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 85) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 86) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 87) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 88) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 89) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 90) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 91) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 92) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 93) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 94) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 95) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 96) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 97) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 98) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b( 99) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(100) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(101) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(102) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(103) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(104) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(105) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(106) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(107) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(108) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(109) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(110) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(111) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(112) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(113) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(114) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(115) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(116) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(117) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(118) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(119) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(120) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(121) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(122) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(123) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(124) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(125) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(126) and (a( 27) xor a( 32) xor a( 33) xor a( 34))) xor
        (b(127) and (a( 26) xor a( 31) xor a( 32) xor a( 33)));
    c( 33) <= 
        (b(  0) and (a( 33))) xor
        (b(  1) and (a( 32))) xor
        (b(  2) and (a( 31))) xor
        (b(  3) and (a( 30))) xor
        (b(  4) and (a( 29))) xor
        (b(  5) and (a( 28))) xor
        (b(  6) and (a( 27))) xor
        (b(  7) and (a( 26))) xor
        (b(  8) and (a( 25))) xor
        (b(  9) and (a( 24))) xor
        (b( 10) and (a( 23))) xor
        (b( 11) and (a( 22))) xor
        (b( 12) and (a( 21))) xor
        (b( 13) and (a( 20))) xor
        (b( 14) and (a( 19))) xor
        (b( 15) and (a( 18))) xor
        (b( 16) and (a( 17))) xor
        (b( 17) and (a( 16))) xor
        (b( 18) and (a( 15))) xor
        (b( 19) and (a( 14))) xor
        (b( 20) and (a( 13))) xor
        (b( 21) and (a( 12))) xor
        (b( 22) and (a( 11))) xor
        (b( 23) and (a( 10))) xor
        (b( 24) and (a(  9))) xor
        (b( 25) and (a(  8))) xor
        (b( 26) and (a(  7))) xor
        (b( 27) and (a(  6) xor a(127))) xor
        (b( 28) and (a(  5) xor a(126))) xor
        (b( 29) and (a(  4) xor a(125))) xor
        (b( 30) and (a(  3) xor a(124))) xor
        (b( 31) and (a(  2) xor a(123))) xor
        (b( 32) and (a(  1) xor a(122) xor a(127))) xor
        (b( 33) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 34) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 35) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 36) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 37) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 38) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 39) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 40) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 41) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 42) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 43) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 44) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 45) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 46) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 47) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 48) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 49) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 50) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 51) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 52) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 53) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 54) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 55) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 56) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 57) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 58) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 59) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 60) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 61) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 62) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 63) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 64) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 65) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 66) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 67) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 68) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 69) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 70) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 71) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 72) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 73) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 74) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 75) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 76) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 77) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 78) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 79) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 80) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 81) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 82) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 83) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 84) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 85) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 86) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 87) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 88) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 89) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 90) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 91) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 92) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 93) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 94) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 95) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 96) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 97) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 98) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b( 99) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(100) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(101) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(102) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(103) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(104) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(105) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(106) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(107) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(108) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(109) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(110) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(111) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(112) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(113) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(114) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(115) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(116) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(117) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(118) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(119) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(120) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(121) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(122) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(123) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(124) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(125) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(126) and (a( 28) xor a( 33) xor a( 34) xor a( 35))) xor
        (b(127) and (a( 27) xor a( 32) xor a( 33) xor a( 34)));
    c( 34) <= 
        (b(  0) and (a( 34))) xor
        (b(  1) and (a( 33))) xor
        (b(  2) and (a( 32))) xor
        (b(  3) and (a( 31))) xor
        (b(  4) and (a( 30))) xor
        (b(  5) and (a( 29))) xor
        (b(  6) and (a( 28))) xor
        (b(  7) and (a( 27))) xor
        (b(  8) and (a( 26))) xor
        (b(  9) and (a( 25))) xor
        (b( 10) and (a( 24))) xor
        (b( 11) and (a( 23))) xor
        (b( 12) and (a( 22))) xor
        (b( 13) and (a( 21))) xor
        (b( 14) and (a( 20))) xor
        (b( 15) and (a( 19))) xor
        (b( 16) and (a( 18))) xor
        (b( 17) and (a( 17))) xor
        (b( 18) and (a( 16))) xor
        (b( 19) and (a( 15))) xor
        (b( 20) and (a( 14))) xor
        (b( 21) and (a( 13))) xor
        (b( 22) and (a( 12))) xor
        (b( 23) and (a( 11))) xor
        (b( 24) and (a( 10))) xor
        (b( 25) and (a(  9))) xor
        (b( 26) and (a(  8))) xor
        (b( 27) and (a(  7))) xor
        (b( 28) and (a(  6) xor a(127))) xor
        (b( 29) and (a(  5) xor a(126))) xor
        (b( 30) and (a(  4) xor a(125))) xor
        (b( 31) and (a(  3) xor a(124))) xor
        (b( 32) and (a(  2) xor a(123))) xor
        (b( 33) and (a(  1) xor a(122) xor a(127))) xor
        (b( 34) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 35) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 36) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 37) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 38) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 39) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 40) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 41) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 42) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 43) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 44) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 45) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 46) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 47) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 48) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 49) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 50) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 51) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 52) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 53) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 54) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 55) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 56) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 57) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 58) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 59) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 60) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 61) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 62) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 63) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 64) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 65) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 66) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 67) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 68) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 69) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 70) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 71) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 72) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 73) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 74) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 75) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 76) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 77) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 78) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 79) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 80) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 81) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 82) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 83) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 84) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 85) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 86) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 87) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 88) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 89) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 90) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 91) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 92) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 93) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 94) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 95) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 96) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 97) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 98) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b( 99) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(100) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(101) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(102) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(103) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(104) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(105) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(106) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(107) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(108) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(109) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(110) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(111) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(112) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(113) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(114) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(115) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(116) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(117) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(118) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(119) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(120) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(121) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(122) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(123) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(124) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(125) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(126) and (a( 29) xor a( 34) xor a( 35) xor a( 36))) xor
        (b(127) and (a( 28) xor a( 33) xor a( 34) xor a( 35)));
    c( 35) <= 
        (b(  0) and (a( 35))) xor
        (b(  1) and (a( 34))) xor
        (b(  2) and (a( 33))) xor
        (b(  3) and (a( 32))) xor
        (b(  4) and (a( 31))) xor
        (b(  5) and (a( 30))) xor
        (b(  6) and (a( 29))) xor
        (b(  7) and (a( 28))) xor
        (b(  8) and (a( 27))) xor
        (b(  9) and (a( 26))) xor
        (b( 10) and (a( 25))) xor
        (b( 11) and (a( 24))) xor
        (b( 12) and (a( 23))) xor
        (b( 13) and (a( 22))) xor
        (b( 14) and (a( 21))) xor
        (b( 15) and (a( 20))) xor
        (b( 16) and (a( 19))) xor
        (b( 17) and (a( 18))) xor
        (b( 18) and (a( 17))) xor
        (b( 19) and (a( 16))) xor
        (b( 20) and (a( 15))) xor
        (b( 21) and (a( 14))) xor
        (b( 22) and (a( 13))) xor
        (b( 23) and (a( 12))) xor
        (b( 24) and (a( 11))) xor
        (b( 25) and (a( 10))) xor
        (b( 26) and (a(  9))) xor
        (b( 27) and (a(  8))) xor
        (b( 28) and (a(  7))) xor
        (b( 29) and (a(  6) xor a(127))) xor
        (b( 30) and (a(  5) xor a(126))) xor
        (b( 31) and (a(  4) xor a(125))) xor
        (b( 32) and (a(  3) xor a(124))) xor
        (b( 33) and (a(  2) xor a(123))) xor
        (b( 34) and (a(  1) xor a(122) xor a(127))) xor
        (b( 35) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 36) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 37) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 38) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 39) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 40) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 41) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 42) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 43) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 44) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 45) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 46) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 47) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 48) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 49) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 50) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 51) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 52) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 53) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 54) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 55) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 56) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 57) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 58) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 59) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 60) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 61) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 62) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 63) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 64) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 65) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 66) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 67) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 68) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 69) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 70) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 71) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 72) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 73) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 74) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 75) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 76) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 77) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 78) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 79) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 80) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 81) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 82) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 83) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 84) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 85) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 86) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 87) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 88) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 89) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 90) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 91) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 92) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 93) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 94) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 95) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 96) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 97) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 98) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b( 99) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(100) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(101) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(102) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(103) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(104) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(105) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(106) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(107) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(108) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(109) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(110) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(111) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(112) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(113) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(114) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(115) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(116) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(117) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(118) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(119) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(120) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(121) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(122) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(123) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(124) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(125) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(126) and (a( 30) xor a( 35) xor a( 36) xor a( 37))) xor
        (b(127) and (a( 29) xor a( 34) xor a( 35) xor a( 36)));
    c( 36) <= 
        (b(  0) and (a( 36))) xor
        (b(  1) and (a( 35))) xor
        (b(  2) and (a( 34))) xor
        (b(  3) and (a( 33))) xor
        (b(  4) and (a( 32))) xor
        (b(  5) and (a( 31))) xor
        (b(  6) and (a( 30))) xor
        (b(  7) and (a( 29))) xor
        (b(  8) and (a( 28))) xor
        (b(  9) and (a( 27))) xor
        (b( 10) and (a( 26))) xor
        (b( 11) and (a( 25))) xor
        (b( 12) and (a( 24))) xor
        (b( 13) and (a( 23))) xor
        (b( 14) and (a( 22))) xor
        (b( 15) and (a( 21))) xor
        (b( 16) and (a( 20))) xor
        (b( 17) and (a( 19))) xor
        (b( 18) and (a( 18))) xor
        (b( 19) and (a( 17))) xor
        (b( 20) and (a( 16))) xor
        (b( 21) and (a( 15))) xor
        (b( 22) and (a( 14))) xor
        (b( 23) and (a( 13))) xor
        (b( 24) and (a( 12))) xor
        (b( 25) and (a( 11))) xor
        (b( 26) and (a( 10))) xor
        (b( 27) and (a(  9))) xor
        (b( 28) and (a(  8))) xor
        (b( 29) and (a(  7))) xor
        (b( 30) and (a(  6) xor a(127))) xor
        (b( 31) and (a(  5) xor a(126))) xor
        (b( 32) and (a(  4) xor a(125))) xor
        (b( 33) and (a(  3) xor a(124))) xor
        (b( 34) and (a(  2) xor a(123))) xor
        (b( 35) and (a(  1) xor a(122) xor a(127))) xor
        (b( 36) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 37) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 38) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 39) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 40) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 41) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 42) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 43) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 44) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 45) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 46) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 47) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 48) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 49) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 50) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 51) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 52) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 53) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 54) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 55) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 56) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 57) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 58) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 59) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 60) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 61) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 62) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 63) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 64) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 65) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 66) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 67) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 68) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 69) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 70) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 71) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 72) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 73) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 74) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 75) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 76) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 77) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 78) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 79) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 80) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 81) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 82) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 83) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 84) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 85) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 86) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 87) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 88) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 89) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 90) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 91) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 92) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 93) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 94) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 95) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 96) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 97) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 98) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b( 99) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(100) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(101) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(102) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(103) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(104) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(105) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(106) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(107) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(108) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(109) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(110) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(111) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(112) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(113) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(114) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(115) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(116) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(117) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(118) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(119) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(120) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(121) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(122) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(123) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(124) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(125) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(126) and (a( 31) xor a( 36) xor a( 37) xor a( 38))) xor
        (b(127) and (a( 30) xor a( 35) xor a( 36) xor a( 37)));
    c( 37) <= 
        (b(  0) and (a( 37))) xor
        (b(  1) and (a( 36))) xor
        (b(  2) and (a( 35))) xor
        (b(  3) and (a( 34))) xor
        (b(  4) and (a( 33))) xor
        (b(  5) and (a( 32))) xor
        (b(  6) and (a( 31))) xor
        (b(  7) and (a( 30))) xor
        (b(  8) and (a( 29))) xor
        (b(  9) and (a( 28))) xor
        (b( 10) and (a( 27))) xor
        (b( 11) and (a( 26))) xor
        (b( 12) and (a( 25))) xor
        (b( 13) and (a( 24))) xor
        (b( 14) and (a( 23))) xor
        (b( 15) and (a( 22))) xor
        (b( 16) and (a( 21))) xor
        (b( 17) and (a( 20))) xor
        (b( 18) and (a( 19))) xor
        (b( 19) and (a( 18))) xor
        (b( 20) and (a( 17))) xor
        (b( 21) and (a( 16))) xor
        (b( 22) and (a( 15))) xor
        (b( 23) and (a( 14))) xor
        (b( 24) and (a( 13))) xor
        (b( 25) and (a( 12))) xor
        (b( 26) and (a( 11))) xor
        (b( 27) and (a( 10))) xor
        (b( 28) and (a(  9))) xor
        (b( 29) and (a(  8))) xor
        (b( 30) and (a(  7))) xor
        (b( 31) and (a(  6) xor a(127))) xor
        (b( 32) and (a(  5) xor a(126))) xor
        (b( 33) and (a(  4) xor a(125))) xor
        (b( 34) and (a(  3) xor a(124))) xor
        (b( 35) and (a(  2) xor a(123))) xor
        (b( 36) and (a(  1) xor a(122) xor a(127))) xor
        (b( 37) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 38) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 39) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 40) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 41) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 42) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 43) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 44) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 45) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 46) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 47) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 48) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 49) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 50) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 51) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 52) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 53) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 54) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 55) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 56) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 57) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 58) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 59) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 60) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 61) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 62) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 63) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 64) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 65) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 66) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 67) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 68) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 69) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 70) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 71) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 72) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 73) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 74) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 75) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 76) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 77) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 78) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 79) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 80) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 81) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 82) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 83) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 84) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 85) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 86) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 87) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 88) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 89) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 90) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 91) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 92) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 93) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 94) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 95) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 96) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 97) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 98) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b( 99) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(100) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(101) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(102) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(103) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(104) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(105) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(106) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(107) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(108) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(109) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(110) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(111) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(112) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(113) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(114) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(115) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(116) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(117) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(118) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(119) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(120) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(121) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(122) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(123) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(124) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(125) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(126) and (a( 32) xor a( 37) xor a( 38) xor a( 39))) xor
        (b(127) and (a( 31) xor a( 36) xor a( 37) xor a( 38)));
    c( 38) <= 
        (b(  0) and (a( 38))) xor
        (b(  1) and (a( 37))) xor
        (b(  2) and (a( 36))) xor
        (b(  3) and (a( 35))) xor
        (b(  4) and (a( 34))) xor
        (b(  5) and (a( 33))) xor
        (b(  6) and (a( 32))) xor
        (b(  7) and (a( 31))) xor
        (b(  8) and (a( 30))) xor
        (b(  9) and (a( 29))) xor
        (b( 10) and (a( 28))) xor
        (b( 11) and (a( 27))) xor
        (b( 12) and (a( 26))) xor
        (b( 13) and (a( 25))) xor
        (b( 14) and (a( 24))) xor
        (b( 15) and (a( 23))) xor
        (b( 16) and (a( 22))) xor
        (b( 17) and (a( 21))) xor
        (b( 18) and (a( 20))) xor
        (b( 19) and (a( 19))) xor
        (b( 20) and (a( 18))) xor
        (b( 21) and (a( 17))) xor
        (b( 22) and (a( 16))) xor
        (b( 23) and (a( 15))) xor
        (b( 24) and (a( 14))) xor
        (b( 25) and (a( 13))) xor
        (b( 26) and (a( 12))) xor
        (b( 27) and (a( 11))) xor
        (b( 28) and (a( 10))) xor
        (b( 29) and (a(  9))) xor
        (b( 30) and (a(  8))) xor
        (b( 31) and (a(  7))) xor
        (b( 32) and (a(  6) xor a(127))) xor
        (b( 33) and (a(  5) xor a(126))) xor
        (b( 34) and (a(  4) xor a(125))) xor
        (b( 35) and (a(  3) xor a(124))) xor
        (b( 36) and (a(  2) xor a(123))) xor
        (b( 37) and (a(  1) xor a(122) xor a(127))) xor
        (b( 38) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 39) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 40) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 41) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 42) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 43) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 44) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 45) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 46) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 47) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 48) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 49) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 50) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 51) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 52) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 53) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 54) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 55) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 56) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 57) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 58) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 59) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 60) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 61) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 62) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 63) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 64) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 65) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 66) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 67) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 68) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 69) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 70) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 71) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 72) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 73) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 74) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 75) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 76) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 77) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 78) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 79) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 80) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 81) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 82) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 83) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 84) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 85) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 86) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 87) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 88) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 89) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 90) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 91) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 92) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 93) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 94) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 95) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 96) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 97) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 98) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b( 99) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(100) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(101) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(102) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(103) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(104) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(105) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(106) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(107) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(108) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(109) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(110) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(111) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(112) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(113) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(114) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(115) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(116) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(117) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(118) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(119) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(120) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(121) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(122) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(123) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(124) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(125) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(126) and (a( 33) xor a( 38) xor a( 39) xor a( 40))) xor
        (b(127) and (a( 32) xor a( 37) xor a( 38) xor a( 39)));
    c( 39) <= 
        (b(  0) and (a( 39))) xor
        (b(  1) and (a( 38))) xor
        (b(  2) and (a( 37))) xor
        (b(  3) and (a( 36))) xor
        (b(  4) and (a( 35))) xor
        (b(  5) and (a( 34))) xor
        (b(  6) and (a( 33))) xor
        (b(  7) and (a( 32))) xor
        (b(  8) and (a( 31))) xor
        (b(  9) and (a( 30))) xor
        (b( 10) and (a( 29))) xor
        (b( 11) and (a( 28))) xor
        (b( 12) and (a( 27))) xor
        (b( 13) and (a( 26))) xor
        (b( 14) and (a( 25))) xor
        (b( 15) and (a( 24))) xor
        (b( 16) and (a( 23))) xor
        (b( 17) and (a( 22))) xor
        (b( 18) and (a( 21))) xor
        (b( 19) and (a( 20))) xor
        (b( 20) and (a( 19))) xor
        (b( 21) and (a( 18))) xor
        (b( 22) and (a( 17))) xor
        (b( 23) and (a( 16))) xor
        (b( 24) and (a( 15))) xor
        (b( 25) and (a( 14))) xor
        (b( 26) and (a( 13))) xor
        (b( 27) and (a( 12))) xor
        (b( 28) and (a( 11))) xor
        (b( 29) and (a( 10))) xor
        (b( 30) and (a(  9))) xor
        (b( 31) and (a(  8))) xor
        (b( 32) and (a(  7))) xor
        (b( 33) and (a(  6) xor a(127))) xor
        (b( 34) and (a(  5) xor a(126))) xor
        (b( 35) and (a(  4) xor a(125))) xor
        (b( 36) and (a(  3) xor a(124))) xor
        (b( 37) and (a(  2) xor a(123))) xor
        (b( 38) and (a(  1) xor a(122) xor a(127))) xor
        (b( 39) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 40) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 41) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 42) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 43) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 44) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 45) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 46) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 47) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 48) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 49) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 50) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 51) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 52) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 53) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 54) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 55) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 56) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 57) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 58) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 59) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 60) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 61) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 62) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 63) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 64) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 65) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 66) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 67) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 68) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 69) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 70) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 71) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 72) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 73) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 74) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 75) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 76) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 77) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 78) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 79) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 80) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 81) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 82) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 83) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 84) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 85) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 86) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 87) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 88) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 89) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 90) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 91) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 92) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 93) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 94) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 95) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 96) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 97) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 98) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b( 99) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(100) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(101) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(102) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(103) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(104) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(105) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(106) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(107) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(108) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(109) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(110) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(111) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(112) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(113) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(114) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(115) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(116) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(117) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(118) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(119) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(120) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(121) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(122) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(123) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(124) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(125) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(126) and (a( 34) xor a( 39) xor a( 40) xor a( 41))) xor
        (b(127) and (a( 33) xor a( 38) xor a( 39) xor a( 40)));
    c( 40) <= 
        (b(  0) and (a( 40))) xor
        (b(  1) and (a( 39))) xor
        (b(  2) and (a( 38))) xor
        (b(  3) and (a( 37))) xor
        (b(  4) and (a( 36))) xor
        (b(  5) and (a( 35))) xor
        (b(  6) and (a( 34))) xor
        (b(  7) and (a( 33))) xor
        (b(  8) and (a( 32))) xor
        (b(  9) and (a( 31))) xor
        (b( 10) and (a( 30))) xor
        (b( 11) and (a( 29))) xor
        (b( 12) and (a( 28))) xor
        (b( 13) and (a( 27))) xor
        (b( 14) and (a( 26))) xor
        (b( 15) and (a( 25))) xor
        (b( 16) and (a( 24))) xor
        (b( 17) and (a( 23))) xor
        (b( 18) and (a( 22))) xor
        (b( 19) and (a( 21))) xor
        (b( 20) and (a( 20))) xor
        (b( 21) and (a( 19))) xor
        (b( 22) and (a( 18))) xor
        (b( 23) and (a( 17))) xor
        (b( 24) and (a( 16))) xor
        (b( 25) and (a( 15))) xor
        (b( 26) and (a( 14))) xor
        (b( 27) and (a( 13))) xor
        (b( 28) and (a( 12))) xor
        (b( 29) and (a( 11))) xor
        (b( 30) and (a( 10))) xor
        (b( 31) and (a(  9))) xor
        (b( 32) and (a(  8))) xor
        (b( 33) and (a(  7))) xor
        (b( 34) and (a(  6) xor a(127))) xor
        (b( 35) and (a(  5) xor a(126))) xor
        (b( 36) and (a(  4) xor a(125))) xor
        (b( 37) and (a(  3) xor a(124))) xor
        (b( 38) and (a(  2) xor a(123))) xor
        (b( 39) and (a(  1) xor a(122) xor a(127))) xor
        (b( 40) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 41) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 42) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 43) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 44) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 45) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 46) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 47) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 48) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 49) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 50) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 51) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 52) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 53) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 54) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 55) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 56) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 57) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 58) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 59) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 60) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 61) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 62) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 63) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 64) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 65) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 66) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 67) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 68) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 69) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 70) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 71) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 72) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 73) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 74) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 75) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 76) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 77) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 78) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 79) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 80) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 81) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 82) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 83) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 84) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 85) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 86) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 87) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 88) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 89) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 90) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 91) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 92) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 93) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 94) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 95) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 96) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 97) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 98) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b( 99) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(100) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(101) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(102) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(103) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(104) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(105) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(106) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(107) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(108) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(109) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(110) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(111) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(112) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(113) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(114) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(115) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(116) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(117) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(118) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(119) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(120) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(121) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(122) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(123) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(124) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(125) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(126) and (a( 35) xor a( 40) xor a( 41) xor a( 42))) xor
        (b(127) and (a( 34) xor a( 39) xor a( 40) xor a( 41)));
    c( 41) <= 
        (b(  0) and (a( 41))) xor
        (b(  1) and (a( 40))) xor
        (b(  2) and (a( 39))) xor
        (b(  3) and (a( 38))) xor
        (b(  4) and (a( 37))) xor
        (b(  5) and (a( 36))) xor
        (b(  6) and (a( 35))) xor
        (b(  7) and (a( 34))) xor
        (b(  8) and (a( 33))) xor
        (b(  9) and (a( 32))) xor
        (b( 10) and (a( 31))) xor
        (b( 11) and (a( 30))) xor
        (b( 12) and (a( 29))) xor
        (b( 13) and (a( 28))) xor
        (b( 14) and (a( 27))) xor
        (b( 15) and (a( 26))) xor
        (b( 16) and (a( 25))) xor
        (b( 17) and (a( 24))) xor
        (b( 18) and (a( 23))) xor
        (b( 19) and (a( 22))) xor
        (b( 20) and (a( 21))) xor
        (b( 21) and (a( 20))) xor
        (b( 22) and (a( 19))) xor
        (b( 23) and (a( 18))) xor
        (b( 24) and (a( 17))) xor
        (b( 25) and (a( 16))) xor
        (b( 26) and (a( 15))) xor
        (b( 27) and (a( 14))) xor
        (b( 28) and (a( 13))) xor
        (b( 29) and (a( 12))) xor
        (b( 30) and (a( 11))) xor
        (b( 31) and (a( 10))) xor
        (b( 32) and (a(  9))) xor
        (b( 33) and (a(  8))) xor
        (b( 34) and (a(  7))) xor
        (b( 35) and (a(  6) xor a(127))) xor
        (b( 36) and (a(  5) xor a(126))) xor
        (b( 37) and (a(  4) xor a(125))) xor
        (b( 38) and (a(  3) xor a(124))) xor
        (b( 39) and (a(  2) xor a(123))) xor
        (b( 40) and (a(  1) xor a(122) xor a(127))) xor
        (b( 41) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 42) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 43) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 44) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 45) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 46) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 47) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 48) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 49) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 50) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 51) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 52) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 53) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 54) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 55) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 56) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 57) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 58) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 59) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 60) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 61) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 62) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 63) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 64) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 65) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 66) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 67) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 68) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 69) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 70) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 71) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 72) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 73) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 74) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 75) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 76) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 77) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 78) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 79) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 80) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 81) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 82) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 83) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 84) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 85) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 86) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 87) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 88) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 89) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 90) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 91) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 92) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 93) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 94) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 95) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 96) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 97) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 98) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b( 99) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(100) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(101) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(102) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(103) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(104) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(105) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(106) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(107) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(108) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(109) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(110) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(111) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(112) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(113) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(114) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(115) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(116) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(117) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(118) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(119) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(120) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(121) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(122) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(123) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(124) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(125) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(126) and (a( 36) xor a( 41) xor a( 42) xor a( 43))) xor
        (b(127) and (a( 35) xor a( 40) xor a( 41) xor a( 42)));
    c( 42) <= 
        (b(  0) and (a( 42))) xor
        (b(  1) and (a( 41))) xor
        (b(  2) and (a( 40))) xor
        (b(  3) and (a( 39))) xor
        (b(  4) and (a( 38))) xor
        (b(  5) and (a( 37))) xor
        (b(  6) and (a( 36))) xor
        (b(  7) and (a( 35))) xor
        (b(  8) and (a( 34))) xor
        (b(  9) and (a( 33))) xor
        (b( 10) and (a( 32))) xor
        (b( 11) and (a( 31))) xor
        (b( 12) and (a( 30))) xor
        (b( 13) and (a( 29))) xor
        (b( 14) and (a( 28))) xor
        (b( 15) and (a( 27))) xor
        (b( 16) and (a( 26))) xor
        (b( 17) and (a( 25))) xor
        (b( 18) and (a( 24))) xor
        (b( 19) and (a( 23))) xor
        (b( 20) and (a( 22))) xor
        (b( 21) and (a( 21))) xor
        (b( 22) and (a( 20))) xor
        (b( 23) and (a( 19))) xor
        (b( 24) and (a( 18))) xor
        (b( 25) and (a( 17))) xor
        (b( 26) and (a( 16))) xor
        (b( 27) and (a( 15))) xor
        (b( 28) and (a( 14))) xor
        (b( 29) and (a( 13))) xor
        (b( 30) and (a( 12))) xor
        (b( 31) and (a( 11))) xor
        (b( 32) and (a( 10))) xor
        (b( 33) and (a(  9))) xor
        (b( 34) and (a(  8))) xor
        (b( 35) and (a(  7))) xor
        (b( 36) and (a(  6) xor a(127))) xor
        (b( 37) and (a(  5) xor a(126))) xor
        (b( 38) and (a(  4) xor a(125))) xor
        (b( 39) and (a(  3) xor a(124))) xor
        (b( 40) and (a(  2) xor a(123))) xor
        (b( 41) and (a(  1) xor a(122) xor a(127))) xor
        (b( 42) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 43) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 44) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 45) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 46) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 47) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 48) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 49) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 50) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 51) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 52) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 53) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 54) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 55) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 56) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 57) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 58) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 59) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 60) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 61) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 62) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 63) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 64) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 65) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 66) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 67) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 68) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 69) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 70) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 71) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 72) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 73) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 74) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 75) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 76) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 77) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 78) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 79) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 80) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 81) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 82) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 83) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 84) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 85) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 86) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 87) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 88) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 89) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 90) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 91) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 92) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 93) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 94) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 95) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 96) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 97) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 98) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b( 99) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(100) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(101) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(102) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(103) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(104) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(105) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(106) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(107) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(108) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(109) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(110) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(111) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(112) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(113) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(114) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(115) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(116) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(117) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(118) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(119) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(120) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(121) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(122) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(123) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(124) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(125) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(126) and (a( 37) xor a( 42) xor a( 43) xor a( 44))) xor
        (b(127) and (a( 36) xor a( 41) xor a( 42) xor a( 43)));
    c( 43) <= 
        (b(  0) and (a( 43))) xor
        (b(  1) and (a( 42))) xor
        (b(  2) and (a( 41))) xor
        (b(  3) and (a( 40))) xor
        (b(  4) and (a( 39))) xor
        (b(  5) and (a( 38))) xor
        (b(  6) and (a( 37))) xor
        (b(  7) and (a( 36))) xor
        (b(  8) and (a( 35))) xor
        (b(  9) and (a( 34))) xor
        (b( 10) and (a( 33))) xor
        (b( 11) and (a( 32))) xor
        (b( 12) and (a( 31))) xor
        (b( 13) and (a( 30))) xor
        (b( 14) and (a( 29))) xor
        (b( 15) and (a( 28))) xor
        (b( 16) and (a( 27))) xor
        (b( 17) and (a( 26))) xor
        (b( 18) and (a( 25))) xor
        (b( 19) and (a( 24))) xor
        (b( 20) and (a( 23))) xor
        (b( 21) and (a( 22))) xor
        (b( 22) and (a( 21))) xor
        (b( 23) and (a( 20))) xor
        (b( 24) and (a( 19))) xor
        (b( 25) and (a( 18))) xor
        (b( 26) and (a( 17))) xor
        (b( 27) and (a( 16))) xor
        (b( 28) and (a( 15))) xor
        (b( 29) and (a( 14))) xor
        (b( 30) and (a( 13))) xor
        (b( 31) and (a( 12))) xor
        (b( 32) and (a( 11))) xor
        (b( 33) and (a( 10))) xor
        (b( 34) and (a(  9))) xor
        (b( 35) and (a(  8))) xor
        (b( 36) and (a(  7))) xor
        (b( 37) and (a(  6) xor a(127))) xor
        (b( 38) and (a(  5) xor a(126))) xor
        (b( 39) and (a(  4) xor a(125))) xor
        (b( 40) and (a(  3) xor a(124))) xor
        (b( 41) and (a(  2) xor a(123))) xor
        (b( 42) and (a(  1) xor a(122) xor a(127))) xor
        (b( 43) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 44) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 45) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 46) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 47) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 48) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 49) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 50) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 51) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 52) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 53) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 54) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 55) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 56) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 57) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 58) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 59) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 60) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 61) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 62) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 63) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 64) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 65) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 66) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 67) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 68) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 69) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 70) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 71) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 72) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 73) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 74) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 75) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 76) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 77) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 78) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 79) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 80) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 81) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 82) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 83) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 84) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 85) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 86) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 87) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 88) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 89) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 90) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 91) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 92) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 93) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 94) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 95) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 96) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 97) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 98) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b( 99) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(100) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(101) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(102) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(103) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(104) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(105) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(106) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(107) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(108) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(109) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(110) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(111) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(112) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(113) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(114) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(115) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(116) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(117) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(118) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(119) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(120) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(121) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(122) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(123) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(124) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(125) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(126) and (a( 38) xor a( 43) xor a( 44) xor a( 45))) xor
        (b(127) and (a( 37) xor a( 42) xor a( 43) xor a( 44)));
    c( 44) <= 
        (b(  0) and (a( 44))) xor
        (b(  1) and (a( 43))) xor
        (b(  2) and (a( 42))) xor
        (b(  3) and (a( 41))) xor
        (b(  4) and (a( 40))) xor
        (b(  5) and (a( 39))) xor
        (b(  6) and (a( 38))) xor
        (b(  7) and (a( 37))) xor
        (b(  8) and (a( 36))) xor
        (b(  9) and (a( 35))) xor
        (b( 10) and (a( 34))) xor
        (b( 11) and (a( 33))) xor
        (b( 12) and (a( 32))) xor
        (b( 13) and (a( 31))) xor
        (b( 14) and (a( 30))) xor
        (b( 15) and (a( 29))) xor
        (b( 16) and (a( 28))) xor
        (b( 17) and (a( 27))) xor
        (b( 18) and (a( 26))) xor
        (b( 19) and (a( 25))) xor
        (b( 20) and (a( 24))) xor
        (b( 21) and (a( 23))) xor
        (b( 22) and (a( 22))) xor
        (b( 23) and (a( 21))) xor
        (b( 24) and (a( 20))) xor
        (b( 25) and (a( 19))) xor
        (b( 26) and (a( 18))) xor
        (b( 27) and (a( 17))) xor
        (b( 28) and (a( 16))) xor
        (b( 29) and (a( 15))) xor
        (b( 30) and (a( 14))) xor
        (b( 31) and (a( 13))) xor
        (b( 32) and (a( 12))) xor
        (b( 33) and (a( 11))) xor
        (b( 34) and (a( 10))) xor
        (b( 35) and (a(  9))) xor
        (b( 36) and (a(  8))) xor
        (b( 37) and (a(  7))) xor
        (b( 38) and (a(  6) xor a(127))) xor
        (b( 39) and (a(  5) xor a(126))) xor
        (b( 40) and (a(  4) xor a(125))) xor
        (b( 41) and (a(  3) xor a(124))) xor
        (b( 42) and (a(  2) xor a(123))) xor
        (b( 43) and (a(  1) xor a(122) xor a(127))) xor
        (b( 44) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 45) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 46) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 47) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 48) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 49) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 50) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 51) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 52) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 53) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 54) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 55) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 56) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 57) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 58) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 59) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 60) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 61) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 62) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 63) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 64) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 65) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 66) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 67) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 68) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 69) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 70) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 71) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 72) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 73) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 74) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 75) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 76) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 77) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 78) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 79) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 80) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 81) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 82) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 83) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 84) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 85) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 86) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 87) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 88) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 89) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 90) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 91) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 92) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 93) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 94) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 95) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 96) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 97) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 98) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b( 99) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(100) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(101) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(102) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(103) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(104) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(105) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(106) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(107) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(108) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(109) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(110) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(111) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(112) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(113) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(114) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(115) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(116) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(117) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(118) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(119) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(120) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(121) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(122) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(123) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(124) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(125) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(126) and (a( 39) xor a( 44) xor a( 45) xor a( 46))) xor
        (b(127) and (a( 38) xor a( 43) xor a( 44) xor a( 45)));
    c( 45) <= 
        (b(  0) and (a( 45))) xor
        (b(  1) and (a( 44))) xor
        (b(  2) and (a( 43))) xor
        (b(  3) and (a( 42))) xor
        (b(  4) and (a( 41))) xor
        (b(  5) and (a( 40))) xor
        (b(  6) and (a( 39))) xor
        (b(  7) and (a( 38))) xor
        (b(  8) and (a( 37))) xor
        (b(  9) and (a( 36))) xor
        (b( 10) and (a( 35))) xor
        (b( 11) and (a( 34))) xor
        (b( 12) and (a( 33))) xor
        (b( 13) and (a( 32))) xor
        (b( 14) and (a( 31))) xor
        (b( 15) and (a( 30))) xor
        (b( 16) and (a( 29))) xor
        (b( 17) and (a( 28))) xor
        (b( 18) and (a( 27))) xor
        (b( 19) and (a( 26))) xor
        (b( 20) and (a( 25))) xor
        (b( 21) and (a( 24))) xor
        (b( 22) and (a( 23))) xor
        (b( 23) and (a( 22))) xor
        (b( 24) and (a( 21))) xor
        (b( 25) and (a( 20))) xor
        (b( 26) and (a( 19))) xor
        (b( 27) and (a( 18))) xor
        (b( 28) and (a( 17))) xor
        (b( 29) and (a( 16))) xor
        (b( 30) and (a( 15))) xor
        (b( 31) and (a( 14))) xor
        (b( 32) and (a( 13))) xor
        (b( 33) and (a( 12))) xor
        (b( 34) and (a( 11))) xor
        (b( 35) and (a( 10))) xor
        (b( 36) and (a(  9))) xor
        (b( 37) and (a(  8))) xor
        (b( 38) and (a(  7))) xor
        (b( 39) and (a(  6) xor a(127))) xor
        (b( 40) and (a(  5) xor a(126))) xor
        (b( 41) and (a(  4) xor a(125))) xor
        (b( 42) and (a(  3) xor a(124))) xor
        (b( 43) and (a(  2) xor a(123))) xor
        (b( 44) and (a(  1) xor a(122) xor a(127))) xor
        (b( 45) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 46) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 47) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 48) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 49) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 50) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 51) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 52) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 53) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 54) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 55) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 56) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 57) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 58) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 59) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 60) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 61) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 62) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 63) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 64) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 65) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 66) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 67) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 68) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 69) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 70) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 71) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 72) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 73) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 74) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 75) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 76) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 77) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 78) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 79) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 80) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 81) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 82) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 83) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 84) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 85) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 86) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 87) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 88) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 89) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 90) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 91) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 92) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 93) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 94) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 95) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 96) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 97) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 98) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b( 99) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(100) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(101) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(102) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(103) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(104) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(105) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(106) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(107) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(108) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(109) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(110) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(111) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(112) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(113) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(114) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(115) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(116) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(117) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(118) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(119) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(120) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(121) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(122) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(123) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(124) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(125) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(126) and (a( 40) xor a( 45) xor a( 46) xor a( 47))) xor
        (b(127) and (a( 39) xor a( 44) xor a( 45) xor a( 46)));
    c( 46) <= 
        (b(  0) and (a( 46))) xor
        (b(  1) and (a( 45))) xor
        (b(  2) and (a( 44))) xor
        (b(  3) and (a( 43))) xor
        (b(  4) and (a( 42))) xor
        (b(  5) and (a( 41))) xor
        (b(  6) and (a( 40))) xor
        (b(  7) and (a( 39))) xor
        (b(  8) and (a( 38))) xor
        (b(  9) and (a( 37))) xor
        (b( 10) and (a( 36))) xor
        (b( 11) and (a( 35))) xor
        (b( 12) and (a( 34))) xor
        (b( 13) and (a( 33))) xor
        (b( 14) and (a( 32))) xor
        (b( 15) and (a( 31))) xor
        (b( 16) and (a( 30))) xor
        (b( 17) and (a( 29))) xor
        (b( 18) and (a( 28))) xor
        (b( 19) and (a( 27))) xor
        (b( 20) and (a( 26))) xor
        (b( 21) and (a( 25))) xor
        (b( 22) and (a( 24))) xor
        (b( 23) and (a( 23))) xor
        (b( 24) and (a( 22))) xor
        (b( 25) and (a( 21))) xor
        (b( 26) and (a( 20))) xor
        (b( 27) and (a( 19))) xor
        (b( 28) and (a( 18))) xor
        (b( 29) and (a( 17))) xor
        (b( 30) and (a( 16))) xor
        (b( 31) and (a( 15))) xor
        (b( 32) and (a( 14))) xor
        (b( 33) and (a( 13))) xor
        (b( 34) and (a( 12))) xor
        (b( 35) and (a( 11))) xor
        (b( 36) and (a( 10))) xor
        (b( 37) and (a(  9))) xor
        (b( 38) and (a(  8))) xor
        (b( 39) and (a(  7))) xor
        (b( 40) and (a(  6) xor a(127))) xor
        (b( 41) and (a(  5) xor a(126))) xor
        (b( 42) and (a(  4) xor a(125))) xor
        (b( 43) and (a(  3) xor a(124))) xor
        (b( 44) and (a(  2) xor a(123))) xor
        (b( 45) and (a(  1) xor a(122) xor a(127))) xor
        (b( 46) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 47) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 48) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 49) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 50) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 51) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 52) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 53) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 54) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 55) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 56) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 57) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 58) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 59) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 60) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 61) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 62) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 63) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 64) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 65) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 66) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 67) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 68) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 69) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 70) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 71) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 72) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 73) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 74) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 75) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 76) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 77) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 78) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 79) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 80) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 81) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 82) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 83) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 84) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 85) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 86) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 87) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 88) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 89) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 90) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 91) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 92) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 93) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 94) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 95) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 96) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 97) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 98) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b( 99) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(100) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(101) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(102) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(103) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(104) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(105) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(106) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(107) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(108) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(109) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(110) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(111) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(112) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(113) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(114) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(115) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(116) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(117) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(118) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(119) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(120) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(121) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(122) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(123) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(124) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(125) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(126) and (a( 41) xor a( 46) xor a( 47) xor a( 48))) xor
        (b(127) and (a( 40) xor a( 45) xor a( 46) xor a( 47)));
    c( 47) <= 
        (b(  0) and (a( 47))) xor
        (b(  1) and (a( 46))) xor
        (b(  2) and (a( 45))) xor
        (b(  3) and (a( 44))) xor
        (b(  4) and (a( 43))) xor
        (b(  5) and (a( 42))) xor
        (b(  6) and (a( 41))) xor
        (b(  7) and (a( 40))) xor
        (b(  8) and (a( 39))) xor
        (b(  9) and (a( 38))) xor
        (b( 10) and (a( 37))) xor
        (b( 11) and (a( 36))) xor
        (b( 12) and (a( 35))) xor
        (b( 13) and (a( 34))) xor
        (b( 14) and (a( 33))) xor
        (b( 15) and (a( 32))) xor
        (b( 16) and (a( 31))) xor
        (b( 17) and (a( 30))) xor
        (b( 18) and (a( 29))) xor
        (b( 19) and (a( 28))) xor
        (b( 20) and (a( 27))) xor
        (b( 21) and (a( 26))) xor
        (b( 22) and (a( 25))) xor
        (b( 23) and (a( 24))) xor
        (b( 24) and (a( 23))) xor
        (b( 25) and (a( 22))) xor
        (b( 26) and (a( 21))) xor
        (b( 27) and (a( 20))) xor
        (b( 28) and (a( 19))) xor
        (b( 29) and (a( 18))) xor
        (b( 30) and (a( 17))) xor
        (b( 31) and (a( 16))) xor
        (b( 32) and (a( 15))) xor
        (b( 33) and (a( 14))) xor
        (b( 34) and (a( 13))) xor
        (b( 35) and (a( 12))) xor
        (b( 36) and (a( 11))) xor
        (b( 37) and (a( 10))) xor
        (b( 38) and (a(  9))) xor
        (b( 39) and (a(  8))) xor
        (b( 40) and (a(  7))) xor
        (b( 41) and (a(  6) xor a(127))) xor
        (b( 42) and (a(  5) xor a(126))) xor
        (b( 43) and (a(  4) xor a(125))) xor
        (b( 44) and (a(  3) xor a(124))) xor
        (b( 45) and (a(  2) xor a(123))) xor
        (b( 46) and (a(  1) xor a(122) xor a(127))) xor
        (b( 47) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 48) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 49) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 50) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 51) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 52) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 53) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 54) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 55) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 56) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 57) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 58) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 59) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 60) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 61) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 62) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 63) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 64) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 65) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 66) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 67) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 68) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 69) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 70) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 71) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 72) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 73) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 74) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 75) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 76) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 77) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 78) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 79) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 80) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 81) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 82) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 83) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 84) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 85) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 86) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 87) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 88) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 89) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 90) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 91) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 92) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 93) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 94) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 95) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 96) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 97) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 98) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b( 99) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(100) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(101) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(102) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(103) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(104) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(105) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(106) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(107) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(108) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(109) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(110) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(111) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(112) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(113) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(114) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(115) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(116) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(117) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(118) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(119) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(120) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(121) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(122) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(123) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(124) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(125) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(126) and (a( 42) xor a( 47) xor a( 48) xor a( 49))) xor
        (b(127) and (a( 41) xor a( 46) xor a( 47) xor a( 48)));
    c( 48) <= 
        (b(  0) and (a( 48))) xor
        (b(  1) and (a( 47))) xor
        (b(  2) and (a( 46))) xor
        (b(  3) and (a( 45))) xor
        (b(  4) and (a( 44))) xor
        (b(  5) and (a( 43))) xor
        (b(  6) and (a( 42))) xor
        (b(  7) and (a( 41))) xor
        (b(  8) and (a( 40))) xor
        (b(  9) and (a( 39))) xor
        (b( 10) and (a( 38))) xor
        (b( 11) and (a( 37))) xor
        (b( 12) and (a( 36))) xor
        (b( 13) and (a( 35))) xor
        (b( 14) and (a( 34))) xor
        (b( 15) and (a( 33))) xor
        (b( 16) and (a( 32))) xor
        (b( 17) and (a( 31))) xor
        (b( 18) and (a( 30))) xor
        (b( 19) and (a( 29))) xor
        (b( 20) and (a( 28))) xor
        (b( 21) and (a( 27))) xor
        (b( 22) and (a( 26))) xor
        (b( 23) and (a( 25))) xor
        (b( 24) and (a( 24))) xor
        (b( 25) and (a( 23))) xor
        (b( 26) and (a( 22))) xor
        (b( 27) and (a( 21))) xor
        (b( 28) and (a( 20))) xor
        (b( 29) and (a( 19))) xor
        (b( 30) and (a( 18))) xor
        (b( 31) and (a( 17))) xor
        (b( 32) and (a( 16))) xor
        (b( 33) and (a( 15))) xor
        (b( 34) and (a( 14))) xor
        (b( 35) and (a( 13))) xor
        (b( 36) and (a( 12))) xor
        (b( 37) and (a( 11))) xor
        (b( 38) and (a( 10))) xor
        (b( 39) and (a(  9))) xor
        (b( 40) and (a(  8))) xor
        (b( 41) and (a(  7))) xor
        (b( 42) and (a(  6) xor a(127))) xor
        (b( 43) and (a(  5) xor a(126))) xor
        (b( 44) and (a(  4) xor a(125))) xor
        (b( 45) and (a(  3) xor a(124))) xor
        (b( 46) and (a(  2) xor a(123))) xor
        (b( 47) and (a(  1) xor a(122) xor a(127))) xor
        (b( 48) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 49) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 50) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 51) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 52) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 53) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 54) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 55) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 56) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 57) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 58) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 59) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 60) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 61) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 62) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 63) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 64) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 65) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 66) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 67) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 68) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 69) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 70) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 71) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 72) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 73) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 74) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 75) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 76) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 77) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 78) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 79) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 80) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 81) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 82) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 83) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 84) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 85) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 86) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 87) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 88) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 89) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 90) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 91) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 92) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 93) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 94) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 95) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 96) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 97) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 98) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b( 99) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(100) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(101) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(102) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(103) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(104) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(105) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(106) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(107) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(108) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(109) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(110) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(111) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(112) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(113) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(114) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(115) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(116) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(117) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(118) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(119) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(120) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(121) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(122) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(123) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(124) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(125) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(126) and (a( 43) xor a( 48) xor a( 49) xor a( 50))) xor
        (b(127) and (a( 42) xor a( 47) xor a( 48) xor a( 49)));
    c( 49) <= 
        (b(  0) and (a( 49))) xor
        (b(  1) and (a( 48))) xor
        (b(  2) and (a( 47))) xor
        (b(  3) and (a( 46))) xor
        (b(  4) and (a( 45))) xor
        (b(  5) and (a( 44))) xor
        (b(  6) and (a( 43))) xor
        (b(  7) and (a( 42))) xor
        (b(  8) and (a( 41))) xor
        (b(  9) and (a( 40))) xor
        (b( 10) and (a( 39))) xor
        (b( 11) and (a( 38))) xor
        (b( 12) and (a( 37))) xor
        (b( 13) and (a( 36))) xor
        (b( 14) and (a( 35))) xor
        (b( 15) and (a( 34))) xor
        (b( 16) and (a( 33))) xor
        (b( 17) and (a( 32))) xor
        (b( 18) and (a( 31))) xor
        (b( 19) and (a( 30))) xor
        (b( 20) and (a( 29))) xor
        (b( 21) and (a( 28))) xor
        (b( 22) and (a( 27))) xor
        (b( 23) and (a( 26))) xor
        (b( 24) and (a( 25))) xor
        (b( 25) and (a( 24))) xor
        (b( 26) and (a( 23))) xor
        (b( 27) and (a( 22))) xor
        (b( 28) and (a( 21))) xor
        (b( 29) and (a( 20))) xor
        (b( 30) and (a( 19))) xor
        (b( 31) and (a( 18))) xor
        (b( 32) and (a( 17))) xor
        (b( 33) and (a( 16))) xor
        (b( 34) and (a( 15))) xor
        (b( 35) and (a( 14))) xor
        (b( 36) and (a( 13))) xor
        (b( 37) and (a( 12))) xor
        (b( 38) and (a( 11))) xor
        (b( 39) and (a( 10))) xor
        (b( 40) and (a(  9))) xor
        (b( 41) and (a(  8))) xor
        (b( 42) and (a(  7))) xor
        (b( 43) and (a(  6) xor a(127))) xor
        (b( 44) and (a(  5) xor a(126))) xor
        (b( 45) and (a(  4) xor a(125))) xor
        (b( 46) and (a(  3) xor a(124))) xor
        (b( 47) and (a(  2) xor a(123))) xor
        (b( 48) and (a(  1) xor a(122) xor a(127))) xor
        (b( 49) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 50) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 51) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 52) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 53) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 54) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 55) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 56) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 57) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 58) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 59) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 60) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 61) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 62) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 63) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 64) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 65) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 66) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 67) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 68) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 69) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 70) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 71) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 72) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 73) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 74) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 75) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 76) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 77) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 78) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 79) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 80) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 81) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 82) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 83) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 84) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 85) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 86) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 87) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 88) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 89) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 90) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 91) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 92) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 93) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 94) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 95) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 96) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 97) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 98) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b( 99) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(100) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(101) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(102) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(103) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(104) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(105) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(106) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(107) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(108) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(109) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(110) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(111) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(112) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(113) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(114) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(115) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(116) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(117) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(118) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(119) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(120) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(121) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(122) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(123) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(124) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(125) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(126) and (a( 44) xor a( 49) xor a( 50) xor a( 51))) xor
        (b(127) and (a( 43) xor a( 48) xor a( 49) xor a( 50)));
    c( 50) <= 
        (b(  0) and (a( 50))) xor
        (b(  1) and (a( 49))) xor
        (b(  2) and (a( 48))) xor
        (b(  3) and (a( 47))) xor
        (b(  4) and (a( 46))) xor
        (b(  5) and (a( 45))) xor
        (b(  6) and (a( 44))) xor
        (b(  7) and (a( 43))) xor
        (b(  8) and (a( 42))) xor
        (b(  9) and (a( 41))) xor
        (b( 10) and (a( 40))) xor
        (b( 11) and (a( 39))) xor
        (b( 12) and (a( 38))) xor
        (b( 13) and (a( 37))) xor
        (b( 14) and (a( 36))) xor
        (b( 15) and (a( 35))) xor
        (b( 16) and (a( 34))) xor
        (b( 17) and (a( 33))) xor
        (b( 18) and (a( 32))) xor
        (b( 19) and (a( 31))) xor
        (b( 20) and (a( 30))) xor
        (b( 21) and (a( 29))) xor
        (b( 22) and (a( 28))) xor
        (b( 23) and (a( 27))) xor
        (b( 24) and (a( 26))) xor
        (b( 25) and (a( 25))) xor
        (b( 26) and (a( 24))) xor
        (b( 27) and (a( 23))) xor
        (b( 28) and (a( 22))) xor
        (b( 29) and (a( 21))) xor
        (b( 30) and (a( 20))) xor
        (b( 31) and (a( 19))) xor
        (b( 32) and (a( 18))) xor
        (b( 33) and (a( 17))) xor
        (b( 34) and (a( 16))) xor
        (b( 35) and (a( 15))) xor
        (b( 36) and (a( 14))) xor
        (b( 37) and (a( 13))) xor
        (b( 38) and (a( 12))) xor
        (b( 39) and (a( 11))) xor
        (b( 40) and (a( 10))) xor
        (b( 41) and (a(  9))) xor
        (b( 42) and (a(  8))) xor
        (b( 43) and (a(  7))) xor
        (b( 44) and (a(  6) xor a(127))) xor
        (b( 45) and (a(  5) xor a(126))) xor
        (b( 46) and (a(  4) xor a(125))) xor
        (b( 47) and (a(  3) xor a(124))) xor
        (b( 48) and (a(  2) xor a(123))) xor
        (b( 49) and (a(  1) xor a(122) xor a(127))) xor
        (b( 50) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 51) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 52) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 53) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 54) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 55) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 56) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 57) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 58) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 59) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 60) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 61) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 62) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 63) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 64) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 65) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 66) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 67) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 68) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 69) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 70) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 71) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 72) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 73) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 74) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 75) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 76) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 77) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 78) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 79) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 80) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 81) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 82) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 83) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 84) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 85) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 86) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 87) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 88) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 89) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 90) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 91) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 92) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 93) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 94) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 95) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 96) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 97) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 98) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b( 99) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(100) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(101) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(102) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(103) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(104) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(105) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(106) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(107) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(108) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(109) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(110) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(111) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(112) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(113) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(114) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(115) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(116) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(117) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(118) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(119) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(120) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(121) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(122) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(123) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(124) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(125) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(126) and (a( 45) xor a( 50) xor a( 51) xor a( 52))) xor
        (b(127) and (a( 44) xor a( 49) xor a( 50) xor a( 51)));
    c( 51) <= 
        (b(  0) and (a( 51))) xor
        (b(  1) and (a( 50))) xor
        (b(  2) and (a( 49))) xor
        (b(  3) and (a( 48))) xor
        (b(  4) and (a( 47))) xor
        (b(  5) and (a( 46))) xor
        (b(  6) and (a( 45))) xor
        (b(  7) and (a( 44))) xor
        (b(  8) and (a( 43))) xor
        (b(  9) and (a( 42))) xor
        (b( 10) and (a( 41))) xor
        (b( 11) and (a( 40))) xor
        (b( 12) and (a( 39))) xor
        (b( 13) and (a( 38))) xor
        (b( 14) and (a( 37))) xor
        (b( 15) and (a( 36))) xor
        (b( 16) and (a( 35))) xor
        (b( 17) and (a( 34))) xor
        (b( 18) and (a( 33))) xor
        (b( 19) and (a( 32))) xor
        (b( 20) and (a( 31))) xor
        (b( 21) and (a( 30))) xor
        (b( 22) and (a( 29))) xor
        (b( 23) and (a( 28))) xor
        (b( 24) and (a( 27))) xor
        (b( 25) and (a( 26))) xor
        (b( 26) and (a( 25))) xor
        (b( 27) and (a( 24))) xor
        (b( 28) and (a( 23))) xor
        (b( 29) and (a( 22))) xor
        (b( 30) and (a( 21))) xor
        (b( 31) and (a( 20))) xor
        (b( 32) and (a( 19))) xor
        (b( 33) and (a( 18))) xor
        (b( 34) and (a( 17))) xor
        (b( 35) and (a( 16))) xor
        (b( 36) and (a( 15))) xor
        (b( 37) and (a( 14))) xor
        (b( 38) and (a( 13))) xor
        (b( 39) and (a( 12))) xor
        (b( 40) and (a( 11))) xor
        (b( 41) and (a( 10))) xor
        (b( 42) and (a(  9))) xor
        (b( 43) and (a(  8))) xor
        (b( 44) and (a(  7))) xor
        (b( 45) and (a(  6) xor a(127))) xor
        (b( 46) and (a(  5) xor a(126))) xor
        (b( 47) and (a(  4) xor a(125))) xor
        (b( 48) and (a(  3) xor a(124))) xor
        (b( 49) and (a(  2) xor a(123))) xor
        (b( 50) and (a(  1) xor a(122) xor a(127))) xor
        (b( 51) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 52) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 53) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 54) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 55) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 56) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 57) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 58) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 59) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 60) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 61) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 62) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 63) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 64) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 65) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 66) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 67) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 68) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 69) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 70) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 71) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 72) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 73) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 74) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 75) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 76) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 77) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 78) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 79) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 80) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 81) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 82) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 83) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 84) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 85) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 86) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 87) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 88) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 89) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 90) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 91) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 92) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 93) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 94) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 95) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 96) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 97) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 98) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b( 99) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(100) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(101) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(102) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(103) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(104) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(105) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(106) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(107) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(108) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(109) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(110) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(111) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(112) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(113) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(114) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(115) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(116) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(117) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(118) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(119) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(120) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(121) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(122) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(123) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(124) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(125) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(126) and (a( 46) xor a( 51) xor a( 52) xor a( 53))) xor
        (b(127) and (a( 45) xor a( 50) xor a( 51) xor a( 52)));
    c( 52) <= 
        (b(  0) and (a( 52))) xor
        (b(  1) and (a( 51))) xor
        (b(  2) and (a( 50))) xor
        (b(  3) and (a( 49))) xor
        (b(  4) and (a( 48))) xor
        (b(  5) and (a( 47))) xor
        (b(  6) and (a( 46))) xor
        (b(  7) and (a( 45))) xor
        (b(  8) and (a( 44))) xor
        (b(  9) and (a( 43))) xor
        (b( 10) and (a( 42))) xor
        (b( 11) and (a( 41))) xor
        (b( 12) and (a( 40))) xor
        (b( 13) and (a( 39))) xor
        (b( 14) and (a( 38))) xor
        (b( 15) and (a( 37))) xor
        (b( 16) and (a( 36))) xor
        (b( 17) and (a( 35))) xor
        (b( 18) and (a( 34))) xor
        (b( 19) and (a( 33))) xor
        (b( 20) and (a( 32))) xor
        (b( 21) and (a( 31))) xor
        (b( 22) and (a( 30))) xor
        (b( 23) and (a( 29))) xor
        (b( 24) and (a( 28))) xor
        (b( 25) and (a( 27))) xor
        (b( 26) and (a( 26))) xor
        (b( 27) and (a( 25))) xor
        (b( 28) and (a( 24))) xor
        (b( 29) and (a( 23))) xor
        (b( 30) and (a( 22))) xor
        (b( 31) and (a( 21))) xor
        (b( 32) and (a( 20))) xor
        (b( 33) and (a( 19))) xor
        (b( 34) and (a( 18))) xor
        (b( 35) and (a( 17))) xor
        (b( 36) and (a( 16))) xor
        (b( 37) and (a( 15))) xor
        (b( 38) and (a( 14))) xor
        (b( 39) and (a( 13))) xor
        (b( 40) and (a( 12))) xor
        (b( 41) and (a( 11))) xor
        (b( 42) and (a( 10))) xor
        (b( 43) and (a(  9))) xor
        (b( 44) and (a(  8))) xor
        (b( 45) and (a(  7))) xor
        (b( 46) and (a(  6) xor a(127))) xor
        (b( 47) and (a(  5) xor a(126))) xor
        (b( 48) and (a(  4) xor a(125))) xor
        (b( 49) and (a(  3) xor a(124))) xor
        (b( 50) and (a(  2) xor a(123))) xor
        (b( 51) and (a(  1) xor a(122) xor a(127))) xor
        (b( 52) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 53) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 54) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 55) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 56) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 57) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 58) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 59) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 60) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 61) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 62) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 63) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 64) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 65) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 66) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 67) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 68) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 69) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 70) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 71) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 72) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 73) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 74) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 75) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 76) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 77) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 78) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 79) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 80) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 81) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 82) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 83) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 84) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 85) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 86) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 87) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 88) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 89) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 90) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 91) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 92) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 93) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 94) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 95) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 96) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 97) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 98) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b( 99) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(100) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(101) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(102) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(103) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(104) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(105) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(106) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(107) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(108) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(109) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(110) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(111) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(112) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(113) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(114) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(115) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(116) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(117) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(118) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(119) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(120) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(121) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(122) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(123) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(124) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(125) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(126) and (a( 47) xor a( 52) xor a( 53) xor a( 54))) xor
        (b(127) and (a( 46) xor a( 51) xor a( 52) xor a( 53)));
    c( 53) <= 
        (b(  0) and (a( 53))) xor
        (b(  1) and (a( 52))) xor
        (b(  2) and (a( 51))) xor
        (b(  3) and (a( 50))) xor
        (b(  4) and (a( 49))) xor
        (b(  5) and (a( 48))) xor
        (b(  6) and (a( 47))) xor
        (b(  7) and (a( 46))) xor
        (b(  8) and (a( 45))) xor
        (b(  9) and (a( 44))) xor
        (b( 10) and (a( 43))) xor
        (b( 11) and (a( 42))) xor
        (b( 12) and (a( 41))) xor
        (b( 13) and (a( 40))) xor
        (b( 14) and (a( 39))) xor
        (b( 15) and (a( 38))) xor
        (b( 16) and (a( 37))) xor
        (b( 17) and (a( 36))) xor
        (b( 18) and (a( 35))) xor
        (b( 19) and (a( 34))) xor
        (b( 20) and (a( 33))) xor
        (b( 21) and (a( 32))) xor
        (b( 22) and (a( 31))) xor
        (b( 23) and (a( 30))) xor
        (b( 24) and (a( 29))) xor
        (b( 25) and (a( 28))) xor
        (b( 26) and (a( 27))) xor
        (b( 27) and (a( 26))) xor
        (b( 28) and (a( 25))) xor
        (b( 29) and (a( 24))) xor
        (b( 30) and (a( 23))) xor
        (b( 31) and (a( 22))) xor
        (b( 32) and (a( 21))) xor
        (b( 33) and (a( 20))) xor
        (b( 34) and (a( 19))) xor
        (b( 35) and (a( 18))) xor
        (b( 36) and (a( 17))) xor
        (b( 37) and (a( 16))) xor
        (b( 38) and (a( 15))) xor
        (b( 39) and (a( 14))) xor
        (b( 40) and (a( 13))) xor
        (b( 41) and (a( 12))) xor
        (b( 42) and (a( 11))) xor
        (b( 43) and (a( 10))) xor
        (b( 44) and (a(  9))) xor
        (b( 45) and (a(  8))) xor
        (b( 46) and (a(  7))) xor
        (b( 47) and (a(  6) xor a(127))) xor
        (b( 48) and (a(  5) xor a(126))) xor
        (b( 49) and (a(  4) xor a(125))) xor
        (b( 50) and (a(  3) xor a(124))) xor
        (b( 51) and (a(  2) xor a(123))) xor
        (b( 52) and (a(  1) xor a(122) xor a(127))) xor
        (b( 53) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 54) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 55) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 56) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 57) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 58) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 59) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 60) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 61) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 62) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 63) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 64) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 65) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 66) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 67) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 68) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 69) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 70) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 71) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 72) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 73) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 74) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 75) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 76) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 77) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 78) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 79) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 80) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 81) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 82) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 83) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 84) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 85) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 86) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 87) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 88) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 89) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 90) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 91) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 92) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 93) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 94) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 95) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 96) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 97) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 98) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b( 99) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(100) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(101) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(102) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(103) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(104) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(105) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(106) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(107) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(108) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(109) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(110) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(111) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(112) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(113) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(114) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(115) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(116) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(117) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(118) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(119) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(120) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(121) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(122) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(123) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(124) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(125) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(126) and (a( 48) xor a( 53) xor a( 54) xor a( 55))) xor
        (b(127) and (a( 47) xor a( 52) xor a( 53) xor a( 54)));
    c( 54) <= 
        (b(  0) and (a( 54))) xor
        (b(  1) and (a( 53))) xor
        (b(  2) and (a( 52))) xor
        (b(  3) and (a( 51))) xor
        (b(  4) and (a( 50))) xor
        (b(  5) and (a( 49))) xor
        (b(  6) and (a( 48))) xor
        (b(  7) and (a( 47))) xor
        (b(  8) and (a( 46))) xor
        (b(  9) and (a( 45))) xor
        (b( 10) and (a( 44))) xor
        (b( 11) and (a( 43))) xor
        (b( 12) and (a( 42))) xor
        (b( 13) and (a( 41))) xor
        (b( 14) and (a( 40))) xor
        (b( 15) and (a( 39))) xor
        (b( 16) and (a( 38))) xor
        (b( 17) and (a( 37))) xor
        (b( 18) and (a( 36))) xor
        (b( 19) and (a( 35))) xor
        (b( 20) and (a( 34))) xor
        (b( 21) and (a( 33))) xor
        (b( 22) and (a( 32))) xor
        (b( 23) and (a( 31))) xor
        (b( 24) and (a( 30))) xor
        (b( 25) and (a( 29))) xor
        (b( 26) and (a( 28))) xor
        (b( 27) and (a( 27))) xor
        (b( 28) and (a( 26))) xor
        (b( 29) and (a( 25))) xor
        (b( 30) and (a( 24))) xor
        (b( 31) and (a( 23))) xor
        (b( 32) and (a( 22))) xor
        (b( 33) and (a( 21))) xor
        (b( 34) and (a( 20))) xor
        (b( 35) and (a( 19))) xor
        (b( 36) and (a( 18))) xor
        (b( 37) and (a( 17))) xor
        (b( 38) and (a( 16))) xor
        (b( 39) and (a( 15))) xor
        (b( 40) and (a( 14))) xor
        (b( 41) and (a( 13))) xor
        (b( 42) and (a( 12))) xor
        (b( 43) and (a( 11))) xor
        (b( 44) and (a( 10))) xor
        (b( 45) and (a(  9))) xor
        (b( 46) and (a(  8))) xor
        (b( 47) and (a(  7))) xor
        (b( 48) and (a(  6) xor a(127))) xor
        (b( 49) and (a(  5) xor a(126))) xor
        (b( 50) and (a(  4) xor a(125))) xor
        (b( 51) and (a(  3) xor a(124))) xor
        (b( 52) and (a(  2) xor a(123))) xor
        (b( 53) and (a(  1) xor a(122) xor a(127))) xor
        (b( 54) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 55) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 56) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 57) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 58) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 59) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 60) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 61) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 62) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 63) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 64) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 65) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 66) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 67) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 68) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 69) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 70) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 71) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 72) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 73) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 74) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 75) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 76) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 77) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 78) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 79) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 80) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 81) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 82) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 83) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 84) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 85) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 86) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 87) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 88) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 89) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 90) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 91) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 92) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 93) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 94) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 95) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 96) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 97) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 98) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b( 99) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(100) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(101) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(102) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(103) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(104) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(105) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(106) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(107) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(108) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(109) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(110) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(111) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(112) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(113) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(114) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(115) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(116) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(117) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(118) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(119) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(120) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(121) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(122) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(123) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(124) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(125) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(126) and (a( 49) xor a( 54) xor a( 55) xor a( 56))) xor
        (b(127) and (a( 48) xor a( 53) xor a( 54) xor a( 55)));
    c( 55) <= 
        (b(  0) and (a( 55))) xor
        (b(  1) and (a( 54))) xor
        (b(  2) and (a( 53))) xor
        (b(  3) and (a( 52))) xor
        (b(  4) and (a( 51))) xor
        (b(  5) and (a( 50))) xor
        (b(  6) and (a( 49))) xor
        (b(  7) and (a( 48))) xor
        (b(  8) and (a( 47))) xor
        (b(  9) and (a( 46))) xor
        (b( 10) and (a( 45))) xor
        (b( 11) and (a( 44))) xor
        (b( 12) and (a( 43))) xor
        (b( 13) and (a( 42))) xor
        (b( 14) and (a( 41))) xor
        (b( 15) and (a( 40))) xor
        (b( 16) and (a( 39))) xor
        (b( 17) and (a( 38))) xor
        (b( 18) and (a( 37))) xor
        (b( 19) and (a( 36))) xor
        (b( 20) and (a( 35))) xor
        (b( 21) and (a( 34))) xor
        (b( 22) and (a( 33))) xor
        (b( 23) and (a( 32))) xor
        (b( 24) and (a( 31))) xor
        (b( 25) and (a( 30))) xor
        (b( 26) and (a( 29))) xor
        (b( 27) and (a( 28))) xor
        (b( 28) and (a( 27))) xor
        (b( 29) and (a( 26))) xor
        (b( 30) and (a( 25))) xor
        (b( 31) and (a( 24))) xor
        (b( 32) and (a( 23))) xor
        (b( 33) and (a( 22))) xor
        (b( 34) and (a( 21))) xor
        (b( 35) and (a( 20))) xor
        (b( 36) and (a( 19))) xor
        (b( 37) and (a( 18))) xor
        (b( 38) and (a( 17))) xor
        (b( 39) and (a( 16))) xor
        (b( 40) and (a( 15))) xor
        (b( 41) and (a( 14))) xor
        (b( 42) and (a( 13))) xor
        (b( 43) and (a( 12))) xor
        (b( 44) and (a( 11))) xor
        (b( 45) and (a( 10))) xor
        (b( 46) and (a(  9))) xor
        (b( 47) and (a(  8))) xor
        (b( 48) and (a(  7))) xor
        (b( 49) and (a(  6) xor a(127))) xor
        (b( 50) and (a(  5) xor a(126))) xor
        (b( 51) and (a(  4) xor a(125))) xor
        (b( 52) and (a(  3) xor a(124))) xor
        (b( 53) and (a(  2) xor a(123))) xor
        (b( 54) and (a(  1) xor a(122) xor a(127))) xor
        (b( 55) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 56) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 57) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 58) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 59) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 60) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 61) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 62) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 63) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 64) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 65) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 66) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 67) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 68) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 69) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 70) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 71) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 72) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 73) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 74) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 75) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 76) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 77) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 78) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 79) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 80) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 81) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 82) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 83) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 84) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 85) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 86) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 87) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 88) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 89) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 90) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 91) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 92) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 93) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 94) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 95) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 96) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 97) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 98) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b( 99) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(100) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(101) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(102) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(103) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(104) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(105) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(106) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(107) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(108) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(109) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(110) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(111) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(112) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(113) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(114) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(115) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(116) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(117) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(118) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(119) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(120) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(121) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(122) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(123) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(124) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(125) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(126) and (a( 50) xor a( 55) xor a( 56) xor a( 57))) xor
        (b(127) and (a( 49) xor a( 54) xor a( 55) xor a( 56)));
    c( 56) <= 
        (b(  0) and (a( 56))) xor
        (b(  1) and (a( 55))) xor
        (b(  2) and (a( 54))) xor
        (b(  3) and (a( 53))) xor
        (b(  4) and (a( 52))) xor
        (b(  5) and (a( 51))) xor
        (b(  6) and (a( 50))) xor
        (b(  7) and (a( 49))) xor
        (b(  8) and (a( 48))) xor
        (b(  9) and (a( 47))) xor
        (b( 10) and (a( 46))) xor
        (b( 11) and (a( 45))) xor
        (b( 12) and (a( 44))) xor
        (b( 13) and (a( 43))) xor
        (b( 14) and (a( 42))) xor
        (b( 15) and (a( 41))) xor
        (b( 16) and (a( 40))) xor
        (b( 17) and (a( 39))) xor
        (b( 18) and (a( 38))) xor
        (b( 19) and (a( 37))) xor
        (b( 20) and (a( 36))) xor
        (b( 21) and (a( 35))) xor
        (b( 22) and (a( 34))) xor
        (b( 23) and (a( 33))) xor
        (b( 24) and (a( 32))) xor
        (b( 25) and (a( 31))) xor
        (b( 26) and (a( 30))) xor
        (b( 27) and (a( 29))) xor
        (b( 28) and (a( 28))) xor
        (b( 29) and (a( 27))) xor
        (b( 30) and (a( 26))) xor
        (b( 31) and (a( 25))) xor
        (b( 32) and (a( 24))) xor
        (b( 33) and (a( 23))) xor
        (b( 34) and (a( 22))) xor
        (b( 35) and (a( 21))) xor
        (b( 36) and (a( 20))) xor
        (b( 37) and (a( 19))) xor
        (b( 38) and (a( 18))) xor
        (b( 39) and (a( 17))) xor
        (b( 40) and (a( 16))) xor
        (b( 41) and (a( 15))) xor
        (b( 42) and (a( 14))) xor
        (b( 43) and (a( 13))) xor
        (b( 44) and (a( 12))) xor
        (b( 45) and (a( 11))) xor
        (b( 46) and (a( 10))) xor
        (b( 47) and (a(  9))) xor
        (b( 48) and (a(  8))) xor
        (b( 49) and (a(  7))) xor
        (b( 50) and (a(  6) xor a(127))) xor
        (b( 51) and (a(  5) xor a(126))) xor
        (b( 52) and (a(  4) xor a(125))) xor
        (b( 53) and (a(  3) xor a(124))) xor
        (b( 54) and (a(  2) xor a(123))) xor
        (b( 55) and (a(  1) xor a(122) xor a(127))) xor
        (b( 56) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 57) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 58) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 59) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 60) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 61) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 62) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 63) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 64) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 65) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 66) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 67) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 68) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 69) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 70) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 71) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 72) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 73) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 74) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 75) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 76) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 77) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 78) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 79) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 80) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 81) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 82) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 83) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 84) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 85) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 86) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 87) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 88) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 89) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 90) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 91) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 92) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 93) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 94) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 95) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 96) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 97) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 98) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b( 99) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(100) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(101) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(102) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(103) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(104) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(105) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(106) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(107) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(108) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(109) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(110) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(111) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(112) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(113) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(114) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(115) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(116) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(117) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(118) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(119) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(120) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(121) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(122) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(123) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(124) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(125) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(126) and (a( 51) xor a( 56) xor a( 57) xor a( 58))) xor
        (b(127) and (a( 50) xor a( 55) xor a( 56) xor a( 57)));
    c( 57) <= 
        (b(  0) and (a( 57))) xor
        (b(  1) and (a( 56))) xor
        (b(  2) and (a( 55))) xor
        (b(  3) and (a( 54))) xor
        (b(  4) and (a( 53))) xor
        (b(  5) and (a( 52))) xor
        (b(  6) and (a( 51))) xor
        (b(  7) and (a( 50))) xor
        (b(  8) and (a( 49))) xor
        (b(  9) and (a( 48))) xor
        (b( 10) and (a( 47))) xor
        (b( 11) and (a( 46))) xor
        (b( 12) and (a( 45))) xor
        (b( 13) and (a( 44))) xor
        (b( 14) and (a( 43))) xor
        (b( 15) and (a( 42))) xor
        (b( 16) and (a( 41))) xor
        (b( 17) and (a( 40))) xor
        (b( 18) and (a( 39))) xor
        (b( 19) and (a( 38))) xor
        (b( 20) and (a( 37))) xor
        (b( 21) and (a( 36))) xor
        (b( 22) and (a( 35))) xor
        (b( 23) and (a( 34))) xor
        (b( 24) and (a( 33))) xor
        (b( 25) and (a( 32))) xor
        (b( 26) and (a( 31))) xor
        (b( 27) and (a( 30))) xor
        (b( 28) and (a( 29))) xor
        (b( 29) and (a( 28))) xor
        (b( 30) and (a( 27))) xor
        (b( 31) and (a( 26))) xor
        (b( 32) and (a( 25))) xor
        (b( 33) and (a( 24))) xor
        (b( 34) and (a( 23))) xor
        (b( 35) and (a( 22))) xor
        (b( 36) and (a( 21))) xor
        (b( 37) and (a( 20))) xor
        (b( 38) and (a( 19))) xor
        (b( 39) and (a( 18))) xor
        (b( 40) and (a( 17))) xor
        (b( 41) and (a( 16))) xor
        (b( 42) and (a( 15))) xor
        (b( 43) and (a( 14))) xor
        (b( 44) and (a( 13))) xor
        (b( 45) and (a( 12))) xor
        (b( 46) and (a( 11))) xor
        (b( 47) and (a( 10))) xor
        (b( 48) and (a(  9))) xor
        (b( 49) and (a(  8))) xor
        (b( 50) and (a(  7))) xor
        (b( 51) and (a(  6) xor a(127))) xor
        (b( 52) and (a(  5) xor a(126))) xor
        (b( 53) and (a(  4) xor a(125))) xor
        (b( 54) and (a(  3) xor a(124))) xor
        (b( 55) and (a(  2) xor a(123))) xor
        (b( 56) and (a(  1) xor a(122) xor a(127))) xor
        (b( 57) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 58) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 59) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 60) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 61) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 62) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 63) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 64) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 65) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 66) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 67) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 68) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 69) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 70) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 71) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 72) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 73) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 74) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 75) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 76) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 77) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 78) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 79) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 80) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 81) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 82) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 83) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 84) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 85) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 86) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 87) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 88) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 89) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 90) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 91) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 92) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 93) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 94) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 95) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 96) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 97) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 98) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b( 99) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(100) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(101) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(102) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(103) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(104) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(105) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(106) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(107) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(108) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(109) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(110) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(111) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(112) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(113) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(114) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(115) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(116) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(117) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(118) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(119) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(120) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(121) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(122) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(123) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(124) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(125) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(126) and (a( 52) xor a( 57) xor a( 58) xor a( 59))) xor
        (b(127) and (a( 51) xor a( 56) xor a( 57) xor a( 58)));
    c( 58) <= 
        (b(  0) and (a( 58))) xor
        (b(  1) and (a( 57))) xor
        (b(  2) and (a( 56))) xor
        (b(  3) and (a( 55))) xor
        (b(  4) and (a( 54))) xor
        (b(  5) and (a( 53))) xor
        (b(  6) and (a( 52))) xor
        (b(  7) and (a( 51))) xor
        (b(  8) and (a( 50))) xor
        (b(  9) and (a( 49))) xor
        (b( 10) and (a( 48))) xor
        (b( 11) and (a( 47))) xor
        (b( 12) and (a( 46))) xor
        (b( 13) and (a( 45))) xor
        (b( 14) and (a( 44))) xor
        (b( 15) and (a( 43))) xor
        (b( 16) and (a( 42))) xor
        (b( 17) and (a( 41))) xor
        (b( 18) and (a( 40))) xor
        (b( 19) and (a( 39))) xor
        (b( 20) and (a( 38))) xor
        (b( 21) and (a( 37))) xor
        (b( 22) and (a( 36))) xor
        (b( 23) and (a( 35))) xor
        (b( 24) and (a( 34))) xor
        (b( 25) and (a( 33))) xor
        (b( 26) and (a( 32))) xor
        (b( 27) and (a( 31))) xor
        (b( 28) and (a( 30))) xor
        (b( 29) and (a( 29))) xor
        (b( 30) and (a( 28))) xor
        (b( 31) and (a( 27))) xor
        (b( 32) and (a( 26))) xor
        (b( 33) and (a( 25))) xor
        (b( 34) and (a( 24))) xor
        (b( 35) and (a( 23))) xor
        (b( 36) and (a( 22))) xor
        (b( 37) and (a( 21))) xor
        (b( 38) and (a( 20))) xor
        (b( 39) and (a( 19))) xor
        (b( 40) and (a( 18))) xor
        (b( 41) and (a( 17))) xor
        (b( 42) and (a( 16))) xor
        (b( 43) and (a( 15))) xor
        (b( 44) and (a( 14))) xor
        (b( 45) and (a( 13))) xor
        (b( 46) and (a( 12))) xor
        (b( 47) and (a( 11))) xor
        (b( 48) and (a( 10))) xor
        (b( 49) and (a(  9))) xor
        (b( 50) and (a(  8))) xor
        (b( 51) and (a(  7))) xor
        (b( 52) and (a(  6) xor a(127))) xor
        (b( 53) and (a(  5) xor a(126))) xor
        (b( 54) and (a(  4) xor a(125))) xor
        (b( 55) and (a(  3) xor a(124))) xor
        (b( 56) and (a(  2) xor a(123))) xor
        (b( 57) and (a(  1) xor a(122) xor a(127))) xor
        (b( 58) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 59) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 60) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 61) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 62) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 63) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 64) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 65) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 66) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 67) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 68) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 69) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 70) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 71) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 72) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 73) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 74) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 75) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 76) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 77) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 78) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 79) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 80) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 81) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 82) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 83) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 84) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 85) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 86) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 87) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 88) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 89) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 90) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 91) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 92) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 93) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 94) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 95) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 96) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 97) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 98) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b( 99) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(100) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(101) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(102) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(103) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(104) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(105) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(106) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(107) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(108) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(109) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(110) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(111) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(112) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(113) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(114) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(115) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(116) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(117) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(118) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(119) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(120) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(121) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(122) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(123) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(124) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(125) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(126) and (a( 53) xor a( 58) xor a( 59) xor a( 60))) xor
        (b(127) and (a( 52) xor a( 57) xor a( 58) xor a( 59)));
    c( 59) <= 
        (b(  0) and (a( 59))) xor
        (b(  1) and (a( 58))) xor
        (b(  2) and (a( 57))) xor
        (b(  3) and (a( 56))) xor
        (b(  4) and (a( 55))) xor
        (b(  5) and (a( 54))) xor
        (b(  6) and (a( 53))) xor
        (b(  7) and (a( 52))) xor
        (b(  8) and (a( 51))) xor
        (b(  9) and (a( 50))) xor
        (b( 10) and (a( 49))) xor
        (b( 11) and (a( 48))) xor
        (b( 12) and (a( 47))) xor
        (b( 13) and (a( 46))) xor
        (b( 14) and (a( 45))) xor
        (b( 15) and (a( 44))) xor
        (b( 16) and (a( 43))) xor
        (b( 17) and (a( 42))) xor
        (b( 18) and (a( 41))) xor
        (b( 19) and (a( 40))) xor
        (b( 20) and (a( 39))) xor
        (b( 21) and (a( 38))) xor
        (b( 22) and (a( 37))) xor
        (b( 23) and (a( 36))) xor
        (b( 24) and (a( 35))) xor
        (b( 25) and (a( 34))) xor
        (b( 26) and (a( 33))) xor
        (b( 27) and (a( 32))) xor
        (b( 28) and (a( 31))) xor
        (b( 29) and (a( 30))) xor
        (b( 30) and (a( 29))) xor
        (b( 31) and (a( 28))) xor
        (b( 32) and (a( 27))) xor
        (b( 33) and (a( 26))) xor
        (b( 34) and (a( 25))) xor
        (b( 35) and (a( 24))) xor
        (b( 36) and (a( 23))) xor
        (b( 37) and (a( 22))) xor
        (b( 38) and (a( 21))) xor
        (b( 39) and (a( 20))) xor
        (b( 40) and (a( 19))) xor
        (b( 41) and (a( 18))) xor
        (b( 42) and (a( 17))) xor
        (b( 43) and (a( 16))) xor
        (b( 44) and (a( 15))) xor
        (b( 45) and (a( 14))) xor
        (b( 46) and (a( 13))) xor
        (b( 47) and (a( 12))) xor
        (b( 48) and (a( 11))) xor
        (b( 49) and (a( 10))) xor
        (b( 50) and (a(  9))) xor
        (b( 51) and (a(  8))) xor
        (b( 52) and (a(  7))) xor
        (b( 53) and (a(  6) xor a(127))) xor
        (b( 54) and (a(  5) xor a(126))) xor
        (b( 55) and (a(  4) xor a(125))) xor
        (b( 56) and (a(  3) xor a(124))) xor
        (b( 57) and (a(  2) xor a(123))) xor
        (b( 58) and (a(  1) xor a(122) xor a(127))) xor
        (b( 59) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 60) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 61) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 62) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 63) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 64) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 65) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 66) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 67) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 68) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 69) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 70) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 71) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 72) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 73) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 74) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 75) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 76) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 77) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 78) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 79) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 80) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 81) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 82) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 83) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 84) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 85) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 86) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 87) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 88) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 89) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 90) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 91) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 92) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 93) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 94) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 95) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 96) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 97) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 98) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b( 99) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(100) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(101) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(102) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(103) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(104) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(105) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(106) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(107) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(108) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(109) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(110) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(111) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(112) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(113) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(114) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(115) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(116) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(117) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(118) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(119) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(120) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(121) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(122) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(123) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(124) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(125) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(126) and (a( 54) xor a( 59) xor a( 60) xor a( 61))) xor
        (b(127) and (a( 53) xor a( 58) xor a( 59) xor a( 60)));
    c( 60) <= 
        (b(  0) and (a( 60))) xor
        (b(  1) and (a( 59))) xor
        (b(  2) and (a( 58))) xor
        (b(  3) and (a( 57))) xor
        (b(  4) and (a( 56))) xor
        (b(  5) and (a( 55))) xor
        (b(  6) and (a( 54))) xor
        (b(  7) and (a( 53))) xor
        (b(  8) and (a( 52))) xor
        (b(  9) and (a( 51))) xor
        (b( 10) and (a( 50))) xor
        (b( 11) and (a( 49))) xor
        (b( 12) and (a( 48))) xor
        (b( 13) and (a( 47))) xor
        (b( 14) and (a( 46))) xor
        (b( 15) and (a( 45))) xor
        (b( 16) and (a( 44))) xor
        (b( 17) and (a( 43))) xor
        (b( 18) and (a( 42))) xor
        (b( 19) and (a( 41))) xor
        (b( 20) and (a( 40))) xor
        (b( 21) and (a( 39))) xor
        (b( 22) and (a( 38))) xor
        (b( 23) and (a( 37))) xor
        (b( 24) and (a( 36))) xor
        (b( 25) and (a( 35))) xor
        (b( 26) and (a( 34))) xor
        (b( 27) and (a( 33))) xor
        (b( 28) and (a( 32))) xor
        (b( 29) and (a( 31))) xor
        (b( 30) and (a( 30))) xor
        (b( 31) and (a( 29))) xor
        (b( 32) and (a( 28))) xor
        (b( 33) and (a( 27))) xor
        (b( 34) and (a( 26))) xor
        (b( 35) and (a( 25))) xor
        (b( 36) and (a( 24))) xor
        (b( 37) and (a( 23))) xor
        (b( 38) and (a( 22))) xor
        (b( 39) and (a( 21))) xor
        (b( 40) and (a( 20))) xor
        (b( 41) and (a( 19))) xor
        (b( 42) and (a( 18))) xor
        (b( 43) and (a( 17))) xor
        (b( 44) and (a( 16))) xor
        (b( 45) and (a( 15))) xor
        (b( 46) and (a( 14))) xor
        (b( 47) and (a( 13))) xor
        (b( 48) and (a( 12))) xor
        (b( 49) and (a( 11))) xor
        (b( 50) and (a( 10))) xor
        (b( 51) and (a(  9))) xor
        (b( 52) and (a(  8))) xor
        (b( 53) and (a(  7))) xor
        (b( 54) and (a(  6) xor a(127))) xor
        (b( 55) and (a(  5) xor a(126))) xor
        (b( 56) and (a(  4) xor a(125))) xor
        (b( 57) and (a(  3) xor a(124))) xor
        (b( 58) and (a(  2) xor a(123))) xor
        (b( 59) and (a(  1) xor a(122) xor a(127))) xor
        (b( 60) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 61) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 62) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 63) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 64) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 65) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 66) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 67) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 68) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 69) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 70) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 71) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 72) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 73) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 74) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 75) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 76) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 77) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 78) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 79) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 80) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 81) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 82) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 83) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 84) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 85) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 86) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 87) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 88) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 89) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 90) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 91) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 92) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 93) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 94) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 95) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 96) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 97) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 98) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b( 99) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(100) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(101) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(102) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(103) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(104) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(105) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(106) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(107) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(108) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(109) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(110) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(111) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(112) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(113) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(114) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(115) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(116) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(117) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(118) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(119) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(120) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(121) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(122) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(123) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(124) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(125) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(126) and (a( 55) xor a( 60) xor a( 61) xor a( 62))) xor
        (b(127) and (a( 54) xor a( 59) xor a( 60) xor a( 61)));
    c( 61) <= 
        (b(  0) and (a( 61))) xor
        (b(  1) and (a( 60))) xor
        (b(  2) and (a( 59))) xor
        (b(  3) and (a( 58))) xor
        (b(  4) and (a( 57))) xor
        (b(  5) and (a( 56))) xor
        (b(  6) and (a( 55))) xor
        (b(  7) and (a( 54))) xor
        (b(  8) and (a( 53))) xor
        (b(  9) and (a( 52))) xor
        (b( 10) and (a( 51))) xor
        (b( 11) and (a( 50))) xor
        (b( 12) and (a( 49))) xor
        (b( 13) and (a( 48))) xor
        (b( 14) and (a( 47))) xor
        (b( 15) and (a( 46))) xor
        (b( 16) and (a( 45))) xor
        (b( 17) and (a( 44))) xor
        (b( 18) and (a( 43))) xor
        (b( 19) and (a( 42))) xor
        (b( 20) and (a( 41))) xor
        (b( 21) and (a( 40))) xor
        (b( 22) and (a( 39))) xor
        (b( 23) and (a( 38))) xor
        (b( 24) and (a( 37))) xor
        (b( 25) and (a( 36))) xor
        (b( 26) and (a( 35))) xor
        (b( 27) and (a( 34))) xor
        (b( 28) and (a( 33))) xor
        (b( 29) and (a( 32))) xor
        (b( 30) and (a( 31))) xor
        (b( 31) and (a( 30))) xor
        (b( 32) and (a( 29))) xor
        (b( 33) and (a( 28))) xor
        (b( 34) and (a( 27))) xor
        (b( 35) and (a( 26))) xor
        (b( 36) and (a( 25))) xor
        (b( 37) and (a( 24))) xor
        (b( 38) and (a( 23))) xor
        (b( 39) and (a( 22))) xor
        (b( 40) and (a( 21))) xor
        (b( 41) and (a( 20))) xor
        (b( 42) and (a( 19))) xor
        (b( 43) and (a( 18))) xor
        (b( 44) and (a( 17))) xor
        (b( 45) and (a( 16))) xor
        (b( 46) and (a( 15))) xor
        (b( 47) and (a( 14))) xor
        (b( 48) and (a( 13))) xor
        (b( 49) and (a( 12))) xor
        (b( 50) and (a( 11))) xor
        (b( 51) and (a( 10))) xor
        (b( 52) and (a(  9))) xor
        (b( 53) and (a(  8))) xor
        (b( 54) and (a(  7))) xor
        (b( 55) and (a(  6) xor a(127))) xor
        (b( 56) and (a(  5) xor a(126))) xor
        (b( 57) and (a(  4) xor a(125))) xor
        (b( 58) and (a(  3) xor a(124))) xor
        (b( 59) and (a(  2) xor a(123))) xor
        (b( 60) and (a(  1) xor a(122) xor a(127))) xor
        (b( 61) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 62) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 63) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 64) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 65) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 66) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 67) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 68) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 69) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 70) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 71) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 72) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 73) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 74) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 75) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 76) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 77) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 78) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 79) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 80) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 81) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 82) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 83) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 84) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 85) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 86) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 87) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 88) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 89) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 90) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 91) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 92) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 93) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 94) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 95) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 96) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 97) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 98) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b( 99) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(100) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(101) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(102) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(103) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(104) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(105) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(106) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(107) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(108) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(109) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(110) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(111) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(112) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(113) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(114) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(115) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(116) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(117) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(118) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(119) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(120) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(121) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(122) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(123) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(124) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(125) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(126) and (a( 56) xor a( 61) xor a( 62) xor a( 63))) xor
        (b(127) and (a( 55) xor a( 60) xor a( 61) xor a( 62)));
    c( 62) <= 
        (b(  0) and (a( 62))) xor
        (b(  1) and (a( 61))) xor
        (b(  2) and (a( 60))) xor
        (b(  3) and (a( 59))) xor
        (b(  4) and (a( 58))) xor
        (b(  5) and (a( 57))) xor
        (b(  6) and (a( 56))) xor
        (b(  7) and (a( 55))) xor
        (b(  8) and (a( 54))) xor
        (b(  9) and (a( 53))) xor
        (b( 10) and (a( 52))) xor
        (b( 11) and (a( 51))) xor
        (b( 12) and (a( 50))) xor
        (b( 13) and (a( 49))) xor
        (b( 14) and (a( 48))) xor
        (b( 15) and (a( 47))) xor
        (b( 16) and (a( 46))) xor
        (b( 17) and (a( 45))) xor
        (b( 18) and (a( 44))) xor
        (b( 19) and (a( 43))) xor
        (b( 20) and (a( 42))) xor
        (b( 21) and (a( 41))) xor
        (b( 22) and (a( 40))) xor
        (b( 23) and (a( 39))) xor
        (b( 24) and (a( 38))) xor
        (b( 25) and (a( 37))) xor
        (b( 26) and (a( 36))) xor
        (b( 27) and (a( 35))) xor
        (b( 28) and (a( 34))) xor
        (b( 29) and (a( 33))) xor
        (b( 30) and (a( 32))) xor
        (b( 31) and (a( 31))) xor
        (b( 32) and (a( 30))) xor
        (b( 33) and (a( 29))) xor
        (b( 34) and (a( 28))) xor
        (b( 35) and (a( 27))) xor
        (b( 36) and (a( 26))) xor
        (b( 37) and (a( 25))) xor
        (b( 38) and (a( 24))) xor
        (b( 39) and (a( 23))) xor
        (b( 40) and (a( 22))) xor
        (b( 41) and (a( 21))) xor
        (b( 42) and (a( 20))) xor
        (b( 43) and (a( 19))) xor
        (b( 44) and (a( 18))) xor
        (b( 45) and (a( 17))) xor
        (b( 46) and (a( 16))) xor
        (b( 47) and (a( 15))) xor
        (b( 48) and (a( 14))) xor
        (b( 49) and (a( 13))) xor
        (b( 50) and (a( 12))) xor
        (b( 51) and (a( 11))) xor
        (b( 52) and (a( 10))) xor
        (b( 53) and (a(  9))) xor
        (b( 54) and (a(  8))) xor
        (b( 55) and (a(  7))) xor
        (b( 56) and (a(  6) xor a(127))) xor
        (b( 57) and (a(  5) xor a(126))) xor
        (b( 58) and (a(  4) xor a(125))) xor
        (b( 59) and (a(  3) xor a(124))) xor
        (b( 60) and (a(  2) xor a(123))) xor
        (b( 61) and (a(  1) xor a(122) xor a(127))) xor
        (b( 62) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 63) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 64) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 65) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 66) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 67) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 68) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 69) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 70) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 71) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 72) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 73) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 74) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 75) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 76) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 77) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 78) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 79) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 80) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 81) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 82) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 83) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 84) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 85) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 86) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 87) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 88) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 89) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 90) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 91) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 92) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 93) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 94) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 95) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 96) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 97) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 98) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b( 99) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(100) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(101) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(102) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(103) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(104) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(105) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(106) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(107) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(108) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(109) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(110) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(111) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(112) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(113) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(114) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(115) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(116) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(117) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(118) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(119) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(120) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(121) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(122) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(123) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(124) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(125) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(126) and (a( 57) xor a( 62) xor a( 63) xor a( 64))) xor
        (b(127) and (a( 56) xor a( 61) xor a( 62) xor a( 63)));
    c( 63) <= 
        (b(  0) and (a( 63))) xor
        (b(  1) and (a( 62))) xor
        (b(  2) and (a( 61))) xor
        (b(  3) and (a( 60))) xor
        (b(  4) and (a( 59))) xor
        (b(  5) and (a( 58))) xor
        (b(  6) and (a( 57))) xor
        (b(  7) and (a( 56))) xor
        (b(  8) and (a( 55))) xor
        (b(  9) and (a( 54))) xor
        (b( 10) and (a( 53))) xor
        (b( 11) and (a( 52))) xor
        (b( 12) and (a( 51))) xor
        (b( 13) and (a( 50))) xor
        (b( 14) and (a( 49))) xor
        (b( 15) and (a( 48))) xor
        (b( 16) and (a( 47))) xor
        (b( 17) and (a( 46))) xor
        (b( 18) and (a( 45))) xor
        (b( 19) and (a( 44))) xor
        (b( 20) and (a( 43))) xor
        (b( 21) and (a( 42))) xor
        (b( 22) and (a( 41))) xor
        (b( 23) and (a( 40))) xor
        (b( 24) and (a( 39))) xor
        (b( 25) and (a( 38))) xor
        (b( 26) and (a( 37))) xor
        (b( 27) and (a( 36))) xor
        (b( 28) and (a( 35))) xor
        (b( 29) and (a( 34))) xor
        (b( 30) and (a( 33))) xor
        (b( 31) and (a( 32))) xor
        (b( 32) and (a( 31))) xor
        (b( 33) and (a( 30))) xor
        (b( 34) and (a( 29))) xor
        (b( 35) and (a( 28))) xor
        (b( 36) and (a( 27))) xor
        (b( 37) and (a( 26))) xor
        (b( 38) and (a( 25))) xor
        (b( 39) and (a( 24))) xor
        (b( 40) and (a( 23))) xor
        (b( 41) and (a( 22))) xor
        (b( 42) and (a( 21))) xor
        (b( 43) and (a( 20))) xor
        (b( 44) and (a( 19))) xor
        (b( 45) and (a( 18))) xor
        (b( 46) and (a( 17))) xor
        (b( 47) and (a( 16))) xor
        (b( 48) and (a( 15))) xor
        (b( 49) and (a( 14))) xor
        (b( 50) and (a( 13))) xor
        (b( 51) and (a( 12))) xor
        (b( 52) and (a( 11))) xor
        (b( 53) and (a( 10))) xor
        (b( 54) and (a(  9))) xor
        (b( 55) and (a(  8))) xor
        (b( 56) and (a(  7))) xor
        (b( 57) and (a(  6) xor a(127))) xor
        (b( 58) and (a(  5) xor a(126))) xor
        (b( 59) and (a(  4) xor a(125))) xor
        (b( 60) and (a(  3) xor a(124))) xor
        (b( 61) and (a(  2) xor a(123))) xor
        (b( 62) and (a(  1) xor a(122) xor a(127))) xor
        (b( 63) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 64) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 65) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 66) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 67) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 68) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 69) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 70) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 71) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 72) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 73) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 74) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 75) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 76) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 77) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 78) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 79) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 80) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 81) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 82) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 83) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 84) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 85) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 86) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 87) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 88) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 89) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 90) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 91) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 92) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 93) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 94) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 95) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 96) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 97) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 98) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b( 99) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(100) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(101) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(102) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(103) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(104) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(105) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(106) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(107) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(108) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(109) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(110) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(111) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(112) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(113) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(114) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(115) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(116) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(117) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(118) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(119) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(120) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(121) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(122) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(123) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(124) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(125) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(126) and (a( 58) xor a( 63) xor a( 64) xor a( 65))) xor
        (b(127) and (a( 57) xor a( 62) xor a( 63) xor a( 64)));
    c( 64) <= 
        (b(  0) and (a( 64))) xor
        (b(  1) and (a( 63))) xor
        (b(  2) and (a( 62))) xor
        (b(  3) and (a( 61))) xor
        (b(  4) and (a( 60))) xor
        (b(  5) and (a( 59))) xor
        (b(  6) and (a( 58))) xor
        (b(  7) and (a( 57))) xor
        (b(  8) and (a( 56))) xor
        (b(  9) and (a( 55))) xor
        (b( 10) and (a( 54))) xor
        (b( 11) and (a( 53))) xor
        (b( 12) and (a( 52))) xor
        (b( 13) and (a( 51))) xor
        (b( 14) and (a( 50))) xor
        (b( 15) and (a( 49))) xor
        (b( 16) and (a( 48))) xor
        (b( 17) and (a( 47))) xor
        (b( 18) and (a( 46))) xor
        (b( 19) and (a( 45))) xor
        (b( 20) and (a( 44))) xor
        (b( 21) and (a( 43))) xor
        (b( 22) and (a( 42))) xor
        (b( 23) and (a( 41))) xor
        (b( 24) and (a( 40))) xor
        (b( 25) and (a( 39))) xor
        (b( 26) and (a( 38))) xor
        (b( 27) and (a( 37))) xor
        (b( 28) and (a( 36))) xor
        (b( 29) and (a( 35))) xor
        (b( 30) and (a( 34))) xor
        (b( 31) and (a( 33))) xor
        (b( 32) and (a( 32))) xor
        (b( 33) and (a( 31))) xor
        (b( 34) and (a( 30))) xor
        (b( 35) and (a( 29))) xor
        (b( 36) and (a( 28))) xor
        (b( 37) and (a( 27))) xor
        (b( 38) and (a( 26))) xor
        (b( 39) and (a( 25))) xor
        (b( 40) and (a( 24))) xor
        (b( 41) and (a( 23))) xor
        (b( 42) and (a( 22))) xor
        (b( 43) and (a( 21))) xor
        (b( 44) and (a( 20))) xor
        (b( 45) and (a( 19))) xor
        (b( 46) and (a( 18))) xor
        (b( 47) and (a( 17))) xor
        (b( 48) and (a( 16))) xor
        (b( 49) and (a( 15))) xor
        (b( 50) and (a( 14))) xor
        (b( 51) and (a( 13))) xor
        (b( 52) and (a( 12))) xor
        (b( 53) and (a( 11))) xor
        (b( 54) and (a( 10))) xor
        (b( 55) and (a(  9))) xor
        (b( 56) and (a(  8))) xor
        (b( 57) and (a(  7))) xor
        (b( 58) and (a(  6) xor a(127))) xor
        (b( 59) and (a(  5) xor a(126))) xor
        (b( 60) and (a(  4) xor a(125))) xor
        (b( 61) and (a(  3) xor a(124))) xor
        (b( 62) and (a(  2) xor a(123))) xor
        (b( 63) and (a(  1) xor a(122) xor a(127))) xor
        (b( 64) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 65) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 66) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 67) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 68) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 69) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 70) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 71) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 72) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 73) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 74) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 75) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 76) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 77) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 78) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 79) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 80) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 81) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 82) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 83) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 84) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 85) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 86) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 87) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 88) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 89) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 90) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 91) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 92) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 93) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 94) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 95) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 96) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 97) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 98) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b( 99) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(100) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(101) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(102) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(103) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(104) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(105) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(106) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(107) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(108) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(109) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(110) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(111) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(112) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(113) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(114) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(115) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(116) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(117) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(118) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(119) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(120) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(121) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(122) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(123) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(124) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(125) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(126) and (a( 59) xor a( 64) xor a( 65) xor a( 66))) xor
        (b(127) and (a( 58) xor a( 63) xor a( 64) xor a( 65)));
    c( 65) <= 
        (b(  0) and (a( 65))) xor
        (b(  1) and (a( 64))) xor
        (b(  2) and (a( 63))) xor
        (b(  3) and (a( 62))) xor
        (b(  4) and (a( 61))) xor
        (b(  5) and (a( 60))) xor
        (b(  6) and (a( 59))) xor
        (b(  7) and (a( 58))) xor
        (b(  8) and (a( 57))) xor
        (b(  9) and (a( 56))) xor
        (b( 10) and (a( 55))) xor
        (b( 11) and (a( 54))) xor
        (b( 12) and (a( 53))) xor
        (b( 13) and (a( 52))) xor
        (b( 14) and (a( 51))) xor
        (b( 15) and (a( 50))) xor
        (b( 16) and (a( 49))) xor
        (b( 17) and (a( 48))) xor
        (b( 18) and (a( 47))) xor
        (b( 19) and (a( 46))) xor
        (b( 20) and (a( 45))) xor
        (b( 21) and (a( 44))) xor
        (b( 22) and (a( 43))) xor
        (b( 23) and (a( 42))) xor
        (b( 24) and (a( 41))) xor
        (b( 25) and (a( 40))) xor
        (b( 26) and (a( 39))) xor
        (b( 27) and (a( 38))) xor
        (b( 28) and (a( 37))) xor
        (b( 29) and (a( 36))) xor
        (b( 30) and (a( 35))) xor
        (b( 31) and (a( 34))) xor
        (b( 32) and (a( 33))) xor
        (b( 33) and (a( 32))) xor
        (b( 34) and (a( 31))) xor
        (b( 35) and (a( 30))) xor
        (b( 36) and (a( 29))) xor
        (b( 37) and (a( 28))) xor
        (b( 38) and (a( 27))) xor
        (b( 39) and (a( 26))) xor
        (b( 40) and (a( 25))) xor
        (b( 41) and (a( 24))) xor
        (b( 42) and (a( 23))) xor
        (b( 43) and (a( 22))) xor
        (b( 44) and (a( 21))) xor
        (b( 45) and (a( 20))) xor
        (b( 46) and (a( 19))) xor
        (b( 47) and (a( 18))) xor
        (b( 48) and (a( 17))) xor
        (b( 49) and (a( 16))) xor
        (b( 50) and (a( 15))) xor
        (b( 51) and (a( 14))) xor
        (b( 52) and (a( 13))) xor
        (b( 53) and (a( 12))) xor
        (b( 54) and (a( 11))) xor
        (b( 55) and (a( 10))) xor
        (b( 56) and (a(  9))) xor
        (b( 57) and (a(  8))) xor
        (b( 58) and (a(  7))) xor
        (b( 59) and (a(  6) xor a(127))) xor
        (b( 60) and (a(  5) xor a(126))) xor
        (b( 61) and (a(  4) xor a(125))) xor
        (b( 62) and (a(  3) xor a(124))) xor
        (b( 63) and (a(  2) xor a(123))) xor
        (b( 64) and (a(  1) xor a(122) xor a(127))) xor
        (b( 65) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 66) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 67) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 68) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 69) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 70) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 71) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 72) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 73) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 74) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 75) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 76) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 77) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 78) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 79) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 80) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 81) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 82) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 83) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 84) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 85) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 86) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 87) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 88) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 89) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 90) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 91) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 92) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 93) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 94) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 95) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 96) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 97) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 98) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b( 99) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(100) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(101) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(102) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(103) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(104) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(105) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(106) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(107) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(108) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(109) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(110) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(111) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(112) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(113) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(114) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(115) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(116) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(117) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(118) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(119) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(120) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(121) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(122) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(123) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(124) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(125) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(126) and (a( 60) xor a( 65) xor a( 66) xor a( 67))) xor
        (b(127) and (a( 59) xor a( 64) xor a( 65) xor a( 66)));
    c( 66) <= 
        (b(  0) and (a( 66))) xor
        (b(  1) and (a( 65))) xor
        (b(  2) and (a( 64))) xor
        (b(  3) and (a( 63))) xor
        (b(  4) and (a( 62))) xor
        (b(  5) and (a( 61))) xor
        (b(  6) and (a( 60))) xor
        (b(  7) and (a( 59))) xor
        (b(  8) and (a( 58))) xor
        (b(  9) and (a( 57))) xor
        (b( 10) and (a( 56))) xor
        (b( 11) and (a( 55))) xor
        (b( 12) and (a( 54))) xor
        (b( 13) and (a( 53))) xor
        (b( 14) and (a( 52))) xor
        (b( 15) and (a( 51))) xor
        (b( 16) and (a( 50))) xor
        (b( 17) and (a( 49))) xor
        (b( 18) and (a( 48))) xor
        (b( 19) and (a( 47))) xor
        (b( 20) and (a( 46))) xor
        (b( 21) and (a( 45))) xor
        (b( 22) and (a( 44))) xor
        (b( 23) and (a( 43))) xor
        (b( 24) and (a( 42))) xor
        (b( 25) and (a( 41))) xor
        (b( 26) and (a( 40))) xor
        (b( 27) and (a( 39))) xor
        (b( 28) and (a( 38))) xor
        (b( 29) and (a( 37))) xor
        (b( 30) and (a( 36))) xor
        (b( 31) and (a( 35))) xor
        (b( 32) and (a( 34))) xor
        (b( 33) and (a( 33))) xor
        (b( 34) and (a( 32))) xor
        (b( 35) and (a( 31))) xor
        (b( 36) and (a( 30))) xor
        (b( 37) and (a( 29))) xor
        (b( 38) and (a( 28))) xor
        (b( 39) and (a( 27))) xor
        (b( 40) and (a( 26))) xor
        (b( 41) and (a( 25))) xor
        (b( 42) and (a( 24))) xor
        (b( 43) and (a( 23))) xor
        (b( 44) and (a( 22))) xor
        (b( 45) and (a( 21))) xor
        (b( 46) and (a( 20))) xor
        (b( 47) and (a( 19))) xor
        (b( 48) and (a( 18))) xor
        (b( 49) and (a( 17))) xor
        (b( 50) and (a( 16))) xor
        (b( 51) and (a( 15))) xor
        (b( 52) and (a( 14))) xor
        (b( 53) and (a( 13))) xor
        (b( 54) and (a( 12))) xor
        (b( 55) and (a( 11))) xor
        (b( 56) and (a( 10))) xor
        (b( 57) and (a(  9))) xor
        (b( 58) and (a(  8))) xor
        (b( 59) and (a(  7))) xor
        (b( 60) and (a(  6) xor a(127))) xor
        (b( 61) and (a(  5) xor a(126))) xor
        (b( 62) and (a(  4) xor a(125))) xor
        (b( 63) and (a(  3) xor a(124))) xor
        (b( 64) and (a(  2) xor a(123))) xor
        (b( 65) and (a(  1) xor a(122) xor a(127))) xor
        (b( 66) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 67) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 68) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 69) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 70) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 71) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 72) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 73) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 74) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 75) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 76) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 77) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 78) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 79) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 80) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 81) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 82) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 83) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 84) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 85) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 86) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 87) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 88) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 89) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 90) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 91) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 92) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 93) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 94) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 95) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 96) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 97) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 98) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b( 99) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(100) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(101) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(102) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(103) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(104) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(105) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(106) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(107) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(108) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(109) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(110) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(111) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(112) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(113) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(114) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(115) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(116) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(117) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(118) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(119) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(120) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(121) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(122) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(123) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(124) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(125) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(126) and (a( 61) xor a( 66) xor a( 67) xor a( 68))) xor
        (b(127) and (a( 60) xor a( 65) xor a( 66) xor a( 67)));
    c( 67) <= 
        (b(  0) and (a( 67))) xor
        (b(  1) and (a( 66))) xor
        (b(  2) and (a( 65))) xor
        (b(  3) and (a( 64))) xor
        (b(  4) and (a( 63))) xor
        (b(  5) and (a( 62))) xor
        (b(  6) and (a( 61))) xor
        (b(  7) and (a( 60))) xor
        (b(  8) and (a( 59))) xor
        (b(  9) and (a( 58))) xor
        (b( 10) and (a( 57))) xor
        (b( 11) and (a( 56))) xor
        (b( 12) and (a( 55))) xor
        (b( 13) and (a( 54))) xor
        (b( 14) and (a( 53))) xor
        (b( 15) and (a( 52))) xor
        (b( 16) and (a( 51))) xor
        (b( 17) and (a( 50))) xor
        (b( 18) and (a( 49))) xor
        (b( 19) and (a( 48))) xor
        (b( 20) and (a( 47))) xor
        (b( 21) and (a( 46))) xor
        (b( 22) and (a( 45))) xor
        (b( 23) and (a( 44))) xor
        (b( 24) and (a( 43))) xor
        (b( 25) and (a( 42))) xor
        (b( 26) and (a( 41))) xor
        (b( 27) and (a( 40))) xor
        (b( 28) and (a( 39))) xor
        (b( 29) and (a( 38))) xor
        (b( 30) and (a( 37))) xor
        (b( 31) and (a( 36))) xor
        (b( 32) and (a( 35))) xor
        (b( 33) and (a( 34))) xor
        (b( 34) and (a( 33))) xor
        (b( 35) and (a( 32))) xor
        (b( 36) and (a( 31))) xor
        (b( 37) and (a( 30))) xor
        (b( 38) and (a( 29))) xor
        (b( 39) and (a( 28))) xor
        (b( 40) and (a( 27))) xor
        (b( 41) and (a( 26))) xor
        (b( 42) and (a( 25))) xor
        (b( 43) and (a( 24))) xor
        (b( 44) and (a( 23))) xor
        (b( 45) and (a( 22))) xor
        (b( 46) and (a( 21))) xor
        (b( 47) and (a( 20))) xor
        (b( 48) and (a( 19))) xor
        (b( 49) and (a( 18))) xor
        (b( 50) and (a( 17))) xor
        (b( 51) and (a( 16))) xor
        (b( 52) and (a( 15))) xor
        (b( 53) and (a( 14))) xor
        (b( 54) and (a( 13))) xor
        (b( 55) and (a( 12))) xor
        (b( 56) and (a( 11))) xor
        (b( 57) and (a( 10))) xor
        (b( 58) and (a(  9))) xor
        (b( 59) and (a(  8))) xor
        (b( 60) and (a(  7))) xor
        (b( 61) and (a(  6) xor a(127))) xor
        (b( 62) and (a(  5) xor a(126))) xor
        (b( 63) and (a(  4) xor a(125))) xor
        (b( 64) and (a(  3) xor a(124))) xor
        (b( 65) and (a(  2) xor a(123))) xor
        (b( 66) and (a(  1) xor a(122) xor a(127))) xor
        (b( 67) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 68) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 69) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 70) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 71) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 72) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 73) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 74) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 75) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 76) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 77) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 78) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 79) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 80) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 81) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 82) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 83) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 84) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 85) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 86) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 87) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 88) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 89) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 90) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 91) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 92) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 93) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 94) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 95) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 96) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 97) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 98) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b( 99) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(100) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(101) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(102) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(103) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(104) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(105) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(106) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(107) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(108) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(109) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(110) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(111) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(112) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(113) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(114) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(115) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(116) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(117) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(118) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(119) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(120) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(121) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(122) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(123) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(124) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(125) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(126) and (a( 62) xor a( 67) xor a( 68) xor a( 69))) xor
        (b(127) and (a( 61) xor a( 66) xor a( 67) xor a( 68)));
    c( 68) <= 
        (b(  0) and (a( 68))) xor
        (b(  1) and (a( 67))) xor
        (b(  2) and (a( 66))) xor
        (b(  3) and (a( 65))) xor
        (b(  4) and (a( 64))) xor
        (b(  5) and (a( 63))) xor
        (b(  6) and (a( 62))) xor
        (b(  7) and (a( 61))) xor
        (b(  8) and (a( 60))) xor
        (b(  9) and (a( 59))) xor
        (b( 10) and (a( 58))) xor
        (b( 11) and (a( 57))) xor
        (b( 12) and (a( 56))) xor
        (b( 13) and (a( 55))) xor
        (b( 14) and (a( 54))) xor
        (b( 15) and (a( 53))) xor
        (b( 16) and (a( 52))) xor
        (b( 17) and (a( 51))) xor
        (b( 18) and (a( 50))) xor
        (b( 19) and (a( 49))) xor
        (b( 20) and (a( 48))) xor
        (b( 21) and (a( 47))) xor
        (b( 22) and (a( 46))) xor
        (b( 23) and (a( 45))) xor
        (b( 24) and (a( 44))) xor
        (b( 25) and (a( 43))) xor
        (b( 26) and (a( 42))) xor
        (b( 27) and (a( 41))) xor
        (b( 28) and (a( 40))) xor
        (b( 29) and (a( 39))) xor
        (b( 30) and (a( 38))) xor
        (b( 31) and (a( 37))) xor
        (b( 32) and (a( 36))) xor
        (b( 33) and (a( 35))) xor
        (b( 34) and (a( 34))) xor
        (b( 35) and (a( 33))) xor
        (b( 36) and (a( 32))) xor
        (b( 37) and (a( 31))) xor
        (b( 38) and (a( 30))) xor
        (b( 39) and (a( 29))) xor
        (b( 40) and (a( 28))) xor
        (b( 41) and (a( 27))) xor
        (b( 42) and (a( 26))) xor
        (b( 43) and (a( 25))) xor
        (b( 44) and (a( 24))) xor
        (b( 45) and (a( 23))) xor
        (b( 46) and (a( 22))) xor
        (b( 47) and (a( 21))) xor
        (b( 48) and (a( 20))) xor
        (b( 49) and (a( 19))) xor
        (b( 50) and (a( 18))) xor
        (b( 51) and (a( 17))) xor
        (b( 52) and (a( 16))) xor
        (b( 53) and (a( 15))) xor
        (b( 54) and (a( 14))) xor
        (b( 55) and (a( 13))) xor
        (b( 56) and (a( 12))) xor
        (b( 57) and (a( 11))) xor
        (b( 58) and (a( 10))) xor
        (b( 59) and (a(  9))) xor
        (b( 60) and (a(  8))) xor
        (b( 61) and (a(  7))) xor
        (b( 62) and (a(  6) xor a(127))) xor
        (b( 63) and (a(  5) xor a(126))) xor
        (b( 64) and (a(  4) xor a(125))) xor
        (b( 65) and (a(  3) xor a(124))) xor
        (b( 66) and (a(  2) xor a(123))) xor
        (b( 67) and (a(  1) xor a(122) xor a(127))) xor
        (b( 68) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 69) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 70) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 71) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 72) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 73) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 74) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 75) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 76) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 77) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 78) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 79) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 80) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 81) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 82) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 83) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 84) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 85) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 86) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 87) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 88) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 89) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 90) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 91) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 92) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 93) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 94) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 95) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 96) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 97) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 98) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b( 99) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(100) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(101) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(102) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(103) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(104) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(105) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(106) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(107) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(108) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(109) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(110) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(111) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(112) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(113) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(114) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(115) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(116) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(117) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(118) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(119) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(120) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(121) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(122) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(123) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(124) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(125) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(126) and (a( 63) xor a( 68) xor a( 69) xor a( 70))) xor
        (b(127) and (a( 62) xor a( 67) xor a( 68) xor a( 69)));
    c( 69) <= 
        (b(  0) and (a( 69))) xor
        (b(  1) and (a( 68))) xor
        (b(  2) and (a( 67))) xor
        (b(  3) and (a( 66))) xor
        (b(  4) and (a( 65))) xor
        (b(  5) and (a( 64))) xor
        (b(  6) and (a( 63))) xor
        (b(  7) and (a( 62))) xor
        (b(  8) and (a( 61))) xor
        (b(  9) and (a( 60))) xor
        (b( 10) and (a( 59))) xor
        (b( 11) and (a( 58))) xor
        (b( 12) and (a( 57))) xor
        (b( 13) and (a( 56))) xor
        (b( 14) and (a( 55))) xor
        (b( 15) and (a( 54))) xor
        (b( 16) and (a( 53))) xor
        (b( 17) and (a( 52))) xor
        (b( 18) and (a( 51))) xor
        (b( 19) and (a( 50))) xor
        (b( 20) and (a( 49))) xor
        (b( 21) and (a( 48))) xor
        (b( 22) and (a( 47))) xor
        (b( 23) and (a( 46))) xor
        (b( 24) and (a( 45))) xor
        (b( 25) and (a( 44))) xor
        (b( 26) and (a( 43))) xor
        (b( 27) and (a( 42))) xor
        (b( 28) and (a( 41))) xor
        (b( 29) and (a( 40))) xor
        (b( 30) and (a( 39))) xor
        (b( 31) and (a( 38))) xor
        (b( 32) and (a( 37))) xor
        (b( 33) and (a( 36))) xor
        (b( 34) and (a( 35))) xor
        (b( 35) and (a( 34))) xor
        (b( 36) and (a( 33))) xor
        (b( 37) and (a( 32))) xor
        (b( 38) and (a( 31))) xor
        (b( 39) and (a( 30))) xor
        (b( 40) and (a( 29))) xor
        (b( 41) and (a( 28))) xor
        (b( 42) and (a( 27))) xor
        (b( 43) and (a( 26))) xor
        (b( 44) and (a( 25))) xor
        (b( 45) and (a( 24))) xor
        (b( 46) and (a( 23))) xor
        (b( 47) and (a( 22))) xor
        (b( 48) and (a( 21))) xor
        (b( 49) and (a( 20))) xor
        (b( 50) and (a( 19))) xor
        (b( 51) and (a( 18))) xor
        (b( 52) and (a( 17))) xor
        (b( 53) and (a( 16))) xor
        (b( 54) and (a( 15))) xor
        (b( 55) and (a( 14))) xor
        (b( 56) and (a( 13))) xor
        (b( 57) and (a( 12))) xor
        (b( 58) and (a( 11))) xor
        (b( 59) and (a( 10))) xor
        (b( 60) and (a(  9))) xor
        (b( 61) and (a(  8))) xor
        (b( 62) and (a(  7))) xor
        (b( 63) and (a(  6) xor a(127))) xor
        (b( 64) and (a(  5) xor a(126))) xor
        (b( 65) and (a(  4) xor a(125))) xor
        (b( 66) and (a(  3) xor a(124))) xor
        (b( 67) and (a(  2) xor a(123))) xor
        (b( 68) and (a(  1) xor a(122) xor a(127))) xor
        (b( 69) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 70) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 71) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 72) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 73) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 74) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 75) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 76) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 77) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 78) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 79) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 80) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 81) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 82) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 83) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 84) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 85) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 86) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 87) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 88) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 89) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 90) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 91) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 92) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 93) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 94) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 95) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 96) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 97) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 98) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b( 99) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(100) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(101) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(102) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(103) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(104) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(105) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(106) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(107) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(108) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(109) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(110) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(111) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(112) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(113) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(114) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(115) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(116) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(117) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(118) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(119) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(120) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(121) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(122) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(123) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(124) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(125) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(126) and (a( 64) xor a( 69) xor a( 70) xor a( 71))) xor
        (b(127) and (a( 63) xor a( 68) xor a( 69) xor a( 70)));
    c( 70) <= 
        (b(  0) and (a( 70))) xor
        (b(  1) and (a( 69))) xor
        (b(  2) and (a( 68))) xor
        (b(  3) and (a( 67))) xor
        (b(  4) and (a( 66))) xor
        (b(  5) and (a( 65))) xor
        (b(  6) and (a( 64))) xor
        (b(  7) and (a( 63))) xor
        (b(  8) and (a( 62))) xor
        (b(  9) and (a( 61))) xor
        (b( 10) and (a( 60))) xor
        (b( 11) and (a( 59))) xor
        (b( 12) and (a( 58))) xor
        (b( 13) and (a( 57))) xor
        (b( 14) and (a( 56))) xor
        (b( 15) and (a( 55))) xor
        (b( 16) and (a( 54))) xor
        (b( 17) and (a( 53))) xor
        (b( 18) and (a( 52))) xor
        (b( 19) and (a( 51))) xor
        (b( 20) and (a( 50))) xor
        (b( 21) and (a( 49))) xor
        (b( 22) and (a( 48))) xor
        (b( 23) and (a( 47))) xor
        (b( 24) and (a( 46))) xor
        (b( 25) and (a( 45))) xor
        (b( 26) and (a( 44))) xor
        (b( 27) and (a( 43))) xor
        (b( 28) and (a( 42))) xor
        (b( 29) and (a( 41))) xor
        (b( 30) and (a( 40))) xor
        (b( 31) and (a( 39))) xor
        (b( 32) and (a( 38))) xor
        (b( 33) and (a( 37))) xor
        (b( 34) and (a( 36))) xor
        (b( 35) and (a( 35))) xor
        (b( 36) and (a( 34))) xor
        (b( 37) and (a( 33))) xor
        (b( 38) and (a( 32))) xor
        (b( 39) and (a( 31))) xor
        (b( 40) and (a( 30))) xor
        (b( 41) and (a( 29))) xor
        (b( 42) and (a( 28))) xor
        (b( 43) and (a( 27))) xor
        (b( 44) and (a( 26))) xor
        (b( 45) and (a( 25))) xor
        (b( 46) and (a( 24))) xor
        (b( 47) and (a( 23))) xor
        (b( 48) and (a( 22))) xor
        (b( 49) and (a( 21))) xor
        (b( 50) and (a( 20))) xor
        (b( 51) and (a( 19))) xor
        (b( 52) and (a( 18))) xor
        (b( 53) and (a( 17))) xor
        (b( 54) and (a( 16))) xor
        (b( 55) and (a( 15))) xor
        (b( 56) and (a( 14))) xor
        (b( 57) and (a( 13))) xor
        (b( 58) and (a( 12))) xor
        (b( 59) and (a( 11))) xor
        (b( 60) and (a( 10))) xor
        (b( 61) and (a(  9))) xor
        (b( 62) and (a(  8))) xor
        (b( 63) and (a(  7))) xor
        (b( 64) and (a(  6) xor a(127))) xor
        (b( 65) and (a(  5) xor a(126))) xor
        (b( 66) and (a(  4) xor a(125))) xor
        (b( 67) and (a(  3) xor a(124))) xor
        (b( 68) and (a(  2) xor a(123))) xor
        (b( 69) and (a(  1) xor a(122) xor a(127))) xor
        (b( 70) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 71) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 72) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 73) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 74) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 75) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 76) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 77) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 78) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 79) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 80) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 81) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 82) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 83) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 84) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 85) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 86) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 87) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 88) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 89) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 90) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 91) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 92) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 93) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 94) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 95) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 96) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 97) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 98) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b( 99) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(100) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(101) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(102) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(103) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(104) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(105) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(106) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(107) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(108) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(109) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(110) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(111) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(112) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(113) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(114) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(115) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(116) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(117) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(118) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(119) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(120) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(121) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(122) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(123) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(124) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(125) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(126) and (a( 65) xor a( 70) xor a( 71) xor a( 72))) xor
        (b(127) and (a( 64) xor a( 69) xor a( 70) xor a( 71)));
    c( 71) <= 
        (b(  0) and (a( 71))) xor
        (b(  1) and (a( 70))) xor
        (b(  2) and (a( 69))) xor
        (b(  3) and (a( 68))) xor
        (b(  4) and (a( 67))) xor
        (b(  5) and (a( 66))) xor
        (b(  6) and (a( 65))) xor
        (b(  7) and (a( 64))) xor
        (b(  8) and (a( 63))) xor
        (b(  9) and (a( 62))) xor
        (b( 10) and (a( 61))) xor
        (b( 11) and (a( 60))) xor
        (b( 12) and (a( 59))) xor
        (b( 13) and (a( 58))) xor
        (b( 14) and (a( 57))) xor
        (b( 15) and (a( 56))) xor
        (b( 16) and (a( 55))) xor
        (b( 17) and (a( 54))) xor
        (b( 18) and (a( 53))) xor
        (b( 19) and (a( 52))) xor
        (b( 20) and (a( 51))) xor
        (b( 21) and (a( 50))) xor
        (b( 22) and (a( 49))) xor
        (b( 23) and (a( 48))) xor
        (b( 24) and (a( 47))) xor
        (b( 25) and (a( 46))) xor
        (b( 26) and (a( 45))) xor
        (b( 27) and (a( 44))) xor
        (b( 28) and (a( 43))) xor
        (b( 29) and (a( 42))) xor
        (b( 30) and (a( 41))) xor
        (b( 31) and (a( 40))) xor
        (b( 32) and (a( 39))) xor
        (b( 33) and (a( 38))) xor
        (b( 34) and (a( 37))) xor
        (b( 35) and (a( 36))) xor
        (b( 36) and (a( 35))) xor
        (b( 37) and (a( 34))) xor
        (b( 38) and (a( 33))) xor
        (b( 39) and (a( 32))) xor
        (b( 40) and (a( 31))) xor
        (b( 41) and (a( 30))) xor
        (b( 42) and (a( 29))) xor
        (b( 43) and (a( 28))) xor
        (b( 44) and (a( 27))) xor
        (b( 45) and (a( 26))) xor
        (b( 46) and (a( 25))) xor
        (b( 47) and (a( 24))) xor
        (b( 48) and (a( 23))) xor
        (b( 49) and (a( 22))) xor
        (b( 50) and (a( 21))) xor
        (b( 51) and (a( 20))) xor
        (b( 52) and (a( 19))) xor
        (b( 53) and (a( 18))) xor
        (b( 54) and (a( 17))) xor
        (b( 55) and (a( 16))) xor
        (b( 56) and (a( 15))) xor
        (b( 57) and (a( 14))) xor
        (b( 58) and (a( 13))) xor
        (b( 59) and (a( 12))) xor
        (b( 60) and (a( 11))) xor
        (b( 61) and (a( 10))) xor
        (b( 62) and (a(  9))) xor
        (b( 63) and (a(  8))) xor
        (b( 64) and (a(  7))) xor
        (b( 65) and (a(  6) xor a(127))) xor
        (b( 66) and (a(  5) xor a(126))) xor
        (b( 67) and (a(  4) xor a(125))) xor
        (b( 68) and (a(  3) xor a(124))) xor
        (b( 69) and (a(  2) xor a(123))) xor
        (b( 70) and (a(  1) xor a(122) xor a(127))) xor
        (b( 71) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 72) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 73) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 74) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 75) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 76) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 77) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 78) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 79) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 80) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 81) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 82) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 83) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 84) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 85) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 86) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 87) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 88) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 89) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 90) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 91) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 92) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 93) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 94) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 95) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 96) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 97) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 98) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b( 99) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(100) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(101) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(102) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(103) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(104) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(105) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(106) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(107) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(108) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(109) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(110) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(111) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(112) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(113) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(114) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(115) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(116) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(117) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(118) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(119) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(120) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(121) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(122) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(123) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(124) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(125) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(126) and (a( 66) xor a( 71) xor a( 72) xor a( 73))) xor
        (b(127) and (a( 65) xor a( 70) xor a( 71) xor a( 72)));
    c( 72) <= 
        (b(  0) and (a( 72))) xor
        (b(  1) and (a( 71))) xor
        (b(  2) and (a( 70))) xor
        (b(  3) and (a( 69))) xor
        (b(  4) and (a( 68))) xor
        (b(  5) and (a( 67))) xor
        (b(  6) and (a( 66))) xor
        (b(  7) and (a( 65))) xor
        (b(  8) and (a( 64))) xor
        (b(  9) and (a( 63))) xor
        (b( 10) and (a( 62))) xor
        (b( 11) and (a( 61))) xor
        (b( 12) and (a( 60))) xor
        (b( 13) and (a( 59))) xor
        (b( 14) and (a( 58))) xor
        (b( 15) and (a( 57))) xor
        (b( 16) and (a( 56))) xor
        (b( 17) and (a( 55))) xor
        (b( 18) and (a( 54))) xor
        (b( 19) and (a( 53))) xor
        (b( 20) and (a( 52))) xor
        (b( 21) and (a( 51))) xor
        (b( 22) and (a( 50))) xor
        (b( 23) and (a( 49))) xor
        (b( 24) and (a( 48))) xor
        (b( 25) and (a( 47))) xor
        (b( 26) and (a( 46))) xor
        (b( 27) and (a( 45))) xor
        (b( 28) and (a( 44))) xor
        (b( 29) and (a( 43))) xor
        (b( 30) and (a( 42))) xor
        (b( 31) and (a( 41))) xor
        (b( 32) and (a( 40))) xor
        (b( 33) and (a( 39))) xor
        (b( 34) and (a( 38))) xor
        (b( 35) and (a( 37))) xor
        (b( 36) and (a( 36))) xor
        (b( 37) and (a( 35))) xor
        (b( 38) and (a( 34))) xor
        (b( 39) and (a( 33))) xor
        (b( 40) and (a( 32))) xor
        (b( 41) and (a( 31))) xor
        (b( 42) and (a( 30))) xor
        (b( 43) and (a( 29))) xor
        (b( 44) and (a( 28))) xor
        (b( 45) and (a( 27))) xor
        (b( 46) and (a( 26))) xor
        (b( 47) and (a( 25))) xor
        (b( 48) and (a( 24))) xor
        (b( 49) and (a( 23))) xor
        (b( 50) and (a( 22))) xor
        (b( 51) and (a( 21))) xor
        (b( 52) and (a( 20))) xor
        (b( 53) and (a( 19))) xor
        (b( 54) and (a( 18))) xor
        (b( 55) and (a( 17))) xor
        (b( 56) and (a( 16))) xor
        (b( 57) and (a( 15))) xor
        (b( 58) and (a( 14))) xor
        (b( 59) and (a( 13))) xor
        (b( 60) and (a( 12))) xor
        (b( 61) and (a( 11))) xor
        (b( 62) and (a( 10))) xor
        (b( 63) and (a(  9))) xor
        (b( 64) and (a(  8))) xor
        (b( 65) and (a(  7))) xor
        (b( 66) and (a(  6) xor a(127))) xor
        (b( 67) and (a(  5) xor a(126))) xor
        (b( 68) and (a(  4) xor a(125))) xor
        (b( 69) and (a(  3) xor a(124))) xor
        (b( 70) and (a(  2) xor a(123))) xor
        (b( 71) and (a(  1) xor a(122) xor a(127))) xor
        (b( 72) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 73) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 74) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 75) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 76) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 77) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 78) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 79) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 80) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 81) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 82) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 83) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 84) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 85) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 86) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 87) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 88) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 89) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 90) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 91) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 92) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 93) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 94) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 95) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 96) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 97) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 98) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b( 99) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(100) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(101) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(102) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(103) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(104) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(105) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(106) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(107) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(108) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(109) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(110) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(111) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(112) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(113) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(114) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(115) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(116) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(117) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(118) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(119) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(120) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(121) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(122) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(123) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(124) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(125) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(126) and (a( 67) xor a( 72) xor a( 73) xor a( 74))) xor
        (b(127) and (a( 66) xor a( 71) xor a( 72) xor a( 73)));
    c( 73) <= 
        (b(  0) and (a( 73))) xor
        (b(  1) and (a( 72))) xor
        (b(  2) and (a( 71))) xor
        (b(  3) and (a( 70))) xor
        (b(  4) and (a( 69))) xor
        (b(  5) and (a( 68))) xor
        (b(  6) and (a( 67))) xor
        (b(  7) and (a( 66))) xor
        (b(  8) and (a( 65))) xor
        (b(  9) and (a( 64))) xor
        (b( 10) and (a( 63))) xor
        (b( 11) and (a( 62))) xor
        (b( 12) and (a( 61))) xor
        (b( 13) and (a( 60))) xor
        (b( 14) and (a( 59))) xor
        (b( 15) and (a( 58))) xor
        (b( 16) and (a( 57))) xor
        (b( 17) and (a( 56))) xor
        (b( 18) and (a( 55))) xor
        (b( 19) and (a( 54))) xor
        (b( 20) and (a( 53))) xor
        (b( 21) and (a( 52))) xor
        (b( 22) and (a( 51))) xor
        (b( 23) and (a( 50))) xor
        (b( 24) and (a( 49))) xor
        (b( 25) and (a( 48))) xor
        (b( 26) and (a( 47))) xor
        (b( 27) and (a( 46))) xor
        (b( 28) and (a( 45))) xor
        (b( 29) and (a( 44))) xor
        (b( 30) and (a( 43))) xor
        (b( 31) and (a( 42))) xor
        (b( 32) and (a( 41))) xor
        (b( 33) and (a( 40))) xor
        (b( 34) and (a( 39))) xor
        (b( 35) and (a( 38))) xor
        (b( 36) and (a( 37))) xor
        (b( 37) and (a( 36))) xor
        (b( 38) and (a( 35))) xor
        (b( 39) and (a( 34))) xor
        (b( 40) and (a( 33))) xor
        (b( 41) and (a( 32))) xor
        (b( 42) and (a( 31))) xor
        (b( 43) and (a( 30))) xor
        (b( 44) and (a( 29))) xor
        (b( 45) and (a( 28))) xor
        (b( 46) and (a( 27))) xor
        (b( 47) and (a( 26))) xor
        (b( 48) and (a( 25))) xor
        (b( 49) and (a( 24))) xor
        (b( 50) and (a( 23))) xor
        (b( 51) and (a( 22))) xor
        (b( 52) and (a( 21))) xor
        (b( 53) and (a( 20))) xor
        (b( 54) and (a( 19))) xor
        (b( 55) and (a( 18))) xor
        (b( 56) and (a( 17))) xor
        (b( 57) and (a( 16))) xor
        (b( 58) and (a( 15))) xor
        (b( 59) and (a( 14))) xor
        (b( 60) and (a( 13))) xor
        (b( 61) and (a( 12))) xor
        (b( 62) and (a( 11))) xor
        (b( 63) and (a( 10))) xor
        (b( 64) and (a(  9))) xor
        (b( 65) and (a(  8))) xor
        (b( 66) and (a(  7))) xor
        (b( 67) and (a(  6) xor a(127))) xor
        (b( 68) and (a(  5) xor a(126))) xor
        (b( 69) and (a(  4) xor a(125))) xor
        (b( 70) and (a(  3) xor a(124))) xor
        (b( 71) and (a(  2) xor a(123))) xor
        (b( 72) and (a(  1) xor a(122) xor a(127))) xor
        (b( 73) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 74) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 75) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 76) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 77) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 78) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 79) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 80) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 81) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 82) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 83) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 84) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 85) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 86) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 87) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 88) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 89) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 90) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 91) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 92) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 93) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 94) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 95) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 96) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 97) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 98) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b( 99) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(100) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(101) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(102) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(103) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(104) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(105) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(106) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(107) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(108) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(109) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(110) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(111) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(112) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(113) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(114) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(115) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(116) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(117) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(118) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(119) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(120) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(121) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(122) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(123) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(124) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(125) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(126) and (a( 68) xor a( 73) xor a( 74) xor a( 75))) xor
        (b(127) and (a( 67) xor a( 72) xor a( 73) xor a( 74)));
    c( 74) <= 
        (b(  0) and (a( 74))) xor
        (b(  1) and (a( 73))) xor
        (b(  2) and (a( 72))) xor
        (b(  3) and (a( 71))) xor
        (b(  4) and (a( 70))) xor
        (b(  5) and (a( 69))) xor
        (b(  6) and (a( 68))) xor
        (b(  7) and (a( 67))) xor
        (b(  8) and (a( 66))) xor
        (b(  9) and (a( 65))) xor
        (b( 10) and (a( 64))) xor
        (b( 11) and (a( 63))) xor
        (b( 12) and (a( 62))) xor
        (b( 13) and (a( 61))) xor
        (b( 14) and (a( 60))) xor
        (b( 15) and (a( 59))) xor
        (b( 16) and (a( 58))) xor
        (b( 17) and (a( 57))) xor
        (b( 18) and (a( 56))) xor
        (b( 19) and (a( 55))) xor
        (b( 20) and (a( 54))) xor
        (b( 21) and (a( 53))) xor
        (b( 22) and (a( 52))) xor
        (b( 23) and (a( 51))) xor
        (b( 24) and (a( 50))) xor
        (b( 25) and (a( 49))) xor
        (b( 26) and (a( 48))) xor
        (b( 27) and (a( 47))) xor
        (b( 28) and (a( 46))) xor
        (b( 29) and (a( 45))) xor
        (b( 30) and (a( 44))) xor
        (b( 31) and (a( 43))) xor
        (b( 32) and (a( 42))) xor
        (b( 33) and (a( 41))) xor
        (b( 34) and (a( 40))) xor
        (b( 35) and (a( 39))) xor
        (b( 36) and (a( 38))) xor
        (b( 37) and (a( 37))) xor
        (b( 38) and (a( 36))) xor
        (b( 39) and (a( 35))) xor
        (b( 40) and (a( 34))) xor
        (b( 41) and (a( 33))) xor
        (b( 42) and (a( 32))) xor
        (b( 43) and (a( 31))) xor
        (b( 44) and (a( 30))) xor
        (b( 45) and (a( 29))) xor
        (b( 46) and (a( 28))) xor
        (b( 47) and (a( 27))) xor
        (b( 48) and (a( 26))) xor
        (b( 49) and (a( 25))) xor
        (b( 50) and (a( 24))) xor
        (b( 51) and (a( 23))) xor
        (b( 52) and (a( 22))) xor
        (b( 53) and (a( 21))) xor
        (b( 54) and (a( 20))) xor
        (b( 55) and (a( 19))) xor
        (b( 56) and (a( 18))) xor
        (b( 57) and (a( 17))) xor
        (b( 58) and (a( 16))) xor
        (b( 59) and (a( 15))) xor
        (b( 60) and (a( 14))) xor
        (b( 61) and (a( 13))) xor
        (b( 62) and (a( 12))) xor
        (b( 63) and (a( 11))) xor
        (b( 64) and (a( 10))) xor
        (b( 65) and (a(  9))) xor
        (b( 66) and (a(  8))) xor
        (b( 67) and (a(  7))) xor
        (b( 68) and (a(  6) xor a(127))) xor
        (b( 69) and (a(  5) xor a(126))) xor
        (b( 70) and (a(  4) xor a(125))) xor
        (b( 71) and (a(  3) xor a(124))) xor
        (b( 72) and (a(  2) xor a(123))) xor
        (b( 73) and (a(  1) xor a(122) xor a(127))) xor
        (b( 74) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 75) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 76) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 77) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 78) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 79) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 80) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 81) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 82) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 83) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 84) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 85) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 86) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 87) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 88) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 89) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 90) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 91) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 92) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 93) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 94) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 95) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 96) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 97) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 98) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b( 99) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(100) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(101) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(102) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(103) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(104) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(105) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(106) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(107) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(108) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(109) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(110) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(111) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(112) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(113) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(114) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(115) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(116) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(117) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(118) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(119) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(120) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(121) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(122) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(123) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(124) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(125) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(126) and (a( 69) xor a( 74) xor a( 75) xor a( 76))) xor
        (b(127) and (a( 68) xor a( 73) xor a( 74) xor a( 75)));
    c( 75) <= 
        (b(  0) and (a( 75))) xor
        (b(  1) and (a( 74))) xor
        (b(  2) and (a( 73))) xor
        (b(  3) and (a( 72))) xor
        (b(  4) and (a( 71))) xor
        (b(  5) and (a( 70))) xor
        (b(  6) and (a( 69))) xor
        (b(  7) and (a( 68))) xor
        (b(  8) and (a( 67))) xor
        (b(  9) and (a( 66))) xor
        (b( 10) and (a( 65))) xor
        (b( 11) and (a( 64))) xor
        (b( 12) and (a( 63))) xor
        (b( 13) and (a( 62))) xor
        (b( 14) and (a( 61))) xor
        (b( 15) and (a( 60))) xor
        (b( 16) and (a( 59))) xor
        (b( 17) and (a( 58))) xor
        (b( 18) and (a( 57))) xor
        (b( 19) and (a( 56))) xor
        (b( 20) and (a( 55))) xor
        (b( 21) and (a( 54))) xor
        (b( 22) and (a( 53))) xor
        (b( 23) and (a( 52))) xor
        (b( 24) and (a( 51))) xor
        (b( 25) and (a( 50))) xor
        (b( 26) and (a( 49))) xor
        (b( 27) and (a( 48))) xor
        (b( 28) and (a( 47))) xor
        (b( 29) and (a( 46))) xor
        (b( 30) and (a( 45))) xor
        (b( 31) and (a( 44))) xor
        (b( 32) and (a( 43))) xor
        (b( 33) and (a( 42))) xor
        (b( 34) and (a( 41))) xor
        (b( 35) and (a( 40))) xor
        (b( 36) and (a( 39))) xor
        (b( 37) and (a( 38))) xor
        (b( 38) and (a( 37))) xor
        (b( 39) and (a( 36))) xor
        (b( 40) and (a( 35))) xor
        (b( 41) and (a( 34))) xor
        (b( 42) and (a( 33))) xor
        (b( 43) and (a( 32))) xor
        (b( 44) and (a( 31))) xor
        (b( 45) and (a( 30))) xor
        (b( 46) and (a( 29))) xor
        (b( 47) and (a( 28))) xor
        (b( 48) and (a( 27))) xor
        (b( 49) and (a( 26))) xor
        (b( 50) and (a( 25))) xor
        (b( 51) and (a( 24))) xor
        (b( 52) and (a( 23))) xor
        (b( 53) and (a( 22))) xor
        (b( 54) and (a( 21))) xor
        (b( 55) and (a( 20))) xor
        (b( 56) and (a( 19))) xor
        (b( 57) and (a( 18))) xor
        (b( 58) and (a( 17))) xor
        (b( 59) and (a( 16))) xor
        (b( 60) and (a( 15))) xor
        (b( 61) and (a( 14))) xor
        (b( 62) and (a( 13))) xor
        (b( 63) and (a( 12))) xor
        (b( 64) and (a( 11))) xor
        (b( 65) and (a( 10))) xor
        (b( 66) and (a(  9))) xor
        (b( 67) and (a(  8))) xor
        (b( 68) and (a(  7))) xor
        (b( 69) and (a(  6) xor a(127))) xor
        (b( 70) and (a(  5) xor a(126))) xor
        (b( 71) and (a(  4) xor a(125))) xor
        (b( 72) and (a(  3) xor a(124))) xor
        (b( 73) and (a(  2) xor a(123))) xor
        (b( 74) and (a(  1) xor a(122) xor a(127))) xor
        (b( 75) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 76) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 77) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 78) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 79) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 80) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 81) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 82) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 83) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 84) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 85) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 86) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 87) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 88) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 89) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 90) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 91) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 92) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 93) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 94) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 95) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 96) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 97) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 98) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b( 99) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(100) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(101) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(102) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(103) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(104) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(105) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(106) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(107) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(108) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(109) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(110) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(111) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(112) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(113) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(114) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(115) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(116) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(117) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(118) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(119) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(120) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(121) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(122) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(123) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(124) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(125) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(126) and (a( 70) xor a( 75) xor a( 76) xor a( 77))) xor
        (b(127) and (a( 69) xor a( 74) xor a( 75) xor a( 76)));
    c( 76) <= 
        (b(  0) and (a( 76))) xor
        (b(  1) and (a( 75))) xor
        (b(  2) and (a( 74))) xor
        (b(  3) and (a( 73))) xor
        (b(  4) and (a( 72))) xor
        (b(  5) and (a( 71))) xor
        (b(  6) and (a( 70))) xor
        (b(  7) and (a( 69))) xor
        (b(  8) and (a( 68))) xor
        (b(  9) and (a( 67))) xor
        (b( 10) and (a( 66))) xor
        (b( 11) and (a( 65))) xor
        (b( 12) and (a( 64))) xor
        (b( 13) and (a( 63))) xor
        (b( 14) and (a( 62))) xor
        (b( 15) and (a( 61))) xor
        (b( 16) and (a( 60))) xor
        (b( 17) and (a( 59))) xor
        (b( 18) and (a( 58))) xor
        (b( 19) and (a( 57))) xor
        (b( 20) and (a( 56))) xor
        (b( 21) and (a( 55))) xor
        (b( 22) and (a( 54))) xor
        (b( 23) and (a( 53))) xor
        (b( 24) and (a( 52))) xor
        (b( 25) and (a( 51))) xor
        (b( 26) and (a( 50))) xor
        (b( 27) and (a( 49))) xor
        (b( 28) and (a( 48))) xor
        (b( 29) and (a( 47))) xor
        (b( 30) and (a( 46))) xor
        (b( 31) and (a( 45))) xor
        (b( 32) and (a( 44))) xor
        (b( 33) and (a( 43))) xor
        (b( 34) and (a( 42))) xor
        (b( 35) and (a( 41))) xor
        (b( 36) and (a( 40))) xor
        (b( 37) and (a( 39))) xor
        (b( 38) and (a( 38))) xor
        (b( 39) and (a( 37))) xor
        (b( 40) and (a( 36))) xor
        (b( 41) and (a( 35))) xor
        (b( 42) and (a( 34))) xor
        (b( 43) and (a( 33))) xor
        (b( 44) and (a( 32))) xor
        (b( 45) and (a( 31))) xor
        (b( 46) and (a( 30))) xor
        (b( 47) and (a( 29))) xor
        (b( 48) and (a( 28))) xor
        (b( 49) and (a( 27))) xor
        (b( 50) and (a( 26))) xor
        (b( 51) and (a( 25))) xor
        (b( 52) and (a( 24))) xor
        (b( 53) and (a( 23))) xor
        (b( 54) and (a( 22))) xor
        (b( 55) and (a( 21))) xor
        (b( 56) and (a( 20))) xor
        (b( 57) and (a( 19))) xor
        (b( 58) and (a( 18))) xor
        (b( 59) and (a( 17))) xor
        (b( 60) and (a( 16))) xor
        (b( 61) and (a( 15))) xor
        (b( 62) and (a( 14))) xor
        (b( 63) and (a( 13))) xor
        (b( 64) and (a( 12))) xor
        (b( 65) and (a( 11))) xor
        (b( 66) and (a( 10))) xor
        (b( 67) and (a(  9))) xor
        (b( 68) and (a(  8))) xor
        (b( 69) and (a(  7))) xor
        (b( 70) and (a(  6) xor a(127))) xor
        (b( 71) and (a(  5) xor a(126))) xor
        (b( 72) and (a(  4) xor a(125))) xor
        (b( 73) and (a(  3) xor a(124))) xor
        (b( 74) and (a(  2) xor a(123))) xor
        (b( 75) and (a(  1) xor a(122) xor a(127))) xor
        (b( 76) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 77) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 78) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 79) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 80) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 81) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 82) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 83) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 84) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 85) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 86) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 87) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 88) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 89) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 90) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 91) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 92) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 93) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 94) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 95) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 96) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 97) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 98) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b( 99) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(100) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(101) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(102) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(103) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(104) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(105) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(106) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(107) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(108) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(109) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(110) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(111) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(112) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(113) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(114) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(115) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(116) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(117) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(118) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(119) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(120) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(121) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(122) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(123) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(124) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(125) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(126) and (a( 71) xor a( 76) xor a( 77) xor a( 78))) xor
        (b(127) and (a( 70) xor a( 75) xor a( 76) xor a( 77)));
    c( 77) <= 
        (b(  0) and (a( 77))) xor
        (b(  1) and (a( 76))) xor
        (b(  2) and (a( 75))) xor
        (b(  3) and (a( 74))) xor
        (b(  4) and (a( 73))) xor
        (b(  5) and (a( 72))) xor
        (b(  6) and (a( 71))) xor
        (b(  7) and (a( 70))) xor
        (b(  8) and (a( 69))) xor
        (b(  9) and (a( 68))) xor
        (b( 10) and (a( 67))) xor
        (b( 11) and (a( 66))) xor
        (b( 12) and (a( 65))) xor
        (b( 13) and (a( 64))) xor
        (b( 14) and (a( 63))) xor
        (b( 15) and (a( 62))) xor
        (b( 16) and (a( 61))) xor
        (b( 17) and (a( 60))) xor
        (b( 18) and (a( 59))) xor
        (b( 19) and (a( 58))) xor
        (b( 20) and (a( 57))) xor
        (b( 21) and (a( 56))) xor
        (b( 22) and (a( 55))) xor
        (b( 23) and (a( 54))) xor
        (b( 24) and (a( 53))) xor
        (b( 25) and (a( 52))) xor
        (b( 26) and (a( 51))) xor
        (b( 27) and (a( 50))) xor
        (b( 28) and (a( 49))) xor
        (b( 29) and (a( 48))) xor
        (b( 30) and (a( 47))) xor
        (b( 31) and (a( 46))) xor
        (b( 32) and (a( 45))) xor
        (b( 33) and (a( 44))) xor
        (b( 34) and (a( 43))) xor
        (b( 35) and (a( 42))) xor
        (b( 36) and (a( 41))) xor
        (b( 37) and (a( 40))) xor
        (b( 38) and (a( 39))) xor
        (b( 39) and (a( 38))) xor
        (b( 40) and (a( 37))) xor
        (b( 41) and (a( 36))) xor
        (b( 42) and (a( 35))) xor
        (b( 43) and (a( 34))) xor
        (b( 44) and (a( 33))) xor
        (b( 45) and (a( 32))) xor
        (b( 46) and (a( 31))) xor
        (b( 47) and (a( 30))) xor
        (b( 48) and (a( 29))) xor
        (b( 49) and (a( 28))) xor
        (b( 50) and (a( 27))) xor
        (b( 51) and (a( 26))) xor
        (b( 52) and (a( 25))) xor
        (b( 53) and (a( 24))) xor
        (b( 54) and (a( 23))) xor
        (b( 55) and (a( 22))) xor
        (b( 56) and (a( 21))) xor
        (b( 57) and (a( 20))) xor
        (b( 58) and (a( 19))) xor
        (b( 59) and (a( 18))) xor
        (b( 60) and (a( 17))) xor
        (b( 61) and (a( 16))) xor
        (b( 62) and (a( 15))) xor
        (b( 63) and (a( 14))) xor
        (b( 64) and (a( 13))) xor
        (b( 65) and (a( 12))) xor
        (b( 66) and (a( 11))) xor
        (b( 67) and (a( 10))) xor
        (b( 68) and (a(  9))) xor
        (b( 69) and (a(  8))) xor
        (b( 70) and (a(  7))) xor
        (b( 71) and (a(  6) xor a(127))) xor
        (b( 72) and (a(  5) xor a(126))) xor
        (b( 73) and (a(  4) xor a(125))) xor
        (b( 74) and (a(  3) xor a(124))) xor
        (b( 75) and (a(  2) xor a(123))) xor
        (b( 76) and (a(  1) xor a(122) xor a(127))) xor
        (b( 77) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 78) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 79) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 80) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 81) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 82) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 83) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 84) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 85) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 86) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 87) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 88) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 89) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 90) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 91) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 92) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 93) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 94) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 95) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 96) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 97) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 98) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b( 99) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(100) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(101) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(102) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(103) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(104) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(105) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(106) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(107) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(108) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(109) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(110) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(111) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(112) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(113) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(114) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(115) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(116) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(117) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(118) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(119) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(120) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(121) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(122) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(123) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(124) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(125) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(126) and (a( 72) xor a( 77) xor a( 78) xor a( 79))) xor
        (b(127) and (a( 71) xor a( 76) xor a( 77) xor a( 78)));
    c( 78) <= 
        (b(  0) and (a( 78))) xor
        (b(  1) and (a( 77))) xor
        (b(  2) and (a( 76))) xor
        (b(  3) and (a( 75))) xor
        (b(  4) and (a( 74))) xor
        (b(  5) and (a( 73))) xor
        (b(  6) and (a( 72))) xor
        (b(  7) and (a( 71))) xor
        (b(  8) and (a( 70))) xor
        (b(  9) and (a( 69))) xor
        (b( 10) and (a( 68))) xor
        (b( 11) and (a( 67))) xor
        (b( 12) and (a( 66))) xor
        (b( 13) and (a( 65))) xor
        (b( 14) and (a( 64))) xor
        (b( 15) and (a( 63))) xor
        (b( 16) and (a( 62))) xor
        (b( 17) and (a( 61))) xor
        (b( 18) and (a( 60))) xor
        (b( 19) and (a( 59))) xor
        (b( 20) and (a( 58))) xor
        (b( 21) and (a( 57))) xor
        (b( 22) and (a( 56))) xor
        (b( 23) and (a( 55))) xor
        (b( 24) and (a( 54))) xor
        (b( 25) and (a( 53))) xor
        (b( 26) and (a( 52))) xor
        (b( 27) and (a( 51))) xor
        (b( 28) and (a( 50))) xor
        (b( 29) and (a( 49))) xor
        (b( 30) and (a( 48))) xor
        (b( 31) and (a( 47))) xor
        (b( 32) and (a( 46))) xor
        (b( 33) and (a( 45))) xor
        (b( 34) and (a( 44))) xor
        (b( 35) and (a( 43))) xor
        (b( 36) and (a( 42))) xor
        (b( 37) and (a( 41))) xor
        (b( 38) and (a( 40))) xor
        (b( 39) and (a( 39))) xor
        (b( 40) and (a( 38))) xor
        (b( 41) and (a( 37))) xor
        (b( 42) and (a( 36))) xor
        (b( 43) and (a( 35))) xor
        (b( 44) and (a( 34))) xor
        (b( 45) and (a( 33))) xor
        (b( 46) and (a( 32))) xor
        (b( 47) and (a( 31))) xor
        (b( 48) and (a( 30))) xor
        (b( 49) and (a( 29))) xor
        (b( 50) and (a( 28))) xor
        (b( 51) and (a( 27))) xor
        (b( 52) and (a( 26))) xor
        (b( 53) and (a( 25))) xor
        (b( 54) and (a( 24))) xor
        (b( 55) and (a( 23))) xor
        (b( 56) and (a( 22))) xor
        (b( 57) and (a( 21))) xor
        (b( 58) and (a( 20))) xor
        (b( 59) and (a( 19))) xor
        (b( 60) and (a( 18))) xor
        (b( 61) and (a( 17))) xor
        (b( 62) and (a( 16))) xor
        (b( 63) and (a( 15))) xor
        (b( 64) and (a( 14))) xor
        (b( 65) and (a( 13))) xor
        (b( 66) and (a( 12))) xor
        (b( 67) and (a( 11))) xor
        (b( 68) and (a( 10))) xor
        (b( 69) and (a(  9))) xor
        (b( 70) and (a(  8))) xor
        (b( 71) and (a(  7))) xor
        (b( 72) and (a(  6) xor a(127))) xor
        (b( 73) and (a(  5) xor a(126))) xor
        (b( 74) and (a(  4) xor a(125))) xor
        (b( 75) and (a(  3) xor a(124))) xor
        (b( 76) and (a(  2) xor a(123))) xor
        (b( 77) and (a(  1) xor a(122) xor a(127))) xor
        (b( 78) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 79) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 80) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 81) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 82) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 83) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 84) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 85) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 86) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 87) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 88) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 89) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 90) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 91) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 92) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 93) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 94) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 95) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 96) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 97) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 98) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b( 99) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(100) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(101) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(102) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(103) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(104) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(105) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(106) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(107) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(108) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(109) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(110) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(111) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(112) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(113) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(114) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(115) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(116) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(117) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(118) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(119) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(120) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(121) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(122) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(123) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(124) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(125) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(126) and (a( 73) xor a( 78) xor a( 79) xor a( 80))) xor
        (b(127) and (a( 72) xor a( 77) xor a( 78) xor a( 79)));
    c( 79) <= 
        (b(  0) and (a( 79))) xor
        (b(  1) and (a( 78))) xor
        (b(  2) and (a( 77))) xor
        (b(  3) and (a( 76))) xor
        (b(  4) and (a( 75))) xor
        (b(  5) and (a( 74))) xor
        (b(  6) and (a( 73))) xor
        (b(  7) and (a( 72))) xor
        (b(  8) and (a( 71))) xor
        (b(  9) and (a( 70))) xor
        (b( 10) and (a( 69))) xor
        (b( 11) and (a( 68))) xor
        (b( 12) and (a( 67))) xor
        (b( 13) and (a( 66))) xor
        (b( 14) and (a( 65))) xor
        (b( 15) and (a( 64))) xor
        (b( 16) and (a( 63))) xor
        (b( 17) and (a( 62))) xor
        (b( 18) and (a( 61))) xor
        (b( 19) and (a( 60))) xor
        (b( 20) and (a( 59))) xor
        (b( 21) and (a( 58))) xor
        (b( 22) and (a( 57))) xor
        (b( 23) and (a( 56))) xor
        (b( 24) and (a( 55))) xor
        (b( 25) and (a( 54))) xor
        (b( 26) and (a( 53))) xor
        (b( 27) and (a( 52))) xor
        (b( 28) and (a( 51))) xor
        (b( 29) and (a( 50))) xor
        (b( 30) and (a( 49))) xor
        (b( 31) and (a( 48))) xor
        (b( 32) and (a( 47))) xor
        (b( 33) and (a( 46))) xor
        (b( 34) and (a( 45))) xor
        (b( 35) and (a( 44))) xor
        (b( 36) and (a( 43))) xor
        (b( 37) and (a( 42))) xor
        (b( 38) and (a( 41))) xor
        (b( 39) and (a( 40))) xor
        (b( 40) and (a( 39))) xor
        (b( 41) and (a( 38))) xor
        (b( 42) and (a( 37))) xor
        (b( 43) and (a( 36))) xor
        (b( 44) and (a( 35))) xor
        (b( 45) and (a( 34))) xor
        (b( 46) and (a( 33))) xor
        (b( 47) and (a( 32))) xor
        (b( 48) and (a( 31))) xor
        (b( 49) and (a( 30))) xor
        (b( 50) and (a( 29))) xor
        (b( 51) and (a( 28))) xor
        (b( 52) and (a( 27))) xor
        (b( 53) and (a( 26))) xor
        (b( 54) and (a( 25))) xor
        (b( 55) and (a( 24))) xor
        (b( 56) and (a( 23))) xor
        (b( 57) and (a( 22))) xor
        (b( 58) and (a( 21))) xor
        (b( 59) and (a( 20))) xor
        (b( 60) and (a( 19))) xor
        (b( 61) and (a( 18))) xor
        (b( 62) and (a( 17))) xor
        (b( 63) and (a( 16))) xor
        (b( 64) and (a( 15))) xor
        (b( 65) and (a( 14))) xor
        (b( 66) and (a( 13))) xor
        (b( 67) and (a( 12))) xor
        (b( 68) and (a( 11))) xor
        (b( 69) and (a( 10))) xor
        (b( 70) and (a(  9))) xor
        (b( 71) and (a(  8))) xor
        (b( 72) and (a(  7))) xor
        (b( 73) and (a(  6) xor a(127))) xor
        (b( 74) and (a(  5) xor a(126))) xor
        (b( 75) and (a(  4) xor a(125))) xor
        (b( 76) and (a(  3) xor a(124))) xor
        (b( 77) and (a(  2) xor a(123))) xor
        (b( 78) and (a(  1) xor a(122) xor a(127))) xor
        (b( 79) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 80) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 81) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 82) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 83) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 84) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 85) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 86) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 87) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 88) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 89) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 90) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 91) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 92) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 93) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 94) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 95) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 96) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 97) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 98) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b( 99) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(100) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(101) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(102) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(103) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(104) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(105) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(106) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(107) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(108) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(109) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(110) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(111) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(112) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(113) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(114) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(115) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(116) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(117) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(118) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(119) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(120) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(121) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(122) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(123) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(124) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(125) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(126) and (a( 74) xor a( 79) xor a( 80) xor a( 81))) xor
        (b(127) and (a( 73) xor a( 78) xor a( 79) xor a( 80)));
    c( 80) <= 
        (b(  0) and (a( 80))) xor
        (b(  1) and (a( 79))) xor
        (b(  2) and (a( 78))) xor
        (b(  3) and (a( 77))) xor
        (b(  4) and (a( 76))) xor
        (b(  5) and (a( 75))) xor
        (b(  6) and (a( 74))) xor
        (b(  7) and (a( 73))) xor
        (b(  8) and (a( 72))) xor
        (b(  9) and (a( 71))) xor
        (b( 10) and (a( 70))) xor
        (b( 11) and (a( 69))) xor
        (b( 12) and (a( 68))) xor
        (b( 13) and (a( 67))) xor
        (b( 14) and (a( 66))) xor
        (b( 15) and (a( 65))) xor
        (b( 16) and (a( 64))) xor
        (b( 17) and (a( 63))) xor
        (b( 18) and (a( 62))) xor
        (b( 19) and (a( 61))) xor
        (b( 20) and (a( 60))) xor
        (b( 21) and (a( 59))) xor
        (b( 22) and (a( 58))) xor
        (b( 23) and (a( 57))) xor
        (b( 24) and (a( 56))) xor
        (b( 25) and (a( 55))) xor
        (b( 26) and (a( 54))) xor
        (b( 27) and (a( 53))) xor
        (b( 28) and (a( 52))) xor
        (b( 29) and (a( 51))) xor
        (b( 30) and (a( 50))) xor
        (b( 31) and (a( 49))) xor
        (b( 32) and (a( 48))) xor
        (b( 33) and (a( 47))) xor
        (b( 34) and (a( 46))) xor
        (b( 35) and (a( 45))) xor
        (b( 36) and (a( 44))) xor
        (b( 37) and (a( 43))) xor
        (b( 38) and (a( 42))) xor
        (b( 39) and (a( 41))) xor
        (b( 40) and (a( 40))) xor
        (b( 41) and (a( 39))) xor
        (b( 42) and (a( 38))) xor
        (b( 43) and (a( 37))) xor
        (b( 44) and (a( 36))) xor
        (b( 45) and (a( 35))) xor
        (b( 46) and (a( 34))) xor
        (b( 47) and (a( 33))) xor
        (b( 48) and (a( 32))) xor
        (b( 49) and (a( 31))) xor
        (b( 50) and (a( 30))) xor
        (b( 51) and (a( 29))) xor
        (b( 52) and (a( 28))) xor
        (b( 53) and (a( 27))) xor
        (b( 54) and (a( 26))) xor
        (b( 55) and (a( 25))) xor
        (b( 56) and (a( 24))) xor
        (b( 57) and (a( 23))) xor
        (b( 58) and (a( 22))) xor
        (b( 59) and (a( 21))) xor
        (b( 60) and (a( 20))) xor
        (b( 61) and (a( 19))) xor
        (b( 62) and (a( 18))) xor
        (b( 63) and (a( 17))) xor
        (b( 64) and (a( 16))) xor
        (b( 65) and (a( 15))) xor
        (b( 66) and (a( 14))) xor
        (b( 67) and (a( 13))) xor
        (b( 68) and (a( 12))) xor
        (b( 69) and (a( 11))) xor
        (b( 70) and (a( 10))) xor
        (b( 71) and (a(  9))) xor
        (b( 72) and (a(  8))) xor
        (b( 73) and (a(  7))) xor
        (b( 74) and (a(  6) xor a(127))) xor
        (b( 75) and (a(  5) xor a(126))) xor
        (b( 76) and (a(  4) xor a(125))) xor
        (b( 77) and (a(  3) xor a(124))) xor
        (b( 78) and (a(  2) xor a(123))) xor
        (b( 79) and (a(  1) xor a(122) xor a(127))) xor
        (b( 80) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 81) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 82) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 83) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 84) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 85) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 86) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 87) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 88) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 89) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 90) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 91) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 92) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 93) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 94) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 95) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 96) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 97) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 98) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b( 99) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(100) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(101) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(102) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(103) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(104) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(105) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(106) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(107) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(108) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(109) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(110) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(111) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(112) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(113) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(114) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(115) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(116) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(117) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(118) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(119) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(120) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(121) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(122) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(123) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(124) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(125) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(126) and (a( 75) xor a( 80) xor a( 81) xor a( 82))) xor
        (b(127) and (a( 74) xor a( 79) xor a( 80) xor a( 81)));
    c( 81) <= 
        (b(  0) and (a( 81))) xor
        (b(  1) and (a( 80))) xor
        (b(  2) and (a( 79))) xor
        (b(  3) and (a( 78))) xor
        (b(  4) and (a( 77))) xor
        (b(  5) and (a( 76))) xor
        (b(  6) and (a( 75))) xor
        (b(  7) and (a( 74))) xor
        (b(  8) and (a( 73))) xor
        (b(  9) and (a( 72))) xor
        (b( 10) and (a( 71))) xor
        (b( 11) and (a( 70))) xor
        (b( 12) and (a( 69))) xor
        (b( 13) and (a( 68))) xor
        (b( 14) and (a( 67))) xor
        (b( 15) and (a( 66))) xor
        (b( 16) and (a( 65))) xor
        (b( 17) and (a( 64))) xor
        (b( 18) and (a( 63))) xor
        (b( 19) and (a( 62))) xor
        (b( 20) and (a( 61))) xor
        (b( 21) and (a( 60))) xor
        (b( 22) and (a( 59))) xor
        (b( 23) and (a( 58))) xor
        (b( 24) and (a( 57))) xor
        (b( 25) and (a( 56))) xor
        (b( 26) and (a( 55))) xor
        (b( 27) and (a( 54))) xor
        (b( 28) and (a( 53))) xor
        (b( 29) and (a( 52))) xor
        (b( 30) and (a( 51))) xor
        (b( 31) and (a( 50))) xor
        (b( 32) and (a( 49))) xor
        (b( 33) and (a( 48))) xor
        (b( 34) and (a( 47))) xor
        (b( 35) and (a( 46))) xor
        (b( 36) and (a( 45))) xor
        (b( 37) and (a( 44))) xor
        (b( 38) and (a( 43))) xor
        (b( 39) and (a( 42))) xor
        (b( 40) and (a( 41))) xor
        (b( 41) and (a( 40))) xor
        (b( 42) and (a( 39))) xor
        (b( 43) and (a( 38))) xor
        (b( 44) and (a( 37))) xor
        (b( 45) and (a( 36))) xor
        (b( 46) and (a( 35))) xor
        (b( 47) and (a( 34))) xor
        (b( 48) and (a( 33))) xor
        (b( 49) and (a( 32))) xor
        (b( 50) and (a( 31))) xor
        (b( 51) and (a( 30))) xor
        (b( 52) and (a( 29))) xor
        (b( 53) and (a( 28))) xor
        (b( 54) and (a( 27))) xor
        (b( 55) and (a( 26))) xor
        (b( 56) and (a( 25))) xor
        (b( 57) and (a( 24))) xor
        (b( 58) and (a( 23))) xor
        (b( 59) and (a( 22))) xor
        (b( 60) and (a( 21))) xor
        (b( 61) and (a( 20))) xor
        (b( 62) and (a( 19))) xor
        (b( 63) and (a( 18))) xor
        (b( 64) and (a( 17))) xor
        (b( 65) and (a( 16))) xor
        (b( 66) and (a( 15))) xor
        (b( 67) and (a( 14))) xor
        (b( 68) and (a( 13))) xor
        (b( 69) and (a( 12))) xor
        (b( 70) and (a( 11))) xor
        (b( 71) and (a( 10))) xor
        (b( 72) and (a(  9))) xor
        (b( 73) and (a(  8))) xor
        (b( 74) and (a(  7))) xor
        (b( 75) and (a(  6) xor a(127))) xor
        (b( 76) and (a(  5) xor a(126))) xor
        (b( 77) and (a(  4) xor a(125))) xor
        (b( 78) and (a(  3) xor a(124))) xor
        (b( 79) and (a(  2) xor a(123))) xor
        (b( 80) and (a(  1) xor a(122) xor a(127))) xor
        (b( 81) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 82) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 83) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 84) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 85) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 86) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 87) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 88) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 89) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 90) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 91) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 92) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 93) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 94) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 95) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 96) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 97) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 98) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b( 99) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(100) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(101) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(102) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(103) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(104) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(105) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(106) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(107) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(108) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(109) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(110) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(111) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(112) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(113) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(114) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(115) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(116) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(117) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(118) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(119) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(120) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(121) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(122) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(123) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(124) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(125) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(126) and (a( 76) xor a( 81) xor a( 82) xor a( 83))) xor
        (b(127) and (a( 75) xor a( 80) xor a( 81) xor a( 82)));
    c( 82) <= 
        (b(  0) and (a( 82))) xor
        (b(  1) and (a( 81))) xor
        (b(  2) and (a( 80))) xor
        (b(  3) and (a( 79))) xor
        (b(  4) and (a( 78))) xor
        (b(  5) and (a( 77))) xor
        (b(  6) and (a( 76))) xor
        (b(  7) and (a( 75))) xor
        (b(  8) and (a( 74))) xor
        (b(  9) and (a( 73))) xor
        (b( 10) and (a( 72))) xor
        (b( 11) and (a( 71))) xor
        (b( 12) and (a( 70))) xor
        (b( 13) and (a( 69))) xor
        (b( 14) and (a( 68))) xor
        (b( 15) and (a( 67))) xor
        (b( 16) and (a( 66))) xor
        (b( 17) and (a( 65))) xor
        (b( 18) and (a( 64))) xor
        (b( 19) and (a( 63))) xor
        (b( 20) and (a( 62))) xor
        (b( 21) and (a( 61))) xor
        (b( 22) and (a( 60))) xor
        (b( 23) and (a( 59))) xor
        (b( 24) and (a( 58))) xor
        (b( 25) and (a( 57))) xor
        (b( 26) and (a( 56))) xor
        (b( 27) and (a( 55))) xor
        (b( 28) and (a( 54))) xor
        (b( 29) and (a( 53))) xor
        (b( 30) and (a( 52))) xor
        (b( 31) and (a( 51))) xor
        (b( 32) and (a( 50))) xor
        (b( 33) and (a( 49))) xor
        (b( 34) and (a( 48))) xor
        (b( 35) and (a( 47))) xor
        (b( 36) and (a( 46))) xor
        (b( 37) and (a( 45))) xor
        (b( 38) and (a( 44))) xor
        (b( 39) and (a( 43))) xor
        (b( 40) and (a( 42))) xor
        (b( 41) and (a( 41))) xor
        (b( 42) and (a( 40))) xor
        (b( 43) and (a( 39))) xor
        (b( 44) and (a( 38))) xor
        (b( 45) and (a( 37))) xor
        (b( 46) and (a( 36))) xor
        (b( 47) and (a( 35))) xor
        (b( 48) and (a( 34))) xor
        (b( 49) and (a( 33))) xor
        (b( 50) and (a( 32))) xor
        (b( 51) and (a( 31))) xor
        (b( 52) and (a( 30))) xor
        (b( 53) and (a( 29))) xor
        (b( 54) and (a( 28))) xor
        (b( 55) and (a( 27))) xor
        (b( 56) and (a( 26))) xor
        (b( 57) and (a( 25))) xor
        (b( 58) and (a( 24))) xor
        (b( 59) and (a( 23))) xor
        (b( 60) and (a( 22))) xor
        (b( 61) and (a( 21))) xor
        (b( 62) and (a( 20))) xor
        (b( 63) and (a( 19))) xor
        (b( 64) and (a( 18))) xor
        (b( 65) and (a( 17))) xor
        (b( 66) and (a( 16))) xor
        (b( 67) and (a( 15))) xor
        (b( 68) and (a( 14))) xor
        (b( 69) and (a( 13))) xor
        (b( 70) and (a( 12))) xor
        (b( 71) and (a( 11))) xor
        (b( 72) and (a( 10))) xor
        (b( 73) and (a(  9))) xor
        (b( 74) and (a(  8))) xor
        (b( 75) and (a(  7))) xor
        (b( 76) and (a(  6) xor a(127))) xor
        (b( 77) and (a(  5) xor a(126))) xor
        (b( 78) and (a(  4) xor a(125))) xor
        (b( 79) and (a(  3) xor a(124))) xor
        (b( 80) and (a(  2) xor a(123))) xor
        (b( 81) and (a(  1) xor a(122) xor a(127))) xor
        (b( 82) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 83) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 84) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 85) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 86) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 87) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 88) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 89) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 90) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 91) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 92) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 93) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 94) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 95) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 96) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 97) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 98) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b( 99) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(100) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(101) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(102) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(103) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(104) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(105) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(106) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(107) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(108) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(109) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(110) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(111) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(112) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(113) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(114) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(115) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(116) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(117) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(118) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(119) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(120) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(121) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(122) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(123) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(124) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(125) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(126) and (a( 77) xor a( 82) xor a( 83) xor a( 84))) xor
        (b(127) and (a( 76) xor a( 81) xor a( 82) xor a( 83)));
    c( 83) <= 
        (b(  0) and (a( 83))) xor
        (b(  1) and (a( 82))) xor
        (b(  2) and (a( 81))) xor
        (b(  3) and (a( 80))) xor
        (b(  4) and (a( 79))) xor
        (b(  5) and (a( 78))) xor
        (b(  6) and (a( 77))) xor
        (b(  7) and (a( 76))) xor
        (b(  8) and (a( 75))) xor
        (b(  9) and (a( 74))) xor
        (b( 10) and (a( 73))) xor
        (b( 11) and (a( 72))) xor
        (b( 12) and (a( 71))) xor
        (b( 13) and (a( 70))) xor
        (b( 14) and (a( 69))) xor
        (b( 15) and (a( 68))) xor
        (b( 16) and (a( 67))) xor
        (b( 17) and (a( 66))) xor
        (b( 18) and (a( 65))) xor
        (b( 19) and (a( 64))) xor
        (b( 20) and (a( 63))) xor
        (b( 21) and (a( 62))) xor
        (b( 22) and (a( 61))) xor
        (b( 23) and (a( 60))) xor
        (b( 24) and (a( 59))) xor
        (b( 25) and (a( 58))) xor
        (b( 26) and (a( 57))) xor
        (b( 27) and (a( 56))) xor
        (b( 28) and (a( 55))) xor
        (b( 29) and (a( 54))) xor
        (b( 30) and (a( 53))) xor
        (b( 31) and (a( 52))) xor
        (b( 32) and (a( 51))) xor
        (b( 33) and (a( 50))) xor
        (b( 34) and (a( 49))) xor
        (b( 35) and (a( 48))) xor
        (b( 36) and (a( 47))) xor
        (b( 37) and (a( 46))) xor
        (b( 38) and (a( 45))) xor
        (b( 39) and (a( 44))) xor
        (b( 40) and (a( 43))) xor
        (b( 41) and (a( 42))) xor
        (b( 42) and (a( 41))) xor
        (b( 43) and (a( 40))) xor
        (b( 44) and (a( 39))) xor
        (b( 45) and (a( 38))) xor
        (b( 46) and (a( 37))) xor
        (b( 47) and (a( 36))) xor
        (b( 48) and (a( 35))) xor
        (b( 49) and (a( 34))) xor
        (b( 50) and (a( 33))) xor
        (b( 51) and (a( 32))) xor
        (b( 52) and (a( 31))) xor
        (b( 53) and (a( 30))) xor
        (b( 54) and (a( 29))) xor
        (b( 55) and (a( 28))) xor
        (b( 56) and (a( 27))) xor
        (b( 57) and (a( 26))) xor
        (b( 58) and (a( 25))) xor
        (b( 59) and (a( 24))) xor
        (b( 60) and (a( 23))) xor
        (b( 61) and (a( 22))) xor
        (b( 62) and (a( 21))) xor
        (b( 63) and (a( 20))) xor
        (b( 64) and (a( 19))) xor
        (b( 65) and (a( 18))) xor
        (b( 66) and (a( 17))) xor
        (b( 67) and (a( 16))) xor
        (b( 68) and (a( 15))) xor
        (b( 69) and (a( 14))) xor
        (b( 70) and (a( 13))) xor
        (b( 71) and (a( 12))) xor
        (b( 72) and (a( 11))) xor
        (b( 73) and (a( 10))) xor
        (b( 74) and (a(  9))) xor
        (b( 75) and (a(  8))) xor
        (b( 76) and (a(  7))) xor
        (b( 77) and (a(  6) xor a(127))) xor
        (b( 78) and (a(  5) xor a(126))) xor
        (b( 79) and (a(  4) xor a(125))) xor
        (b( 80) and (a(  3) xor a(124))) xor
        (b( 81) and (a(  2) xor a(123))) xor
        (b( 82) and (a(  1) xor a(122) xor a(127))) xor
        (b( 83) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 84) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 85) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 86) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 87) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 88) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 89) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 90) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 91) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 92) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 93) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 94) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 95) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 96) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 97) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 98) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b( 99) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(100) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(101) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(102) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(103) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(104) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(105) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(106) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(107) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(108) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(109) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(110) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(111) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(112) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(113) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(114) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(115) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(116) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(117) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(118) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(119) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(120) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(121) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(122) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(123) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(124) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(125) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(126) and (a( 78) xor a( 83) xor a( 84) xor a( 85))) xor
        (b(127) and (a( 77) xor a( 82) xor a( 83) xor a( 84)));
    c( 84) <= 
        (b(  0) and (a( 84))) xor
        (b(  1) and (a( 83))) xor
        (b(  2) and (a( 82))) xor
        (b(  3) and (a( 81))) xor
        (b(  4) and (a( 80))) xor
        (b(  5) and (a( 79))) xor
        (b(  6) and (a( 78))) xor
        (b(  7) and (a( 77))) xor
        (b(  8) and (a( 76))) xor
        (b(  9) and (a( 75))) xor
        (b( 10) and (a( 74))) xor
        (b( 11) and (a( 73))) xor
        (b( 12) and (a( 72))) xor
        (b( 13) and (a( 71))) xor
        (b( 14) and (a( 70))) xor
        (b( 15) and (a( 69))) xor
        (b( 16) and (a( 68))) xor
        (b( 17) and (a( 67))) xor
        (b( 18) and (a( 66))) xor
        (b( 19) and (a( 65))) xor
        (b( 20) and (a( 64))) xor
        (b( 21) and (a( 63))) xor
        (b( 22) and (a( 62))) xor
        (b( 23) and (a( 61))) xor
        (b( 24) and (a( 60))) xor
        (b( 25) and (a( 59))) xor
        (b( 26) and (a( 58))) xor
        (b( 27) and (a( 57))) xor
        (b( 28) and (a( 56))) xor
        (b( 29) and (a( 55))) xor
        (b( 30) and (a( 54))) xor
        (b( 31) and (a( 53))) xor
        (b( 32) and (a( 52))) xor
        (b( 33) and (a( 51))) xor
        (b( 34) and (a( 50))) xor
        (b( 35) and (a( 49))) xor
        (b( 36) and (a( 48))) xor
        (b( 37) and (a( 47))) xor
        (b( 38) and (a( 46))) xor
        (b( 39) and (a( 45))) xor
        (b( 40) and (a( 44))) xor
        (b( 41) and (a( 43))) xor
        (b( 42) and (a( 42))) xor
        (b( 43) and (a( 41))) xor
        (b( 44) and (a( 40))) xor
        (b( 45) and (a( 39))) xor
        (b( 46) and (a( 38))) xor
        (b( 47) and (a( 37))) xor
        (b( 48) and (a( 36))) xor
        (b( 49) and (a( 35))) xor
        (b( 50) and (a( 34))) xor
        (b( 51) and (a( 33))) xor
        (b( 52) and (a( 32))) xor
        (b( 53) and (a( 31))) xor
        (b( 54) and (a( 30))) xor
        (b( 55) and (a( 29))) xor
        (b( 56) and (a( 28))) xor
        (b( 57) and (a( 27))) xor
        (b( 58) and (a( 26))) xor
        (b( 59) and (a( 25))) xor
        (b( 60) and (a( 24))) xor
        (b( 61) and (a( 23))) xor
        (b( 62) and (a( 22))) xor
        (b( 63) and (a( 21))) xor
        (b( 64) and (a( 20))) xor
        (b( 65) and (a( 19))) xor
        (b( 66) and (a( 18))) xor
        (b( 67) and (a( 17))) xor
        (b( 68) and (a( 16))) xor
        (b( 69) and (a( 15))) xor
        (b( 70) and (a( 14))) xor
        (b( 71) and (a( 13))) xor
        (b( 72) and (a( 12))) xor
        (b( 73) and (a( 11))) xor
        (b( 74) and (a( 10))) xor
        (b( 75) and (a(  9))) xor
        (b( 76) and (a(  8))) xor
        (b( 77) and (a(  7))) xor
        (b( 78) and (a(  6) xor a(127))) xor
        (b( 79) and (a(  5) xor a(126))) xor
        (b( 80) and (a(  4) xor a(125))) xor
        (b( 81) and (a(  3) xor a(124))) xor
        (b( 82) and (a(  2) xor a(123))) xor
        (b( 83) and (a(  1) xor a(122) xor a(127))) xor
        (b( 84) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 85) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 86) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 87) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 88) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 89) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 90) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 91) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 92) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 93) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 94) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 95) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 96) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 97) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 98) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b( 99) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(100) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(101) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(102) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(103) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(104) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(105) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(106) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(107) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(108) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(109) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(110) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(111) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(112) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(113) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(114) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(115) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(116) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(117) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(118) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(119) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(120) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(121) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(122) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(123) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(124) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(125) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(126) and (a( 79) xor a( 84) xor a( 85) xor a( 86))) xor
        (b(127) and (a( 78) xor a( 83) xor a( 84) xor a( 85)));
    c( 85) <= 
        (b(  0) and (a( 85))) xor
        (b(  1) and (a( 84))) xor
        (b(  2) and (a( 83))) xor
        (b(  3) and (a( 82))) xor
        (b(  4) and (a( 81))) xor
        (b(  5) and (a( 80))) xor
        (b(  6) and (a( 79))) xor
        (b(  7) and (a( 78))) xor
        (b(  8) and (a( 77))) xor
        (b(  9) and (a( 76))) xor
        (b( 10) and (a( 75))) xor
        (b( 11) and (a( 74))) xor
        (b( 12) and (a( 73))) xor
        (b( 13) and (a( 72))) xor
        (b( 14) and (a( 71))) xor
        (b( 15) and (a( 70))) xor
        (b( 16) and (a( 69))) xor
        (b( 17) and (a( 68))) xor
        (b( 18) and (a( 67))) xor
        (b( 19) and (a( 66))) xor
        (b( 20) and (a( 65))) xor
        (b( 21) and (a( 64))) xor
        (b( 22) and (a( 63))) xor
        (b( 23) and (a( 62))) xor
        (b( 24) and (a( 61))) xor
        (b( 25) and (a( 60))) xor
        (b( 26) and (a( 59))) xor
        (b( 27) and (a( 58))) xor
        (b( 28) and (a( 57))) xor
        (b( 29) and (a( 56))) xor
        (b( 30) and (a( 55))) xor
        (b( 31) and (a( 54))) xor
        (b( 32) and (a( 53))) xor
        (b( 33) and (a( 52))) xor
        (b( 34) and (a( 51))) xor
        (b( 35) and (a( 50))) xor
        (b( 36) and (a( 49))) xor
        (b( 37) and (a( 48))) xor
        (b( 38) and (a( 47))) xor
        (b( 39) and (a( 46))) xor
        (b( 40) and (a( 45))) xor
        (b( 41) and (a( 44))) xor
        (b( 42) and (a( 43))) xor
        (b( 43) and (a( 42))) xor
        (b( 44) and (a( 41))) xor
        (b( 45) and (a( 40))) xor
        (b( 46) and (a( 39))) xor
        (b( 47) and (a( 38))) xor
        (b( 48) and (a( 37))) xor
        (b( 49) and (a( 36))) xor
        (b( 50) and (a( 35))) xor
        (b( 51) and (a( 34))) xor
        (b( 52) and (a( 33))) xor
        (b( 53) and (a( 32))) xor
        (b( 54) and (a( 31))) xor
        (b( 55) and (a( 30))) xor
        (b( 56) and (a( 29))) xor
        (b( 57) and (a( 28))) xor
        (b( 58) and (a( 27))) xor
        (b( 59) and (a( 26))) xor
        (b( 60) and (a( 25))) xor
        (b( 61) and (a( 24))) xor
        (b( 62) and (a( 23))) xor
        (b( 63) and (a( 22))) xor
        (b( 64) and (a( 21))) xor
        (b( 65) and (a( 20))) xor
        (b( 66) and (a( 19))) xor
        (b( 67) and (a( 18))) xor
        (b( 68) and (a( 17))) xor
        (b( 69) and (a( 16))) xor
        (b( 70) and (a( 15))) xor
        (b( 71) and (a( 14))) xor
        (b( 72) and (a( 13))) xor
        (b( 73) and (a( 12))) xor
        (b( 74) and (a( 11))) xor
        (b( 75) and (a( 10))) xor
        (b( 76) and (a(  9))) xor
        (b( 77) and (a(  8))) xor
        (b( 78) and (a(  7))) xor
        (b( 79) and (a(  6) xor a(127))) xor
        (b( 80) and (a(  5) xor a(126))) xor
        (b( 81) and (a(  4) xor a(125))) xor
        (b( 82) and (a(  3) xor a(124))) xor
        (b( 83) and (a(  2) xor a(123))) xor
        (b( 84) and (a(  1) xor a(122) xor a(127))) xor
        (b( 85) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 86) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 87) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 88) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 89) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 90) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 91) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 92) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 93) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 94) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 95) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 96) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 97) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 98) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b( 99) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(100) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(101) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(102) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(103) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(104) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(105) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(106) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(107) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(108) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(109) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(110) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(111) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(112) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(113) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(114) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(115) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(116) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(117) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(118) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(119) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(120) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(121) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(122) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(123) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(124) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(125) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(126) and (a( 80) xor a( 85) xor a( 86) xor a( 87))) xor
        (b(127) and (a( 79) xor a( 84) xor a( 85) xor a( 86)));
    c( 86) <= 
        (b(  0) and (a( 86))) xor
        (b(  1) and (a( 85))) xor
        (b(  2) and (a( 84))) xor
        (b(  3) and (a( 83))) xor
        (b(  4) and (a( 82))) xor
        (b(  5) and (a( 81))) xor
        (b(  6) and (a( 80))) xor
        (b(  7) and (a( 79))) xor
        (b(  8) and (a( 78))) xor
        (b(  9) and (a( 77))) xor
        (b( 10) and (a( 76))) xor
        (b( 11) and (a( 75))) xor
        (b( 12) and (a( 74))) xor
        (b( 13) and (a( 73))) xor
        (b( 14) and (a( 72))) xor
        (b( 15) and (a( 71))) xor
        (b( 16) and (a( 70))) xor
        (b( 17) and (a( 69))) xor
        (b( 18) and (a( 68))) xor
        (b( 19) and (a( 67))) xor
        (b( 20) and (a( 66))) xor
        (b( 21) and (a( 65))) xor
        (b( 22) and (a( 64))) xor
        (b( 23) and (a( 63))) xor
        (b( 24) and (a( 62))) xor
        (b( 25) and (a( 61))) xor
        (b( 26) and (a( 60))) xor
        (b( 27) and (a( 59))) xor
        (b( 28) and (a( 58))) xor
        (b( 29) and (a( 57))) xor
        (b( 30) and (a( 56))) xor
        (b( 31) and (a( 55))) xor
        (b( 32) and (a( 54))) xor
        (b( 33) and (a( 53))) xor
        (b( 34) and (a( 52))) xor
        (b( 35) and (a( 51))) xor
        (b( 36) and (a( 50))) xor
        (b( 37) and (a( 49))) xor
        (b( 38) and (a( 48))) xor
        (b( 39) and (a( 47))) xor
        (b( 40) and (a( 46))) xor
        (b( 41) and (a( 45))) xor
        (b( 42) and (a( 44))) xor
        (b( 43) and (a( 43))) xor
        (b( 44) and (a( 42))) xor
        (b( 45) and (a( 41))) xor
        (b( 46) and (a( 40))) xor
        (b( 47) and (a( 39))) xor
        (b( 48) and (a( 38))) xor
        (b( 49) and (a( 37))) xor
        (b( 50) and (a( 36))) xor
        (b( 51) and (a( 35))) xor
        (b( 52) and (a( 34))) xor
        (b( 53) and (a( 33))) xor
        (b( 54) and (a( 32))) xor
        (b( 55) and (a( 31))) xor
        (b( 56) and (a( 30))) xor
        (b( 57) and (a( 29))) xor
        (b( 58) and (a( 28))) xor
        (b( 59) and (a( 27))) xor
        (b( 60) and (a( 26))) xor
        (b( 61) and (a( 25))) xor
        (b( 62) and (a( 24))) xor
        (b( 63) and (a( 23))) xor
        (b( 64) and (a( 22))) xor
        (b( 65) and (a( 21))) xor
        (b( 66) and (a( 20))) xor
        (b( 67) and (a( 19))) xor
        (b( 68) and (a( 18))) xor
        (b( 69) and (a( 17))) xor
        (b( 70) and (a( 16))) xor
        (b( 71) and (a( 15))) xor
        (b( 72) and (a( 14))) xor
        (b( 73) and (a( 13))) xor
        (b( 74) and (a( 12))) xor
        (b( 75) and (a( 11))) xor
        (b( 76) and (a( 10))) xor
        (b( 77) and (a(  9))) xor
        (b( 78) and (a(  8))) xor
        (b( 79) and (a(  7))) xor
        (b( 80) and (a(  6) xor a(127))) xor
        (b( 81) and (a(  5) xor a(126))) xor
        (b( 82) and (a(  4) xor a(125))) xor
        (b( 83) and (a(  3) xor a(124))) xor
        (b( 84) and (a(  2) xor a(123))) xor
        (b( 85) and (a(  1) xor a(122) xor a(127))) xor
        (b( 86) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 87) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 88) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 89) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 90) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 91) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 92) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 93) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 94) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 95) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 96) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 97) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 98) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b( 99) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(100) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(101) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(102) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(103) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(104) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(105) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(106) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(107) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(108) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(109) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(110) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(111) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(112) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(113) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(114) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(115) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(116) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(117) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(118) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(119) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(120) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(121) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(122) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(123) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(124) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(125) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(126) and (a( 81) xor a( 86) xor a( 87) xor a( 88))) xor
        (b(127) and (a( 80) xor a( 85) xor a( 86) xor a( 87)));
    c( 87) <= 
        (b(  0) and (a( 87))) xor
        (b(  1) and (a( 86))) xor
        (b(  2) and (a( 85))) xor
        (b(  3) and (a( 84))) xor
        (b(  4) and (a( 83))) xor
        (b(  5) and (a( 82))) xor
        (b(  6) and (a( 81))) xor
        (b(  7) and (a( 80))) xor
        (b(  8) and (a( 79))) xor
        (b(  9) and (a( 78))) xor
        (b( 10) and (a( 77))) xor
        (b( 11) and (a( 76))) xor
        (b( 12) and (a( 75))) xor
        (b( 13) and (a( 74))) xor
        (b( 14) and (a( 73))) xor
        (b( 15) and (a( 72))) xor
        (b( 16) and (a( 71))) xor
        (b( 17) and (a( 70))) xor
        (b( 18) and (a( 69))) xor
        (b( 19) and (a( 68))) xor
        (b( 20) and (a( 67))) xor
        (b( 21) and (a( 66))) xor
        (b( 22) and (a( 65))) xor
        (b( 23) and (a( 64))) xor
        (b( 24) and (a( 63))) xor
        (b( 25) and (a( 62))) xor
        (b( 26) and (a( 61))) xor
        (b( 27) and (a( 60))) xor
        (b( 28) and (a( 59))) xor
        (b( 29) and (a( 58))) xor
        (b( 30) and (a( 57))) xor
        (b( 31) and (a( 56))) xor
        (b( 32) and (a( 55))) xor
        (b( 33) and (a( 54))) xor
        (b( 34) and (a( 53))) xor
        (b( 35) and (a( 52))) xor
        (b( 36) and (a( 51))) xor
        (b( 37) and (a( 50))) xor
        (b( 38) and (a( 49))) xor
        (b( 39) and (a( 48))) xor
        (b( 40) and (a( 47))) xor
        (b( 41) and (a( 46))) xor
        (b( 42) and (a( 45))) xor
        (b( 43) and (a( 44))) xor
        (b( 44) and (a( 43))) xor
        (b( 45) and (a( 42))) xor
        (b( 46) and (a( 41))) xor
        (b( 47) and (a( 40))) xor
        (b( 48) and (a( 39))) xor
        (b( 49) and (a( 38))) xor
        (b( 50) and (a( 37))) xor
        (b( 51) and (a( 36))) xor
        (b( 52) and (a( 35))) xor
        (b( 53) and (a( 34))) xor
        (b( 54) and (a( 33))) xor
        (b( 55) and (a( 32))) xor
        (b( 56) and (a( 31))) xor
        (b( 57) and (a( 30))) xor
        (b( 58) and (a( 29))) xor
        (b( 59) and (a( 28))) xor
        (b( 60) and (a( 27))) xor
        (b( 61) and (a( 26))) xor
        (b( 62) and (a( 25))) xor
        (b( 63) and (a( 24))) xor
        (b( 64) and (a( 23))) xor
        (b( 65) and (a( 22))) xor
        (b( 66) and (a( 21))) xor
        (b( 67) and (a( 20))) xor
        (b( 68) and (a( 19))) xor
        (b( 69) and (a( 18))) xor
        (b( 70) and (a( 17))) xor
        (b( 71) and (a( 16))) xor
        (b( 72) and (a( 15))) xor
        (b( 73) and (a( 14))) xor
        (b( 74) and (a( 13))) xor
        (b( 75) and (a( 12))) xor
        (b( 76) and (a( 11))) xor
        (b( 77) and (a( 10))) xor
        (b( 78) and (a(  9))) xor
        (b( 79) and (a(  8))) xor
        (b( 80) and (a(  7))) xor
        (b( 81) and (a(  6) xor a(127))) xor
        (b( 82) and (a(  5) xor a(126))) xor
        (b( 83) and (a(  4) xor a(125))) xor
        (b( 84) and (a(  3) xor a(124))) xor
        (b( 85) and (a(  2) xor a(123))) xor
        (b( 86) and (a(  1) xor a(122) xor a(127))) xor
        (b( 87) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 88) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 89) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 90) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 91) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 92) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 93) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 94) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 95) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 96) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 97) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 98) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b( 99) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(100) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(101) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(102) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(103) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(104) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(105) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(106) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(107) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(108) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(109) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(110) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(111) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(112) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(113) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(114) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(115) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(116) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(117) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(118) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(119) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(120) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(121) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(122) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(123) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(124) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(125) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(126) and (a( 82) xor a( 87) xor a( 88) xor a( 89))) xor
        (b(127) and (a( 81) xor a( 86) xor a( 87) xor a( 88)));
    c( 88) <= 
        (b(  0) and (a( 88))) xor
        (b(  1) and (a( 87))) xor
        (b(  2) and (a( 86))) xor
        (b(  3) and (a( 85))) xor
        (b(  4) and (a( 84))) xor
        (b(  5) and (a( 83))) xor
        (b(  6) and (a( 82))) xor
        (b(  7) and (a( 81))) xor
        (b(  8) and (a( 80))) xor
        (b(  9) and (a( 79))) xor
        (b( 10) and (a( 78))) xor
        (b( 11) and (a( 77))) xor
        (b( 12) and (a( 76))) xor
        (b( 13) and (a( 75))) xor
        (b( 14) and (a( 74))) xor
        (b( 15) and (a( 73))) xor
        (b( 16) and (a( 72))) xor
        (b( 17) and (a( 71))) xor
        (b( 18) and (a( 70))) xor
        (b( 19) and (a( 69))) xor
        (b( 20) and (a( 68))) xor
        (b( 21) and (a( 67))) xor
        (b( 22) and (a( 66))) xor
        (b( 23) and (a( 65))) xor
        (b( 24) and (a( 64))) xor
        (b( 25) and (a( 63))) xor
        (b( 26) and (a( 62))) xor
        (b( 27) and (a( 61))) xor
        (b( 28) and (a( 60))) xor
        (b( 29) and (a( 59))) xor
        (b( 30) and (a( 58))) xor
        (b( 31) and (a( 57))) xor
        (b( 32) and (a( 56))) xor
        (b( 33) and (a( 55))) xor
        (b( 34) and (a( 54))) xor
        (b( 35) and (a( 53))) xor
        (b( 36) and (a( 52))) xor
        (b( 37) and (a( 51))) xor
        (b( 38) and (a( 50))) xor
        (b( 39) and (a( 49))) xor
        (b( 40) and (a( 48))) xor
        (b( 41) and (a( 47))) xor
        (b( 42) and (a( 46))) xor
        (b( 43) and (a( 45))) xor
        (b( 44) and (a( 44))) xor
        (b( 45) and (a( 43))) xor
        (b( 46) and (a( 42))) xor
        (b( 47) and (a( 41))) xor
        (b( 48) and (a( 40))) xor
        (b( 49) and (a( 39))) xor
        (b( 50) and (a( 38))) xor
        (b( 51) and (a( 37))) xor
        (b( 52) and (a( 36))) xor
        (b( 53) and (a( 35))) xor
        (b( 54) and (a( 34))) xor
        (b( 55) and (a( 33))) xor
        (b( 56) and (a( 32))) xor
        (b( 57) and (a( 31))) xor
        (b( 58) and (a( 30))) xor
        (b( 59) and (a( 29))) xor
        (b( 60) and (a( 28))) xor
        (b( 61) and (a( 27))) xor
        (b( 62) and (a( 26))) xor
        (b( 63) and (a( 25))) xor
        (b( 64) and (a( 24))) xor
        (b( 65) and (a( 23))) xor
        (b( 66) and (a( 22))) xor
        (b( 67) and (a( 21))) xor
        (b( 68) and (a( 20))) xor
        (b( 69) and (a( 19))) xor
        (b( 70) and (a( 18))) xor
        (b( 71) and (a( 17))) xor
        (b( 72) and (a( 16))) xor
        (b( 73) and (a( 15))) xor
        (b( 74) and (a( 14))) xor
        (b( 75) and (a( 13))) xor
        (b( 76) and (a( 12))) xor
        (b( 77) and (a( 11))) xor
        (b( 78) and (a( 10))) xor
        (b( 79) and (a(  9))) xor
        (b( 80) and (a(  8))) xor
        (b( 81) and (a(  7))) xor
        (b( 82) and (a(  6) xor a(127))) xor
        (b( 83) and (a(  5) xor a(126))) xor
        (b( 84) and (a(  4) xor a(125))) xor
        (b( 85) and (a(  3) xor a(124))) xor
        (b( 86) and (a(  2) xor a(123))) xor
        (b( 87) and (a(  1) xor a(122) xor a(127))) xor
        (b( 88) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 89) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 90) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 91) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 92) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 93) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 94) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 95) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 96) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 97) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 98) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b( 99) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(100) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(101) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(102) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(103) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(104) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(105) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(106) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(107) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(108) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(109) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(110) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(111) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(112) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(113) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(114) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(115) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(116) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(117) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(118) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(119) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(120) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(121) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(122) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(123) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(124) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(125) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(126) and (a( 83) xor a( 88) xor a( 89) xor a( 90))) xor
        (b(127) and (a( 82) xor a( 87) xor a( 88) xor a( 89)));
    c( 89) <= 
        (b(  0) and (a( 89))) xor
        (b(  1) and (a( 88))) xor
        (b(  2) and (a( 87))) xor
        (b(  3) and (a( 86))) xor
        (b(  4) and (a( 85))) xor
        (b(  5) and (a( 84))) xor
        (b(  6) and (a( 83))) xor
        (b(  7) and (a( 82))) xor
        (b(  8) and (a( 81))) xor
        (b(  9) and (a( 80))) xor
        (b( 10) and (a( 79))) xor
        (b( 11) and (a( 78))) xor
        (b( 12) and (a( 77))) xor
        (b( 13) and (a( 76))) xor
        (b( 14) and (a( 75))) xor
        (b( 15) and (a( 74))) xor
        (b( 16) and (a( 73))) xor
        (b( 17) and (a( 72))) xor
        (b( 18) and (a( 71))) xor
        (b( 19) and (a( 70))) xor
        (b( 20) and (a( 69))) xor
        (b( 21) and (a( 68))) xor
        (b( 22) and (a( 67))) xor
        (b( 23) and (a( 66))) xor
        (b( 24) and (a( 65))) xor
        (b( 25) and (a( 64))) xor
        (b( 26) and (a( 63))) xor
        (b( 27) and (a( 62))) xor
        (b( 28) and (a( 61))) xor
        (b( 29) and (a( 60))) xor
        (b( 30) and (a( 59))) xor
        (b( 31) and (a( 58))) xor
        (b( 32) and (a( 57))) xor
        (b( 33) and (a( 56))) xor
        (b( 34) and (a( 55))) xor
        (b( 35) and (a( 54))) xor
        (b( 36) and (a( 53))) xor
        (b( 37) and (a( 52))) xor
        (b( 38) and (a( 51))) xor
        (b( 39) and (a( 50))) xor
        (b( 40) and (a( 49))) xor
        (b( 41) and (a( 48))) xor
        (b( 42) and (a( 47))) xor
        (b( 43) and (a( 46))) xor
        (b( 44) and (a( 45))) xor
        (b( 45) and (a( 44))) xor
        (b( 46) and (a( 43))) xor
        (b( 47) and (a( 42))) xor
        (b( 48) and (a( 41))) xor
        (b( 49) and (a( 40))) xor
        (b( 50) and (a( 39))) xor
        (b( 51) and (a( 38))) xor
        (b( 52) and (a( 37))) xor
        (b( 53) and (a( 36))) xor
        (b( 54) and (a( 35))) xor
        (b( 55) and (a( 34))) xor
        (b( 56) and (a( 33))) xor
        (b( 57) and (a( 32))) xor
        (b( 58) and (a( 31))) xor
        (b( 59) and (a( 30))) xor
        (b( 60) and (a( 29))) xor
        (b( 61) and (a( 28))) xor
        (b( 62) and (a( 27))) xor
        (b( 63) and (a( 26))) xor
        (b( 64) and (a( 25))) xor
        (b( 65) and (a( 24))) xor
        (b( 66) and (a( 23))) xor
        (b( 67) and (a( 22))) xor
        (b( 68) and (a( 21))) xor
        (b( 69) and (a( 20))) xor
        (b( 70) and (a( 19))) xor
        (b( 71) and (a( 18))) xor
        (b( 72) and (a( 17))) xor
        (b( 73) and (a( 16))) xor
        (b( 74) and (a( 15))) xor
        (b( 75) and (a( 14))) xor
        (b( 76) and (a( 13))) xor
        (b( 77) and (a( 12))) xor
        (b( 78) and (a( 11))) xor
        (b( 79) and (a( 10))) xor
        (b( 80) and (a(  9))) xor
        (b( 81) and (a(  8))) xor
        (b( 82) and (a(  7))) xor
        (b( 83) and (a(  6) xor a(127))) xor
        (b( 84) and (a(  5) xor a(126))) xor
        (b( 85) and (a(  4) xor a(125))) xor
        (b( 86) and (a(  3) xor a(124))) xor
        (b( 87) and (a(  2) xor a(123))) xor
        (b( 88) and (a(  1) xor a(122) xor a(127))) xor
        (b( 89) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 90) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 91) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 92) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 93) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 94) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 95) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 96) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 97) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 98) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b( 99) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(100) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(101) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(102) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(103) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(104) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(105) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(106) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(107) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(108) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(109) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(110) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(111) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(112) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(113) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(114) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(115) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(116) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(117) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(118) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(119) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(120) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(121) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(122) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(123) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(124) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(125) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(126) and (a( 84) xor a( 89) xor a( 90) xor a( 91))) xor
        (b(127) and (a( 83) xor a( 88) xor a( 89) xor a( 90)));
    c( 90) <= 
        (b(  0) and (a( 90))) xor
        (b(  1) and (a( 89))) xor
        (b(  2) and (a( 88))) xor
        (b(  3) and (a( 87))) xor
        (b(  4) and (a( 86))) xor
        (b(  5) and (a( 85))) xor
        (b(  6) and (a( 84))) xor
        (b(  7) and (a( 83))) xor
        (b(  8) and (a( 82))) xor
        (b(  9) and (a( 81))) xor
        (b( 10) and (a( 80))) xor
        (b( 11) and (a( 79))) xor
        (b( 12) and (a( 78))) xor
        (b( 13) and (a( 77))) xor
        (b( 14) and (a( 76))) xor
        (b( 15) and (a( 75))) xor
        (b( 16) and (a( 74))) xor
        (b( 17) and (a( 73))) xor
        (b( 18) and (a( 72))) xor
        (b( 19) and (a( 71))) xor
        (b( 20) and (a( 70))) xor
        (b( 21) and (a( 69))) xor
        (b( 22) and (a( 68))) xor
        (b( 23) and (a( 67))) xor
        (b( 24) and (a( 66))) xor
        (b( 25) and (a( 65))) xor
        (b( 26) and (a( 64))) xor
        (b( 27) and (a( 63))) xor
        (b( 28) and (a( 62))) xor
        (b( 29) and (a( 61))) xor
        (b( 30) and (a( 60))) xor
        (b( 31) and (a( 59))) xor
        (b( 32) and (a( 58))) xor
        (b( 33) and (a( 57))) xor
        (b( 34) and (a( 56))) xor
        (b( 35) and (a( 55))) xor
        (b( 36) and (a( 54))) xor
        (b( 37) and (a( 53))) xor
        (b( 38) and (a( 52))) xor
        (b( 39) and (a( 51))) xor
        (b( 40) and (a( 50))) xor
        (b( 41) and (a( 49))) xor
        (b( 42) and (a( 48))) xor
        (b( 43) and (a( 47))) xor
        (b( 44) and (a( 46))) xor
        (b( 45) and (a( 45))) xor
        (b( 46) and (a( 44))) xor
        (b( 47) and (a( 43))) xor
        (b( 48) and (a( 42))) xor
        (b( 49) and (a( 41))) xor
        (b( 50) and (a( 40))) xor
        (b( 51) and (a( 39))) xor
        (b( 52) and (a( 38))) xor
        (b( 53) and (a( 37))) xor
        (b( 54) and (a( 36))) xor
        (b( 55) and (a( 35))) xor
        (b( 56) and (a( 34))) xor
        (b( 57) and (a( 33))) xor
        (b( 58) and (a( 32))) xor
        (b( 59) and (a( 31))) xor
        (b( 60) and (a( 30))) xor
        (b( 61) and (a( 29))) xor
        (b( 62) and (a( 28))) xor
        (b( 63) and (a( 27))) xor
        (b( 64) and (a( 26))) xor
        (b( 65) and (a( 25))) xor
        (b( 66) and (a( 24))) xor
        (b( 67) and (a( 23))) xor
        (b( 68) and (a( 22))) xor
        (b( 69) and (a( 21))) xor
        (b( 70) and (a( 20))) xor
        (b( 71) and (a( 19))) xor
        (b( 72) and (a( 18))) xor
        (b( 73) and (a( 17))) xor
        (b( 74) and (a( 16))) xor
        (b( 75) and (a( 15))) xor
        (b( 76) and (a( 14))) xor
        (b( 77) and (a( 13))) xor
        (b( 78) and (a( 12))) xor
        (b( 79) and (a( 11))) xor
        (b( 80) and (a( 10))) xor
        (b( 81) and (a(  9))) xor
        (b( 82) and (a(  8))) xor
        (b( 83) and (a(  7))) xor
        (b( 84) and (a(  6) xor a(127))) xor
        (b( 85) and (a(  5) xor a(126))) xor
        (b( 86) and (a(  4) xor a(125))) xor
        (b( 87) and (a(  3) xor a(124))) xor
        (b( 88) and (a(  2) xor a(123))) xor
        (b( 89) and (a(  1) xor a(122) xor a(127))) xor
        (b( 90) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 91) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 92) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 93) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 94) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 95) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 96) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 97) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 98) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b( 99) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(100) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(101) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(102) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(103) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(104) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(105) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(106) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(107) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(108) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(109) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(110) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(111) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(112) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(113) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(114) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(115) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(116) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(117) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(118) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(119) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(120) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(121) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(122) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(123) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(124) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(125) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(126) and (a( 85) xor a( 90) xor a( 91) xor a( 92))) xor
        (b(127) and (a( 84) xor a( 89) xor a( 90) xor a( 91)));
    c( 91) <= 
        (b(  0) and (a( 91))) xor
        (b(  1) and (a( 90))) xor
        (b(  2) and (a( 89))) xor
        (b(  3) and (a( 88))) xor
        (b(  4) and (a( 87))) xor
        (b(  5) and (a( 86))) xor
        (b(  6) and (a( 85))) xor
        (b(  7) and (a( 84))) xor
        (b(  8) and (a( 83))) xor
        (b(  9) and (a( 82))) xor
        (b( 10) and (a( 81))) xor
        (b( 11) and (a( 80))) xor
        (b( 12) and (a( 79))) xor
        (b( 13) and (a( 78))) xor
        (b( 14) and (a( 77))) xor
        (b( 15) and (a( 76))) xor
        (b( 16) and (a( 75))) xor
        (b( 17) and (a( 74))) xor
        (b( 18) and (a( 73))) xor
        (b( 19) and (a( 72))) xor
        (b( 20) and (a( 71))) xor
        (b( 21) and (a( 70))) xor
        (b( 22) and (a( 69))) xor
        (b( 23) and (a( 68))) xor
        (b( 24) and (a( 67))) xor
        (b( 25) and (a( 66))) xor
        (b( 26) and (a( 65))) xor
        (b( 27) and (a( 64))) xor
        (b( 28) and (a( 63))) xor
        (b( 29) and (a( 62))) xor
        (b( 30) and (a( 61))) xor
        (b( 31) and (a( 60))) xor
        (b( 32) and (a( 59))) xor
        (b( 33) and (a( 58))) xor
        (b( 34) and (a( 57))) xor
        (b( 35) and (a( 56))) xor
        (b( 36) and (a( 55))) xor
        (b( 37) and (a( 54))) xor
        (b( 38) and (a( 53))) xor
        (b( 39) and (a( 52))) xor
        (b( 40) and (a( 51))) xor
        (b( 41) and (a( 50))) xor
        (b( 42) and (a( 49))) xor
        (b( 43) and (a( 48))) xor
        (b( 44) and (a( 47))) xor
        (b( 45) and (a( 46))) xor
        (b( 46) and (a( 45))) xor
        (b( 47) and (a( 44))) xor
        (b( 48) and (a( 43))) xor
        (b( 49) and (a( 42))) xor
        (b( 50) and (a( 41))) xor
        (b( 51) and (a( 40))) xor
        (b( 52) and (a( 39))) xor
        (b( 53) and (a( 38))) xor
        (b( 54) and (a( 37))) xor
        (b( 55) and (a( 36))) xor
        (b( 56) and (a( 35))) xor
        (b( 57) and (a( 34))) xor
        (b( 58) and (a( 33))) xor
        (b( 59) and (a( 32))) xor
        (b( 60) and (a( 31))) xor
        (b( 61) and (a( 30))) xor
        (b( 62) and (a( 29))) xor
        (b( 63) and (a( 28))) xor
        (b( 64) and (a( 27))) xor
        (b( 65) and (a( 26))) xor
        (b( 66) and (a( 25))) xor
        (b( 67) and (a( 24))) xor
        (b( 68) and (a( 23))) xor
        (b( 69) and (a( 22))) xor
        (b( 70) and (a( 21))) xor
        (b( 71) and (a( 20))) xor
        (b( 72) and (a( 19))) xor
        (b( 73) and (a( 18))) xor
        (b( 74) and (a( 17))) xor
        (b( 75) and (a( 16))) xor
        (b( 76) and (a( 15))) xor
        (b( 77) and (a( 14))) xor
        (b( 78) and (a( 13))) xor
        (b( 79) and (a( 12))) xor
        (b( 80) and (a( 11))) xor
        (b( 81) and (a( 10))) xor
        (b( 82) and (a(  9))) xor
        (b( 83) and (a(  8))) xor
        (b( 84) and (a(  7))) xor
        (b( 85) and (a(  6) xor a(127))) xor
        (b( 86) and (a(  5) xor a(126))) xor
        (b( 87) and (a(  4) xor a(125))) xor
        (b( 88) and (a(  3) xor a(124))) xor
        (b( 89) and (a(  2) xor a(123))) xor
        (b( 90) and (a(  1) xor a(122) xor a(127))) xor
        (b( 91) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 92) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 93) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 94) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 95) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 96) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 97) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 98) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b( 99) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(100) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(101) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(102) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(103) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(104) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(105) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(106) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(107) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(108) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(109) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(110) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(111) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(112) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(113) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(114) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(115) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(116) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(117) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(118) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(119) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(120) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(121) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(122) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(123) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(124) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(125) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(126) and (a( 86) xor a( 91) xor a( 92) xor a( 93))) xor
        (b(127) and (a( 85) xor a( 90) xor a( 91) xor a( 92)));
    c( 92) <= 
        (b(  0) and (a( 92))) xor
        (b(  1) and (a( 91))) xor
        (b(  2) and (a( 90))) xor
        (b(  3) and (a( 89))) xor
        (b(  4) and (a( 88))) xor
        (b(  5) and (a( 87))) xor
        (b(  6) and (a( 86))) xor
        (b(  7) and (a( 85))) xor
        (b(  8) and (a( 84))) xor
        (b(  9) and (a( 83))) xor
        (b( 10) and (a( 82))) xor
        (b( 11) and (a( 81))) xor
        (b( 12) and (a( 80))) xor
        (b( 13) and (a( 79))) xor
        (b( 14) and (a( 78))) xor
        (b( 15) and (a( 77))) xor
        (b( 16) and (a( 76))) xor
        (b( 17) and (a( 75))) xor
        (b( 18) and (a( 74))) xor
        (b( 19) and (a( 73))) xor
        (b( 20) and (a( 72))) xor
        (b( 21) and (a( 71))) xor
        (b( 22) and (a( 70))) xor
        (b( 23) and (a( 69))) xor
        (b( 24) and (a( 68))) xor
        (b( 25) and (a( 67))) xor
        (b( 26) and (a( 66))) xor
        (b( 27) and (a( 65))) xor
        (b( 28) and (a( 64))) xor
        (b( 29) and (a( 63))) xor
        (b( 30) and (a( 62))) xor
        (b( 31) and (a( 61))) xor
        (b( 32) and (a( 60))) xor
        (b( 33) and (a( 59))) xor
        (b( 34) and (a( 58))) xor
        (b( 35) and (a( 57))) xor
        (b( 36) and (a( 56))) xor
        (b( 37) and (a( 55))) xor
        (b( 38) and (a( 54))) xor
        (b( 39) and (a( 53))) xor
        (b( 40) and (a( 52))) xor
        (b( 41) and (a( 51))) xor
        (b( 42) and (a( 50))) xor
        (b( 43) and (a( 49))) xor
        (b( 44) and (a( 48))) xor
        (b( 45) and (a( 47))) xor
        (b( 46) and (a( 46))) xor
        (b( 47) and (a( 45))) xor
        (b( 48) and (a( 44))) xor
        (b( 49) and (a( 43))) xor
        (b( 50) and (a( 42))) xor
        (b( 51) and (a( 41))) xor
        (b( 52) and (a( 40))) xor
        (b( 53) and (a( 39))) xor
        (b( 54) and (a( 38))) xor
        (b( 55) and (a( 37))) xor
        (b( 56) and (a( 36))) xor
        (b( 57) and (a( 35))) xor
        (b( 58) and (a( 34))) xor
        (b( 59) and (a( 33))) xor
        (b( 60) and (a( 32))) xor
        (b( 61) and (a( 31))) xor
        (b( 62) and (a( 30))) xor
        (b( 63) and (a( 29))) xor
        (b( 64) and (a( 28))) xor
        (b( 65) and (a( 27))) xor
        (b( 66) and (a( 26))) xor
        (b( 67) and (a( 25))) xor
        (b( 68) and (a( 24))) xor
        (b( 69) and (a( 23))) xor
        (b( 70) and (a( 22))) xor
        (b( 71) and (a( 21))) xor
        (b( 72) and (a( 20))) xor
        (b( 73) and (a( 19))) xor
        (b( 74) and (a( 18))) xor
        (b( 75) and (a( 17))) xor
        (b( 76) and (a( 16))) xor
        (b( 77) and (a( 15))) xor
        (b( 78) and (a( 14))) xor
        (b( 79) and (a( 13))) xor
        (b( 80) and (a( 12))) xor
        (b( 81) and (a( 11))) xor
        (b( 82) and (a( 10))) xor
        (b( 83) and (a(  9))) xor
        (b( 84) and (a(  8))) xor
        (b( 85) and (a(  7))) xor
        (b( 86) and (a(  6) xor a(127))) xor
        (b( 87) and (a(  5) xor a(126))) xor
        (b( 88) and (a(  4) xor a(125))) xor
        (b( 89) and (a(  3) xor a(124))) xor
        (b( 90) and (a(  2) xor a(123))) xor
        (b( 91) and (a(  1) xor a(122) xor a(127))) xor
        (b( 92) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 93) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 94) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 95) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 96) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 97) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 98) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b( 99) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(100) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(101) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(102) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(103) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(104) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(105) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(106) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(107) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(108) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(109) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(110) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(111) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(112) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(113) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(114) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(115) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(116) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(117) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(118) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(119) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(120) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(121) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(122) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(123) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(124) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(125) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(126) and (a( 87) xor a( 92) xor a( 93) xor a( 94))) xor
        (b(127) and (a( 86) xor a( 91) xor a( 92) xor a( 93)));
    c( 93) <= 
        (b(  0) and (a( 93))) xor
        (b(  1) and (a( 92))) xor
        (b(  2) and (a( 91))) xor
        (b(  3) and (a( 90))) xor
        (b(  4) and (a( 89))) xor
        (b(  5) and (a( 88))) xor
        (b(  6) and (a( 87))) xor
        (b(  7) and (a( 86))) xor
        (b(  8) and (a( 85))) xor
        (b(  9) and (a( 84))) xor
        (b( 10) and (a( 83))) xor
        (b( 11) and (a( 82))) xor
        (b( 12) and (a( 81))) xor
        (b( 13) and (a( 80))) xor
        (b( 14) and (a( 79))) xor
        (b( 15) and (a( 78))) xor
        (b( 16) and (a( 77))) xor
        (b( 17) and (a( 76))) xor
        (b( 18) and (a( 75))) xor
        (b( 19) and (a( 74))) xor
        (b( 20) and (a( 73))) xor
        (b( 21) and (a( 72))) xor
        (b( 22) and (a( 71))) xor
        (b( 23) and (a( 70))) xor
        (b( 24) and (a( 69))) xor
        (b( 25) and (a( 68))) xor
        (b( 26) and (a( 67))) xor
        (b( 27) and (a( 66))) xor
        (b( 28) and (a( 65))) xor
        (b( 29) and (a( 64))) xor
        (b( 30) and (a( 63))) xor
        (b( 31) and (a( 62))) xor
        (b( 32) and (a( 61))) xor
        (b( 33) and (a( 60))) xor
        (b( 34) and (a( 59))) xor
        (b( 35) and (a( 58))) xor
        (b( 36) and (a( 57))) xor
        (b( 37) and (a( 56))) xor
        (b( 38) and (a( 55))) xor
        (b( 39) and (a( 54))) xor
        (b( 40) and (a( 53))) xor
        (b( 41) and (a( 52))) xor
        (b( 42) and (a( 51))) xor
        (b( 43) and (a( 50))) xor
        (b( 44) and (a( 49))) xor
        (b( 45) and (a( 48))) xor
        (b( 46) and (a( 47))) xor
        (b( 47) and (a( 46))) xor
        (b( 48) and (a( 45))) xor
        (b( 49) and (a( 44))) xor
        (b( 50) and (a( 43))) xor
        (b( 51) and (a( 42))) xor
        (b( 52) and (a( 41))) xor
        (b( 53) and (a( 40))) xor
        (b( 54) and (a( 39))) xor
        (b( 55) and (a( 38))) xor
        (b( 56) and (a( 37))) xor
        (b( 57) and (a( 36))) xor
        (b( 58) and (a( 35))) xor
        (b( 59) and (a( 34))) xor
        (b( 60) and (a( 33))) xor
        (b( 61) and (a( 32))) xor
        (b( 62) and (a( 31))) xor
        (b( 63) and (a( 30))) xor
        (b( 64) and (a( 29))) xor
        (b( 65) and (a( 28))) xor
        (b( 66) and (a( 27))) xor
        (b( 67) and (a( 26))) xor
        (b( 68) and (a( 25))) xor
        (b( 69) and (a( 24))) xor
        (b( 70) and (a( 23))) xor
        (b( 71) and (a( 22))) xor
        (b( 72) and (a( 21))) xor
        (b( 73) and (a( 20))) xor
        (b( 74) and (a( 19))) xor
        (b( 75) and (a( 18))) xor
        (b( 76) and (a( 17))) xor
        (b( 77) and (a( 16))) xor
        (b( 78) and (a( 15))) xor
        (b( 79) and (a( 14))) xor
        (b( 80) and (a( 13))) xor
        (b( 81) and (a( 12))) xor
        (b( 82) and (a( 11))) xor
        (b( 83) and (a( 10))) xor
        (b( 84) and (a(  9))) xor
        (b( 85) and (a(  8))) xor
        (b( 86) and (a(  7))) xor
        (b( 87) and (a(  6) xor a(127))) xor
        (b( 88) and (a(  5) xor a(126))) xor
        (b( 89) and (a(  4) xor a(125))) xor
        (b( 90) and (a(  3) xor a(124))) xor
        (b( 91) and (a(  2) xor a(123))) xor
        (b( 92) and (a(  1) xor a(122) xor a(127))) xor
        (b( 93) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 94) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 95) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 96) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 97) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 98) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b( 99) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(100) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(101) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(102) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(103) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(104) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(105) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(106) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(107) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(108) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(109) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(110) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(111) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(112) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(113) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(114) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(115) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(116) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(117) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(118) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(119) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(120) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(121) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(122) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(123) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(124) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(125) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(126) and (a( 88) xor a( 93) xor a( 94) xor a( 95))) xor
        (b(127) and (a( 87) xor a( 92) xor a( 93) xor a( 94)));
    c( 94) <= 
        (b(  0) and (a( 94))) xor
        (b(  1) and (a( 93))) xor
        (b(  2) and (a( 92))) xor
        (b(  3) and (a( 91))) xor
        (b(  4) and (a( 90))) xor
        (b(  5) and (a( 89))) xor
        (b(  6) and (a( 88))) xor
        (b(  7) and (a( 87))) xor
        (b(  8) and (a( 86))) xor
        (b(  9) and (a( 85))) xor
        (b( 10) and (a( 84))) xor
        (b( 11) and (a( 83))) xor
        (b( 12) and (a( 82))) xor
        (b( 13) and (a( 81))) xor
        (b( 14) and (a( 80))) xor
        (b( 15) and (a( 79))) xor
        (b( 16) and (a( 78))) xor
        (b( 17) and (a( 77))) xor
        (b( 18) and (a( 76))) xor
        (b( 19) and (a( 75))) xor
        (b( 20) and (a( 74))) xor
        (b( 21) and (a( 73))) xor
        (b( 22) and (a( 72))) xor
        (b( 23) and (a( 71))) xor
        (b( 24) and (a( 70))) xor
        (b( 25) and (a( 69))) xor
        (b( 26) and (a( 68))) xor
        (b( 27) and (a( 67))) xor
        (b( 28) and (a( 66))) xor
        (b( 29) and (a( 65))) xor
        (b( 30) and (a( 64))) xor
        (b( 31) and (a( 63))) xor
        (b( 32) and (a( 62))) xor
        (b( 33) and (a( 61))) xor
        (b( 34) and (a( 60))) xor
        (b( 35) and (a( 59))) xor
        (b( 36) and (a( 58))) xor
        (b( 37) and (a( 57))) xor
        (b( 38) and (a( 56))) xor
        (b( 39) and (a( 55))) xor
        (b( 40) and (a( 54))) xor
        (b( 41) and (a( 53))) xor
        (b( 42) and (a( 52))) xor
        (b( 43) and (a( 51))) xor
        (b( 44) and (a( 50))) xor
        (b( 45) and (a( 49))) xor
        (b( 46) and (a( 48))) xor
        (b( 47) and (a( 47))) xor
        (b( 48) and (a( 46))) xor
        (b( 49) and (a( 45))) xor
        (b( 50) and (a( 44))) xor
        (b( 51) and (a( 43))) xor
        (b( 52) and (a( 42))) xor
        (b( 53) and (a( 41))) xor
        (b( 54) and (a( 40))) xor
        (b( 55) and (a( 39))) xor
        (b( 56) and (a( 38))) xor
        (b( 57) and (a( 37))) xor
        (b( 58) and (a( 36))) xor
        (b( 59) and (a( 35))) xor
        (b( 60) and (a( 34))) xor
        (b( 61) and (a( 33))) xor
        (b( 62) and (a( 32))) xor
        (b( 63) and (a( 31))) xor
        (b( 64) and (a( 30))) xor
        (b( 65) and (a( 29))) xor
        (b( 66) and (a( 28))) xor
        (b( 67) and (a( 27))) xor
        (b( 68) and (a( 26))) xor
        (b( 69) and (a( 25))) xor
        (b( 70) and (a( 24))) xor
        (b( 71) and (a( 23))) xor
        (b( 72) and (a( 22))) xor
        (b( 73) and (a( 21))) xor
        (b( 74) and (a( 20))) xor
        (b( 75) and (a( 19))) xor
        (b( 76) and (a( 18))) xor
        (b( 77) and (a( 17))) xor
        (b( 78) and (a( 16))) xor
        (b( 79) and (a( 15))) xor
        (b( 80) and (a( 14))) xor
        (b( 81) and (a( 13))) xor
        (b( 82) and (a( 12))) xor
        (b( 83) and (a( 11))) xor
        (b( 84) and (a( 10))) xor
        (b( 85) and (a(  9))) xor
        (b( 86) and (a(  8))) xor
        (b( 87) and (a(  7))) xor
        (b( 88) and (a(  6) xor a(127))) xor
        (b( 89) and (a(  5) xor a(126))) xor
        (b( 90) and (a(  4) xor a(125))) xor
        (b( 91) and (a(  3) xor a(124))) xor
        (b( 92) and (a(  2) xor a(123))) xor
        (b( 93) and (a(  1) xor a(122) xor a(127))) xor
        (b( 94) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 95) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 96) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 97) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 98) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b( 99) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(100) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(101) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(102) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(103) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(104) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(105) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(106) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(107) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(108) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(109) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(110) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(111) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(112) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(113) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(114) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(115) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(116) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(117) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(118) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(119) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(120) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(121) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(122) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(123) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(124) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(125) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(126) and (a( 89) xor a( 94) xor a( 95) xor a( 96))) xor
        (b(127) and (a( 88) xor a( 93) xor a( 94) xor a( 95)));
    c( 95) <= 
        (b(  0) and (a( 95))) xor
        (b(  1) and (a( 94))) xor
        (b(  2) and (a( 93))) xor
        (b(  3) and (a( 92))) xor
        (b(  4) and (a( 91))) xor
        (b(  5) and (a( 90))) xor
        (b(  6) and (a( 89))) xor
        (b(  7) and (a( 88))) xor
        (b(  8) and (a( 87))) xor
        (b(  9) and (a( 86))) xor
        (b( 10) and (a( 85))) xor
        (b( 11) and (a( 84))) xor
        (b( 12) and (a( 83))) xor
        (b( 13) and (a( 82))) xor
        (b( 14) and (a( 81))) xor
        (b( 15) and (a( 80))) xor
        (b( 16) and (a( 79))) xor
        (b( 17) and (a( 78))) xor
        (b( 18) and (a( 77))) xor
        (b( 19) and (a( 76))) xor
        (b( 20) and (a( 75))) xor
        (b( 21) and (a( 74))) xor
        (b( 22) and (a( 73))) xor
        (b( 23) and (a( 72))) xor
        (b( 24) and (a( 71))) xor
        (b( 25) and (a( 70))) xor
        (b( 26) and (a( 69))) xor
        (b( 27) and (a( 68))) xor
        (b( 28) and (a( 67))) xor
        (b( 29) and (a( 66))) xor
        (b( 30) and (a( 65))) xor
        (b( 31) and (a( 64))) xor
        (b( 32) and (a( 63))) xor
        (b( 33) and (a( 62))) xor
        (b( 34) and (a( 61))) xor
        (b( 35) and (a( 60))) xor
        (b( 36) and (a( 59))) xor
        (b( 37) and (a( 58))) xor
        (b( 38) and (a( 57))) xor
        (b( 39) and (a( 56))) xor
        (b( 40) and (a( 55))) xor
        (b( 41) and (a( 54))) xor
        (b( 42) and (a( 53))) xor
        (b( 43) and (a( 52))) xor
        (b( 44) and (a( 51))) xor
        (b( 45) and (a( 50))) xor
        (b( 46) and (a( 49))) xor
        (b( 47) and (a( 48))) xor
        (b( 48) and (a( 47))) xor
        (b( 49) and (a( 46))) xor
        (b( 50) and (a( 45))) xor
        (b( 51) and (a( 44))) xor
        (b( 52) and (a( 43))) xor
        (b( 53) and (a( 42))) xor
        (b( 54) and (a( 41))) xor
        (b( 55) and (a( 40))) xor
        (b( 56) and (a( 39))) xor
        (b( 57) and (a( 38))) xor
        (b( 58) and (a( 37))) xor
        (b( 59) and (a( 36))) xor
        (b( 60) and (a( 35))) xor
        (b( 61) and (a( 34))) xor
        (b( 62) and (a( 33))) xor
        (b( 63) and (a( 32))) xor
        (b( 64) and (a( 31))) xor
        (b( 65) and (a( 30))) xor
        (b( 66) and (a( 29))) xor
        (b( 67) and (a( 28))) xor
        (b( 68) and (a( 27))) xor
        (b( 69) and (a( 26))) xor
        (b( 70) and (a( 25))) xor
        (b( 71) and (a( 24))) xor
        (b( 72) and (a( 23))) xor
        (b( 73) and (a( 22))) xor
        (b( 74) and (a( 21))) xor
        (b( 75) and (a( 20))) xor
        (b( 76) and (a( 19))) xor
        (b( 77) and (a( 18))) xor
        (b( 78) and (a( 17))) xor
        (b( 79) and (a( 16))) xor
        (b( 80) and (a( 15))) xor
        (b( 81) and (a( 14))) xor
        (b( 82) and (a( 13))) xor
        (b( 83) and (a( 12))) xor
        (b( 84) and (a( 11))) xor
        (b( 85) and (a( 10))) xor
        (b( 86) and (a(  9))) xor
        (b( 87) and (a(  8))) xor
        (b( 88) and (a(  7))) xor
        (b( 89) and (a(  6) xor a(127))) xor
        (b( 90) and (a(  5) xor a(126))) xor
        (b( 91) and (a(  4) xor a(125))) xor
        (b( 92) and (a(  3) xor a(124))) xor
        (b( 93) and (a(  2) xor a(123))) xor
        (b( 94) and (a(  1) xor a(122) xor a(127))) xor
        (b( 95) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 96) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 97) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 98) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b( 99) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(100) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(101) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(102) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(103) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(104) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(105) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(106) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(107) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(108) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(109) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(110) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(111) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(112) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(113) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(114) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(115) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(116) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(117) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(118) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(119) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(120) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(121) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(122) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(123) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(124) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(125) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(126) and (a( 90) xor a( 95) xor a( 96) xor a( 97))) xor
        (b(127) and (a( 89) xor a( 94) xor a( 95) xor a( 96)));
    c( 96) <= 
        (b(  0) and (a( 96))) xor
        (b(  1) and (a( 95))) xor
        (b(  2) and (a( 94))) xor
        (b(  3) and (a( 93))) xor
        (b(  4) and (a( 92))) xor
        (b(  5) and (a( 91))) xor
        (b(  6) and (a( 90))) xor
        (b(  7) and (a( 89))) xor
        (b(  8) and (a( 88))) xor
        (b(  9) and (a( 87))) xor
        (b( 10) and (a( 86))) xor
        (b( 11) and (a( 85))) xor
        (b( 12) and (a( 84))) xor
        (b( 13) and (a( 83))) xor
        (b( 14) and (a( 82))) xor
        (b( 15) and (a( 81))) xor
        (b( 16) and (a( 80))) xor
        (b( 17) and (a( 79))) xor
        (b( 18) and (a( 78))) xor
        (b( 19) and (a( 77))) xor
        (b( 20) and (a( 76))) xor
        (b( 21) and (a( 75))) xor
        (b( 22) and (a( 74))) xor
        (b( 23) and (a( 73))) xor
        (b( 24) and (a( 72))) xor
        (b( 25) and (a( 71))) xor
        (b( 26) and (a( 70))) xor
        (b( 27) and (a( 69))) xor
        (b( 28) and (a( 68))) xor
        (b( 29) and (a( 67))) xor
        (b( 30) and (a( 66))) xor
        (b( 31) and (a( 65))) xor
        (b( 32) and (a( 64))) xor
        (b( 33) and (a( 63))) xor
        (b( 34) and (a( 62))) xor
        (b( 35) and (a( 61))) xor
        (b( 36) and (a( 60))) xor
        (b( 37) and (a( 59))) xor
        (b( 38) and (a( 58))) xor
        (b( 39) and (a( 57))) xor
        (b( 40) and (a( 56))) xor
        (b( 41) and (a( 55))) xor
        (b( 42) and (a( 54))) xor
        (b( 43) and (a( 53))) xor
        (b( 44) and (a( 52))) xor
        (b( 45) and (a( 51))) xor
        (b( 46) and (a( 50))) xor
        (b( 47) and (a( 49))) xor
        (b( 48) and (a( 48))) xor
        (b( 49) and (a( 47))) xor
        (b( 50) and (a( 46))) xor
        (b( 51) and (a( 45))) xor
        (b( 52) and (a( 44))) xor
        (b( 53) and (a( 43))) xor
        (b( 54) and (a( 42))) xor
        (b( 55) and (a( 41))) xor
        (b( 56) and (a( 40))) xor
        (b( 57) and (a( 39))) xor
        (b( 58) and (a( 38))) xor
        (b( 59) and (a( 37))) xor
        (b( 60) and (a( 36))) xor
        (b( 61) and (a( 35))) xor
        (b( 62) and (a( 34))) xor
        (b( 63) and (a( 33))) xor
        (b( 64) and (a( 32))) xor
        (b( 65) and (a( 31))) xor
        (b( 66) and (a( 30))) xor
        (b( 67) and (a( 29))) xor
        (b( 68) and (a( 28))) xor
        (b( 69) and (a( 27))) xor
        (b( 70) and (a( 26))) xor
        (b( 71) and (a( 25))) xor
        (b( 72) and (a( 24))) xor
        (b( 73) and (a( 23))) xor
        (b( 74) and (a( 22))) xor
        (b( 75) and (a( 21))) xor
        (b( 76) and (a( 20))) xor
        (b( 77) and (a( 19))) xor
        (b( 78) and (a( 18))) xor
        (b( 79) and (a( 17))) xor
        (b( 80) and (a( 16))) xor
        (b( 81) and (a( 15))) xor
        (b( 82) and (a( 14))) xor
        (b( 83) and (a( 13))) xor
        (b( 84) and (a( 12))) xor
        (b( 85) and (a( 11))) xor
        (b( 86) and (a( 10))) xor
        (b( 87) and (a(  9))) xor
        (b( 88) and (a(  8))) xor
        (b( 89) and (a(  7))) xor
        (b( 90) and (a(  6) xor a(127))) xor
        (b( 91) and (a(  5) xor a(126))) xor
        (b( 92) and (a(  4) xor a(125))) xor
        (b( 93) and (a(  3) xor a(124))) xor
        (b( 94) and (a(  2) xor a(123))) xor
        (b( 95) and (a(  1) xor a(122) xor a(127))) xor
        (b( 96) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 97) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 98) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b( 99) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(100) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(101) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(102) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(103) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(104) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(105) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(106) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(107) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(108) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(109) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(110) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(111) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(112) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(113) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(114) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(115) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(116) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(117) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(118) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(119) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(120) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(121) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(122) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(123) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(124) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(125) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(126) and (a( 91) xor a( 96) xor a( 97) xor a( 98))) xor
        (b(127) and (a( 90) xor a( 95) xor a( 96) xor a( 97)));
    c( 97) <= 
        (b(  0) and (a( 97))) xor
        (b(  1) and (a( 96))) xor
        (b(  2) and (a( 95))) xor
        (b(  3) and (a( 94))) xor
        (b(  4) and (a( 93))) xor
        (b(  5) and (a( 92))) xor
        (b(  6) and (a( 91))) xor
        (b(  7) and (a( 90))) xor
        (b(  8) and (a( 89))) xor
        (b(  9) and (a( 88))) xor
        (b( 10) and (a( 87))) xor
        (b( 11) and (a( 86))) xor
        (b( 12) and (a( 85))) xor
        (b( 13) and (a( 84))) xor
        (b( 14) and (a( 83))) xor
        (b( 15) and (a( 82))) xor
        (b( 16) and (a( 81))) xor
        (b( 17) and (a( 80))) xor
        (b( 18) and (a( 79))) xor
        (b( 19) and (a( 78))) xor
        (b( 20) and (a( 77))) xor
        (b( 21) and (a( 76))) xor
        (b( 22) and (a( 75))) xor
        (b( 23) and (a( 74))) xor
        (b( 24) and (a( 73))) xor
        (b( 25) and (a( 72))) xor
        (b( 26) and (a( 71))) xor
        (b( 27) and (a( 70))) xor
        (b( 28) and (a( 69))) xor
        (b( 29) and (a( 68))) xor
        (b( 30) and (a( 67))) xor
        (b( 31) and (a( 66))) xor
        (b( 32) and (a( 65))) xor
        (b( 33) and (a( 64))) xor
        (b( 34) and (a( 63))) xor
        (b( 35) and (a( 62))) xor
        (b( 36) and (a( 61))) xor
        (b( 37) and (a( 60))) xor
        (b( 38) and (a( 59))) xor
        (b( 39) and (a( 58))) xor
        (b( 40) and (a( 57))) xor
        (b( 41) and (a( 56))) xor
        (b( 42) and (a( 55))) xor
        (b( 43) and (a( 54))) xor
        (b( 44) and (a( 53))) xor
        (b( 45) and (a( 52))) xor
        (b( 46) and (a( 51))) xor
        (b( 47) and (a( 50))) xor
        (b( 48) and (a( 49))) xor
        (b( 49) and (a( 48))) xor
        (b( 50) and (a( 47))) xor
        (b( 51) and (a( 46))) xor
        (b( 52) and (a( 45))) xor
        (b( 53) and (a( 44))) xor
        (b( 54) and (a( 43))) xor
        (b( 55) and (a( 42))) xor
        (b( 56) and (a( 41))) xor
        (b( 57) and (a( 40))) xor
        (b( 58) and (a( 39))) xor
        (b( 59) and (a( 38))) xor
        (b( 60) and (a( 37))) xor
        (b( 61) and (a( 36))) xor
        (b( 62) and (a( 35))) xor
        (b( 63) and (a( 34))) xor
        (b( 64) and (a( 33))) xor
        (b( 65) and (a( 32))) xor
        (b( 66) and (a( 31))) xor
        (b( 67) and (a( 30))) xor
        (b( 68) and (a( 29))) xor
        (b( 69) and (a( 28))) xor
        (b( 70) and (a( 27))) xor
        (b( 71) and (a( 26))) xor
        (b( 72) and (a( 25))) xor
        (b( 73) and (a( 24))) xor
        (b( 74) and (a( 23))) xor
        (b( 75) and (a( 22))) xor
        (b( 76) and (a( 21))) xor
        (b( 77) and (a( 20))) xor
        (b( 78) and (a( 19))) xor
        (b( 79) and (a( 18))) xor
        (b( 80) and (a( 17))) xor
        (b( 81) and (a( 16))) xor
        (b( 82) and (a( 15))) xor
        (b( 83) and (a( 14))) xor
        (b( 84) and (a( 13))) xor
        (b( 85) and (a( 12))) xor
        (b( 86) and (a( 11))) xor
        (b( 87) and (a( 10))) xor
        (b( 88) and (a(  9))) xor
        (b( 89) and (a(  8))) xor
        (b( 90) and (a(  7))) xor
        (b( 91) and (a(  6) xor a(127))) xor
        (b( 92) and (a(  5) xor a(126))) xor
        (b( 93) and (a(  4) xor a(125))) xor
        (b( 94) and (a(  3) xor a(124))) xor
        (b( 95) and (a(  2) xor a(123))) xor
        (b( 96) and (a(  1) xor a(122) xor a(127))) xor
        (b( 97) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 98) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b( 99) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(100) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(101) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(102) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(103) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(104) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(105) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(106) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(107) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(108) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(109) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(110) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(111) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(112) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(113) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(114) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(115) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(116) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(117) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(118) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(119) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(120) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(121) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(122) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(123) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(124) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(125) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(126) and (a( 92) xor a( 97) xor a( 98) xor a( 99))) xor
        (b(127) and (a( 91) xor a( 96) xor a( 97) xor a( 98)));
    c( 98) <= 
        (b(  0) and (a( 98))) xor
        (b(  1) and (a( 97))) xor
        (b(  2) and (a( 96))) xor
        (b(  3) and (a( 95))) xor
        (b(  4) and (a( 94))) xor
        (b(  5) and (a( 93))) xor
        (b(  6) and (a( 92))) xor
        (b(  7) and (a( 91))) xor
        (b(  8) and (a( 90))) xor
        (b(  9) and (a( 89))) xor
        (b( 10) and (a( 88))) xor
        (b( 11) and (a( 87))) xor
        (b( 12) and (a( 86))) xor
        (b( 13) and (a( 85))) xor
        (b( 14) and (a( 84))) xor
        (b( 15) and (a( 83))) xor
        (b( 16) and (a( 82))) xor
        (b( 17) and (a( 81))) xor
        (b( 18) and (a( 80))) xor
        (b( 19) and (a( 79))) xor
        (b( 20) and (a( 78))) xor
        (b( 21) and (a( 77))) xor
        (b( 22) and (a( 76))) xor
        (b( 23) and (a( 75))) xor
        (b( 24) and (a( 74))) xor
        (b( 25) and (a( 73))) xor
        (b( 26) and (a( 72))) xor
        (b( 27) and (a( 71))) xor
        (b( 28) and (a( 70))) xor
        (b( 29) and (a( 69))) xor
        (b( 30) and (a( 68))) xor
        (b( 31) and (a( 67))) xor
        (b( 32) and (a( 66))) xor
        (b( 33) and (a( 65))) xor
        (b( 34) and (a( 64))) xor
        (b( 35) and (a( 63))) xor
        (b( 36) and (a( 62))) xor
        (b( 37) and (a( 61))) xor
        (b( 38) and (a( 60))) xor
        (b( 39) and (a( 59))) xor
        (b( 40) and (a( 58))) xor
        (b( 41) and (a( 57))) xor
        (b( 42) and (a( 56))) xor
        (b( 43) and (a( 55))) xor
        (b( 44) and (a( 54))) xor
        (b( 45) and (a( 53))) xor
        (b( 46) and (a( 52))) xor
        (b( 47) and (a( 51))) xor
        (b( 48) and (a( 50))) xor
        (b( 49) and (a( 49))) xor
        (b( 50) and (a( 48))) xor
        (b( 51) and (a( 47))) xor
        (b( 52) and (a( 46))) xor
        (b( 53) and (a( 45))) xor
        (b( 54) and (a( 44))) xor
        (b( 55) and (a( 43))) xor
        (b( 56) and (a( 42))) xor
        (b( 57) and (a( 41))) xor
        (b( 58) and (a( 40))) xor
        (b( 59) and (a( 39))) xor
        (b( 60) and (a( 38))) xor
        (b( 61) and (a( 37))) xor
        (b( 62) and (a( 36))) xor
        (b( 63) and (a( 35))) xor
        (b( 64) and (a( 34))) xor
        (b( 65) and (a( 33))) xor
        (b( 66) and (a( 32))) xor
        (b( 67) and (a( 31))) xor
        (b( 68) and (a( 30))) xor
        (b( 69) and (a( 29))) xor
        (b( 70) and (a( 28))) xor
        (b( 71) and (a( 27))) xor
        (b( 72) and (a( 26))) xor
        (b( 73) and (a( 25))) xor
        (b( 74) and (a( 24))) xor
        (b( 75) and (a( 23))) xor
        (b( 76) and (a( 22))) xor
        (b( 77) and (a( 21))) xor
        (b( 78) and (a( 20))) xor
        (b( 79) and (a( 19))) xor
        (b( 80) and (a( 18))) xor
        (b( 81) and (a( 17))) xor
        (b( 82) and (a( 16))) xor
        (b( 83) and (a( 15))) xor
        (b( 84) and (a( 14))) xor
        (b( 85) and (a( 13))) xor
        (b( 86) and (a( 12))) xor
        (b( 87) and (a( 11))) xor
        (b( 88) and (a( 10))) xor
        (b( 89) and (a(  9))) xor
        (b( 90) and (a(  8))) xor
        (b( 91) and (a(  7))) xor
        (b( 92) and (a(  6) xor a(127))) xor
        (b( 93) and (a(  5) xor a(126))) xor
        (b( 94) and (a(  4) xor a(125))) xor
        (b( 95) and (a(  3) xor a(124))) xor
        (b( 96) and (a(  2) xor a(123))) xor
        (b( 97) and (a(  1) xor a(122) xor a(127))) xor
        (b( 98) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b( 99) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(100) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(101) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(102) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(103) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(104) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(105) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(106) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(107) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(108) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(109) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(110) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(111) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(112) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(113) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(114) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(115) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(116) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(117) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(118) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(119) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(120) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(121) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(122) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(123) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(124) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(125) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(126) and (a( 93) xor a( 98) xor a( 99) xor a(100))) xor
        (b(127) and (a( 92) xor a( 97) xor a( 98) xor a( 99)));
    c( 99) <= 
        (b(  0) and (a( 99))) xor
        (b(  1) and (a( 98))) xor
        (b(  2) and (a( 97))) xor
        (b(  3) and (a( 96))) xor
        (b(  4) and (a( 95))) xor
        (b(  5) and (a( 94))) xor
        (b(  6) and (a( 93))) xor
        (b(  7) and (a( 92))) xor
        (b(  8) and (a( 91))) xor
        (b(  9) and (a( 90))) xor
        (b( 10) and (a( 89))) xor
        (b( 11) and (a( 88))) xor
        (b( 12) and (a( 87))) xor
        (b( 13) and (a( 86))) xor
        (b( 14) and (a( 85))) xor
        (b( 15) and (a( 84))) xor
        (b( 16) and (a( 83))) xor
        (b( 17) and (a( 82))) xor
        (b( 18) and (a( 81))) xor
        (b( 19) and (a( 80))) xor
        (b( 20) and (a( 79))) xor
        (b( 21) and (a( 78))) xor
        (b( 22) and (a( 77))) xor
        (b( 23) and (a( 76))) xor
        (b( 24) and (a( 75))) xor
        (b( 25) and (a( 74))) xor
        (b( 26) and (a( 73))) xor
        (b( 27) and (a( 72))) xor
        (b( 28) and (a( 71))) xor
        (b( 29) and (a( 70))) xor
        (b( 30) and (a( 69))) xor
        (b( 31) and (a( 68))) xor
        (b( 32) and (a( 67))) xor
        (b( 33) and (a( 66))) xor
        (b( 34) and (a( 65))) xor
        (b( 35) and (a( 64))) xor
        (b( 36) and (a( 63))) xor
        (b( 37) and (a( 62))) xor
        (b( 38) and (a( 61))) xor
        (b( 39) and (a( 60))) xor
        (b( 40) and (a( 59))) xor
        (b( 41) and (a( 58))) xor
        (b( 42) and (a( 57))) xor
        (b( 43) and (a( 56))) xor
        (b( 44) and (a( 55))) xor
        (b( 45) and (a( 54))) xor
        (b( 46) and (a( 53))) xor
        (b( 47) and (a( 52))) xor
        (b( 48) and (a( 51))) xor
        (b( 49) and (a( 50))) xor
        (b( 50) and (a( 49))) xor
        (b( 51) and (a( 48))) xor
        (b( 52) and (a( 47))) xor
        (b( 53) and (a( 46))) xor
        (b( 54) and (a( 45))) xor
        (b( 55) and (a( 44))) xor
        (b( 56) and (a( 43))) xor
        (b( 57) and (a( 42))) xor
        (b( 58) and (a( 41))) xor
        (b( 59) and (a( 40))) xor
        (b( 60) and (a( 39))) xor
        (b( 61) and (a( 38))) xor
        (b( 62) and (a( 37))) xor
        (b( 63) and (a( 36))) xor
        (b( 64) and (a( 35))) xor
        (b( 65) and (a( 34))) xor
        (b( 66) and (a( 33))) xor
        (b( 67) and (a( 32))) xor
        (b( 68) and (a( 31))) xor
        (b( 69) and (a( 30))) xor
        (b( 70) and (a( 29))) xor
        (b( 71) and (a( 28))) xor
        (b( 72) and (a( 27))) xor
        (b( 73) and (a( 26))) xor
        (b( 74) and (a( 25))) xor
        (b( 75) and (a( 24))) xor
        (b( 76) and (a( 23))) xor
        (b( 77) and (a( 22))) xor
        (b( 78) and (a( 21))) xor
        (b( 79) and (a( 20))) xor
        (b( 80) and (a( 19))) xor
        (b( 81) and (a( 18))) xor
        (b( 82) and (a( 17))) xor
        (b( 83) and (a( 16))) xor
        (b( 84) and (a( 15))) xor
        (b( 85) and (a( 14))) xor
        (b( 86) and (a( 13))) xor
        (b( 87) and (a( 12))) xor
        (b( 88) and (a( 11))) xor
        (b( 89) and (a( 10))) xor
        (b( 90) and (a(  9))) xor
        (b( 91) and (a(  8))) xor
        (b( 92) and (a(  7))) xor
        (b( 93) and (a(  6) xor a(127))) xor
        (b( 94) and (a(  5) xor a(126))) xor
        (b( 95) and (a(  4) xor a(125))) xor
        (b( 96) and (a(  3) xor a(124))) xor
        (b( 97) and (a(  2) xor a(123))) xor
        (b( 98) and (a(  1) xor a(122) xor a(127))) xor
        (b( 99) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(100) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(101) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(102) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(103) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(104) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(105) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(106) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(107) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(108) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(109) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(110) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(111) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(112) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(113) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(114) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(115) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(116) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(117) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(118) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(119) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(120) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(121) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(122) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(123) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(124) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(125) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(126) and (a( 94) xor a( 99) xor a(100) xor a(101))) xor
        (b(127) and (a( 93) xor a( 98) xor a( 99) xor a(100)));
    c(100) <= 
        (b(  0) and (a(100))) xor
        (b(  1) and (a( 99))) xor
        (b(  2) and (a( 98))) xor
        (b(  3) and (a( 97))) xor
        (b(  4) and (a( 96))) xor
        (b(  5) and (a( 95))) xor
        (b(  6) and (a( 94))) xor
        (b(  7) and (a( 93))) xor
        (b(  8) and (a( 92))) xor
        (b(  9) and (a( 91))) xor
        (b( 10) and (a( 90))) xor
        (b( 11) and (a( 89))) xor
        (b( 12) and (a( 88))) xor
        (b( 13) and (a( 87))) xor
        (b( 14) and (a( 86))) xor
        (b( 15) and (a( 85))) xor
        (b( 16) and (a( 84))) xor
        (b( 17) and (a( 83))) xor
        (b( 18) and (a( 82))) xor
        (b( 19) and (a( 81))) xor
        (b( 20) and (a( 80))) xor
        (b( 21) and (a( 79))) xor
        (b( 22) and (a( 78))) xor
        (b( 23) and (a( 77))) xor
        (b( 24) and (a( 76))) xor
        (b( 25) and (a( 75))) xor
        (b( 26) and (a( 74))) xor
        (b( 27) and (a( 73))) xor
        (b( 28) and (a( 72))) xor
        (b( 29) and (a( 71))) xor
        (b( 30) and (a( 70))) xor
        (b( 31) and (a( 69))) xor
        (b( 32) and (a( 68))) xor
        (b( 33) and (a( 67))) xor
        (b( 34) and (a( 66))) xor
        (b( 35) and (a( 65))) xor
        (b( 36) and (a( 64))) xor
        (b( 37) and (a( 63))) xor
        (b( 38) and (a( 62))) xor
        (b( 39) and (a( 61))) xor
        (b( 40) and (a( 60))) xor
        (b( 41) and (a( 59))) xor
        (b( 42) and (a( 58))) xor
        (b( 43) and (a( 57))) xor
        (b( 44) and (a( 56))) xor
        (b( 45) and (a( 55))) xor
        (b( 46) and (a( 54))) xor
        (b( 47) and (a( 53))) xor
        (b( 48) and (a( 52))) xor
        (b( 49) and (a( 51))) xor
        (b( 50) and (a( 50))) xor
        (b( 51) and (a( 49))) xor
        (b( 52) and (a( 48))) xor
        (b( 53) and (a( 47))) xor
        (b( 54) and (a( 46))) xor
        (b( 55) and (a( 45))) xor
        (b( 56) and (a( 44))) xor
        (b( 57) and (a( 43))) xor
        (b( 58) and (a( 42))) xor
        (b( 59) and (a( 41))) xor
        (b( 60) and (a( 40))) xor
        (b( 61) and (a( 39))) xor
        (b( 62) and (a( 38))) xor
        (b( 63) and (a( 37))) xor
        (b( 64) and (a( 36))) xor
        (b( 65) and (a( 35))) xor
        (b( 66) and (a( 34))) xor
        (b( 67) and (a( 33))) xor
        (b( 68) and (a( 32))) xor
        (b( 69) and (a( 31))) xor
        (b( 70) and (a( 30))) xor
        (b( 71) and (a( 29))) xor
        (b( 72) and (a( 28))) xor
        (b( 73) and (a( 27))) xor
        (b( 74) and (a( 26))) xor
        (b( 75) and (a( 25))) xor
        (b( 76) and (a( 24))) xor
        (b( 77) and (a( 23))) xor
        (b( 78) and (a( 22))) xor
        (b( 79) and (a( 21))) xor
        (b( 80) and (a( 20))) xor
        (b( 81) and (a( 19))) xor
        (b( 82) and (a( 18))) xor
        (b( 83) and (a( 17))) xor
        (b( 84) and (a( 16))) xor
        (b( 85) and (a( 15))) xor
        (b( 86) and (a( 14))) xor
        (b( 87) and (a( 13))) xor
        (b( 88) and (a( 12))) xor
        (b( 89) and (a( 11))) xor
        (b( 90) and (a( 10))) xor
        (b( 91) and (a(  9))) xor
        (b( 92) and (a(  8))) xor
        (b( 93) and (a(  7))) xor
        (b( 94) and (a(  6) xor a(127))) xor
        (b( 95) and (a(  5) xor a(126))) xor
        (b( 96) and (a(  4) xor a(125))) xor
        (b( 97) and (a(  3) xor a(124))) xor
        (b( 98) and (a(  2) xor a(123))) xor
        (b( 99) and (a(  1) xor a(122) xor a(127))) xor
        (b(100) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(101) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(102) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(103) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(104) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(105) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(106) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(107) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(108) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(109) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(110) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(111) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(112) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(113) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(114) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(115) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(116) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(117) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(118) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(119) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(120) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(121) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(122) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(123) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(124) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(125) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(126) and (a( 95) xor a(100) xor a(101) xor a(102))) xor
        (b(127) and (a( 94) xor a( 99) xor a(100) xor a(101)));
    c(101) <= 
        (b(  0) and (a(101))) xor
        (b(  1) and (a(100))) xor
        (b(  2) and (a( 99))) xor
        (b(  3) and (a( 98))) xor
        (b(  4) and (a( 97))) xor
        (b(  5) and (a( 96))) xor
        (b(  6) and (a( 95))) xor
        (b(  7) and (a( 94))) xor
        (b(  8) and (a( 93))) xor
        (b(  9) and (a( 92))) xor
        (b( 10) and (a( 91))) xor
        (b( 11) and (a( 90))) xor
        (b( 12) and (a( 89))) xor
        (b( 13) and (a( 88))) xor
        (b( 14) and (a( 87))) xor
        (b( 15) and (a( 86))) xor
        (b( 16) and (a( 85))) xor
        (b( 17) and (a( 84))) xor
        (b( 18) and (a( 83))) xor
        (b( 19) and (a( 82))) xor
        (b( 20) and (a( 81))) xor
        (b( 21) and (a( 80))) xor
        (b( 22) and (a( 79))) xor
        (b( 23) and (a( 78))) xor
        (b( 24) and (a( 77))) xor
        (b( 25) and (a( 76))) xor
        (b( 26) and (a( 75))) xor
        (b( 27) and (a( 74))) xor
        (b( 28) and (a( 73))) xor
        (b( 29) and (a( 72))) xor
        (b( 30) and (a( 71))) xor
        (b( 31) and (a( 70))) xor
        (b( 32) and (a( 69))) xor
        (b( 33) and (a( 68))) xor
        (b( 34) and (a( 67))) xor
        (b( 35) and (a( 66))) xor
        (b( 36) and (a( 65))) xor
        (b( 37) and (a( 64))) xor
        (b( 38) and (a( 63))) xor
        (b( 39) and (a( 62))) xor
        (b( 40) and (a( 61))) xor
        (b( 41) and (a( 60))) xor
        (b( 42) and (a( 59))) xor
        (b( 43) and (a( 58))) xor
        (b( 44) and (a( 57))) xor
        (b( 45) and (a( 56))) xor
        (b( 46) and (a( 55))) xor
        (b( 47) and (a( 54))) xor
        (b( 48) and (a( 53))) xor
        (b( 49) and (a( 52))) xor
        (b( 50) and (a( 51))) xor
        (b( 51) and (a( 50))) xor
        (b( 52) and (a( 49))) xor
        (b( 53) and (a( 48))) xor
        (b( 54) and (a( 47))) xor
        (b( 55) and (a( 46))) xor
        (b( 56) and (a( 45))) xor
        (b( 57) and (a( 44))) xor
        (b( 58) and (a( 43))) xor
        (b( 59) and (a( 42))) xor
        (b( 60) and (a( 41))) xor
        (b( 61) and (a( 40))) xor
        (b( 62) and (a( 39))) xor
        (b( 63) and (a( 38))) xor
        (b( 64) and (a( 37))) xor
        (b( 65) and (a( 36))) xor
        (b( 66) and (a( 35))) xor
        (b( 67) and (a( 34))) xor
        (b( 68) and (a( 33))) xor
        (b( 69) and (a( 32))) xor
        (b( 70) and (a( 31))) xor
        (b( 71) and (a( 30))) xor
        (b( 72) and (a( 29))) xor
        (b( 73) and (a( 28))) xor
        (b( 74) and (a( 27))) xor
        (b( 75) and (a( 26))) xor
        (b( 76) and (a( 25))) xor
        (b( 77) and (a( 24))) xor
        (b( 78) and (a( 23))) xor
        (b( 79) and (a( 22))) xor
        (b( 80) and (a( 21))) xor
        (b( 81) and (a( 20))) xor
        (b( 82) and (a( 19))) xor
        (b( 83) and (a( 18))) xor
        (b( 84) and (a( 17))) xor
        (b( 85) and (a( 16))) xor
        (b( 86) and (a( 15))) xor
        (b( 87) and (a( 14))) xor
        (b( 88) and (a( 13))) xor
        (b( 89) and (a( 12))) xor
        (b( 90) and (a( 11))) xor
        (b( 91) and (a( 10))) xor
        (b( 92) and (a(  9))) xor
        (b( 93) and (a(  8))) xor
        (b( 94) and (a(  7))) xor
        (b( 95) and (a(  6) xor a(127))) xor
        (b( 96) and (a(  5) xor a(126))) xor
        (b( 97) and (a(  4) xor a(125))) xor
        (b( 98) and (a(  3) xor a(124))) xor
        (b( 99) and (a(  2) xor a(123))) xor
        (b(100) and (a(  1) xor a(122) xor a(127))) xor
        (b(101) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(102) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(103) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(104) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(105) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(106) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(107) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(108) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(109) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(110) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(111) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(112) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(113) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(114) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(115) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(116) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(117) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(118) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(119) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(120) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(121) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(122) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(123) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(124) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(125) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(126) and (a( 96) xor a(101) xor a(102) xor a(103))) xor
        (b(127) and (a( 95) xor a(100) xor a(101) xor a(102)));
    c(102) <= 
        (b(  0) and (a(102))) xor
        (b(  1) and (a(101))) xor
        (b(  2) and (a(100))) xor
        (b(  3) and (a( 99))) xor
        (b(  4) and (a( 98))) xor
        (b(  5) and (a( 97))) xor
        (b(  6) and (a( 96))) xor
        (b(  7) and (a( 95))) xor
        (b(  8) and (a( 94))) xor
        (b(  9) and (a( 93))) xor
        (b( 10) and (a( 92))) xor
        (b( 11) and (a( 91))) xor
        (b( 12) and (a( 90))) xor
        (b( 13) and (a( 89))) xor
        (b( 14) and (a( 88))) xor
        (b( 15) and (a( 87))) xor
        (b( 16) and (a( 86))) xor
        (b( 17) and (a( 85))) xor
        (b( 18) and (a( 84))) xor
        (b( 19) and (a( 83))) xor
        (b( 20) and (a( 82))) xor
        (b( 21) and (a( 81))) xor
        (b( 22) and (a( 80))) xor
        (b( 23) and (a( 79))) xor
        (b( 24) and (a( 78))) xor
        (b( 25) and (a( 77))) xor
        (b( 26) and (a( 76))) xor
        (b( 27) and (a( 75))) xor
        (b( 28) and (a( 74))) xor
        (b( 29) and (a( 73))) xor
        (b( 30) and (a( 72))) xor
        (b( 31) and (a( 71))) xor
        (b( 32) and (a( 70))) xor
        (b( 33) and (a( 69))) xor
        (b( 34) and (a( 68))) xor
        (b( 35) and (a( 67))) xor
        (b( 36) and (a( 66))) xor
        (b( 37) and (a( 65))) xor
        (b( 38) and (a( 64))) xor
        (b( 39) and (a( 63))) xor
        (b( 40) and (a( 62))) xor
        (b( 41) and (a( 61))) xor
        (b( 42) and (a( 60))) xor
        (b( 43) and (a( 59))) xor
        (b( 44) and (a( 58))) xor
        (b( 45) and (a( 57))) xor
        (b( 46) and (a( 56))) xor
        (b( 47) and (a( 55))) xor
        (b( 48) and (a( 54))) xor
        (b( 49) and (a( 53))) xor
        (b( 50) and (a( 52))) xor
        (b( 51) and (a( 51))) xor
        (b( 52) and (a( 50))) xor
        (b( 53) and (a( 49))) xor
        (b( 54) and (a( 48))) xor
        (b( 55) and (a( 47))) xor
        (b( 56) and (a( 46))) xor
        (b( 57) and (a( 45))) xor
        (b( 58) and (a( 44))) xor
        (b( 59) and (a( 43))) xor
        (b( 60) and (a( 42))) xor
        (b( 61) and (a( 41))) xor
        (b( 62) and (a( 40))) xor
        (b( 63) and (a( 39))) xor
        (b( 64) and (a( 38))) xor
        (b( 65) and (a( 37))) xor
        (b( 66) and (a( 36))) xor
        (b( 67) and (a( 35))) xor
        (b( 68) and (a( 34))) xor
        (b( 69) and (a( 33))) xor
        (b( 70) and (a( 32))) xor
        (b( 71) and (a( 31))) xor
        (b( 72) and (a( 30))) xor
        (b( 73) and (a( 29))) xor
        (b( 74) and (a( 28))) xor
        (b( 75) and (a( 27))) xor
        (b( 76) and (a( 26))) xor
        (b( 77) and (a( 25))) xor
        (b( 78) and (a( 24))) xor
        (b( 79) and (a( 23))) xor
        (b( 80) and (a( 22))) xor
        (b( 81) and (a( 21))) xor
        (b( 82) and (a( 20))) xor
        (b( 83) and (a( 19))) xor
        (b( 84) and (a( 18))) xor
        (b( 85) and (a( 17))) xor
        (b( 86) and (a( 16))) xor
        (b( 87) and (a( 15))) xor
        (b( 88) and (a( 14))) xor
        (b( 89) and (a( 13))) xor
        (b( 90) and (a( 12))) xor
        (b( 91) and (a( 11))) xor
        (b( 92) and (a( 10))) xor
        (b( 93) and (a(  9))) xor
        (b( 94) and (a(  8))) xor
        (b( 95) and (a(  7))) xor
        (b( 96) and (a(  6) xor a(127))) xor
        (b( 97) and (a(  5) xor a(126))) xor
        (b( 98) and (a(  4) xor a(125))) xor
        (b( 99) and (a(  3) xor a(124))) xor
        (b(100) and (a(  2) xor a(123))) xor
        (b(101) and (a(  1) xor a(122) xor a(127))) xor
        (b(102) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(103) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(104) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(105) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(106) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(107) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(108) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(109) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(110) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(111) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(112) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(113) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(114) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(115) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(116) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(117) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(118) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(119) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(120) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(121) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(122) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(123) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(124) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(125) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(126) and (a( 97) xor a(102) xor a(103) xor a(104))) xor
        (b(127) and (a( 96) xor a(101) xor a(102) xor a(103)));
    c(103) <= 
        (b(  0) and (a(103))) xor
        (b(  1) and (a(102))) xor
        (b(  2) and (a(101))) xor
        (b(  3) and (a(100))) xor
        (b(  4) and (a( 99))) xor
        (b(  5) and (a( 98))) xor
        (b(  6) and (a( 97))) xor
        (b(  7) and (a( 96))) xor
        (b(  8) and (a( 95))) xor
        (b(  9) and (a( 94))) xor
        (b( 10) and (a( 93))) xor
        (b( 11) and (a( 92))) xor
        (b( 12) and (a( 91))) xor
        (b( 13) and (a( 90))) xor
        (b( 14) and (a( 89))) xor
        (b( 15) and (a( 88))) xor
        (b( 16) and (a( 87))) xor
        (b( 17) and (a( 86))) xor
        (b( 18) and (a( 85))) xor
        (b( 19) and (a( 84))) xor
        (b( 20) and (a( 83))) xor
        (b( 21) and (a( 82))) xor
        (b( 22) and (a( 81))) xor
        (b( 23) and (a( 80))) xor
        (b( 24) and (a( 79))) xor
        (b( 25) and (a( 78))) xor
        (b( 26) and (a( 77))) xor
        (b( 27) and (a( 76))) xor
        (b( 28) and (a( 75))) xor
        (b( 29) and (a( 74))) xor
        (b( 30) and (a( 73))) xor
        (b( 31) and (a( 72))) xor
        (b( 32) and (a( 71))) xor
        (b( 33) and (a( 70))) xor
        (b( 34) and (a( 69))) xor
        (b( 35) and (a( 68))) xor
        (b( 36) and (a( 67))) xor
        (b( 37) and (a( 66))) xor
        (b( 38) and (a( 65))) xor
        (b( 39) and (a( 64))) xor
        (b( 40) and (a( 63))) xor
        (b( 41) and (a( 62))) xor
        (b( 42) and (a( 61))) xor
        (b( 43) and (a( 60))) xor
        (b( 44) and (a( 59))) xor
        (b( 45) and (a( 58))) xor
        (b( 46) and (a( 57))) xor
        (b( 47) and (a( 56))) xor
        (b( 48) and (a( 55))) xor
        (b( 49) and (a( 54))) xor
        (b( 50) and (a( 53))) xor
        (b( 51) and (a( 52))) xor
        (b( 52) and (a( 51))) xor
        (b( 53) and (a( 50))) xor
        (b( 54) and (a( 49))) xor
        (b( 55) and (a( 48))) xor
        (b( 56) and (a( 47))) xor
        (b( 57) and (a( 46))) xor
        (b( 58) and (a( 45))) xor
        (b( 59) and (a( 44))) xor
        (b( 60) and (a( 43))) xor
        (b( 61) and (a( 42))) xor
        (b( 62) and (a( 41))) xor
        (b( 63) and (a( 40))) xor
        (b( 64) and (a( 39))) xor
        (b( 65) and (a( 38))) xor
        (b( 66) and (a( 37))) xor
        (b( 67) and (a( 36))) xor
        (b( 68) and (a( 35))) xor
        (b( 69) and (a( 34))) xor
        (b( 70) and (a( 33))) xor
        (b( 71) and (a( 32))) xor
        (b( 72) and (a( 31))) xor
        (b( 73) and (a( 30))) xor
        (b( 74) and (a( 29))) xor
        (b( 75) and (a( 28))) xor
        (b( 76) and (a( 27))) xor
        (b( 77) and (a( 26))) xor
        (b( 78) and (a( 25))) xor
        (b( 79) and (a( 24))) xor
        (b( 80) and (a( 23))) xor
        (b( 81) and (a( 22))) xor
        (b( 82) and (a( 21))) xor
        (b( 83) and (a( 20))) xor
        (b( 84) and (a( 19))) xor
        (b( 85) and (a( 18))) xor
        (b( 86) and (a( 17))) xor
        (b( 87) and (a( 16))) xor
        (b( 88) and (a( 15))) xor
        (b( 89) and (a( 14))) xor
        (b( 90) and (a( 13))) xor
        (b( 91) and (a( 12))) xor
        (b( 92) and (a( 11))) xor
        (b( 93) and (a( 10))) xor
        (b( 94) and (a(  9))) xor
        (b( 95) and (a(  8))) xor
        (b( 96) and (a(  7))) xor
        (b( 97) and (a(  6) xor a(127))) xor
        (b( 98) and (a(  5) xor a(126))) xor
        (b( 99) and (a(  4) xor a(125))) xor
        (b(100) and (a(  3) xor a(124))) xor
        (b(101) and (a(  2) xor a(123))) xor
        (b(102) and (a(  1) xor a(122) xor a(127))) xor
        (b(103) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(104) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(105) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(106) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(107) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(108) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(109) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(110) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(111) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(112) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(113) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(114) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(115) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(116) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(117) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(118) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(119) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(120) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(121) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(122) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(123) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(124) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(125) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(126) and (a( 98) xor a(103) xor a(104) xor a(105))) xor
        (b(127) and (a( 97) xor a(102) xor a(103) xor a(104)));
    c(104) <= 
        (b(  0) and (a(104))) xor
        (b(  1) and (a(103))) xor
        (b(  2) and (a(102))) xor
        (b(  3) and (a(101))) xor
        (b(  4) and (a(100))) xor
        (b(  5) and (a( 99))) xor
        (b(  6) and (a( 98))) xor
        (b(  7) and (a( 97))) xor
        (b(  8) and (a( 96))) xor
        (b(  9) and (a( 95))) xor
        (b( 10) and (a( 94))) xor
        (b( 11) and (a( 93))) xor
        (b( 12) and (a( 92))) xor
        (b( 13) and (a( 91))) xor
        (b( 14) and (a( 90))) xor
        (b( 15) and (a( 89))) xor
        (b( 16) and (a( 88))) xor
        (b( 17) and (a( 87))) xor
        (b( 18) and (a( 86))) xor
        (b( 19) and (a( 85))) xor
        (b( 20) and (a( 84))) xor
        (b( 21) and (a( 83))) xor
        (b( 22) and (a( 82))) xor
        (b( 23) and (a( 81))) xor
        (b( 24) and (a( 80))) xor
        (b( 25) and (a( 79))) xor
        (b( 26) and (a( 78))) xor
        (b( 27) and (a( 77))) xor
        (b( 28) and (a( 76))) xor
        (b( 29) and (a( 75))) xor
        (b( 30) and (a( 74))) xor
        (b( 31) and (a( 73))) xor
        (b( 32) and (a( 72))) xor
        (b( 33) and (a( 71))) xor
        (b( 34) and (a( 70))) xor
        (b( 35) and (a( 69))) xor
        (b( 36) and (a( 68))) xor
        (b( 37) and (a( 67))) xor
        (b( 38) and (a( 66))) xor
        (b( 39) and (a( 65))) xor
        (b( 40) and (a( 64))) xor
        (b( 41) and (a( 63))) xor
        (b( 42) and (a( 62))) xor
        (b( 43) and (a( 61))) xor
        (b( 44) and (a( 60))) xor
        (b( 45) and (a( 59))) xor
        (b( 46) and (a( 58))) xor
        (b( 47) and (a( 57))) xor
        (b( 48) and (a( 56))) xor
        (b( 49) and (a( 55))) xor
        (b( 50) and (a( 54))) xor
        (b( 51) and (a( 53))) xor
        (b( 52) and (a( 52))) xor
        (b( 53) and (a( 51))) xor
        (b( 54) and (a( 50))) xor
        (b( 55) and (a( 49))) xor
        (b( 56) and (a( 48))) xor
        (b( 57) and (a( 47))) xor
        (b( 58) and (a( 46))) xor
        (b( 59) and (a( 45))) xor
        (b( 60) and (a( 44))) xor
        (b( 61) and (a( 43))) xor
        (b( 62) and (a( 42))) xor
        (b( 63) and (a( 41))) xor
        (b( 64) and (a( 40))) xor
        (b( 65) and (a( 39))) xor
        (b( 66) and (a( 38))) xor
        (b( 67) and (a( 37))) xor
        (b( 68) and (a( 36))) xor
        (b( 69) and (a( 35))) xor
        (b( 70) and (a( 34))) xor
        (b( 71) and (a( 33))) xor
        (b( 72) and (a( 32))) xor
        (b( 73) and (a( 31))) xor
        (b( 74) and (a( 30))) xor
        (b( 75) and (a( 29))) xor
        (b( 76) and (a( 28))) xor
        (b( 77) and (a( 27))) xor
        (b( 78) and (a( 26))) xor
        (b( 79) and (a( 25))) xor
        (b( 80) and (a( 24))) xor
        (b( 81) and (a( 23))) xor
        (b( 82) and (a( 22))) xor
        (b( 83) and (a( 21))) xor
        (b( 84) and (a( 20))) xor
        (b( 85) and (a( 19))) xor
        (b( 86) and (a( 18))) xor
        (b( 87) and (a( 17))) xor
        (b( 88) and (a( 16))) xor
        (b( 89) and (a( 15))) xor
        (b( 90) and (a( 14))) xor
        (b( 91) and (a( 13))) xor
        (b( 92) and (a( 12))) xor
        (b( 93) and (a( 11))) xor
        (b( 94) and (a( 10))) xor
        (b( 95) and (a(  9))) xor
        (b( 96) and (a(  8))) xor
        (b( 97) and (a(  7))) xor
        (b( 98) and (a(  6) xor a(127))) xor
        (b( 99) and (a(  5) xor a(126))) xor
        (b(100) and (a(  4) xor a(125))) xor
        (b(101) and (a(  3) xor a(124))) xor
        (b(102) and (a(  2) xor a(123))) xor
        (b(103) and (a(  1) xor a(122) xor a(127))) xor
        (b(104) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(105) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(106) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(107) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(108) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(109) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(110) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(111) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(112) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(113) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(114) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(115) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(116) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(117) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(118) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(119) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(120) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(121) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(122) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(123) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(124) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(125) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(126) and (a( 99) xor a(104) xor a(105) xor a(106))) xor
        (b(127) and (a( 98) xor a(103) xor a(104) xor a(105)));
    c(105) <= 
        (b(  0) and (a(105))) xor
        (b(  1) and (a(104))) xor
        (b(  2) and (a(103))) xor
        (b(  3) and (a(102))) xor
        (b(  4) and (a(101))) xor
        (b(  5) and (a(100))) xor
        (b(  6) and (a( 99))) xor
        (b(  7) and (a( 98))) xor
        (b(  8) and (a( 97))) xor
        (b(  9) and (a( 96))) xor
        (b( 10) and (a( 95))) xor
        (b( 11) and (a( 94))) xor
        (b( 12) and (a( 93))) xor
        (b( 13) and (a( 92))) xor
        (b( 14) and (a( 91))) xor
        (b( 15) and (a( 90))) xor
        (b( 16) and (a( 89))) xor
        (b( 17) and (a( 88))) xor
        (b( 18) and (a( 87))) xor
        (b( 19) and (a( 86))) xor
        (b( 20) and (a( 85))) xor
        (b( 21) and (a( 84))) xor
        (b( 22) and (a( 83))) xor
        (b( 23) and (a( 82))) xor
        (b( 24) and (a( 81))) xor
        (b( 25) and (a( 80))) xor
        (b( 26) and (a( 79))) xor
        (b( 27) and (a( 78))) xor
        (b( 28) and (a( 77))) xor
        (b( 29) and (a( 76))) xor
        (b( 30) and (a( 75))) xor
        (b( 31) and (a( 74))) xor
        (b( 32) and (a( 73))) xor
        (b( 33) and (a( 72))) xor
        (b( 34) and (a( 71))) xor
        (b( 35) and (a( 70))) xor
        (b( 36) and (a( 69))) xor
        (b( 37) and (a( 68))) xor
        (b( 38) and (a( 67))) xor
        (b( 39) and (a( 66))) xor
        (b( 40) and (a( 65))) xor
        (b( 41) and (a( 64))) xor
        (b( 42) and (a( 63))) xor
        (b( 43) and (a( 62))) xor
        (b( 44) and (a( 61))) xor
        (b( 45) and (a( 60))) xor
        (b( 46) and (a( 59))) xor
        (b( 47) and (a( 58))) xor
        (b( 48) and (a( 57))) xor
        (b( 49) and (a( 56))) xor
        (b( 50) and (a( 55))) xor
        (b( 51) and (a( 54))) xor
        (b( 52) and (a( 53))) xor
        (b( 53) and (a( 52))) xor
        (b( 54) and (a( 51))) xor
        (b( 55) and (a( 50))) xor
        (b( 56) and (a( 49))) xor
        (b( 57) and (a( 48))) xor
        (b( 58) and (a( 47))) xor
        (b( 59) and (a( 46))) xor
        (b( 60) and (a( 45))) xor
        (b( 61) and (a( 44))) xor
        (b( 62) and (a( 43))) xor
        (b( 63) and (a( 42))) xor
        (b( 64) and (a( 41))) xor
        (b( 65) and (a( 40))) xor
        (b( 66) and (a( 39))) xor
        (b( 67) and (a( 38))) xor
        (b( 68) and (a( 37))) xor
        (b( 69) and (a( 36))) xor
        (b( 70) and (a( 35))) xor
        (b( 71) and (a( 34))) xor
        (b( 72) and (a( 33))) xor
        (b( 73) and (a( 32))) xor
        (b( 74) and (a( 31))) xor
        (b( 75) and (a( 30))) xor
        (b( 76) and (a( 29))) xor
        (b( 77) and (a( 28))) xor
        (b( 78) and (a( 27))) xor
        (b( 79) and (a( 26))) xor
        (b( 80) and (a( 25))) xor
        (b( 81) and (a( 24))) xor
        (b( 82) and (a( 23))) xor
        (b( 83) and (a( 22))) xor
        (b( 84) and (a( 21))) xor
        (b( 85) and (a( 20))) xor
        (b( 86) and (a( 19))) xor
        (b( 87) and (a( 18))) xor
        (b( 88) and (a( 17))) xor
        (b( 89) and (a( 16))) xor
        (b( 90) and (a( 15))) xor
        (b( 91) and (a( 14))) xor
        (b( 92) and (a( 13))) xor
        (b( 93) and (a( 12))) xor
        (b( 94) and (a( 11))) xor
        (b( 95) and (a( 10))) xor
        (b( 96) and (a(  9))) xor
        (b( 97) and (a(  8))) xor
        (b( 98) and (a(  7))) xor
        (b( 99) and (a(  6) xor a(127))) xor
        (b(100) and (a(  5) xor a(126))) xor
        (b(101) and (a(  4) xor a(125))) xor
        (b(102) and (a(  3) xor a(124))) xor
        (b(103) and (a(  2) xor a(123))) xor
        (b(104) and (a(  1) xor a(122) xor a(127))) xor
        (b(105) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(106) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(107) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(108) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(109) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(110) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(111) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(112) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(113) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(114) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(115) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(116) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(117) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(118) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(119) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(120) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(121) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(122) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(123) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(124) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(125) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(126) and (a(100) xor a(105) xor a(106) xor a(107))) xor
        (b(127) and (a( 99) xor a(104) xor a(105) xor a(106)));
    c(106) <= 
        (b(  0) and (a(106))) xor
        (b(  1) and (a(105))) xor
        (b(  2) and (a(104))) xor
        (b(  3) and (a(103))) xor
        (b(  4) and (a(102))) xor
        (b(  5) and (a(101))) xor
        (b(  6) and (a(100))) xor
        (b(  7) and (a( 99))) xor
        (b(  8) and (a( 98))) xor
        (b(  9) and (a( 97))) xor
        (b( 10) and (a( 96))) xor
        (b( 11) and (a( 95))) xor
        (b( 12) and (a( 94))) xor
        (b( 13) and (a( 93))) xor
        (b( 14) and (a( 92))) xor
        (b( 15) and (a( 91))) xor
        (b( 16) and (a( 90))) xor
        (b( 17) and (a( 89))) xor
        (b( 18) and (a( 88))) xor
        (b( 19) and (a( 87))) xor
        (b( 20) and (a( 86))) xor
        (b( 21) and (a( 85))) xor
        (b( 22) and (a( 84))) xor
        (b( 23) and (a( 83))) xor
        (b( 24) and (a( 82))) xor
        (b( 25) and (a( 81))) xor
        (b( 26) and (a( 80))) xor
        (b( 27) and (a( 79))) xor
        (b( 28) and (a( 78))) xor
        (b( 29) and (a( 77))) xor
        (b( 30) and (a( 76))) xor
        (b( 31) and (a( 75))) xor
        (b( 32) and (a( 74))) xor
        (b( 33) and (a( 73))) xor
        (b( 34) and (a( 72))) xor
        (b( 35) and (a( 71))) xor
        (b( 36) and (a( 70))) xor
        (b( 37) and (a( 69))) xor
        (b( 38) and (a( 68))) xor
        (b( 39) and (a( 67))) xor
        (b( 40) and (a( 66))) xor
        (b( 41) and (a( 65))) xor
        (b( 42) and (a( 64))) xor
        (b( 43) and (a( 63))) xor
        (b( 44) and (a( 62))) xor
        (b( 45) and (a( 61))) xor
        (b( 46) and (a( 60))) xor
        (b( 47) and (a( 59))) xor
        (b( 48) and (a( 58))) xor
        (b( 49) and (a( 57))) xor
        (b( 50) and (a( 56))) xor
        (b( 51) and (a( 55))) xor
        (b( 52) and (a( 54))) xor
        (b( 53) and (a( 53))) xor
        (b( 54) and (a( 52))) xor
        (b( 55) and (a( 51))) xor
        (b( 56) and (a( 50))) xor
        (b( 57) and (a( 49))) xor
        (b( 58) and (a( 48))) xor
        (b( 59) and (a( 47))) xor
        (b( 60) and (a( 46))) xor
        (b( 61) and (a( 45))) xor
        (b( 62) and (a( 44))) xor
        (b( 63) and (a( 43))) xor
        (b( 64) and (a( 42))) xor
        (b( 65) and (a( 41))) xor
        (b( 66) and (a( 40))) xor
        (b( 67) and (a( 39))) xor
        (b( 68) and (a( 38))) xor
        (b( 69) and (a( 37))) xor
        (b( 70) and (a( 36))) xor
        (b( 71) and (a( 35))) xor
        (b( 72) and (a( 34))) xor
        (b( 73) and (a( 33))) xor
        (b( 74) and (a( 32))) xor
        (b( 75) and (a( 31))) xor
        (b( 76) and (a( 30))) xor
        (b( 77) and (a( 29))) xor
        (b( 78) and (a( 28))) xor
        (b( 79) and (a( 27))) xor
        (b( 80) and (a( 26))) xor
        (b( 81) and (a( 25))) xor
        (b( 82) and (a( 24))) xor
        (b( 83) and (a( 23))) xor
        (b( 84) and (a( 22))) xor
        (b( 85) and (a( 21))) xor
        (b( 86) and (a( 20))) xor
        (b( 87) and (a( 19))) xor
        (b( 88) and (a( 18))) xor
        (b( 89) and (a( 17))) xor
        (b( 90) and (a( 16))) xor
        (b( 91) and (a( 15))) xor
        (b( 92) and (a( 14))) xor
        (b( 93) and (a( 13))) xor
        (b( 94) and (a( 12))) xor
        (b( 95) and (a( 11))) xor
        (b( 96) and (a( 10))) xor
        (b( 97) and (a(  9))) xor
        (b( 98) and (a(  8))) xor
        (b( 99) and (a(  7))) xor
        (b(100) and (a(  6) xor a(127))) xor
        (b(101) and (a(  5) xor a(126))) xor
        (b(102) and (a(  4) xor a(125))) xor
        (b(103) and (a(  3) xor a(124))) xor
        (b(104) and (a(  2) xor a(123))) xor
        (b(105) and (a(  1) xor a(122) xor a(127))) xor
        (b(106) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(107) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(108) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(109) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(110) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(111) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(112) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(113) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(114) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(115) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(116) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(117) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(118) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(119) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(120) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(121) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(122) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(123) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(124) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(125) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(126) and (a(101) xor a(106) xor a(107) xor a(108))) xor
        (b(127) and (a(100) xor a(105) xor a(106) xor a(107)));
    c(107) <= 
        (b(  0) and (a(107))) xor
        (b(  1) and (a(106))) xor
        (b(  2) and (a(105))) xor
        (b(  3) and (a(104))) xor
        (b(  4) and (a(103))) xor
        (b(  5) and (a(102))) xor
        (b(  6) and (a(101))) xor
        (b(  7) and (a(100))) xor
        (b(  8) and (a( 99))) xor
        (b(  9) and (a( 98))) xor
        (b( 10) and (a( 97))) xor
        (b( 11) and (a( 96))) xor
        (b( 12) and (a( 95))) xor
        (b( 13) and (a( 94))) xor
        (b( 14) and (a( 93))) xor
        (b( 15) and (a( 92))) xor
        (b( 16) and (a( 91))) xor
        (b( 17) and (a( 90))) xor
        (b( 18) and (a( 89))) xor
        (b( 19) and (a( 88))) xor
        (b( 20) and (a( 87))) xor
        (b( 21) and (a( 86))) xor
        (b( 22) and (a( 85))) xor
        (b( 23) and (a( 84))) xor
        (b( 24) and (a( 83))) xor
        (b( 25) and (a( 82))) xor
        (b( 26) and (a( 81))) xor
        (b( 27) and (a( 80))) xor
        (b( 28) and (a( 79))) xor
        (b( 29) and (a( 78))) xor
        (b( 30) and (a( 77))) xor
        (b( 31) and (a( 76))) xor
        (b( 32) and (a( 75))) xor
        (b( 33) and (a( 74))) xor
        (b( 34) and (a( 73))) xor
        (b( 35) and (a( 72))) xor
        (b( 36) and (a( 71))) xor
        (b( 37) and (a( 70))) xor
        (b( 38) and (a( 69))) xor
        (b( 39) and (a( 68))) xor
        (b( 40) and (a( 67))) xor
        (b( 41) and (a( 66))) xor
        (b( 42) and (a( 65))) xor
        (b( 43) and (a( 64))) xor
        (b( 44) and (a( 63))) xor
        (b( 45) and (a( 62))) xor
        (b( 46) and (a( 61))) xor
        (b( 47) and (a( 60))) xor
        (b( 48) and (a( 59))) xor
        (b( 49) and (a( 58))) xor
        (b( 50) and (a( 57))) xor
        (b( 51) and (a( 56))) xor
        (b( 52) and (a( 55))) xor
        (b( 53) and (a( 54))) xor
        (b( 54) and (a( 53))) xor
        (b( 55) and (a( 52))) xor
        (b( 56) and (a( 51))) xor
        (b( 57) and (a( 50))) xor
        (b( 58) and (a( 49))) xor
        (b( 59) and (a( 48))) xor
        (b( 60) and (a( 47))) xor
        (b( 61) and (a( 46))) xor
        (b( 62) and (a( 45))) xor
        (b( 63) and (a( 44))) xor
        (b( 64) and (a( 43))) xor
        (b( 65) and (a( 42))) xor
        (b( 66) and (a( 41))) xor
        (b( 67) and (a( 40))) xor
        (b( 68) and (a( 39))) xor
        (b( 69) and (a( 38))) xor
        (b( 70) and (a( 37))) xor
        (b( 71) and (a( 36))) xor
        (b( 72) and (a( 35))) xor
        (b( 73) and (a( 34))) xor
        (b( 74) and (a( 33))) xor
        (b( 75) and (a( 32))) xor
        (b( 76) and (a( 31))) xor
        (b( 77) and (a( 30))) xor
        (b( 78) and (a( 29))) xor
        (b( 79) and (a( 28))) xor
        (b( 80) and (a( 27))) xor
        (b( 81) and (a( 26))) xor
        (b( 82) and (a( 25))) xor
        (b( 83) and (a( 24))) xor
        (b( 84) and (a( 23))) xor
        (b( 85) and (a( 22))) xor
        (b( 86) and (a( 21))) xor
        (b( 87) and (a( 20))) xor
        (b( 88) and (a( 19))) xor
        (b( 89) and (a( 18))) xor
        (b( 90) and (a( 17))) xor
        (b( 91) and (a( 16))) xor
        (b( 92) and (a( 15))) xor
        (b( 93) and (a( 14))) xor
        (b( 94) and (a( 13))) xor
        (b( 95) and (a( 12))) xor
        (b( 96) and (a( 11))) xor
        (b( 97) and (a( 10))) xor
        (b( 98) and (a(  9))) xor
        (b( 99) and (a(  8))) xor
        (b(100) and (a(  7))) xor
        (b(101) and (a(  6) xor a(127))) xor
        (b(102) and (a(  5) xor a(126))) xor
        (b(103) and (a(  4) xor a(125))) xor
        (b(104) and (a(  3) xor a(124))) xor
        (b(105) and (a(  2) xor a(123))) xor
        (b(106) and (a(  1) xor a(122) xor a(127))) xor
        (b(107) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(108) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(109) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(110) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(111) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(112) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(113) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(114) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(115) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(116) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(117) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(118) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(119) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(120) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(121) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(122) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(123) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(124) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(125) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(126) and (a(102) xor a(107) xor a(108) xor a(109))) xor
        (b(127) and (a(101) xor a(106) xor a(107) xor a(108)));
    c(108) <= 
        (b(  0) and (a(108))) xor
        (b(  1) and (a(107))) xor
        (b(  2) and (a(106))) xor
        (b(  3) and (a(105))) xor
        (b(  4) and (a(104))) xor
        (b(  5) and (a(103))) xor
        (b(  6) and (a(102))) xor
        (b(  7) and (a(101))) xor
        (b(  8) and (a(100))) xor
        (b(  9) and (a( 99))) xor
        (b( 10) and (a( 98))) xor
        (b( 11) and (a( 97))) xor
        (b( 12) and (a( 96))) xor
        (b( 13) and (a( 95))) xor
        (b( 14) and (a( 94))) xor
        (b( 15) and (a( 93))) xor
        (b( 16) and (a( 92))) xor
        (b( 17) and (a( 91))) xor
        (b( 18) and (a( 90))) xor
        (b( 19) and (a( 89))) xor
        (b( 20) and (a( 88))) xor
        (b( 21) and (a( 87))) xor
        (b( 22) and (a( 86))) xor
        (b( 23) and (a( 85))) xor
        (b( 24) and (a( 84))) xor
        (b( 25) and (a( 83))) xor
        (b( 26) and (a( 82))) xor
        (b( 27) and (a( 81))) xor
        (b( 28) and (a( 80))) xor
        (b( 29) and (a( 79))) xor
        (b( 30) and (a( 78))) xor
        (b( 31) and (a( 77))) xor
        (b( 32) and (a( 76))) xor
        (b( 33) and (a( 75))) xor
        (b( 34) and (a( 74))) xor
        (b( 35) and (a( 73))) xor
        (b( 36) and (a( 72))) xor
        (b( 37) and (a( 71))) xor
        (b( 38) and (a( 70))) xor
        (b( 39) and (a( 69))) xor
        (b( 40) and (a( 68))) xor
        (b( 41) and (a( 67))) xor
        (b( 42) and (a( 66))) xor
        (b( 43) and (a( 65))) xor
        (b( 44) and (a( 64))) xor
        (b( 45) and (a( 63))) xor
        (b( 46) and (a( 62))) xor
        (b( 47) and (a( 61))) xor
        (b( 48) and (a( 60))) xor
        (b( 49) and (a( 59))) xor
        (b( 50) and (a( 58))) xor
        (b( 51) and (a( 57))) xor
        (b( 52) and (a( 56))) xor
        (b( 53) and (a( 55))) xor
        (b( 54) and (a( 54))) xor
        (b( 55) and (a( 53))) xor
        (b( 56) and (a( 52))) xor
        (b( 57) and (a( 51))) xor
        (b( 58) and (a( 50))) xor
        (b( 59) and (a( 49))) xor
        (b( 60) and (a( 48))) xor
        (b( 61) and (a( 47))) xor
        (b( 62) and (a( 46))) xor
        (b( 63) and (a( 45))) xor
        (b( 64) and (a( 44))) xor
        (b( 65) and (a( 43))) xor
        (b( 66) and (a( 42))) xor
        (b( 67) and (a( 41))) xor
        (b( 68) and (a( 40))) xor
        (b( 69) and (a( 39))) xor
        (b( 70) and (a( 38))) xor
        (b( 71) and (a( 37))) xor
        (b( 72) and (a( 36))) xor
        (b( 73) and (a( 35))) xor
        (b( 74) and (a( 34))) xor
        (b( 75) and (a( 33))) xor
        (b( 76) and (a( 32))) xor
        (b( 77) and (a( 31))) xor
        (b( 78) and (a( 30))) xor
        (b( 79) and (a( 29))) xor
        (b( 80) and (a( 28))) xor
        (b( 81) and (a( 27))) xor
        (b( 82) and (a( 26))) xor
        (b( 83) and (a( 25))) xor
        (b( 84) and (a( 24))) xor
        (b( 85) and (a( 23))) xor
        (b( 86) and (a( 22))) xor
        (b( 87) and (a( 21))) xor
        (b( 88) and (a( 20))) xor
        (b( 89) and (a( 19))) xor
        (b( 90) and (a( 18))) xor
        (b( 91) and (a( 17))) xor
        (b( 92) and (a( 16))) xor
        (b( 93) and (a( 15))) xor
        (b( 94) and (a( 14))) xor
        (b( 95) and (a( 13))) xor
        (b( 96) and (a( 12))) xor
        (b( 97) and (a( 11))) xor
        (b( 98) and (a( 10))) xor
        (b( 99) and (a(  9))) xor
        (b(100) and (a(  8))) xor
        (b(101) and (a(  7))) xor
        (b(102) and (a(  6) xor a(127))) xor
        (b(103) and (a(  5) xor a(126))) xor
        (b(104) and (a(  4) xor a(125))) xor
        (b(105) and (a(  3) xor a(124))) xor
        (b(106) and (a(  2) xor a(123))) xor
        (b(107) and (a(  1) xor a(122) xor a(127))) xor
        (b(108) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(109) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(110) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(111) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(112) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(113) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(114) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(115) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(116) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(117) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(118) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(119) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(120) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(121) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(122) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(123) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(124) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(125) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(126) and (a(103) xor a(108) xor a(109) xor a(110))) xor
        (b(127) and (a(102) xor a(107) xor a(108) xor a(109)));
    c(109) <= 
        (b(  0) and (a(109))) xor
        (b(  1) and (a(108))) xor
        (b(  2) and (a(107))) xor
        (b(  3) and (a(106))) xor
        (b(  4) and (a(105))) xor
        (b(  5) and (a(104))) xor
        (b(  6) and (a(103))) xor
        (b(  7) and (a(102))) xor
        (b(  8) and (a(101))) xor
        (b(  9) and (a(100))) xor
        (b( 10) and (a( 99))) xor
        (b( 11) and (a( 98))) xor
        (b( 12) and (a( 97))) xor
        (b( 13) and (a( 96))) xor
        (b( 14) and (a( 95))) xor
        (b( 15) and (a( 94))) xor
        (b( 16) and (a( 93))) xor
        (b( 17) and (a( 92))) xor
        (b( 18) and (a( 91))) xor
        (b( 19) and (a( 90))) xor
        (b( 20) and (a( 89))) xor
        (b( 21) and (a( 88))) xor
        (b( 22) and (a( 87))) xor
        (b( 23) and (a( 86))) xor
        (b( 24) and (a( 85))) xor
        (b( 25) and (a( 84))) xor
        (b( 26) and (a( 83))) xor
        (b( 27) and (a( 82))) xor
        (b( 28) and (a( 81))) xor
        (b( 29) and (a( 80))) xor
        (b( 30) and (a( 79))) xor
        (b( 31) and (a( 78))) xor
        (b( 32) and (a( 77))) xor
        (b( 33) and (a( 76))) xor
        (b( 34) and (a( 75))) xor
        (b( 35) and (a( 74))) xor
        (b( 36) and (a( 73))) xor
        (b( 37) and (a( 72))) xor
        (b( 38) and (a( 71))) xor
        (b( 39) and (a( 70))) xor
        (b( 40) and (a( 69))) xor
        (b( 41) and (a( 68))) xor
        (b( 42) and (a( 67))) xor
        (b( 43) and (a( 66))) xor
        (b( 44) and (a( 65))) xor
        (b( 45) and (a( 64))) xor
        (b( 46) and (a( 63))) xor
        (b( 47) and (a( 62))) xor
        (b( 48) and (a( 61))) xor
        (b( 49) and (a( 60))) xor
        (b( 50) and (a( 59))) xor
        (b( 51) and (a( 58))) xor
        (b( 52) and (a( 57))) xor
        (b( 53) and (a( 56))) xor
        (b( 54) and (a( 55))) xor
        (b( 55) and (a( 54))) xor
        (b( 56) and (a( 53))) xor
        (b( 57) and (a( 52))) xor
        (b( 58) and (a( 51))) xor
        (b( 59) and (a( 50))) xor
        (b( 60) and (a( 49))) xor
        (b( 61) and (a( 48))) xor
        (b( 62) and (a( 47))) xor
        (b( 63) and (a( 46))) xor
        (b( 64) and (a( 45))) xor
        (b( 65) and (a( 44))) xor
        (b( 66) and (a( 43))) xor
        (b( 67) and (a( 42))) xor
        (b( 68) and (a( 41))) xor
        (b( 69) and (a( 40))) xor
        (b( 70) and (a( 39))) xor
        (b( 71) and (a( 38))) xor
        (b( 72) and (a( 37))) xor
        (b( 73) and (a( 36))) xor
        (b( 74) and (a( 35))) xor
        (b( 75) and (a( 34))) xor
        (b( 76) and (a( 33))) xor
        (b( 77) and (a( 32))) xor
        (b( 78) and (a( 31))) xor
        (b( 79) and (a( 30))) xor
        (b( 80) and (a( 29))) xor
        (b( 81) and (a( 28))) xor
        (b( 82) and (a( 27))) xor
        (b( 83) and (a( 26))) xor
        (b( 84) and (a( 25))) xor
        (b( 85) and (a( 24))) xor
        (b( 86) and (a( 23))) xor
        (b( 87) and (a( 22))) xor
        (b( 88) and (a( 21))) xor
        (b( 89) and (a( 20))) xor
        (b( 90) and (a( 19))) xor
        (b( 91) and (a( 18))) xor
        (b( 92) and (a( 17))) xor
        (b( 93) and (a( 16))) xor
        (b( 94) and (a( 15))) xor
        (b( 95) and (a( 14))) xor
        (b( 96) and (a( 13))) xor
        (b( 97) and (a( 12))) xor
        (b( 98) and (a( 11))) xor
        (b( 99) and (a( 10))) xor
        (b(100) and (a(  9))) xor
        (b(101) and (a(  8))) xor
        (b(102) and (a(  7))) xor
        (b(103) and (a(  6) xor a(127))) xor
        (b(104) and (a(  5) xor a(126))) xor
        (b(105) and (a(  4) xor a(125))) xor
        (b(106) and (a(  3) xor a(124))) xor
        (b(107) and (a(  2) xor a(123))) xor
        (b(108) and (a(  1) xor a(122) xor a(127))) xor
        (b(109) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(110) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(111) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(112) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(113) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(114) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(115) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(116) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(117) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(118) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(119) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(120) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(121) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(122) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(123) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(124) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(125) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(126) and (a(104) xor a(109) xor a(110) xor a(111))) xor
        (b(127) and (a(103) xor a(108) xor a(109) xor a(110)));
    c(110) <= 
        (b(  0) and (a(110))) xor
        (b(  1) and (a(109))) xor
        (b(  2) and (a(108))) xor
        (b(  3) and (a(107))) xor
        (b(  4) and (a(106))) xor
        (b(  5) and (a(105))) xor
        (b(  6) and (a(104))) xor
        (b(  7) and (a(103))) xor
        (b(  8) and (a(102))) xor
        (b(  9) and (a(101))) xor
        (b( 10) and (a(100))) xor
        (b( 11) and (a( 99))) xor
        (b( 12) and (a( 98))) xor
        (b( 13) and (a( 97))) xor
        (b( 14) and (a( 96))) xor
        (b( 15) and (a( 95))) xor
        (b( 16) and (a( 94))) xor
        (b( 17) and (a( 93))) xor
        (b( 18) and (a( 92))) xor
        (b( 19) and (a( 91))) xor
        (b( 20) and (a( 90))) xor
        (b( 21) and (a( 89))) xor
        (b( 22) and (a( 88))) xor
        (b( 23) and (a( 87))) xor
        (b( 24) and (a( 86))) xor
        (b( 25) and (a( 85))) xor
        (b( 26) and (a( 84))) xor
        (b( 27) and (a( 83))) xor
        (b( 28) and (a( 82))) xor
        (b( 29) and (a( 81))) xor
        (b( 30) and (a( 80))) xor
        (b( 31) and (a( 79))) xor
        (b( 32) and (a( 78))) xor
        (b( 33) and (a( 77))) xor
        (b( 34) and (a( 76))) xor
        (b( 35) and (a( 75))) xor
        (b( 36) and (a( 74))) xor
        (b( 37) and (a( 73))) xor
        (b( 38) and (a( 72))) xor
        (b( 39) and (a( 71))) xor
        (b( 40) and (a( 70))) xor
        (b( 41) and (a( 69))) xor
        (b( 42) and (a( 68))) xor
        (b( 43) and (a( 67))) xor
        (b( 44) and (a( 66))) xor
        (b( 45) and (a( 65))) xor
        (b( 46) and (a( 64))) xor
        (b( 47) and (a( 63))) xor
        (b( 48) and (a( 62))) xor
        (b( 49) and (a( 61))) xor
        (b( 50) and (a( 60))) xor
        (b( 51) and (a( 59))) xor
        (b( 52) and (a( 58))) xor
        (b( 53) and (a( 57))) xor
        (b( 54) and (a( 56))) xor
        (b( 55) and (a( 55))) xor
        (b( 56) and (a( 54))) xor
        (b( 57) and (a( 53))) xor
        (b( 58) and (a( 52))) xor
        (b( 59) and (a( 51))) xor
        (b( 60) and (a( 50))) xor
        (b( 61) and (a( 49))) xor
        (b( 62) and (a( 48))) xor
        (b( 63) and (a( 47))) xor
        (b( 64) and (a( 46))) xor
        (b( 65) and (a( 45))) xor
        (b( 66) and (a( 44))) xor
        (b( 67) and (a( 43))) xor
        (b( 68) and (a( 42))) xor
        (b( 69) and (a( 41))) xor
        (b( 70) and (a( 40))) xor
        (b( 71) and (a( 39))) xor
        (b( 72) and (a( 38))) xor
        (b( 73) and (a( 37))) xor
        (b( 74) and (a( 36))) xor
        (b( 75) and (a( 35))) xor
        (b( 76) and (a( 34))) xor
        (b( 77) and (a( 33))) xor
        (b( 78) and (a( 32))) xor
        (b( 79) and (a( 31))) xor
        (b( 80) and (a( 30))) xor
        (b( 81) and (a( 29))) xor
        (b( 82) and (a( 28))) xor
        (b( 83) and (a( 27))) xor
        (b( 84) and (a( 26))) xor
        (b( 85) and (a( 25))) xor
        (b( 86) and (a( 24))) xor
        (b( 87) and (a( 23))) xor
        (b( 88) and (a( 22))) xor
        (b( 89) and (a( 21))) xor
        (b( 90) and (a( 20))) xor
        (b( 91) and (a( 19))) xor
        (b( 92) and (a( 18))) xor
        (b( 93) and (a( 17))) xor
        (b( 94) and (a( 16))) xor
        (b( 95) and (a( 15))) xor
        (b( 96) and (a( 14))) xor
        (b( 97) and (a( 13))) xor
        (b( 98) and (a( 12))) xor
        (b( 99) and (a( 11))) xor
        (b(100) and (a( 10))) xor
        (b(101) and (a(  9))) xor
        (b(102) and (a(  8))) xor
        (b(103) and (a(  7))) xor
        (b(104) and (a(  6) xor a(127))) xor
        (b(105) and (a(  5) xor a(126))) xor
        (b(106) and (a(  4) xor a(125))) xor
        (b(107) and (a(  3) xor a(124))) xor
        (b(108) and (a(  2) xor a(123))) xor
        (b(109) and (a(  1) xor a(122) xor a(127))) xor
        (b(110) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(111) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(112) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(113) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(114) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(115) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(116) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(117) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(118) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(119) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(120) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(121) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(122) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(123) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(124) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(125) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(126) and (a(105) xor a(110) xor a(111) xor a(112))) xor
        (b(127) and (a(104) xor a(109) xor a(110) xor a(111)));
    c(111) <= 
        (b(  0) and (a(111))) xor
        (b(  1) and (a(110))) xor
        (b(  2) and (a(109))) xor
        (b(  3) and (a(108))) xor
        (b(  4) and (a(107))) xor
        (b(  5) and (a(106))) xor
        (b(  6) and (a(105))) xor
        (b(  7) and (a(104))) xor
        (b(  8) and (a(103))) xor
        (b(  9) and (a(102))) xor
        (b( 10) and (a(101))) xor
        (b( 11) and (a(100))) xor
        (b( 12) and (a( 99))) xor
        (b( 13) and (a( 98))) xor
        (b( 14) and (a( 97))) xor
        (b( 15) and (a( 96))) xor
        (b( 16) and (a( 95))) xor
        (b( 17) and (a( 94))) xor
        (b( 18) and (a( 93))) xor
        (b( 19) and (a( 92))) xor
        (b( 20) and (a( 91))) xor
        (b( 21) and (a( 90))) xor
        (b( 22) and (a( 89))) xor
        (b( 23) and (a( 88))) xor
        (b( 24) and (a( 87))) xor
        (b( 25) and (a( 86))) xor
        (b( 26) and (a( 85))) xor
        (b( 27) and (a( 84))) xor
        (b( 28) and (a( 83))) xor
        (b( 29) and (a( 82))) xor
        (b( 30) and (a( 81))) xor
        (b( 31) and (a( 80))) xor
        (b( 32) and (a( 79))) xor
        (b( 33) and (a( 78))) xor
        (b( 34) and (a( 77))) xor
        (b( 35) and (a( 76))) xor
        (b( 36) and (a( 75))) xor
        (b( 37) and (a( 74))) xor
        (b( 38) and (a( 73))) xor
        (b( 39) and (a( 72))) xor
        (b( 40) and (a( 71))) xor
        (b( 41) and (a( 70))) xor
        (b( 42) and (a( 69))) xor
        (b( 43) and (a( 68))) xor
        (b( 44) and (a( 67))) xor
        (b( 45) and (a( 66))) xor
        (b( 46) and (a( 65))) xor
        (b( 47) and (a( 64))) xor
        (b( 48) and (a( 63))) xor
        (b( 49) and (a( 62))) xor
        (b( 50) and (a( 61))) xor
        (b( 51) and (a( 60))) xor
        (b( 52) and (a( 59))) xor
        (b( 53) and (a( 58))) xor
        (b( 54) and (a( 57))) xor
        (b( 55) and (a( 56))) xor
        (b( 56) and (a( 55))) xor
        (b( 57) and (a( 54))) xor
        (b( 58) and (a( 53))) xor
        (b( 59) and (a( 52))) xor
        (b( 60) and (a( 51))) xor
        (b( 61) and (a( 50))) xor
        (b( 62) and (a( 49))) xor
        (b( 63) and (a( 48))) xor
        (b( 64) and (a( 47))) xor
        (b( 65) and (a( 46))) xor
        (b( 66) and (a( 45))) xor
        (b( 67) and (a( 44))) xor
        (b( 68) and (a( 43))) xor
        (b( 69) and (a( 42))) xor
        (b( 70) and (a( 41))) xor
        (b( 71) and (a( 40))) xor
        (b( 72) and (a( 39))) xor
        (b( 73) and (a( 38))) xor
        (b( 74) and (a( 37))) xor
        (b( 75) and (a( 36))) xor
        (b( 76) and (a( 35))) xor
        (b( 77) and (a( 34))) xor
        (b( 78) and (a( 33))) xor
        (b( 79) and (a( 32))) xor
        (b( 80) and (a( 31))) xor
        (b( 81) and (a( 30))) xor
        (b( 82) and (a( 29))) xor
        (b( 83) and (a( 28))) xor
        (b( 84) and (a( 27))) xor
        (b( 85) and (a( 26))) xor
        (b( 86) and (a( 25))) xor
        (b( 87) and (a( 24))) xor
        (b( 88) and (a( 23))) xor
        (b( 89) and (a( 22))) xor
        (b( 90) and (a( 21))) xor
        (b( 91) and (a( 20))) xor
        (b( 92) and (a( 19))) xor
        (b( 93) and (a( 18))) xor
        (b( 94) and (a( 17))) xor
        (b( 95) and (a( 16))) xor
        (b( 96) and (a( 15))) xor
        (b( 97) and (a( 14))) xor
        (b( 98) and (a( 13))) xor
        (b( 99) and (a( 12))) xor
        (b(100) and (a( 11))) xor
        (b(101) and (a( 10))) xor
        (b(102) and (a(  9))) xor
        (b(103) and (a(  8))) xor
        (b(104) and (a(  7))) xor
        (b(105) and (a(  6) xor a(127))) xor
        (b(106) and (a(  5) xor a(126))) xor
        (b(107) and (a(  4) xor a(125))) xor
        (b(108) and (a(  3) xor a(124))) xor
        (b(109) and (a(  2) xor a(123))) xor
        (b(110) and (a(  1) xor a(122) xor a(127))) xor
        (b(111) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(112) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(113) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(114) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(115) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(116) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(117) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(118) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(119) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(120) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(121) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(122) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(123) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(124) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(125) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(126) and (a(106) xor a(111) xor a(112) xor a(113))) xor
        (b(127) and (a(105) xor a(110) xor a(111) xor a(112)));
    c(112) <= 
        (b(  0) and (a(112))) xor
        (b(  1) and (a(111))) xor
        (b(  2) and (a(110))) xor
        (b(  3) and (a(109))) xor
        (b(  4) and (a(108))) xor
        (b(  5) and (a(107))) xor
        (b(  6) and (a(106))) xor
        (b(  7) and (a(105))) xor
        (b(  8) and (a(104))) xor
        (b(  9) and (a(103))) xor
        (b( 10) and (a(102))) xor
        (b( 11) and (a(101))) xor
        (b( 12) and (a(100))) xor
        (b( 13) and (a( 99))) xor
        (b( 14) and (a( 98))) xor
        (b( 15) and (a( 97))) xor
        (b( 16) and (a( 96))) xor
        (b( 17) and (a( 95))) xor
        (b( 18) and (a( 94))) xor
        (b( 19) and (a( 93))) xor
        (b( 20) and (a( 92))) xor
        (b( 21) and (a( 91))) xor
        (b( 22) and (a( 90))) xor
        (b( 23) and (a( 89))) xor
        (b( 24) and (a( 88))) xor
        (b( 25) and (a( 87))) xor
        (b( 26) and (a( 86))) xor
        (b( 27) and (a( 85))) xor
        (b( 28) and (a( 84))) xor
        (b( 29) and (a( 83))) xor
        (b( 30) and (a( 82))) xor
        (b( 31) and (a( 81))) xor
        (b( 32) and (a( 80))) xor
        (b( 33) and (a( 79))) xor
        (b( 34) and (a( 78))) xor
        (b( 35) and (a( 77))) xor
        (b( 36) and (a( 76))) xor
        (b( 37) and (a( 75))) xor
        (b( 38) and (a( 74))) xor
        (b( 39) and (a( 73))) xor
        (b( 40) and (a( 72))) xor
        (b( 41) and (a( 71))) xor
        (b( 42) and (a( 70))) xor
        (b( 43) and (a( 69))) xor
        (b( 44) and (a( 68))) xor
        (b( 45) and (a( 67))) xor
        (b( 46) and (a( 66))) xor
        (b( 47) and (a( 65))) xor
        (b( 48) and (a( 64))) xor
        (b( 49) and (a( 63))) xor
        (b( 50) and (a( 62))) xor
        (b( 51) and (a( 61))) xor
        (b( 52) and (a( 60))) xor
        (b( 53) and (a( 59))) xor
        (b( 54) and (a( 58))) xor
        (b( 55) and (a( 57))) xor
        (b( 56) and (a( 56))) xor
        (b( 57) and (a( 55))) xor
        (b( 58) and (a( 54))) xor
        (b( 59) and (a( 53))) xor
        (b( 60) and (a( 52))) xor
        (b( 61) and (a( 51))) xor
        (b( 62) and (a( 50))) xor
        (b( 63) and (a( 49))) xor
        (b( 64) and (a( 48))) xor
        (b( 65) and (a( 47))) xor
        (b( 66) and (a( 46))) xor
        (b( 67) and (a( 45))) xor
        (b( 68) and (a( 44))) xor
        (b( 69) and (a( 43))) xor
        (b( 70) and (a( 42))) xor
        (b( 71) and (a( 41))) xor
        (b( 72) and (a( 40))) xor
        (b( 73) and (a( 39))) xor
        (b( 74) and (a( 38))) xor
        (b( 75) and (a( 37))) xor
        (b( 76) and (a( 36))) xor
        (b( 77) and (a( 35))) xor
        (b( 78) and (a( 34))) xor
        (b( 79) and (a( 33))) xor
        (b( 80) and (a( 32))) xor
        (b( 81) and (a( 31))) xor
        (b( 82) and (a( 30))) xor
        (b( 83) and (a( 29))) xor
        (b( 84) and (a( 28))) xor
        (b( 85) and (a( 27))) xor
        (b( 86) and (a( 26))) xor
        (b( 87) and (a( 25))) xor
        (b( 88) and (a( 24))) xor
        (b( 89) and (a( 23))) xor
        (b( 90) and (a( 22))) xor
        (b( 91) and (a( 21))) xor
        (b( 92) and (a( 20))) xor
        (b( 93) and (a( 19))) xor
        (b( 94) and (a( 18))) xor
        (b( 95) and (a( 17))) xor
        (b( 96) and (a( 16))) xor
        (b( 97) and (a( 15))) xor
        (b( 98) and (a( 14))) xor
        (b( 99) and (a( 13))) xor
        (b(100) and (a( 12))) xor
        (b(101) and (a( 11))) xor
        (b(102) and (a( 10))) xor
        (b(103) and (a(  9))) xor
        (b(104) and (a(  8))) xor
        (b(105) and (a(  7))) xor
        (b(106) and (a(  6) xor a(127))) xor
        (b(107) and (a(  5) xor a(126))) xor
        (b(108) and (a(  4) xor a(125))) xor
        (b(109) and (a(  3) xor a(124))) xor
        (b(110) and (a(  2) xor a(123))) xor
        (b(111) and (a(  1) xor a(122) xor a(127))) xor
        (b(112) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(113) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(114) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(115) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(116) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(117) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(118) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(119) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(120) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(121) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(122) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(123) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(124) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(125) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(126) and (a(107) xor a(112) xor a(113) xor a(114))) xor
        (b(127) and (a(106) xor a(111) xor a(112) xor a(113)));
    c(113) <= 
        (b(  0) and (a(113))) xor
        (b(  1) and (a(112))) xor
        (b(  2) and (a(111))) xor
        (b(  3) and (a(110))) xor
        (b(  4) and (a(109))) xor
        (b(  5) and (a(108))) xor
        (b(  6) and (a(107))) xor
        (b(  7) and (a(106))) xor
        (b(  8) and (a(105))) xor
        (b(  9) and (a(104))) xor
        (b( 10) and (a(103))) xor
        (b( 11) and (a(102))) xor
        (b( 12) and (a(101))) xor
        (b( 13) and (a(100))) xor
        (b( 14) and (a( 99))) xor
        (b( 15) and (a( 98))) xor
        (b( 16) and (a( 97))) xor
        (b( 17) and (a( 96))) xor
        (b( 18) and (a( 95))) xor
        (b( 19) and (a( 94))) xor
        (b( 20) and (a( 93))) xor
        (b( 21) and (a( 92))) xor
        (b( 22) and (a( 91))) xor
        (b( 23) and (a( 90))) xor
        (b( 24) and (a( 89))) xor
        (b( 25) and (a( 88))) xor
        (b( 26) and (a( 87))) xor
        (b( 27) and (a( 86))) xor
        (b( 28) and (a( 85))) xor
        (b( 29) and (a( 84))) xor
        (b( 30) and (a( 83))) xor
        (b( 31) and (a( 82))) xor
        (b( 32) and (a( 81))) xor
        (b( 33) and (a( 80))) xor
        (b( 34) and (a( 79))) xor
        (b( 35) and (a( 78))) xor
        (b( 36) and (a( 77))) xor
        (b( 37) and (a( 76))) xor
        (b( 38) and (a( 75))) xor
        (b( 39) and (a( 74))) xor
        (b( 40) and (a( 73))) xor
        (b( 41) and (a( 72))) xor
        (b( 42) and (a( 71))) xor
        (b( 43) and (a( 70))) xor
        (b( 44) and (a( 69))) xor
        (b( 45) and (a( 68))) xor
        (b( 46) and (a( 67))) xor
        (b( 47) and (a( 66))) xor
        (b( 48) and (a( 65))) xor
        (b( 49) and (a( 64))) xor
        (b( 50) and (a( 63))) xor
        (b( 51) and (a( 62))) xor
        (b( 52) and (a( 61))) xor
        (b( 53) and (a( 60))) xor
        (b( 54) and (a( 59))) xor
        (b( 55) and (a( 58))) xor
        (b( 56) and (a( 57))) xor
        (b( 57) and (a( 56))) xor
        (b( 58) and (a( 55))) xor
        (b( 59) and (a( 54))) xor
        (b( 60) and (a( 53))) xor
        (b( 61) and (a( 52))) xor
        (b( 62) and (a( 51))) xor
        (b( 63) and (a( 50))) xor
        (b( 64) and (a( 49))) xor
        (b( 65) and (a( 48))) xor
        (b( 66) and (a( 47))) xor
        (b( 67) and (a( 46))) xor
        (b( 68) and (a( 45))) xor
        (b( 69) and (a( 44))) xor
        (b( 70) and (a( 43))) xor
        (b( 71) and (a( 42))) xor
        (b( 72) and (a( 41))) xor
        (b( 73) and (a( 40))) xor
        (b( 74) and (a( 39))) xor
        (b( 75) and (a( 38))) xor
        (b( 76) and (a( 37))) xor
        (b( 77) and (a( 36))) xor
        (b( 78) and (a( 35))) xor
        (b( 79) and (a( 34))) xor
        (b( 80) and (a( 33))) xor
        (b( 81) and (a( 32))) xor
        (b( 82) and (a( 31))) xor
        (b( 83) and (a( 30))) xor
        (b( 84) and (a( 29))) xor
        (b( 85) and (a( 28))) xor
        (b( 86) and (a( 27))) xor
        (b( 87) and (a( 26))) xor
        (b( 88) and (a( 25))) xor
        (b( 89) and (a( 24))) xor
        (b( 90) and (a( 23))) xor
        (b( 91) and (a( 22))) xor
        (b( 92) and (a( 21))) xor
        (b( 93) and (a( 20))) xor
        (b( 94) and (a( 19))) xor
        (b( 95) and (a( 18))) xor
        (b( 96) and (a( 17))) xor
        (b( 97) and (a( 16))) xor
        (b( 98) and (a( 15))) xor
        (b( 99) and (a( 14))) xor
        (b(100) and (a( 13))) xor
        (b(101) and (a( 12))) xor
        (b(102) and (a( 11))) xor
        (b(103) and (a( 10))) xor
        (b(104) and (a(  9))) xor
        (b(105) and (a(  8))) xor
        (b(106) and (a(  7))) xor
        (b(107) and (a(  6) xor a(127))) xor
        (b(108) and (a(  5) xor a(126))) xor
        (b(109) and (a(  4) xor a(125))) xor
        (b(110) and (a(  3) xor a(124))) xor
        (b(111) and (a(  2) xor a(123))) xor
        (b(112) and (a(  1) xor a(122) xor a(127))) xor
        (b(113) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(114) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(115) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(116) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(117) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(118) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(119) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(120) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(121) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(122) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(123) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(124) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(125) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(126) and (a(108) xor a(113) xor a(114) xor a(115))) xor
        (b(127) and (a(107) xor a(112) xor a(113) xor a(114)));
    c(114) <= 
        (b(  0) and (a(114))) xor
        (b(  1) and (a(113))) xor
        (b(  2) and (a(112))) xor
        (b(  3) and (a(111))) xor
        (b(  4) and (a(110))) xor
        (b(  5) and (a(109))) xor
        (b(  6) and (a(108))) xor
        (b(  7) and (a(107))) xor
        (b(  8) and (a(106))) xor
        (b(  9) and (a(105))) xor
        (b( 10) and (a(104))) xor
        (b( 11) and (a(103))) xor
        (b( 12) and (a(102))) xor
        (b( 13) and (a(101))) xor
        (b( 14) and (a(100))) xor
        (b( 15) and (a( 99))) xor
        (b( 16) and (a( 98))) xor
        (b( 17) and (a( 97))) xor
        (b( 18) and (a( 96))) xor
        (b( 19) and (a( 95))) xor
        (b( 20) and (a( 94))) xor
        (b( 21) and (a( 93))) xor
        (b( 22) and (a( 92))) xor
        (b( 23) and (a( 91))) xor
        (b( 24) and (a( 90))) xor
        (b( 25) and (a( 89))) xor
        (b( 26) and (a( 88))) xor
        (b( 27) and (a( 87))) xor
        (b( 28) and (a( 86))) xor
        (b( 29) and (a( 85))) xor
        (b( 30) and (a( 84))) xor
        (b( 31) and (a( 83))) xor
        (b( 32) and (a( 82))) xor
        (b( 33) and (a( 81))) xor
        (b( 34) and (a( 80))) xor
        (b( 35) and (a( 79))) xor
        (b( 36) and (a( 78))) xor
        (b( 37) and (a( 77))) xor
        (b( 38) and (a( 76))) xor
        (b( 39) and (a( 75))) xor
        (b( 40) and (a( 74))) xor
        (b( 41) and (a( 73))) xor
        (b( 42) and (a( 72))) xor
        (b( 43) and (a( 71))) xor
        (b( 44) and (a( 70))) xor
        (b( 45) and (a( 69))) xor
        (b( 46) and (a( 68))) xor
        (b( 47) and (a( 67))) xor
        (b( 48) and (a( 66))) xor
        (b( 49) and (a( 65))) xor
        (b( 50) and (a( 64))) xor
        (b( 51) and (a( 63))) xor
        (b( 52) and (a( 62))) xor
        (b( 53) and (a( 61))) xor
        (b( 54) and (a( 60))) xor
        (b( 55) and (a( 59))) xor
        (b( 56) and (a( 58))) xor
        (b( 57) and (a( 57))) xor
        (b( 58) and (a( 56))) xor
        (b( 59) and (a( 55))) xor
        (b( 60) and (a( 54))) xor
        (b( 61) and (a( 53))) xor
        (b( 62) and (a( 52))) xor
        (b( 63) and (a( 51))) xor
        (b( 64) and (a( 50))) xor
        (b( 65) and (a( 49))) xor
        (b( 66) and (a( 48))) xor
        (b( 67) and (a( 47))) xor
        (b( 68) and (a( 46))) xor
        (b( 69) and (a( 45))) xor
        (b( 70) and (a( 44))) xor
        (b( 71) and (a( 43))) xor
        (b( 72) and (a( 42))) xor
        (b( 73) and (a( 41))) xor
        (b( 74) and (a( 40))) xor
        (b( 75) and (a( 39))) xor
        (b( 76) and (a( 38))) xor
        (b( 77) and (a( 37))) xor
        (b( 78) and (a( 36))) xor
        (b( 79) and (a( 35))) xor
        (b( 80) and (a( 34))) xor
        (b( 81) and (a( 33))) xor
        (b( 82) and (a( 32))) xor
        (b( 83) and (a( 31))) xor
        (b( 84) and (a( 30))) xor
        (b( 85) and (a( 29))) xor
        (b( 86) and (a( 28))) xor
        (b( 87) and (a( 27))) xor
        (b( 88) and (a( 26))) xor
        (b( 89) and (a( 25))) xor
        (b( 90) and (a( 24))) xor
        (b( 91) and (a( 23))) xor
        (b( 92) and (a( 22))) xor
        (b( 93) and (a( 21))) xor
        (b( 94) and (a( 20))) xor
        (b( 95) and (a( 19))) xor
        (b( 96) and (a( 18))) xor
        (b( 97) and (a( 17))) xor
        (b( 98) and (a( 16))) xor
        (b( 99) and (a( 15))) xor
        (b(100) and (a( 14))) xor
        (b(101) and (a( 13))) xor
        (b(102) and (a( 12))) xor
        (b(103) and (a( 11))) xor
        (b(104) and (a( 10))) xor
        (b(105) and (a(  9))) xor
        (b(106) and (a(  8))) xor
        (b(107) and (a(  7))) xor
        (b(108) and (a(  6) xor a(127))) xor
        (b(109) and (a(  5) xor a(126))) xor
        (b(110) and (a(  4) xor a(125))) xor
        (b(111) and (a(  3) xor a(124))) xor
        (b(112) and (a(  2) xor a(123))) xor
        (b(113) and (a(  1) xor a(122) xor a(127))) xor
        (b(114) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(115) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(116) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(117) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(118) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(119) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(120) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(121) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(122) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(123) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(124) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(125) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(126) and (a(109) xor a(114) xor a(115) xor a(116))) xor
        (b(127) and (a(108) xor a(113) xor a(114) xor a(115)));
    c(115) <= 
        (b(  0) and (a(115))) xor
        (b(  1) and (a(114))) xor
        (b(  2) and (a(113))) xor
        (b(  3) and (a(112))) xor
        (b(  4) and (a(111))) xor
        (b(  5) and (a(110))) xor
        (b(  6) and (a(109))) xor
        (b(  7) and (a(108))) xor
        (b(  8) and (a(107))) xor
        (b(  9) and (a(106))) xor
        (b( 10) and (a(105))) xor
        (b( 11) and (a(104))) xor
        (b( 12) and (a(103))) xor
        (b( 13) and (a(102))) xor
        (b( 14) and (a(101))) xor
        (b( 15) and (a(100))) xor
        (b( 16) and (a( 99))) xor
        (b( 17) and (a( 98))) xor
        (b( 18) and (a( 97))) xor
        (b( 19) and (a( 96))) xor
        (b( 20) and (a( 95))) xor
        (b( 21) and (a( 94))) xor
        (b( 22) and (a( 93))) xor
        (b( 23) and (a( 92))) xor
        (b( 24) and (a( 91))) xor
        (b( 25) and (a( 90))) xor
        (b( 26) and (a( 89))) xor
        (b( 27) and (a( 88))) xor
        (b( 28) and (a( 87))) xor
        (b( 29) and (a( 86))) xor
        (b( 30) and (a( 85))) xor
        (b( 31) and (a( 84))) xor
        (b( 32) and (a( 83))) xor
        (b( 33) and (a( 82))) xor
        (b( 34) and (a( 81))) xor
        (b( 35) and (a( 80))) xor
        (b( 36) and (a( 79))) xor
        (b( 37) and (a( 78))) xor
        (b( 38) and (a( 77))) xor
        (b( 39) and (a( 76))) xor
        (b( 40) and (a( 75))) xor
        (b( 41) and (a( 74))) xor
        (b( 42) and (a( 73))) xor
        (b( 43) and (a( 72))) xor
        (b( 44) and (a( 71))) xor
        (b( 45) and (a( 70))) xor
        (b( 46) and (a( 69))) xor
        (b( 47) and (a( 68))) xor
        (b( 48) and (a( 67))) xor
        (b( 49) and (a( 66))) xor
        (b( 50) and (a( 65))) xor
        (b( 51) and (a( 64))) xor
        (b( 52) and (a( 63))) xor
        (b( 53) and (a( 62))) xor
        (b( 54) and (a( 61))) xor
        (b( 55) and (a( 60))) xor
        (b( 56) and (a( 59))) xor
        (b( 57) and (a( 58))) xor
        (b( 58) and (a( 57))) xor
        (b( 59) and (a( 56))) xor
        (b( 60) and (a( 55))) xor
        (b( 61) and (a( 54))) xor
        (b( 62) and (a( 53))) xor
        (b( 63) and (a( 52))) xor
        (b( 64) and (a( 51))) xor
        (b( 65) and (a( 50))) xor
        (b( 66) and (a( 49))) xor
        (b( 67) and (a( 48))) xor
        (b( 68) and (a( 47))) xor
        (b( 69) and (a( 46))) xor
        (b( 70) and (a( 45))) xor
        (b( 71) and (a( 44))) xor
        (b( 72) and (a( 43))) xor
        (b( 73) and (a( 42))) xor
        (b( 74) and (a( 41))) xor
        (b( 75) and (a( 40))) xor
        (b( 76) and (a( 39))) xor
        (b( 77) and (a( 38))) xor
        (b( 78) and (a( 37))) xor
        (b( 79) and (a( 36))) xor
        (b( 80) and (a( 35))) xor
        (b( 81) and (a( 34))) xor
        (b( 82) and (a( 33))) xor
        (b( 83) and (a( 32))) xor
        (b( 84) and (a( 31))) xor
        (b( 85) and (a( 30))) xor
        (b( 86) and (a( 29))) xor
        (b( 87) and (a( 28))) xor
        (b( 88) and (a( 27))) xor
        (b( 89) and (a( 26))) xor
        (b( 90) and (a( 25))) xor
        (b( 91) and (a( 24))) xor
        (b( 92) and (a( 23))) xor
        (b( 93) and (a( 22))) xor
        (b( 94) and (a( 21))) xor
        (b( 95) and (a( 20))) xor
        (b( 96) and (a( 19))) xor
        (b( 97) and (a( 18))) xor
        (b( 98) and (a( 17))) xor
        (b( 99) and (a( 16))) xor
        (b(100) and (a( 15))) xor
        (b(101) and (a( 14))) xor
        (b(102) and (a( 13))) xor
        (b(103) and (a( 12))) xor
        (b(104) and (a( 11))) xor
        (b(105) and (a( 10))) xor
        (b(106) and (a(  9))) xor
        (b(107) and (a(  8))) xor
        (b(108) and (a(  7))) xor
        (b(109) and (a(  6) xor a(127))) xor
        (b(110) and (a(  5) xor a(126))) xor
        (b(111) and (a(  4) xor a(125))) xor
        (b(112) and (a(  3) xor a(124))) xor
        (b(113) and (a(  2) xor a(123))) xor
        (b(114) and (a(  1) xor a(122) xor a(127))) xor
        (b(115) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(116) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(117) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(118) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(119) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(120) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(121) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(122) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(123) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(124) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(125) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(126) and (a(110) xor a(115) xor a(116) xor a(117))) xor
        (b(127) and (a(109) xor a(114) xor a(115) xor a(116)));
    c(116) <= 
        (b(  0) and (a(116))) xor
        (b(  1) and (a(115))) xor
        (b(  2) and (a(114))) xor
        (b(  3) and (a(113))) xor
        (b(  4) and (a(112))) xor
        (b(  5) and (a(111))) xor
        (b(  6) and (a(110))) xor
        (b(  7) and (a(109))) xor
        (b(  8) and (a(108))) xor
        (b(  9) and (a(107))) xor
        (b( 10) and (a(106))) xor
        (b( 11) and (a(105))) xor
        (b( 12) and (a(104))) xor
        (b( 13) and (a(103))) xor
        (b( 14) and (a(102))) xor
        (b( 15) and (a(101))) xor
        (b( 16) and (a(100))) xor
        (b( 17) and (a( 99))) xor
        (b( 18) and (a( 98))) xor
        (b( 19) and (a( 97))) xor
        (b( 20) and (a( 96))) xor
        (b( 21) and (a( 95))) xor
        (b( 22) and (a( 94))) xor
        (b( 23) and (a( 93))) xor
        (b( 24) and (a( 92))) xor
        (b( 25) and (a( 91))) xor
        (b( 26) and (a( 90))) xor
        (b( 27) and (a( 89))) xor
        (b( 28) and (a( 88))) xor
        (b( 29) and (a( 87))) xor
        (b( 30) and (a( 86))) xor
        (b( 31) and (a( 85))) xor
        (b( 32) and (a( 84))) xor
        (b( 33) and (a( 83))) xor
        (b( 34) and (a( 82))) xor
        (b( 35) and (a( 81))) xor
        (b( 36) and (a( 80))) xor
        (b( 37) and (a( 79))) xor
        (b( 38) and (a( 78))) xor
        (b( 39) and (a( 77))) xor
        (b( 40) and (a( 76))) xor
        (b( 41) and (a( 75))) xor
        (b( 42) and (a( 74))) xor
        (b( 43) and (a( 73))) xor
        (b( 44) and (a( 72))) xor
        (b( 45) and (a( 71))) xor
        (b( 46) and (a( 70))) xor
        (b( 47) and (a( 69))) xor
        (b( 48) and (a( 68))) xor
        (b( 49) and (a( 67))) xor
        (b( 50) and (a( 66))) xor
        (b( 51) and (a( 65))) xor
        (b( 52) and (a( 64))) xor
        (b( 53) and (a( 63))) xor
        (b( 54) and (a( 62))) xor
        (b( 55) and (a( 61))) xor
        (b( 56) and (a( 60))) xor
        (b( 57) and (a( 59))) xor
        (b( 58) and (a( 58))) xor
        (b( 59) and (a( 57))) xor
        (b( 60) and (a( 56))) xor
        (b( 61) and (a( 55))) xor
        (b( 62) and (a( 54))) xor
        (b( 63) and (a( 53))) xor
        (b( 64) and (a( 52))) xor
        (b( 65) and (a( 51))) xor
        (b( 66) and (a( 50))) xor
        (b( 67) and (a( 49))) xor
        (b( 68) and (a( 48))) xor
        (b( 69) and (a( 47))) xor
        (b( 70) and (a( 46))) xor
        (b( 71) and (a( 45))) xor
        (b( 72) and (a( 44))) xor
        (b( 73) and (a( 43))) xor
        (b( 74) and (a( 42))) xor
        (b( 75) and (a( 41))) xor
        (b( 76) and (a( 40))) xor
        (b( 77) and (a( 39))) xor
        (b( 78) and (a( 38))) xor
        (b( 79) and (a( 37))) xor
        (b( 80) and (a( 36))) xor
        (b( 81) and (a( 35))) xor
        (b( 82) and (a( 34))) xor
        (b( 83) and (a( 33))) xor
        (b( 84) and (a( 32))) xor
        (b( 85) and (a( 31))) xor
        (b( 86) and (a( 30))) xor
        (b( 87) and (a( 29))) xor
        (b( 88) and (a( 28))) xor
        (b( 89) and (a( 27))) xor
        (b( 90) and (a( 26))) xor
        (b( 91) and (a( 25))) xor
        (b( 92) and (a( 24))) xor
        (b( 93) and (a( 23))) xor
        (b( 94) and (a( 22))) xor
        (b( 95) and (a( 21))) xor
        (b( 96) and (a( 20))) xor
        (b( 97) and (a( 19))) xor
        (b( 98) and (a( 18))) xor
        (b( 99) and (a( 17))) xor
        (b(100) and (a( 16))) xor
        (b(101) and (a( 15))) xor
        (b(102) and (a( 14))) xor
        (b(103) and (a( 13))) xor
        (b(104) and (a( 12))) xor
        (b(105) and (a( 11))) xor
        (b(106) and (a( 10))) xor
        (b(107) and (a(  9))) xor
        (b(108) and (a(  8))) xor
        (b(109) and (a(  7))) xor
        (b(110) and (a(  6) xor a(127))) xor
        (b(111) and (a(  5) xor a(126))) xor
        (b(112) and (a(  4) xor a(125))) xor
        (b(113) and (a(  3) xor a(124))) xor
        (b(114) and (a(  2) xor a(123))) xor
        (b(115) and (a(  1) xor a(122) xor a(127))) xor
        (b(116) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(117) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(118) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(119) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(120) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(121) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(122) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(123) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(124) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(125) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(126) and (a(111) xor a(116) xor a(117) xor a(118))) xor
        (b(127) and (a(110) xor a(115) xor a(116) xor a(117)));
    c(117) <= 
        (b(  0) and (a(117))) xor
        (b(  1) and (a(116))) xor
        (b(  2) and (a(115))) xor
        (b(  3) and (a(114))) xor
        (b(  4) and (a(113))) xor
        (b(  5) and (a(112))) xor
        (b(  6) and (a(111))) xor
        (b(  7) and (a(110))) xor
        (b(  8) and (a(109))) xor
        (b(  9) and (a(108))) xor
        (b( 10) and (a(107))) xor
        (b( 11) and (a(106))) xor
        (b( 12) and (a(105))) xor
        (b( 13) and (a(104))) xor
        (b( 14) and (a(103))) xor
        (b( 15) and (a(102))) xor
        (b( 16) and (a(101))) xor
        (b( 17) and (a(100))) xor
        (b( 18) and (a( 99))) xor
        (b( 19) and (a( 98))) xor
        (b( 20) and (a( 97))) xor
        (b( 21) and (a( 96))) xor
        (b( 22) and (a( 95))) xor
        (b( 23) and (a( 94))) xor
        (b( 24) and (a( 93))) xor
        (b( 25) and (a( 92))) xor
        (b( 26) and (a( 91))) xor
        (b( 27) and (a( 90))) xor
        (b( 28) and (a( 89))) xor
        (b( 29) and (a( 88))) xor
        (b( 30) and (a( 87))) xor
        (b( 31) and (a( 86))) xor
        (b( 32) and (a( 85))) xor
        (b( 33) and (a( 84))) xor
        (b( 34) and (a( 83))) xor
        (b( 35) and (a( 82))) xor
        (b( 36) and (a( 81))) xor
        (b( 37) and (a( 80))) xor
        (b( 38) and (a( 79))) xor
        (b( 39) and (a( 78))) xor
        (b( 40) and (a( 77))) xor
        (b( 41) and (a( 76))) xor
        (b( 42) and (a( 75))) xor
        (b( 43) and (a( 74))) xor
        (b( 44) and (a( 73))) xor
        (b( 45) and (a( 72))) xor
        (b( 46) and (a( 71))) xor
        (b( 47) and (a( 70))) xor
        (b( 48) and (a( 69))) xor
        (b( 49) and (a( 68))) xor
        (b( 50) and (a( 67))) xor
        (b( 51) and (a( 66))) xor
        (b( 52) and (a( 65))) xor
        (b( 53) and (a( 64))) xor
        (b( 54) and (a( 63))) xor
        (b( 55) and (a( 62))) xor
        (b( 56) and (a( 61))) xor
        (b( 57) and (a( 60))) xor
        (b( 58) and (a( 59))) xor
        (b( 59) and (a( 58))) xor
        (b( 60) and (a( 57))) xor
        (b( 61) and (a( 56))) xor
        (b( 62) and (a( 55))) xor
        (b( 63) and (a( 54))) xor
        (b( 64) and (a( 53))) xor
        (b( 65) and (a( 52))) xor
        (b( 66) and (a( 51))) xor
        (b( 67) and (a( 50))) xor
        (b( 68) and (a( 49))) xor
        (b( 69) and (a( 48))) xor
        (b( 70) and (a( 47))) xor
        (b( 71) and (a( 46))) xor
        (b( 72) and (a( 45))) xor
        (b( 73) and (a( 44))) xor
        (b( 74) and (a( 43))) xor
        (b( 75) and (a( 42))) xor
        (b( 76) and (a( 41))) xor
        (b( 77) and (a( 40))) xor
        (b( 78) and (a( 39))) xor
        (b( 79) and (a( 38))) xor
        (b( 80) and (a( 37))) xor
        (b( 81) and (a( 36))) xor
        (b( 82) and (a( 35))) xor
        (b( 83) and (a( 34))) xor
        (b( 84) and (a( 33))) xor
        (b( 85) and (a( 32))) xor
        (b( 86) and (a( 31))) xor
        (b( 87) and (a( 30))) xor
        (b( 88) and (a( 29))) xor
        (b( 89) and (a( 28))) xor
        (b( 90) and (a( 27))) xor
        (b( 91) and (a( 26))) xor
        (b( 92) and (a( 25))) xor
        (b( 93) and (a( 24))) xor
        (b( 94) and (a( 23))) xor
        (b( 95) and (a( 22))) xor
        (b( 96) and (a( 21))) xor
        (b( 97) and (a( 20))) xor
        (b( 98) and (a( 19))) xor
        (b( 99) and (a( 18))) xor
        (b(100) and (a( 17))) xor
        (b(101) and (a( 16))) xor
        (b(102) and (a( 15))) xor
        (b(103) and (a( 14))) xor
        (b(104) and (a( 13))) xor
        (b(105) and (a( 12))) xor
        (b(106) and (a( 11))) xor
        (b(107) and (a( 10))) xor
        (b(108) and (a(  9))) xor
        (b(109) and (a(  8))) xor
        (b(110) and (a(  7))) xor
        (b(111) and (a(  6) xor a(127))) xor
        (b(112) and (a(  5) xor a(126))) xor
        (b(113) and (a(  4) xor a(125))) xor
        (b(114) and (a(  3) xor a(124))) xor
        (b(115) and (a(  2) xor a(123))) xor
        (b(116) and (a(  1) xor a(122) xor a(127))) xor
        (b(117) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(118) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(119) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(120) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(121) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(122) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(123) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(124) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(125) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(126) and (a(112) xor a(117) xor a(118) xor a(119))) xor
        (b(127) and (a(111) xor a(116) xor a(117) xor a(118)));
    c(118) <= 
        (b(  0) and (a(118))) xor
        (b(  1) and (a(117))) xor
        (b(  2) and (a(116))) xor
        (b(  3) and (a(115))) xor
        (b(  4) and (a(114))) xor
        (b(  5) and (a(113))) xor
        (b(  6) and (a(112))) xor
        (b(  7) and (a(111))) xor
        (b(  8) and (a(110))) xor
        (b(  9) and (a(109))) xor
        (b( 10) and (a(108))) xor
        (b( 11) and (a(107))) xor
        (b( 12) and (a(106))) xor
        (b( 13) and (a(105))) xor
        (b( 14) and (a(104))) xor
        (b( 15) and (a(103))) xor
        (b( 16) and (a(102))) xor
        (b( 17) and (a(101))) xor
        (b( 18) and (a(100))) xor
        (b( 19) and (a( 99))) xor
        (b( 20) and (a( 98))) xor
        (b( 21) and (a( 97))) xor
        (b( 22) and (a( 96))) xor
        (b( 23) and (a( 95))) xor
        (b( 24) and (a( 94))) xor
        (b( 25) and (a( 93))) xor
        (b( 26) and (a( 92))) xor
        (b( 27) and (a( 91))) xor
        (b( 28) and (a( 90))) xor
        (b( 29) and (a( 89))) xor
        (b( 30) and (a( 88))) xor
        (b( 31) and (a( 87))) xor
        (b( 32) and (a( 86))) xor
        (b( 33) and (a( 85))) xor
        (b( 34) and (a( 84))) xor
        (b( 35) and (a( 83))) xor
        (b( 36) and (a( 82))) xor
        (b( 37) and (a( 81))) xor
        (b( 38) and (a( 80))) xor
        (b( 39) and (a( 79))) xor
        (b( 40) and (a( 78))) xor
        (b( 41) and (a( 77))) xor
        (b( 42) and (a( 76))) xor
        (b( 43) and (a( 75))) xor
        (b( 44) and (a( 74))) xor
        (b( 45) and (a( 73))) xor
        (b( 46) and (a( 72))) xor
        (b( 47) and (a( 71))) xor
        (b( 48) and (a( 70))) xor
        (b( 49) and (a( 69))) xor
        (b( 50) and (a( 68))) xor
        (b( 51) and (a( 67))) xor
        (b( 52) and (a( 66))) xor
        (b( 53) and (a( 65))) xor
        (b( 54) and (a( 64))) xor
        (b( 55) and (a( 63))) xor
        (b( 56) and (a( 62))) xor
        (b( 57) and (a( 61))) xor
        (b( 58) and (a( 60))) xor
        (b( 59) and (a( 59))) xor
        (b( 60) and (a( 58))) xor
        (b( 61) and (a( 57))) xor
        (b( 62) and (a( 56))) xor
        (b( 63) and (a( 55))) xor
        (b( 64) and (a( 54))) xor
        (b( 65) and (a( 53))) xor
        (b( 66) and (a( 52))) xor
        (b( 67) and (a( 51))) xor
        (b( 68) and (a( 50))) xor
        (b( 69) and (a( 49))) xor
        (b( 70) and (a( 48))) xor
        (b( 71) and (a( 47))) xor
        (b( 72) and (a( 46))) xor
        (b( 73) and (a( 45))) xor
        (b( 74) and (a( 44))) xor
        (b( 75) and (a( 43))) xor
        (b( 76) and (a( 42))) xor
        (b( 77) and (a( 41))) xor
        (b( 78) and (a( 40))) xor
        (b( 79) and (a( 39))) xor
        (b( 80) and (a( 38))) xor
        (b( 81) and (a( 37))) xor
        (b( 82) and (a( 36))) xor
        (b( 83) and (a( 35))) xor
        (b( 84) and (a( 34))) xor
        (b( 85) and (a( 33))) xor
        (b( 86) and (a( 32))) xor
        (b( 87) and (a( 31))) xor
        (b( 88) and (a( 30))) xor
        (b( 89) and (a( 29))) xor
        (b( 90) and (a( 28))) xor
        (b( 91) and (a( 27))) xor
        (b( 92) and (a( 26))) xor
        (b( 93) and (a( 25))) xor
        (b( 94) and (a( 24))) xor
        (b( 95) and (a( 23))) xor
        (b( 96) and (a( 22))) xor
        (b( 97) and (a( 21))) xor
        (b( 98) and (a( 20))) xor
        (b( 99) and (a( 19))) xor
        (b(100) and (a( 18))) xor
        (b(101) and (a( 17))) xor
        (b(102) and (a( 16))) xor
        (b(103) and (a( 15))) xor
        (b(104) and (a( 14))) xor
        (b(105) and (a( 13))) xor
        (b(106) and (a( 12))) xor
        (b(107) and (a( 11))) xor
        (b(108) and (a( 10))) xor
        (b(109) and (a(  9))) xor
        (b(110) and (a(  8))) xor
        (b(111) and (a(  7))) xor
        (b(112) and (a(  6) xor a(127))) xor
        (b(113) and (a(  5) xor a(126))) xor
        (b(114) and (a(  4) xor a(125))) xor
        (b(115) and (a(  3) xor a(124))) xor
        (b(116) and (a(  2) xor a(123))) xor
        (b(117) and (a(  1) xor a(122) xor a(127))) xor
        (b(118) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(119) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(120) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(121) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(122) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(123) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(124) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(125) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(126) and (a(113) xor a(118) xor a(119) xor a(120))) xor
        (b(127) and (a(112) xor a(117) xor a(118) xor a(119)));
    c(119) <= 
        (b(  0) and (a(119))) xor
        (b(  1) and (a(118))) xor
        (b(  2) and (a(117))) xor
        (b(  3) and (a(116))) xor
        (b(  4) and (a(115))) xor
        (b(  5) and (a(114))) xor
        (b(  6) and (a(113))) xor
        (b(  7) and (a(112))) xor
        (b(  8) and (a(111))) xor
        (b(  9) and (a(110))) xor
        (b( 10) and (a(109))) xor
        (b( 11) and (a(108))) xor
        (b( 12) and (a(107))) xor
        (b( 13) and (a(106))) xor
        (b( 14) and (a(105))) xor
        (b( 15) and (a(104))) xor
        (b( 16) and (a(103))) xor
        (b( 17) and (a(102))) xor
        (b( 18) and (a(101))) xor
        (b( 19) and (a(100))) xor
        (b( 20) and (a( 99))) xor
        (b( 21) and (a( 98))) xor
        (b( 22) and (a( 97))) xor
        (b( 23) and (a( 96))) xor
        (b( 24) and (a( 95))) xor
        (b( 25) and (a( 94))) xor
        (b( 26) and (a( 93))) xor
        (b( 27) and (a( 92))) xor
        (b( 28) and (a( 91))) xor
        (b( 29) and (a( 90))) xor
        (b( 30) and (a( 89))) xor
        (b( 31) and (a( 88))) xor
        (b( 32) and (a( 87))) xor
        (b( 33) and (a( 86))) xor
        (b( 34) and (a( 85))) xor
        (b( 35) and (a( 84))) xor
        (b( 36) and (a( 83))) xor
        (b( 37) and (a( 82))) xor
        (b( 38) and (a( 81))) xor
        (b( 39) and (a( 80))) xor
        (b( 40) and (a( 79))) xor
        (b( 41) and (a( 78))) xor
        (b( 42) and (a( 77))) xor
        (b( 43) and (a( 76))) xor
        (b( 44) and (a( 75))) xor
        (b( 45) and (a( 74))) xor
        (b( 46) and (a( 73))) xor
        (b( 47) and (a( 72))) xor
        (b( 48) and (a( 71))) xor
        (b( 49) and (a( 70))) xor
        (b( 50) and (a( 69))) xor
        (b( 51) and (a( 68))) xor
        (b( 52) and (a( 67))) xor
        (b( 53) and (a( 66))) xor
        (b( 54) and (a( 65))) xor
        (b( 55) and (a( 64))) xor
        (b( 56) and (a( 63))) xor
        (b( 57) and (a( 62))) xor
        (b( 58) and (a( 61))) xor
        (b( 59) and (a( 60))) xor
        (b( 60) and (a( 59))) xor
        (b( 61) and (a( 58))) xor
        (b( 62) and (a( 57))) xor
        (b( 63) and (a( 56))) xor
        (b( 64) and (a( 55))) xor
        (b( 65) and (a( 54))) xor
        (b( 66) and (a( 53))) xor
        (b( 67) and (a( 52))) xor
        (b( 68) and (a( 51))) xor
        (b( 69) and (a( 50))) xor
        (b( 70) and (a( 49))) xor
        (b( 71) and (a( 48))) xor
        (b( 72) and (a( 47))) xor
        (b( 73) and (a( 46))) xor
        (b( 74) and (a( 45))) xor
        (b( 75) and (a( 44))) xor
        (b( 76) and (a( 43))) xor
        (b( 77) and (a( 42))) xor
        (b( 78) and (a( 41))) xor
        (b( 79) and (a( 40))) xor
        (b( 80) and (a( 39))) xor
        (b( 81) and (a( 38))) xor
        (b( 82) and (a( 37))) xor
        (b( 83) and (a( 36))) xor
        (b( 84) and (a( 35))) xor
        (b( 85) and (a( 34))) xor
        (b( 86) and (a( 33))) xor
        (b( 87) and (a( 32))) xor
        (b( 88) and (a( 31))) xor
        (b( 89) and (a( 30))) xor
        (b( 90) and (a( 29))) xor
        (b( 91) and (a( 28))) xor
        (b( 92) and (a( 27))) xor
        (b( 93) and (a( 26))) xor
        (b( 94) and (a( 25))) xor
        (b( 95) and (a( 24))) xor
        (b( 96) and (a( 23))) xor
        (b( 97) and (a( 22))) xor
        (b( 98) and (a( 21))) xor
        (b( 99) and (a( 20))) xor
        (b(100) and (a( 19))) xor
        (b(101) and (a( 18))) xor
        (b(102) and (a( 17))) xor
        (b(103) and (a( 16))) xor
        (b(104) and (a( 15))) xor
        (b(105) and (a( 14))) xor
        (b(106) and (a( 13))) xor
        (b(107) and (a( 12))) xor
        (b(108) and (a( 11))) xor
        (b(109) and (a( 10))) xor
        (b(110) and (a(  9))) xor
        (b(111) and (a(  8))) xor
        (b(112) and (a(  7))) xor
        (b(113) and (a(  6) xor a(127))) xor
        (b(114) and (a(  5) xor a(126))) xor
        (b(115) and (a(  4) xor a(125))) xor
        (b(116) and (a(  3) xor a(124))) xor
        (b(117) and (a(  2) xor a(123))) xor
        (b(118) and (a(  1) xor a(122) xor a(127))) xor
        (b(119) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(120) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(121) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(122) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(123) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(124) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(125) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(126) and (a(114) xor a(119) xor a(120) xor a(121))) xor
        (b(127) and (a(113) xor a(118) xor a(119) xor a(120)));
    c(120) <= 
        (b(  0) and (a(120))) xor
        (b(  1) and (a(119))) xor
        (b(  2) and (a(118))) xor
        (b(  3) and (a(117))) xor
        (b(  4) and (a(116))) xor
        (b(  5) and (a(115))) xor
        (b(  6) and (a(114))) xor
        (b(  7) and (a(113))) xor
        (b(  8) and (a(112))) xor
        (b(  9) and (a(111))) xor
        (b( 10) and (a(110))) xor
        (b( 11) and (a(109))) xor
        (b( 12) and (a(108))) xor
        (b( 13) and (a(107))) xor
        (b( 14) and (a(106))) xor
        (b( 15) and (a(105))) xor
        (b( 16) and (a(104))) xor
        (b( 17) and (a(103))) xor
        (b( 18) and (a(102))) xor
        (b( 19) and (a(101))) xor
        (b( 20) and (a(100))) xor
        (b( 21) and (a( 99))) xor
        (b( 22) and (a( 98))) xor
        (b( 23) and (a( 97))) xor
        (b( 24) and (a( 96))) xor
        (b( 25) and (a( 95))) xor
        (b( 26) and (a( 94))) xor
        (b( 27) and (a( 93))) xor
        (b( 28) and (a( 92))) xor
        (b( 29) and (a( 91))) xor
        (b( 30) and (a( 90))) xor
        (b( 31) and (a( 89))) xor
        (b( 32) and (a( 88))) xor
        (b( 33) and (a( 87))) xor
        (b( 34) and (a( 86))) xor
        (b( 35) and (a( 85))) xor
        (b( 36) and (a( 84))) xor
        (b( 37) and (a( 83))) xor
        (b( 38) and (a( 82))) xor
        (b( 39) and (a( 81))) xor
        (b( 40) and (a( 80))) xor
        (b( 41) and (a( 79))) xor
        (b( 42) and (a( 78))) xor
        (b( 43) and (a( 77))) xor
        (b( 44) and (a( 76))) xor
        (b( 45) and (a( 75))) xor
        (b( 46) and (a( 74))) xor
        (b( 47) and (a( 73))) xor
        (b( 48) and (a( 72))) xor
        (b( 49) and (a( 71))) xor
        (b( 50) and (a( 70))) xor
        (b( 51) and (a( 69))) xor
        (b( 52) and (a( 68))) xor
        (b( 53) and (a( 67))) xor
        (b( 54) and (a( 66))) xor
        (b( 55) and (a( 65))) xor
        (b( 56) and (a( 64))) xor
        (b( 57) and (a( 63))) xor
        (b( 58) and (a( 62))) xor
        (b( 59) and (a( 61))) xor
        (b( 60) and (a( 60))) xor
        (b( 61) and (a( 59))) xor
        (b( 62) and (a( 58))) xor
        (b( 63) and (a( 57))) xor
        (b( 64) and (a( 56))) xor
        (b( 65) and (a( 55))) xor
        (b( 66) and (a( 54))) xor
        (b( 67) and (a( 53))) xor
        (b( 68) and (a( 52))) xor
        (b( 69) and (a( 51))) xor
        (b( 70) and (a( 50))) xor
        (b( 71) and (a( 49))) xor
        (b( 72) and (a( 48))) xor
        (b( 73) and (a( 47))) xor
        (b( 74) and (a( 46))) xor
        (b( 75) and (a( 45))) xor
        (b( 76) and (a( 44))) xor
        (b( 77) and (a( 43))) xor
        (b( 78) and (a( 42))) xor
        (b( 79) and (a( 41))) xor
        (b( 80) and (a( 40))) xor
        (b( 81) and (a( 39))) xor
        (b( 82) and (a( 38))) xor
        (b( 83) and (a( 37))) xor
        (b( 84) and (a( 36))) xor
        (b( 85) and (a( 35))) xor
        (b( 86) and (a( 34))) xor
        (b( 87) and (a( 33))) xor
        (b( 88) and (a( 32))) xor
        (b( 89) and (a( 31))) xor
        (b( 90) and (a( 30))) xor
        (b( 91) and (a( 29))) xor
        (b( 92) and (a( 28))) xor
        (b( 93) and (a( 27))) xor
        (b( 94) and (a( 26))) xor
        (b( 95) and (a( 25))) xor
        (b( 96) and (a( 24))) xor
        (b( 97) and (a( 23))) xor
        (b( 98) and (a( 22))) xor
        (b( 99) and (a( 21))) xor
        (b(100) and (a( 20))) xor
        (b(101) and (a( 19))) xor
        (b(102) and (a( 18))) xor
        (b(103) and (a( 17))) xor
        (b(104) and (a( 16))) xor
        (b(105) and (a( 15))) xor
        (b(106) and (a( 14))) xor
        (b(107) and (a( 13))) xor
        (b(108) and (a( 12))) xor
        (b(109) and (a( 11))) xor
        (b(110) and (a( 10))) xor
        (b(111) and (a(  9))) xor
        (b(112) and (a(  8))) xor
        (b(113) and (a(  7))) xor
        (b(114) and (a(  6) xor a(127))) xor
        (b(115) and (a(  5) xor a(126))) xor
        (b(116) and (a(  4) xor a(125))) xor
        (b(117) and (a(  3) xor a(124))) xor
        (b(118) and (a(  2) xor a(123))) xor
        (b(119) and (a(  1) xor a(122) xor a(127))) xor
        (b(120) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(121) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(122) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(123) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(124) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(125) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(126) and (a(115) xor a(120) xor a(121) xor a(122))) xor
        (b(127) and (a(114) xor a(119) xor a(120) xor a(121)));
    c(121) <= 
        (b(  0) and (a(121))) xor
        (b(  1) and (a(120))) xor
        (b(  2) and (a(119))) xor
        (b(  3) and (a(118))) xor
        (b(  4) and (a(117))) xor
        (b(  5) and (a(116))) xor
        (b(  6) and (a(115))) xor
        (b(  7) and (a(114))) xor
        (b(  8) and (a(113))) xor
        (b(  9) and (a(112))) xor
        (b( 10) and (a(111))) xor
        (b( 11) and (a(110))) xor
        (b( 12) and (a(109))) xor
        (b( 13) and (a(108))) xor
        (b( 14) and (a(107))) xor
        (b( 15) and (a(106))) xor
        (b( 16) and (a(105))) xor
        (b( 17) and (a(104))) xor
        (b( 18) and (a(103))) xor
        (b( 19) and (a(102))) xor
        (b( 20) and (a(101))) xor
        (b( 21) and (a(100))) xor
        (b( 22) and (a( 99))) xor
        (b( 23) and (a( 98))) xor
        (b( 24) and (a( 97))) xor
        (b( 25) and (a( 96))) xor
        (b( 26) and (a( 95))) xor
        (b( 27) and (a( 94))) xor
        (b( 28) and (a( 93))) xor
        (b( 29) and (a( 92))) xor
        (b( 30) and (a( 91))) xor
        (b( 31) and (a( 90))) xor
        (b( 32) and (a( 89))) xor
        (b( 33) and (a( 88))) xor
        (b( 34) and (a( 87))) xor
        (b( 35) and (a( 86))) xor
        (b( 36) and (a( 85))) xor
        (b( 37) and (a( 84))) xor
        (b( 38) and (a( 83))) xor
        (b( 39) and (a( 82))) xor
        (b( 40) and (a( 81))) xor
        (b( 41) and (a( 80))) xor
        (b( 42) and (a( 79))) xor
        (b( 43) and (a( 78))) xor
        (b( 44) and (a( 77))) xor
        (b( 45) and (a( 76))) xor
        (b( 46) and (a( 75))) xor
        (b( 47) and (a( 74))) xor
        (b( 48) and (a( 73))) xor
        (b( 49) and (a( 72))) xor
        (b( 50) and (a( 71))) xor
        (b( 51) and (a( 70))) xor
        (b( 52) and (a( 69))) xor
        (b( 53) and (a( 68))) xor
        (b( 54) and (a( 67))) xor
        (b( 55) and (a( 66))) xor
        (b( 56) and (a( 65))) xor
        (b( 57) and (a( 64))) xor
        (b( 58) and (a( 63))) xor
        (b( 59) and (a( 62))) xor
        (b( 60) and (a( 61))) xor
        (b( 61) and (a( 60))) xor
        (b( 62) and (a( 59))) xor
        (b( 63) and (a( 58))) xor
        (b( 64) and (a( 57))) xor
        (b( 65) and (a( 56))) xor
        (b( 66) and (a( 55))) xor
        (b( 67) and (a( 54))) xor
        (b( 68) and (a( 53))) xor
        (b( 69) and (a( 52))) xor
        (b( 70) and (a( 51))) xor
        (b( 71) and (a( 50))) xor
        (b( 72) and (a( 49))) xor
        (b( 73) and (a( 48))) xor
        (b( 74) and (a( 47))) xor
        (b( 75) and (a( 46))) xor
        (b( 76) and (a( 45))) xor
        (b( 77) and (a( 44))) xor
        (b( 78) and (a( 43))) xor
        (b( 79) and (a( 42))) xor
        (b( 80) and (a( 41))) xor
        (b( 81) and (a( 40))) xor
        (b( 82) and (a( 39))) xor
        (b( 83) and (a( 38))) xor
        (b( 84) and (a( 37))) xor
        (b( 85) and (a( 36))) xor
        (b( 86) and (a( 35))) xor
        (b( 87) and (a( 34))) xor
        (b( 88) and (a( 33))) xor
        (b( 89) and (a( 32))) xor
        (b( 90) and (a( 31))) xor
        (b( 91) and (a( 30))) xor
        (b( 92) and (a( 29))) xor
        (b( 93) and (a( 28))) xor
        (b( 94) and (a( 27))) xor
        (b( 95) and (a( 26))) xor
        (b( 96) and (a( 25))) xor
        (b( 97) and (a( 24))) xor
        (b( 98) and (a( 23))) xor
        (b( 99) and (a( 22))) xor
        (b(100) and (a( 21))) xor
        (b(101) and (a( 20))) xor
        (b(102) and (a( 19))) xor
        (b(103) and (a( 18))) xor
        (b(104) and (a( 17))) xor
        (b(105) and (a( 16))) xor
        (b(106) and (a( 15))) xor
        (b(107) and (a( 14))) xor
        (b(108) and (a( 13))) xor
        (b(109) and (a( 12))) xor
        (b(110) and (a( 11))) xor
        (b(111) and (a( 10))) xor
        (b(112) and (a(  9))) xor
        (b(113) and (a(  8))) xor
        (b(114) and (a(  7))) xor
        (b(115) and (a(  6) xor a(127))) xor
        (b(116) and (a(  5) xor a(126))) xor
        (b(117) and (a(  4) xor a(125))) xor
        (b(118) and (a(  3) xor a(124))) xor
        (b(119) and (a(  2) xor a(123))) xor
        (b(120) and (a(  1) xor a(122) xor a(127))) xor
        (b(121) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(122) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(123) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(124) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(125) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(126) and (a(116) xor a(121) xor a(122) xor a(123))) xor
        (b(127) and (a(115) xor a(120) xor a(121) xor a(122)));
    c(122) <= 
        (b(  0) and (a(122))) xor
        (b(  1) and (a(121))) xor
        (b(  2) and (a(120))) xor
        (b(  3) and (a(119))) xor
        (b(  4) and (a(118))) xor
        (b(  5) and (a(117))) xor
        (b(  6) and (a(116))) xor
        (b(  7) and (a(115))) xor
        (b(  8) and (a(114))) xor
        (b(  9) and (a(113))) xor
        (b( 10) and (a(112))) xor
        (b( 11) and (a(111))) xor
        (b( 12) and (a(110))) xor
        (b( 13) and (a(109))) xor
        (b( 14) and (a(108))) xor
        (b( 15) and (a(107))) xor
        (b( 16) and (a(106))) xor
        (b( 17) and (a(105))) xor
        (b( 18) and (a(104))) xor
        (b( 19) and (a(103))) xor
        (b( 20) and (a(102))) xor
        (b( 21) and (a(101))) xor
        (b( 22) and (a(100))) xor
        (b( 23) and (a( 99))) xor
        (b( 24) and (a( 98))) xor
        (b( 25) and (a( 97))) xor
        (b( 26) and (a( 96))) xor
        (b( 27) and (a( 95))) xor
        (b( 28) and (a( 94))) xor
        (b( 29) and (a( 93))) xor
        (b( 30) and (a( 92))) xor
        (b( 31) and (a( 91))) xor
        (b( 32) and (a( 90))) xor
        (b( 33) and (a( 89))) xor
        (b( 34) and (a( 88))) xor
        (b( 35) and (a( 87))) xor
        (b( 36) and (a( 86))) xor
        (b( 37) and (a( 85))) xor
        (b( 38) and (a( 84))) xor
        (b( 39) and (a( 83))) xor
        (b( 40) and (a( 82))) xor
        (b( 41) and (a( 81))) xor
        (b( 42) and (a( 80))) xor
        (b( 43) and (a( 79))) xor
        (b( 44) and (a( 78))) xor
        (b( 45) and (a( 77))) xor
        (b( 46) and (a( 76))) xor
        (b( 47) and (a( 75))) xor
        (b( 48) and (a( 74))) xor
        (b( 49) and (a( 73))) xor
        (b( 50) and (a( 72))) xor
        (b( 51) and (a( 71))) xor
        (b( 52) and (a( 70))) xor
        (b( 53) and (a( 69))) xor
        (b( 54) and (a( 68))) xor
        (b( 55) and (a( 67))) xor
        (b( 56) and (a( 66))) xor
        (b( 57) and (a( 65))) xor
        (b( 58) and (a( 64))) xor
        (b( 59) and (a( 63))) xor
        (b( 60) and (a( 62))) xor
        (b( 61) and (a( 61))) xor
        (b( 62) and (a( 60))) xor
        (b( 63) and (a( 59))) xor
        (b( 64) and (a( 58))) xor
        (b( 65) and (a( 57))) xor
        (b( 66) and (a( 56))) xor
        (b( 67) and (a( 55))) xor
        (b( 68) and (a( 54))) xor
        (b( 69) and (a( 53))) xor
        (b( 70) and (a( 52))) xor
        (b( 71) and (a( 51))) xor
        (b( 72) and (a( 50))) xor
        (b( 73) and (a( 49))) xor
        (b( 74) and (a( 48))) xor
        (b( 75) and (a( 47))) xor
        (b( 76) and (a( 46))) xor
        (b( 77) and (a( 45))) xor
        (b( 78) and (a( 44))) xor
        (b( 79) and (a( 43))) xor
        (b( 80) and (a( 42))) xor
        (b( 81) and (a( 41))) xor
        (b( 82) and (a( 40))) xor
        (b( 83) and (a( 39))) xor
        (b( 84) and (a( 38))) xor
        (b( 85) and (a( 37))) xor
        (b( 86) and (a( 36))) xor
        (b( 87) and (a( 35))) xor
        (b( 88) and (a( 34))) xor
        (b( 89) and (a( 33))) xor
        (b( 90) and (a( 32))) xor
        (b( 91) and (a( 31))) xor
        (b( 92) and (a( 30))) xor
        (b( 93) and (a( 29))) xor
        (b( 94) and (a( 28))) xor
        (b( 95) and (a( 27))) xor
        (b( 96) and (a( 26))) xor
        (b( 97) and (a( 25))) xor
        (b( 98) and (a( 24))) xor
        (b( 99) and (a( 23))) xor
        (b(100) and (a( 22))) xor
        (b(101) and (a( 21))) xor
        (b(102) and (a( 20))) xor
        (b(103) and (a( 19))) xor
        (b(104) and (a( 18))) xor
        (b(105) and (a( 17))) xor
        (b(106) and (a( 16))) xor
        (b(107) and (a( 15))) xor
        (b(108) and (a( 14))) xor
        (b(109) and (a( 13))) xor
        (b(110) and (a( 12))) xor
        (b(111) and (a( 11))) xor
        (b(112) and (a( 10))) xor
        (b(113) and (a(  9))) xor
        (b(114) and (a(  8))) xor
        (b(115) and (a(  7))) xor
        (b(116) and (a(  6) xor a(127))) xor
        (b(117) and (a(  5) xor a(126))) xor
        (b(118) and (a(  4) xor a(125))) xor
        (b(119) and (a(  3) xor a(124))) xor
        (b(120) and (a(  2) xor a(123))) xor
        (b(121) and (a(  1) xor a(122) xor a(127))) xor
        (b(122) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(123) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(124) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(125) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(126) and (a(117) xor a(122) xor a(123) xor a(124))) xor
        (b(127) and (a(116) xor a(121) xor a(122) xor a(123)));
    c(123) <= 
        (b(  0) and (a(123))) xor
        (b(  1) and (a(122))) xor
        (b(  2) and (a(121))) xor
        (b(  3) and (a(120))) xor
        (b(  4) and (a(119))) xor
        (b(  5) and (a(118))) xor
        (b(  6) and (a(117))) xor
        (b(  7) and (a(116))) xor
        (b(  8) and (a(115))) xor
        (b(  9) and (a(114))) xor
        (b( 10) and (a(113))) xor
        (b( 11) and (a(112))) xor
        (b( 12) and (a(111))) xor
        (b( 13) and (a(110))) xor
        (b( 14) and (a(109))) xor
        (b( 15) and (a(108))) xor
        (b( 16) and (a(107))) xor
        (b( 17) and (a(106))) xor
        (b( 18) and (a(105))) xor
        (b( 19) and (a(104))) xor
        (b( 20) and (a(103))) xor
        (b( 21) and (a(102))) xor
        (b( 22) and (a(101))) xor
        (b( 23) and (a(100))) xor
        (b( 24) and (a( 99))) xor
        (b( 25) and (a( 98))) xor
        (b( 26) and (a( 97))) xor
        (b( 27) and (a( 96))) xor
        (b( 28) and (a( 95))) xor
        (b( 29) and (a( 94))) xor
        (b( 30) and (a( 93))) xor
        (b( 31) and (a( 92))) xor
        (b( 32) and (a( 91))) xor
        (b( 33) and (a( 90))) xor
        (b( 34) and (a( 89))) xor
        (b( 35) and (a( 88))) xor
        (b( 36) and (a( 87))) xor
        (b( 37) and (a( 86))) xor
        (b( 38) and (a( 85))) xor
        (b( 39) and (a( 84))) xor
        (b( 40) and (a( 83))) xor
        (b( 41) and (a( 82))) xor
        (b( 42) and (a( 81))) xor
        (b( 43) and (a( 80))) xor
        (b( 44) and (a( 79))) xor
        (b( 45) and (a( 78))) xor
        (b( 46) and (a( 77))) xor
        (b( 47) and (a( 76))) xor
        (b( 48) and (a( 75))) xor
        (b( 49) and (a( 74))) xor
        (b( 50) and (a( 73))) xor
        (b( 51) and (a( 72))) xor
        (b( 52) and (a( 71))) xor
        (b( 53) and (a( 70))) xor
        (b( 54) and (a( 69))) xor
        (b( 55) and (a( 68))) xor
        (b( 56) and (a( 67))) xor
        (b( 57) and (a( 66))) xor
        (b( 58) and (a( 65))) xor
        (b( 59) and (a( 64))) xor
        (b( 60) and (a( 63))) xor
        (b( 61) and (a( 62))) xor
        (b( 62) and (a( 61))) xor
        (b( 63) and (a( 60))) xor
        (b( 64) and (a( 59))) xor
        (b( 65) and (a( 58))) xor
        (b( 66) and (a( 57))) xor
        (b( 67) and (a( 56))) xor
        (b( 68) and (a( 55))) xor
        (b( 69) and (a( 54))) xor
        (b( 70) and (a( 53))) xor
        (b( 71) and (a( 52))) xor
        (b( 72) and (a( 51))) xor
        (b( 73) and (a( 50))) xor
        (b( 74) and (a( 49))) xor
        (b( 75) and (a( 48))) xor
        (b( 76) and (a( 47))) xor
        (b( 77) and (a( 46))) xor
        (b( 78) and (a( 45))) xor
        (b( 79) and (a( 44))) xor
        (b( 80) and (a( 43))) xor
        (b( 81) and (a( 42))) xor
        (b( 82) and (a( 41))) xor
        (b( 83) and (a( 40))) xor
        (b( 84) and (a( 39))) xor
        (b( 85) and (a( 38))) xor
        (b( 86) and (a( 37))) xor
        (b( 87) and (a( 36))) xor
        (b( 88) and (a( 35))) xor
        (b( 89) and (a( 34))) xor
        (b( 90) and (a( 33))) xor
        (b( 91) and (a( 32))) xor
        (b( 92) and (a( 31))) xor
        (b( 93) and (a( 30))) xor
        (b( 94) and (a( 29))) xor
        (b( 95) and (a( 28))) xor
        (b( 96) and (a( 27))) xor
        (b( 97) and (a( 26))) xor
        (b( 98) and (a( 25))) xor
        (b( 99) and (a( 24))) xor
        (b(100) and (a( 23))) xor
        (b(101) and (a( 22))) xor
        (b(102) and (a( 21))) xor
        (b(103) and (a( 20))) xor
        (b(104) and (a( 19))) xor
        (b(105) and (a( 18))) xor
        (b(106) and (a( 17))) xor
        (b(107) and (a( 16))) xor
        (b(108) and (a( 15))) xor
        (b(109) and (a( 14))) xor
        (b(110) and (a( 13))) xor
        (b(111) and (a( 12))) xor
        (b(112) and (a( 11))) xor
        (b(113) and (a( 10))) xor
        (b(114) and (a(  9))) xor
        (b(115) and (a(  8))) xor
        (b(116) and (a(  7))) xor
        (b(117) and (a(  6) xor a(127))) xor
        (b(118) and (a(  5) xor a(126))) xor
        (b(119) and (a(  4) xor a(125))) xor
        (b(120) and (a(  3) xor a(124))) xor
        (b(121) and (a(  2) xor a(123))) xor
        (b(122) and (a(  1) xor a(122) xor a(127))) xor
        (b(123) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(124) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(125) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(126) and (a(118) xor a(123) xor a(124) xor a(125))) xor
        (b(127) and (a(117) xor a(122) xor a(123) xor a(124)));
    c(124) <= 
        (b(  0) and (a(124))) xor
        (b(  1) and (a(123))) xor
        (b(  2) and (a(122))) xor
        (b(  3) and (a(121))) xor
        (b(  4) and (a(120))) xor
        (b(  5) and (a(119))) xor
        (b(  6) and (a(118))) xor
        (b(  7) and (a(117))) xor
        (b(  8) and (a(116))) xor
        (b(  9) and (a(115))) xor
        (b( 10) and (a(114))) xor
        (b( 11) and (a(113))) xor
        (b( 12) and (a(112))) xor
        (b( 13) and (a(111))) xor
        (b( 14) and (a(110))) xor
        (b( 15) and (a(109))) xor
        (b( 16) and (a(108))) xor
        (b( 17) and (a(107))) xor
        (b( 18) and (a(106))) xor
        (b( 19) and (a(105))) xor
        (b( 20) and (a(104))) xor
        (b( 21) and (a(103))) xor
        (b( 22) and (a(102))) xor
        (b( 23) and (a(101))) xor
        (b( 24) and (a(100))) xor
        (b( 25) and (a( 99))) xor
        (b( 26) and (a( 98))) xor
        (b( 27) and (a( 97))) xor
        (b( 28) and (a( 96))) xor
        (b( 29) and (a( 95))) xor
        (b( 30) and (a( 94))) xor
        (b( 31) and (a( 93))) xor
        (b( 32) and (a( 92))) xor
        (b( 33) and (a( 91))) xor
        (b( 34) and (a( 90))) xor
        (b( 35) and (a( 89))) xor
        (b( 36) and (a( 88))) xor
        (b( 37) and (a( 87))) xor
        (b( 38) and (a( 86))) xor
        (b( 39) and (a( 85))) xor
        (b( 40) and (a( 84))) xor
        (b( 41) and (a( 83))) xor
        (b( 42) and (a( 82))) xor
        (b( 43) and (a( 81))) xor
        (b( 44) and (a( 80))) xor
        (b( 45) and (a( 79))) xor
        (b( 46) and (a( 78))) xor
        (b( 47) and (a( 77))) xor
        (b( 48) and (a( 76))) xor
        (b( 49) and (a( 75))) xor
        (b( 50) and (a( 74))) xor
        (b( 51) and (a( 73))) xor
        (b( 52) and (a( 72))) xor
        (b( 53) and (a( 71))) xor
        (b( 54) and (a( 70))) xor
        (b( 55) and (a( 69))) xor
        (b( 56) and (a( 68))) xor
        (b( 57) and (a( 67))) xor
        (b( 58) and (a( 66))) xor
        (b( 59) and (a( 65))) xor
        (b( 60) and (a( 64))) xor
        (b( 61) and (a( 63))) xor
        (b( 62) and (a( 62))) xor
        (b( 63) and (a( 61))) xor
        (b( 64) and (a( 60))) xor
        (b( 65) and (a( 59))) xor
        (b( 66) and (a( 58))) xor
        (b( 67) and (a( 57))) xor
        (b( 68) and (a( 56))) xor
        (b( 69) and (a( 55))) xor
        (b( 70) and (a( 54))) xor
        (b( 71) and (a( 53))) xor
        (b( 72) and (a( 52))) xor
        (b( 73) and (a( 51))) xor
        (b( 74) and (a( 50))) xor
        (b( 75) and (a( 49))) xor
        (b( 76) and (a( 48))) xor
        (b( 77) and (a( 47))) xor
        (b( 78) and (a( 46))) xor
        (b( 79) and (a( 45))) xor
        (b( 80) and (a( 44))) xor
        (b( 81) and (a( 43))) xor
        (b( 82) and (a( 42))) xor
        (b( 83) and (a( 41))) xor
        (b( 84) and (a( 40))) xor
        (b( 85) and (a( 39))) xor
        (b( 86) and (a( 38))) xor
        (b( 87) and (a( 37))) xor
        (b( 88) and (a( 36))) xor
        (b( 89) and (a( 35))) xor
        (b( 90) and (a( 34))) xor
        (b( 91) and (a( 33))) xor
        (b( 92) and (a( 32))) xor
        (b( 93) and (a( 31))) xor
        (b( 94) and (a( 30))) xor
        (b( 95) and (a( 29))) xor
        (b( 96) and (a( 28))) xor
        (b( 97) and (a( 27))) xor
        (b( 98) and (a( 26))) xor
        (b( 99) and (a( 25))) xor
        (b(100) and (a( 24))) xor
        (b(101) and (a( 23))) xor
        (b(102) and (a( 22))) xor
        (b(103) and (a( 21))) xor
        (b(104) and (a( 20))) xor
        (b(105) and (a( 19))) xor
        (b(106) and (a( 18))) xor
        (b(107) and (a( 17))) xor
        (b(108) and (a( 16))) xor
        (b(109) and (a( 15))) xor
        (b(110) and (a( 14))) xor
        (b(111) and (a( 13))) xor
        (b(112) and (a( 12))) xor
        (b(113) and (a( 11))) xor
        (b(114) and (a( 10))) xor
        (b(115) and (a(  9))) xor
        (b(116) and (a(  8))) xor
        (b(117) and (a(  7))) xor
        (b(118) and (a(  6) xor a(127))) xor
        (b(119) and (a(  5) xor a(126))) xor
        (b(120) and (a(  4) xor a(125))) xor
        (b(121) and (a(  3) xor a(124))) xor
        (b(122) and (a(  2) xor a(123))) xor
        (b(123) and (a(  1) xor a(122) xor a(127))) xor
        (b(124) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(125) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(126) and (a(119) xor a(124) xor a(125) xor a(126))) xor
        (b(127) and (a(118) xor a(123) xor a(124) xor a(125)));
    c(125) <= 
        (b(  0) and (a(125))) xor
        (b(  1) and (a(124))) xor
        (b(  2) and (a(123))) xor
        (b(  3) and (a(122))) xor
        (b(  4) and (a(121))) xor
        (b(  5) and (a(120))) xor
        (b(  6) and (a(119))) xor
        (b(  7) and (a(118))) xor
        (b(  8) and (a(117))) xor
        (b(  9) and (a(116))) xor
        (b( 10) and (a(115))) xor
        (b( 11) and (a(114))) xor
        (b( 12) and (a(113))) xor
        (b( 13) and (a(112))) xor
        (b( 14) and (a(111))) xor
        (b( 15) and (a(110))) xor
        (b( 16) and (a(109))) xor
        (b( 17) and (a(108))) xor
        (b( 18) and (a(107))) xor
        (b( 19) and (a(106))) xor
        (b( 20) and (a(105))) xor
        (b( 21) and (a(104))) xor
        (b( 22) and (a(103))) xor
        (b( 23) and (a(102))) xor
        (b( 24) and (a(101))) xor
        (b( 25) and (a(100))) xor
        (b( 26) and (a( 99))) xor
        (b( 27) and (a( 98))) xor
        (b( 28) and (a( 97))) xor
        (b( 29) and (a( 96))) xor
        (b( 30) and (a( 95))) xor
        (b( 31) and (a( 94))) xor
        (b( 32) and (a( 93))) xor
        (b( 33) and (a( 92))) xor
        (b( 34) and (a( 91))) xor
        (b( 35) and (a( 90))) xor
        (b( 36) and (a( 89))) xor
        (b( 37) and (a( 88))) xor
        (b( 38) and (a( 87))) xor
        (b( 39) and (a( 86))) xor
        (b( 40) and (a( 85))) xor
        (b( 41) and (a( 84))) xor
        (b( 42) and (a( 83))) xor
        (b( 43) and (a( 82))) xor
        (b( 44) and (a( 81))) xor
        (b( 45) and (a( 80))) xor
        (b( 46) and (a( 79))) xor
        (b( 47) and (a( 78))) xor
        (b( 48) and (a( 77))) xor
        (b( 49) and (a( 76))) xor
        (b( 50) and (a( 75))) xor
        (b( 51) and (a( 74))) xor
        (b( 52) and (a( 73))) xor
        (b( 53) and (a( 72))) xor
        (b( 54) and (a( 71))) xor
        (b( 55) and (a( 70))) xor
        (b( 56) and (a( 69))) xor
        (b( 57) and (a( 68))) xor
        (b( 58) and (a( 67))) xor
        (b( 59) and (a( 66))) xor
        (b( 60) and (a( 65))) xor
        (b( 61) and (a( 64))) xor
        (b( 62) and (a( 63))) xor
        (b( 63) and (a( 62))) xor
        (b( 64) and (a( 61))) xor
        (b( 65) and (a( 60))) xor
        (b( 66) and (a( 59))) xor
        (b( 67) and (a( 58))) xor
        (b( 68) and (a( 57))) xor
        (b( 69) and (a( 56))) xor
        (b( 70) and (a( 55))) xor
        (b( 71) and (a( 54))) xor
        (b( 72) and (a( 53))) xor
        (b( 73) and (a( 52))) xor
        (b( 74) and (a( 51))) xor
        (b( 75) and (a( 50))) xor
        (b( 76) and (a( 49))) xor
        (b( 77) and (a( 48))) xor
        (b( 78) and (a( 47))) xor
        (b( 79) and (a( 46))) xor
        (b( 80) and (a( 45))) xor
        (b( 81) and (a( 44))) xor
        (b( 82) and (a( 43))) xor
        (b( 83) and (a( 42))) xor
        (b( 84) and (a( 41))) xor
        (b( 85) and (a( 40))) xor
        (b( 86) and (a( 39))) xor
        (b( 87) and (a( 38))) xor
        (b( 88) and (a( 37))) xor
        (b( 89) and (a( 36))) xor
        (b( 90) and (a( 35))) xor
        (b( 91) and (a( 34))) xor
        (b( 92) and (a( 33))) xor
        (b( 93) and (a( 32))) xor
        (b( 94) and (a( 31))) xor
        (b( 95) and (a( 30))) xor
        (b( 96) and (a( 29))) xor
        (b( 97) and (a( 28))) xor
        (b( 98) and (a( 27))) xor
        (b( 99) and (a( 26))) xor
        (b(100) and (a( 25))) xor
        (b(101) and (a( 24))) xor
        (b(102) and (a( 23))) xor
        (b(103) and (a( 22))) xor
        (b(104) and (a( 21))) xor
        (b(105) and (a( 20))) xor
        (b(106) and (a( 19))) xor
        (b(107) and (a( 18))) xor
        (b(108) and (a( 17))) xor
        (b(109) and (a( 16))) xor
        (b(110) and (a( 15))) xor
        (b(111) and (a( 14))) xor
        (b(112) and (a( 13))) xor
        (b(113) and (a( 12))) xor
        (b(114) and (a( 11))) xor
        (b(115) and (a( 10))) xor
        (b(116) and (a(  9))) xor
        (b(117) and (a(  8))) xor
        (b(118) and (a(  7))) xor
        (b(119) and (a(  6) xor a(127))) xor
        (b(120) and (a(  5) xor a(126))) xor
        (b(121) and (a(  4) xor a(125))) xor
        (b(122) and (a(  3) xor a(124))) xor
        (b(123) and (a(  2) xor a(123))) xor
        (b(124) and (a(  1) xor a(122) xor a(127))) xor
        (b(125) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(126) and (a(120) xor a(125) xor a(126) xor a(127))) xor
        (b(127) and (a(119) xor a(124) xor a(125) xor a(126)));
    c(126) <= 
        (b(  0) and (a(126))) xor
        (b(  1) and (a(125))) xor
        (b(  2) and (a(124))) xor
        (b(  3) and (a(123))) xor
        (b(  4) and (a(122))) xor
        (b(  5) and (a(121))) xor
        (b(  6) and (a(120))) xor
        (b(  7) and (a(119))) xor
        (b(  8) and (a(118))) xor
        (b(  9) and (a(117))) xor
        (b( 10) and (a(116))) xor
        (b( 11) and (a(115))) xor
        (b( 12) and (a(114))) xor
        (b( 13) and (a(113))) xor
        (b( 14) and (a(112))) xor
        (b( 15) and (a(111))) xor
        (b( 16) and (a(110))) xor
        (b( 17) and (a(109))) xor
        (b( 18) and (a(108))) xor
        (b( 19) and (a(107))) xor
        (b( 20) and (a(106))) xor
        (b( 21) and (a(105))) xor
        (b( 22) and (a(104))) xor
        (b( 23) and (a(103))) xor
        (b( 24) and (a(102))) xor
        (b( 25) and (a(101))) xor
        (b( 26) and (a(100))) xor
        (b( 27) and (a( 99))) xor
        (b( 28) and (a( 98))) xor
        (b( 29) and (a( 97))) xor
        (b( 30) and (a( 96))) xor
        (b( 31) and (a( 95))) xor
        (b( 32) and (a( 94))) xor
        (b( 33) and (a( 93))) xor
        (b( 34) and (a( 92))) xor
        (b( 35) and (a( 91))) xor
        (b( 36) and (a( 90))) xor
        (b( 37) and (a( 89))) xor
        (b( 38) and (a( 88))) xor
        (b( 39) and (a( 87))) xor
        (b( 40) and (a( 86))) xor
        (b( 41) and (a( 85))) xor
        (b( 42) and (a( 84))) xor
        (b( 43) and (a( 83))) xor
        (b( 44) and (a( 82))) xor
        (b( 45) and (a( 81))) xor
        (b( 46) and (a( 80))) xor
        (b( 47) and (a( 79))) xor
        (b( 48) and (a( 78))) xor
        (b( 49) and (a( 77))) xor
        (b( 50) and (a( 76))) xor
        (b( 51) and (a( 75))) xor
        (b( 52) and (a( 74))) xor
        (b( 53) and (a( 73))) xor
        (b( 54) and (a( 72))) xor
        (b( 55) and (a( 71))) xor
        (b( 56) and (a( 70))) xor
        (b( 57) and (a( 69))) xor
        (b( 58) and (a( 68))) xor
        (b( 59) and (a( 67))) xor
        (b( 60) and (a( 66))) xor
        (b( 61) and (a( 65))) xor
        (b( 62) and (a( 64))) xor
        (b( 63) and (a( 63))) xor
        (b( 64) and (a( 62))) xor
        (b( 65) and (a( 61))) xor
        (b( 66) and (a( 60))) xor
        (b( 67) and (a( 59))) xor
        (b( 68) and (a( 58))) xor
        (b( 69) and (a( 57))) xor
        (b( 70) and (a( 56))) xor
        (b( 71) and (a( 55))) xor
        (b( 72) and (a( 54))) xor
        (b( 73) and (a( 53))) xor
        (b( 74) and (a( 52))) xor
        (b( 75) and (a( 51))) xor
        (b( 76) and (a( 50))) xor
        (b( 77) and (a( 49))) xor
        (b( 78) and (a( 48))) xor
        (b( 79) and (a( 47))) xor
        (b( 80) and (a( 46))) xor
        (b( 81) and (a( 45))) xor
        (b( 82) and (a( 44))) xor
        (b( 83) and (a( 43))) xor
        (b( 84) and (a( 42))) xor
        (b( 85) and (a( 41))) xor
        (b( 86) and (a( 40))) xor
        (b( 87) and (a( 39))) xor
        (b( 88) and (a( 38))) xor
        (b( 89) and (a( 37))) xor
        (b( 90) and (a( 36))) xor
        (b( 91) and (a( 35))) xor
        (b( 92) and (a( 34))) xor
        (b( 93) and (a( 33))) xor
        (b( 94) and (a( 32))) xor
        (b( 95) and (a( 31))) xor
        (b( 96) and (a( 30))) xor
        (b( 97) and (a( 29))) xor
        (b( 98) and (a( 28))) xor
        (b( 99) and (a( 27))) xor
        (b(100) and (a( 26))) xor
        (b(101) and (a( 25))) xor
        (b(102) and (a( 24))) xor
        (b(103) and (a( 23))) xor
        (b(104) and (a( 22))) xor
        (b(105) and (a( 21))) xor
        (b(106) and (a( 20))) xor
        (b(107) and (a( 19))) xor
        (b(108) and (a( 18))) xor
        (b(109) and (a( 17))) xor
        (b(110) and (a( 16))) xor
        (b(111) and (a( 15))) xor
        (b(112) and (a( 14))) xor
        (b(113) and (a( 13))) xor
        (b(114) and (a( 12))) xor
        (b(115) and (a( 11))) xor
        (b(116) and (a( 10))) xor
        (b(117) and (a(  9))) xor
        (b(118) and (a(  8))) xor
        (b(119) and (a(  7))) xor
        (b(120) and (a(  6) xor a(127))) xor
        (b(121) and (a(  5) xor a(126))) xor
        (b(122) and (a(  4) xor a(125))) xor
        (b(123) and (a(  3) xor a(124))) xor
        (b(124) and (a(  2) xor a(123))) xor
        (b(125) and (a(  1) xor a(122) xor a(127))) xor
        (b(126) and (a(  0) xor a(121) xor a(126) xor a(127))) xor
        (b(127) and (a(120) xor a(125) xor a(126) xor a(127)));
    c(127) <= 
        (b(  0) and (a(127))) xor
        (b(  1) and (a(126))) xor
        (b(  2) and (a(125))) xor
        (b(  3) and (a(124))) xor
        (b(  4) and (a(123))) xor
        (b(  5) and (a(122))) xor
        (b(  6) and (a(121))) xor
        (b(  7) and (a(120))) xor
        (b(  8) and (a(119))) xor
        (b(  9) and (a(118))) xor
        (b( 10) and (a(117))) xor
        (b( 11) and (a(116))) xor
        (b( 12) and (a(115))) xor
        (b( 13) and (a(114))) xor
        (b( 14) and (a(113))) xor
        (b( 15) and (a(112))) xor
        (b( 16) and (a(111))) xor
        (b( 17) and (a(110))) xor
        (b( 18) and (a(109))) xor
        (b( 19) and (a(108))) xor
        (b( 20) and (a(107))) xor
        (b( 21) and (a(106))) xor
        (b( 22) and (a(105))) xor
        (b( 23) and (a(104))) xor
        (b( 24) and (a(103))) xor
        (b( 25) and (a(102))) xor
        (b( 26) and (a(101))) xor
        (b( 27) and (a(100))) xor
        (b( 28) and (a( 99))) xor
        (b( 29) and (a( 98))) xor
        (b( 30) and (a( 97))) xor
        (b( 31) and (a( 96))) xor
        (b( 32) and (a( 95))) xor
        (b( 33) and (a( 94))) xor
        (b( 34) and (a( 93))) xor
        (b( 35) and (a( 92))) xor
        (b( 36) and (a( 91))) xor
        (b( 37) and (a( 90))) xor
        (b( 38) and (a( 89))) xor
        (b( 39) and (a( 88))) xor
        (b( 40) and (a( 87))) xor
        (b( 41) and (a( 86))) xor
        (b( 42) and (a( 85))) xor
        (b( 43) and (a( 84))) xor
        (b( 44) and (a( 83))) xor
        (b( 45) and (a( 82))) xor
        (b( 46) and (a( 81))) xor
        (b( 47) and (a( 80))) xor
        (b( 48) and (a( 79))) xor
        (b( 49) and (a( 78))) xor
        (b( 50) and (a( 77))) xor
        (b( 51) and (a( 76))) xor
        (b( 52) and (a( 75))) xor
        (b( 53) and (a( 74))) xor
        (b( 54) and (a( 73))) xor
        (b( 55) and (a( 72))) xor
        (b( 56) and (a( 71))) xor
        (b( 57) and (a( 70))) xor
        (b( 58) and (a( 69))) xor
        (b( 59) and (a( 68))) xor
        (b( 60) and (a( 67))) xor
        (b( 61) and (a( 66))) xor
        (b( 62) and (a( 65))) xor
        (b( 63) and (a( 64))) xor
        (b( 64) and (a( 63))) xor
        (b( 65) and (a( 62))) xor
        (b( 66) and (a( 61))) xor
        (b( 67) and (a( 60))) xor
        (b( 68) and (a( 59))) xor
        (b( 69) and (a( 58))) xor
        (b( 70) and (a( 57))) xor
        (b( 71) and (a( 56))) xor
        (b( 72) and (a( 55))) xor
        (b( 73) and (a( 54))) xor
        (b( 74) and (a( 53))) xor
        (b( 75) and (a( 52))) xor
        (b( 76) and (a( 51))) xor
        (b( 77) and (a( 50))) xor
        (b( 78) and (a( 49))) xor
        (b( 79) and (a( 48))) xor
        (b( 80) and (a( 47))) xor
        (b( 81) and (a( 46))) xor
        (b( 82) and (a( 45))) xor
        (b( 83) and (a( 44))) xor
        (b( 84) and (a( 43))) xor
        (b( 85) and (a( 42))) xor
        (b( 86) and (a( 41))) xor
        (b( 87) and (a( 40))) xor
        (b( 88) and (a( 39))) xor
        (b( 89) and (a( 38))) xor
        (b( 90) and (a( 37))) xor
        (b( 91) and (a( 36))) xor
        (b( 92) and (a( 35))) xor
        (b( 93) and (a( 34))) xor
        (b( 94) and (a( 33))) xor
        (b( 95) and (a( 32))) xor
        (b( 96) and (a( 31))) xor
        (b( 97) and (a( 30))) xor
        (b( 98) and (a( 29))) xor
        (b( 99) and (a( 28))) xor
        (b(100) and (a( 27))) xor
        (b(101) and (a( 26))) xor
        (b(102) and (a( 25))) xor
        (b(103) and (a( 24))) xor
        (b(104) and (a( 23))) xor
        (b(105) and (a( 22))) xor
        (b(106) and (a( 21))) xor
        (b(107) and (a( 20))) xor
        (b(108) and (a( 19))) xor
        (b(109) and (a( 18))) xor
        (b(110) and (a( 17))) xor
        (b(111) and (a( 16))) xor
        (b(112) and (a( 15))) xor
        (b(113) and (a( 14))) xor
        (b(114) and (a( 13))) xor
        (b(115) and (a( 12))) xor
        (b(116) and (a( 11))) xor
        (b(117) and (a( 10))) xor
        (b(118) and (a(  9))) xor
        (b(119) and (a(  8))) xor
        (b(120) and (a(  7))) xor
        (b(121) and (a(  6) xor a(127))) xor
        (b(122) and (a(  5) xor a(126))) xor
        (b(123) and (a(  4) xor a(125))) xor
        (b(124) and (a(  3) xor a(124))) xor
        (b(125) and (a(  2) xor a(123))) xor
        (b(126) and (a(  1) xor a(122) xor a(127))) xor
        (b(127) and (a(  0) xor a(121) xor a(126) xor a(127)));
end architecture;