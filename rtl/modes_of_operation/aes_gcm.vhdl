-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

library LIB_AXIS;

entity aes_gcm is
    generic (
        IV_NBITS_G              : integer := 96;
        KEY_NBITS_G             : integer := 256;
        DECRYPT_G               : boolean := false;
        HIGH_THROUGHPUT_G       : boolean := false
    );
    port (
        clk                     : in    std_logic;
        rst                     : in    std_logic;

        fail_flag               : out   std_logic;

        -- Key AXIS interface
        s_key_axis_tvalid       : in    std_logic;
        s_key_axis_tready       : out   std_logic;
        s_key_axis_tdata        : in    std_logic_vector(KEY_NBITS_G-1 downto 0);

        -- Current key in use AXIS interface
        m_curr_key_axis_tdata   : out   std_logic_vector(KEY_NBITS_G-1 downto 0);
        m_curr_key_axis_tvalid  : out   std_logic;

        -- IV AXIS interface
        s_iv_axis_tvalid        : in    std_logic;
        s_iv_axis_tready        : out   std_logic;
        s_iv_axis_tdata         : in    std_logic_vector(IV_NBITS_G-1 downto 0);

        -- Auth tag AXIS interface, used in decrypt mode but not encrypt
        s_auth_axis_tvalid      : in    std_logic;
        s_auth_axis_tready      : out   std_logic;
        s_auth_axis_tdata       : in    std_logic_vector(127 downto 0);

        -- Additional Authenticated Data AXIS input interface, pass-through to
        -- m_aad_axis
        s_aad_axis_tvalid       : in    std_logic;
        s_aad_axis_tready       : out   std_logic;
        s_aad_axis_tlast        : in    std_logic;
        s_aad_axis_tkeep        : in    std_logic_vector(15 downto 0);
        s_aad_axis_tdata        : in    std_logic_vector(127 downto 0);

        -- Data input AXIS interface, plaintext in encrypt mode, ciphertext in
        -- decrypt mode
        s_axis_tvalid           : in    std_logic;
        s_axis_tready           : out   std_logic;
        s_axis_tlast            : in    std_logic;
        s_axis_tkeep            : in    std_logic_vector(15 downto 0);
        s_axis_tdata            : in    std_logic_vector(127 downto 0);

        -- Auth tag AXIS interface,
        --      outputs auth tag in encrypt mode
        --      outputs 0 if authentication check passes otherwise all 1s if it
        --          fails in decrypt mode
        m_auth_axis_tvalid      : out   std_logic;
        m_auth_axis_tready      : in    std_logic;
        m_auth_axis_tdata       : out   std_logic_vector(127 downto 0);

        -- Additional Authenticated Data AXIS output interface, pass-through
        -- from s_aad_axis
        m_aad_axis_tvalid       : out   std_logic;
        m_aad_axis_tready       : in    std_logic;
        m_aad_axis_tlast        : out   std_logic;
        m_aad_axis_tkeep        : out   std_logic_vector(15 downto 0);
        m_aad_axis_tdata        : out   std_logic_vector(127 downto 0);

        -- Data output AXIS interface, ciphertext in encrypt mode, plaintext in
        -- decrypt mode
        m_axis_tvalid           : out   std_logic;
        m_axis_tready           : in    std_logic;
        m_axis_tlast            : out   std_logic;
        m_axis_tkeep            : out   std_logic_vector(15 downto 0);
        m_axis_tdata            : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of aes_gcm is
    -- RESET_S              - waiting for new key
    -- NEW_KEY_S            - Compute H (block cipher of (others => '0'))
    -- WAIT_FOR_IV_S        - Waiting for new IV
    -- COMPUTE_E_IV_0_S     - Computes block cipher with counter = 0
    -- AAD_PROCESSING_S     - Processes AAD
    -- DATA_PROCESSING_S    - Processes Data (either plaintext or ciphertext)
    -- FINISH_S             - Outputs auth tag
    type state_t is (RESET_S, NEW_KEY_S, WAIT_FOR_IV_S, COMPUTE_E_IV_0_S, IDLE_S,
                     AAD_PROCESSING_S, DATA_PROCESSING_S, FINISH_S);
    type auth_state_t is (AUTH_PROCESSING_S, AUTH_LENGTH_MULTI_S,
                          AUTH_XOR_WITH_EK0_S, AUTH_VALIDATE_S, AUTH_FINISH_S);

    constant COUNTER_NEW_KEY_VALUE_C    : unsigned := to_unsigned(0, 128-IV_NBITS_G);
    constant COUNTER_NEW_IV_VALUE_C     : unsigned := to_unsigned(1,
                                                            128-IV_NBITS_G);

    signal in_state                     : state_t := RESET_S;
    signal out_state                    : state_t := RESET_S;
    signal auth_state                   : auth_state_t := AUTH_PROCESSING_S;

    signal s_key_axis_tready_sig        : std_logic;
    signal s_key_axis_en                : std_logic;
    signal s_iv_axis_tready_sig         : std_logic;
    signal s_iv_axis_en                 : std_logic;

    signal s_aad_axis_en                : std_logic;
    signal s_axis_en                    : std_logic;
    signal s_axis_tready_sig            : std_logic;
    signal s_aad_axis_tready_sig        : std_logic;
    signal m_auth_axis_en               : std_logic;

    signal iv_reg_valid                 : std_logic;
    signal iv_reg                       : std_logic_vector(IV_NBITS_G-1 downto 0);
    signal cntr_rst                     : std_logic;
    signal cntr_unsigned                : unsigned(128-IV_NBITS_G-1 downto 0);
    signal cntr                         : std_logic_vector(128-IV_NBITS_G-1 downto 0);

    signal key_reg_axis_tvalid          : std_logic;
    signal key_reg_axis_tready          : std_logic;
    signal key_reg_axis_tdata           : std_logic_vector(KEY_NBITS_G-1 downto 0);
    signal iv_reg_axis_tvalid           : std_logic;
    signal iv_reg_axis_tready           : std_logic;
    signal iv_reg_axis_en               : std_logic;
    signal iv_reg_axis_tdata            : std_logic_vector(127 downto 0);
    signal aes_output_axis_tvalid       : std_logic;
    signal aes_output_axis_tready       : std_logic;
    signal aes_output_axis_tdata        : std_logic_vector(127 downto 0);
    signal aes_output_axis_en           : std_logic;

    signal flush_aes                    : std_logic;

    signal encrypt_all_zeros_valid      : std_logic;
    signal encrypt_all_zeros_reg        : std_logic_vector(127 downto 0);
    signal encrypt_cntr_eq_0_valid      : std_logic;
    signal encrypt_cntr_eq_0_reg        : std_logic_vector(127 downto 0);

    signal ciphertext_tready            : std_logic;
    signal ciphertext_tvalid            : std_logic;
    signal ciphertext_tlast             : std_logic;
    signal ciphertext_tkeep             : std_logic_vector(15 downto 0);
    signal ciphertext_tdata             : std_logic_vector(127 downto 0);

    signal plaintext_tdata              : std_logic_vector(127 downto 0);

    signal ciphertext_reg_tvalid        : std_logic;
    signal ciphertext_reg_tready        : std_logic;
    signal ciphertext_reg_tlast         : std_logic;
    signal ciphertext_reg_tkeep         : std_logic_vector(15 downto 0);
    signal ciphertext_reg_tdata         : std_logic_vector(127 downto 0);

    signal aad_tready                   : std_logic;
    signal aad_tvalid                   : std_logic;
    signal aad_tlast                    : std_logic;
    signal aad_tkeep                    : std_logic_vector(15 downto 0);
    signal aad_tdata                    : std_logic_vector(127 downto 0);

    signal aad_reg_tvalid               : std_logic;
    signal aad_reg_tready               : std_logic;
    signal aad_reg_tlast                : std_logic;
    signal aad_reg_tkeep                : std_logic_vector(15 downto 0);
    signal aad_reg_tdata                : std_logic_vector(127 downto 0);

    signal tag                          : std_logic_vector(127 downto 0);
    signal new_tag_before_xor           : std_logic_vector(127 downto 0);
    signal tag_operand                  : std_logic_vector(127 downto 0);

    signal aad_count                    : unsigned(63 downto 0);
    signal plaintext_count              : unsigned(63 downto 0);
    signal input_lengths                : std_logic_vector(127 downto 0);

    signal multiplier_a                 : std_logic_vector(127 downto 0);
    signal multiplier_b                 : std_logic_vector(127 downto 0);
    signal multiplier_c                 : std_logic_vector(127 downto 0);

    signal auth_tag_tvalid              : std_logic;

    signal fail_flag_sig                : std_logic;
    signal s_auth_axis_tready_sig       : std_logic;
    signal s_auth_axis_en               : std_logic;

    function toggle_endianess(
            tdata                       : std_logic_vector(127 downto 0))
        return std_logic_vector is
            variable tmp                : std_logic_vector(127 downto 0);
    begin
        tmp := (others => '0');

        for i in 0 to 15 loop
            tmp(127-8*i downto 120-8*i) := tdata(7+8*i downto 8*i);
        end loop;

        return tmp;
    end function;

    function tkeep_mask(
            tdata                       : std_logic_vector(127 downto 0);
            tkeep                       : std_logic_vector(15 downto 0))
        return std_logic_vector is
            variable tmp                : std_logic_vector(127 downto 0);
    begin
        tmp := (others => '0');

        case tkeep is
            when X"0001" => tmp(127 downto 120) := tdata(127 downto 120);
            when X"0003" => tmp(127 downto 112) := tdata(127 downto 112);
            when X"0007" => tmp(127 downto 104) := tdata(127 downto 104);
            when X"000F" => tmp(127 downto  96) := tdata(127 downto  96);
            when X"001F" => tmp(127 downto  88) := tdata(127 downto  88);
            when X"003F" => tmp(127 downto  80) := tdata(127 downto  80);
            when X"007F" => tmp(127 downto  72) := tdata(127 downto  72);
            when X"00FF" => tmp(127 downto  64) := tdata(127 downto  64);
            when X"01FF" => tmp(127 downto  56) := tdata(127 downto  56);
            when X"03FF" => tmp(127 downto  48) := tdata(127 downto  48);
            when X"07FF" => tmp(127 downto  40) := tdata(127 downto  40);
            when X"0FFF" => tmp(127 downto  32) := tdata(127 downto  32);
            when X"1FFF" => tmp(127 downto  24) := tdata(127 downto  24);
            when X"3FFF" => tmp(127 downto  16) := tdata(127 downto  16);
            when X"7FFF" => tmp(127 downto   8) := tdata(127 downto   8);
            when others =>  tmp := tdata;
        end case;

        return tmp;
    end function;

    function count_ones(in_data : std_logic_vector)
        return integer is
            variable tmp : integer;
    begin
        tmp := 0;

        for i in in_data'low to in_data'high loop
            if (in_data(i) = '1') then
                tmp := tmp + 1;
            end if;
        end loop;

        return tmp;
    end function;

    function reflect(in_data : std_logic_vector)
        return std_logic_vector is
            variable tmp    : std_logic_vector(in_data'length-1 downto 0);
    begin
        for i in 0 to in_data'length-1 loop
            tmp(in_data'length-i-1) := in_data(in_data'low + i);
        end loop;

        return tmp;
    end function;

    signal m_aad_axis_en                : std_logic;
    signal m_axis_en                    : std_logic;

begin
    m_aad_axis_en <= aad_reg_tready and aad_reg_tvalid;
    m_axis_en <= ciphertext_reg_tvalid and ciphertext_reg_tready;


    s_key_axis_tready       <= s_key_axis_tready_sig;
    s_key_axis_tready_sig   <= '1' when (in_state = RESET_S or in_state = WAIT_FOR_IV_S or in_state = IDLE_S) and (out_state = RESET_S or out_state = WAIT_FOR_IV_S or out_state = IDLE_S) and rst /= '1' else '0';
    s_key_axis_en           <= s_key_axis_tvalid and s_key_axis_tready_sig;
    s_iv_axis_tready        <= s_iv_axis_tready_sig;
    s_iv_axis_tready_sig    <= '1' when (in_state = WAIT_FOR_IV_S or in_state = IDLE_S) and (out_state = WAIT_FOR_IV_S or out_state = IDLE_S) else '0';
    s_iv_axis_en            <= s_iv_axis_tvalid and s_iv_axis_tready_sig;

--------------------------------------------------------------------------------
---------------------------- Input Registers -----------------------------------
--------------------------------------------------------------------------------
    key_iv_reg_proc : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                iv_reg_valid <= '0';
                iv_reg <= (others => '0');
                key_reg_axis_tvalid <= '0';
                key_reg_axis_tdata <= (others => 'X');
            else

                if (key_reg_axis_tready = '1') then
                    key_reg_axis_tvalid <= '0';
                end if;

                if (s_iv_axis_en = '1') then
                    iv_reg <= s_iv_axis_tdata;
                    iv_reg_valid <= '1';
                end if;

                if (s_key_axis_en = '1') then
                    key_reg_axis_tdata  <= s_key_axis_tdata;
                    key_reg_axis_tvalid <= '1';
                end if;
            end if;
        end if;
    end process;

--------------------------------------------------------------------------------
------------------------------ GCM Counter -------------------------------------
--------------------------------------------------------------------------------
    cntr_proc : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                cntr_unsigned <= COUNTER_NEW_KEY_VALUE_C;
            else
                if (iv_reg_axis_en = '1') then
                    cntr_unsigned <= cntr_unsigned + 1;
                end if;
                if (cntr_rst = '1') then
                    cntr_unsigned <= to_unsigned(2, cntr_unsigned'length);
                end if;
            end if;
        end if;
    end process;

    cntr <= std_logic_vector(cntr_unsigned);


--------------------------------------------------------------------------------
------------------------------- AES Input --------------------------------------
--------------------------------------------------------------------------------
    iv_reg_axis_proc : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                in_state <= RESET_S;
                cntr_rst <= '1';
            else
                cntr_rst <= '1';
                flush_aes <= '0';

                if (iv_reg_axis_tready = '1') then
                    iv_reg_axis_tvalid <= '0';
                end if;

                case (in_state) is
                when RESET_S =>
                    iv_reg_axis_tdata <= (others => '0');
                    iv_reg_axis_tvalid <= '0';

                    if (s_key_axis_en = '1') then
                        iv_reg_axis_tvalid <= '1';
                        iv_reg_axis_tdata <= (others => '0');
                        in_state <= NEW_KEY_S;
                    end if;

                when NEW_KEY_S =>
                    iv_reg_axis_tdata <= (others => '0');

                    if (iv_reg_axis_en = '1') then
                        in_state <= WAIT_FOR_IV_S;

                        if (iv_reg_valid = '1') then
                            iv_reg_axis_tdata <= iv_reg & std_logic_vector(COUNTER_NEW_IV_VALUE_C);
                            iv_reg_axis_tvalid <= '1';
                            in_state <= COMPUTE_E_IV_0_S;
                        end if;
                    end if;

                when WAIT_FOR_IV_S =>
                    if (iv_reg_valid = '1') then
                        iv_reg_axis_tdata <= iv_reg & std_logic_vector(COUNTER_NEW_IV_VALUE_C);
                        iv_reg_axis_tvalid <= '1';
                        in_state <= COMPUTE_E_IV_0_S;
                    end if;

                    if (s_key_axis_en = '1') then
                        flush_aes <= '1';
                        iv_reg_axis_tvalid <= '1';
                        iv_reg_axis_tdata <= (others => '0');
                        in_state <= NEW_KEY_S;
                    end if;

                when COMPUTE_E_IV_0_S =>
                    iv_reg_axis_tdata <= iv_reg & std_logic_vector(COUNTER_NEW_IV_VALUE_C);
                    if (iv_reg_axis_en = '1') then
                        in_state <= IDLE_S;
                    end if;

                when IDLE_S =>
                    cntr_rst <= '0';
                    iv_reg_axis_tvalid <= '1';
                    iv_reg_axis_tdata <= iv_reg & cntr;
                    if (s_axis_en = '1' or s_aad_axis_en = '1') then
                        in_state <= DATA_PROCESSING_S;
                    end if;

                    if (s_iv_axis_en = '1') then
                        flush_aes <= '1';
                        iv_reg_axis_tvalid <= '1';
                        iv_reg_axis_tdata <= iv_reg & std_logic_vector(COUNTER_NEW_IV_VALUE_C);
                        in_state <= COMPUTE_E_IV_0_S;
                    end if;

                    if (s_key_axis_en = '1') then
                        flush_aes <= '1';
                        iv_reg_axis_tvalid <= '1';
                        iv_reg_axis_tdata <= (others => '0');
                        in_state <= NEW_KEY_S;
                    end if;

                when DATA_PROCESSING_S =>
                    cntr_rst <= '0';
                    iv_reg_axis_tdata <= iv_reg & cntr;
                    iv_reg_axis_tvalid <= '1';
                    if (s_axis_tlast = '1' and s_axis_en = '1') then
                        in_state <= IDLE_S;
                    end if;

                when others =>
                    in_state <= RESET_S;
                end case;
            end if;
        end if;
    end process;

    iv_reg_axis_en <= iv_reg_axis_tready and iv_reg_axis_tvalid;

--------------------------------------------------------------------------------
---------------------------------- AES -----------------------------------------
--------------------------------------------------------------------------------

    aes_encrypt_block_e : entity LIB_AES.aes_encrypt_block
        generic map (
            KEY_NBITS_G             => KEY_NBITS_G,
            HIGH_THROUGHPUT_G       => HIGH_THROUGHPUT_G
        )   
        port map (  
            clk                     => clk,
            rst                     => rst,

            flush_pipeline          => flush_aes,

            s_key_axis_tvalid       => key_reg_axis_tvalid,
            s_key_axis_tready       => key_reg_axis_tready,
            s_key_axis_tdata        => key_reg_axis_tdata,

            m_curr_key_axis_tdata   => m_curr_key_axis_tdata,
            m_curr_key_axis_tvalid  => m_curr_key_axis_tvalid,

            s_axis_tvalid           => iv_reg_axis_tvalid,
            s_axis_tready           => iv_reg_axis_tready,
            s_axis_tdata            => iv_reg_axis_tdata,

            m_axis_tvalid           => aes_output_axis_tvalid,
            m_axis_tready           => aes_output_axis_tready,
            m_axis_tdata            => aes_output_axis_tdata
        );

    aes_output_axis_tready <=   s_axis_tvalid and ciphertext_tready when out_state = DATA_PROCESSING_S else
                                '1' when out_state = NEW_KEY_S else
                                '1' when out_state = COMPUTE_E_IV_0_S else
                                '0';

    aes_output_axis_en <= aes_output_axis_tready and aes_output_axis_tvalid;

--------------------------------------------------------------------------------
------------------------ AES Output State Machine ------------------------------
--------------------------------------------------------------------------------
    output_state_proc : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                out_state <= RESET_S;
                encrypt_cntr_eq_0_valid <= '0';
                encrypt_all_zeros_valid <= '0';
            else
                case (out_state) is
                when RESET_S =>
                    if (key_reg_axis_tvalid = '1') then
                        out_state <= NEW_KEY_S;
                    end if;

                when NEW_KEY_S =>
                    if (aes_output_axis_en = '1') then
                        encrypt_all_zeros_valid <= '1';
                        encrypt_all_zeros_reg <= aes_output_axis_tdata;
                        out_state <= WAIT_FOR_IV_S;

                        if (iv_reg_axis_tvalid = '1') then
                            out_state <= COMPUTE_E_IV_0_S;
                        end if;
                    end if;

                when WAIT_FOR_IV_S =>
                    if (iv_reg_axis_tvalid = '1') then
                        out_state <= COMPUTE_E_IV_0_S;
                    end if;
                    if (s_key_axis_en = '1') then
                        out_state <= NEW_KEY_S;
                    end if;

                when COMPUTE_E_IV_0_S =>
                    if (aes_output_axis_en = '1') then
                        encrypt_cntr_eq_0_valid <= '1';
                        encrypt_cntr_eq_0_reg <= aes_output_axis_tdata;
                        out_state <= IDLE_S;
                    end if;

                when IDLE_S =>
                    if (s_axis_tvalid = '1') then
                        out_state <= DATA_PROCESSING_S;
                    end if;

                    if (s_aad_axis_tvalid = '1') then
                        out_state <= AAD_PROCESSING_S;
                    end if;

                    if (s_iv_axis_en = '1') then
                        encrypt_cntr_eq_0_valid <= '0';
                        out_state <= COMPUTE_E_IV_0_S;
                    end if;

                    if (s_key_axis_en = '1') then
                        encrypt_all_zeros_valid <= '0';
                        out_state <= NEW_KEY_S;
                    end if;

                when AAD_PROCESSING_S =>
                    if (s_aad_axis_tlast = '1' and s_aad_axis_en = '1') then
                        out_state <= DATA_PROCESSING_S;
                    end if;

                when DATA_PROCESSING_S =>
                    if (s_axis_tlast = '1' and s_axis_en = '1') then
                        out_state <= FINISH_S;
                    end if;

                when FINISH_S =>
                    if (m_auth_axis_en = '1') then
                        out_state <= IDLE_S;

                        if (s_axis_tvalid = '1') then
                            out_state <= DATA_PROCESSING_S;
                        end if;

                        if (s_aad_axis_tvalid = '1') then
                            out_state <= AAD_PROCESSING_S;
                        end if;

                        if (s_iv_axis_en = '1') then
                            out_state <= COMPUTE_E_IV_0_S;
                        end if;

                        if (s_key_axis_en = '1') then
                            out_state <= NEW_KEY_S;
                        end if;
                    end if;

                when others =>
                    out_state <= RESET_S;
                end case;
            end if;
        end if;
    end process;

--------------------------------------------------------------------------------
-------------------------------- Encryption ------------------------------------
--------------------------------------------------------------------------------
    s_axis_tready       <= s_axis_tready_sig;
    s_axis_en           <= s_axis_tready_sig and s_axis_tvalid;
    s_axis_tready_sig   <= ciphertext_tready and aes_output_axis_tvalid when out_state = DATA_PROCESSING_S else '0';

    ciphertext_tvalid   <= s_axis_tvalid and aes_output_axis_tvalid when out_state = DATA_PROCESSING_S else '0';
    ciphertext_tlast    <= s_axis_tlast;
    ciphertext_tkeep    <= s_axis_tkeep;
    plaintext_tdata     <= toggle_endianess(s_axis_tdata);
    ciphertext_tdata    <= tkeep_mask(plaintext_tdata xor aes_output_axis_tdata, s_axis_tkeep);

    ciphertext_reg_e : entity LIB_AXIS.axi_stream_register
        generic map (
            TDATA_WIDTH_G   => 128,
            TKEEP_WIDTH_G   => 16
        )
        port map (
            clk             => clk,
            rst             => rst,

            s_axis_tvalid   => ciphertext_tvalid,
            s_axis_tready   => ciphertext_tready,
            s_axis_tlast    => ciphertext_tlast,
            s_axis_tkeep    => ciphertext_tkeep,
            s_axis_tdata    => ciphertext_tdata,

            m_axis_tvalid   => ciphertext_reg_tvalid,
            m_axis_tready   => ciphertext_reg_tready,
            m_axis_tlast    => ciphertext_reg_tlast,
            m_axis_tkeep    => ciphertext_reg_tkeep,
            m_axis_tdata    => ciphertext_reg_tdata
        );

    ciphertext_reg_tready   <= m_axis_tready;
    m_axis_tvalid           <= ciphertext_reg_tvalid;
    m_axis_tlast            <= ciphertext_reg_tlast;
    m_axis_tkeep            <= ciphertext_reg_tkeep;
    m_axis_tdata            <= toggle_endianess(ciphertext_reg_tdata);

--------------------------------------------------------------------------------
------------------------------- Data Counters ----------------------------------
--------------------------------------------------------------------------------

    data_count : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                aad_count       <= (others => '0');
                plaintext_count <= (others => '0');
            else
                if (s_aad_axis_en = '1' and out_state = AAD_PROCESSING_S) then
                    aad_count <= aad_count + count_ones(s_aad_axis_tkeep)*8;
                end if;

                if (s_axis_en = '1' and out_state = DATA_PROCESSING_S) then
                    plaintext_count <= plaintext_count + count_ones(ciphertext_tkeep)*8;
                end if;

                if (not (out_state = AAD_PROCESSING_S or out_state = DATA_PROCESSING_S or out_state = FINISH_S)) then
                    aad_count       <= (others => '0');
                    plaintext_count <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    input_lengths <= std_logic_vector(aad_count) & std_logic_vector(plaintext_count);

--------------------------------------------------------------------------------
------------------------------ Authentication ----------------------------------
--------------------------------------------------------------------------------

    s_aad_axis_tready       <= s_aad_axis_tready_sig;
    s_aad_axis_en           <= s_aad_axis_tready_sig and s_aad_axis_tvalid;
    s_aad_axis_tready_sig   <= aad_tready and not s_iv_axis_en when out_state = AAD_PROCESSING_S else '0';

    aad_tvalid  <= s_aad_axis_tvalid and not s_iv_axis_en when out_state = AAD_PROCESSING_S else '0';
    aad_tlast   <= s_aad_axis_tlast;
    aad_tkeep   <= s_aad_axis_tkeep;
    aad_tdata   <= toggle_endianess(s_aad_axis_tdata);

    aad_reg_e : entity LIB_AXIS.axi_stream_register
        generic map (
            TDATA_WIDTH_G   => 128,
            TKEEP_WIDTH_G   => 16
        )
        port map (
            clk             => clk,
            rst             => rst,

            s_axis_tvalid   => aad_tvalid,
            s_axis_tready   => aad_tready,
            s_axis_tlast    => aad_tlast,
            s_axis_tkeep    => aad_tkeep,
            s_axis_tdata    => aad_tdata,

            m_axis_tvalid   => aad_reg_tvalid,
            m_axis_tready   => aad_reg_tready,
            m_axis_tlast    => aad_reg_tlast,
            m_axis_tkeep    => aad_reg_tkeep,
            m_axis_tdata    => aad_reg_tdata
        );

    aad_reg_tready      <= m_aad_axis_tready;
    m_aad_axis_tvalid   <= aad_reg_tvalid;
    m_aad_axis_tlast    <= aad_reg_tlast;
    m_aad_axis_tkeep    <= aad_reg_tkeep;
    m_aad_axis_tdata    <= toggle_endianess(aad_reg_tdata);

    -- Plaintext in decrypt mode is actually the ciphertext input.
    tag_operand <=  aad_tdata           when out_state = AAD_PROCESSING_S   else
                    ciphertext_tdata    when out_state = DATA_PROCESSING_S  and DECRYPT_G = false else
                    tkeep_mask(plaintext_tdata, s_axis_tkeep)   when out_state = DATA_PROCESSING_S  and DECRYPT_G = true  else
                    input_lengths       when out_state = FINISH_S           else
                    (others => '0');

    multiplier_a <= reflect(encrypt_all_zeros_reg);
    multiplier_b <= reflect(tag);

    galois_multiplier_e : entity LIB_AES.galois_multiplier
        port map (
            a => multiplier_a,
            b => multiplier_b,
            c => multiplier_c
        );

    new_tag_before_xor <= reflect(multiplier_c);

    auth_proc : process (clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                fail_flag_sig <= '0';
                auth_tag_tvalid <= '0';
                auth_state <= AUTH_PROCESSING_S;
                tag <= (others => '0');
            else
                if (auth_tag_tvalid = '1' and m_auth_axis_tready = '1') then
                    auth_tag_tvalid <= '0';
                end if;

                case (auth_state) is
                when AUTH_PROCESSING_S =>
                    if (s_aad_axis_en = '1' or s_axis_en = '1') then
                        tag <= new_tag_before_xor xor tag_operand;
                        if (s_axis_tlast = '1' and s_axis_en = '1') then
                            auth_state <= AUTH_LENGTH_MULTI_S;
                        end if;
                    end if;
                when AUTH_LENGTH_MULTI_S =>
                    tag <= new_tag_before_xor xor tag_operand;
                    auth_state <= AUTH_XOR_WITH_EK0_S;
                when AUTH_XOR_WITH_EK0_S =>
                    tag <= new_tag_before_xor xor encrypt_cntr_eq_0_reg;
                    if (DECRYPT_G = false) then
                        auth_tag_tvalid <= '1';
                        auth_state <= AUTH_FINISH_S;
                    else
                        auth_state <= AUTH_VALIDATE_S;
                    end if;
                when AUTH_VALIDATE_S =>
                    if (s_auth_axis_en = '1') then
                        if (toggle_endianess(s_auth_axis_tdata) /= tag) then
                            fail_flag_sig <= '1';
                        else
                            fail_flag_sig <= '0';
                        end if;
                        auth_tag_tvalid <= '1';
                        auth_state <= AUTH_FINISH_S;
                    end if;
                when AUTH_FINISH_S =>
                    if (out_state /= FINISH_S) then
                        tag <= (others => '0');
                        auth_state <= AUTH_PROCESSING_S;
                    end if;
                when others =>
                    auth_state <= AUTH_PROCESSING_S;
                end case;
            end if;
        end if;
    end process;

    s_auth_axis_tready <= s_auth_axis_tready_sig;
    s_auth_axis_en <= s_auth_axis_tready_sig and s_auth_axis_tvalid;
    s_auth_axis_tready_sig <=   '1' when auth_state = AUTH_VALIDATE_S else
                                '1' when DECRYPT_G = false else '0';

    m_auth_axis_en      <= auth_tag_tvalid and m_auth_axis_tready;

    m_auth_axis_tvalid  <= auth_tag_tvalid;
    m_auth_axis_tdata   <= toggle_endianess(tag);

    fail_flag <= fail_flag_sig;

end architecture;