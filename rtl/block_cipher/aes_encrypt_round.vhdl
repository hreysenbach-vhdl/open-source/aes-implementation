-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

-- @brief                   Computes one round of AES encryption
--
-- @generic FINAL_ROUND_G   Set if final round of encryption. This will
--                          skip the mix columns step as specified in
--                          FIPS-197
--
-- @input   in_data         Plaintext to encrypt
-- @input   key             The key for this round
-- @output  result          The cipher text output

entity aes_encrypt_round is
    port (
        clk                     : in    std_logic;
        rst                     : in    std_logic;

        final_round             : in    std_logic;

        s_axis_tvalid           : in    std_logic;
        s_axis_tready           : out   std_logic;
        s_axis_tdata            : in    std_logic_vector(127 downto 0);

        key_tvalid              : in    std_logic;
        key_tdata               : in    std_logic_vector(127 downto 0);

        m_axis_tvalid           : out   std_logic;
        m_axis_tready           : in    std_logic;
        m_axis_tdata            : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of aes_encrypt_round is
    type word_array_t is array (0 to 16) of std_logic_vector(31 downto 0);
    constant number_of_bytes        : integer := 128/8;
    constant number_of_words        : integer := 128/32;

    signal res                      : std_logic_vector(127 downto 0);
    signal res_valid                : std_logic;
    signal wire_s_tready            : std_logic;

    signal s_axis_en                : std_logic;
    
    signal s_box_sig                : std_logic_vector(127 downto 0);
    signal rot_sig                  : std_logic_vector(127 downto 0);
    signal mix_col_sig              : std_logic_vector(127 downto 0);
    signal rows                     : word_array_t;
    signal cols                     : word_array_t;
begin
    s_axis_en   <= wire_s_tready and s_axis_tvalid;

    wire_s_tready <=    '0' when rst = '1' else 
                        '0' when key_tvalid = '0' else m_axis_tready;
    s_axis_tready <= wire_s_tready;

    aes_sbox_e : entity LIB_AES.aes_sbox
        generic map (
            INPUT_WIDTH_G           => 128,
            INVERSE_G               => false
        )
        port map (
            enable                  => s_axis_tvalid,
            in_data                 => s_axis_tdata,
            out_data                => s_box_sig
        );

    aes_rotate_state_e : entity LIB_AES.aes_rotate_state
        generic map (
            ROTATE_LEFT_G           => true
        )
        port map (
            enable                  => s_axis_tvalid,
            in_data                 => s_box_sig,
            out_data                => rot_sig
        );

    aes_mix_cols_e : entity LIB_AES.aes_mix_cols
        port map (
            enable              => s_axis_tvalid,
            in_data             => rot_sig,
            out_data            => mix_col_sig
        );

    res <= mix_col_sig  xor key_tdata when final_round = '0' and key_tvalid = '1' else 
           rot_sig      xor key_tdata when final_round = '1' and key_tvalid = '1' else  (others => '0');

    m_axis_tvalid   <= s_axis_tvalid and key_tvalid;
    m_axis_tdata    <= res;

end architecture;
