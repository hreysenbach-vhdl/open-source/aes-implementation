-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_mix_cols is
    generic (
        INVERSE_G           : boolean := false
    );
    port (
        enable              : in    std_logic;
        in_data             : in    std_logic_vector(127 downto 0);
        out_data            : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of aes_mix_cols is
begin

    non_inverse_gen : if INVERSE_G = false generate
        mix_cols_gen : for i in 0 to 3 generate
            signal column       : std_logic_vector(31 downto 0);
        begin
            column <= fips197_get_column(in_data, i);
            out_data(127-32*i downto 96-32*i) <= mix_columns(column) when enable = '1' else (others => '0');
        end generate;
    end generate;

    inverse_gen : if INVERSE_G = true generate
        mix_cols_gen : for i in 0 to 3 generate
            signal column       : std_logic_vector(31 downto 0);
        begin
            column <= fips197_get_column(in_data, i);
            out_data(127-32*i downto 96-32*i) <= inv_mix_columns(column) when enable = '1' else (others => '0');
        end generate;
    end generate;

end architecture;