-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;


entity aes_key_expansion is
    generic (
        KEY_NBITS_G         : NATURAL := 128
    );
    port (
        clk                 : in    std_logic;
        rst                 : in    std_logic;

        s_key_axis_tvalid   : in    std_logic;
        s_key_axis_tready   : out   std_logic;
        s_key_axis_tdata    : in    std_logic_vector(KEY_NBITS_G-1 downto 0);

        keys_out_valid      : out   std_logic_vector(KEY_NBITS_G/32+6 downto 0);
        keys_out            : out   std_logic_vector(128*(KEY_NBITS_G/32+7)-1 downto 0)
    );
end entity;

architecture rtl of aes_key_expansion is
    type round_array_t  is array (natural range <>) of std_logic_vector(127 downto 0);
    type key_array_t    is array (natural range <>) of std_logic_vector(KEY_NBITS_G-1 downto 0);

    constant Nk                     : integer := KEY_NBITS_G/32;
    constant Nr                     : integer := Nk + 6;

    -- This is a bit oversized for 192 and 256 keys but I think the compiler
    -- will optimise it away because it shouldn't drive logic
    signal key_array_reg            : key_array_t(10 downto 0);
    signal key_array_valid_reg      : std_logic_vector(10 downto 0);

    signal s_round_axis_tvalid      : std_logic;
    signal s_round_axis_tdata       : std_logic_vector(KEY_NBITS_G-1 downto 0);
    signal m_round_axis_tvalid      : std_logic;
    signal m_round_axis_tdata       : std_logic_vector(KEY_NBITS_G-1 downto 0);

    signal counter                  : unsigned(3 downto 0);
    signal counter_limit            : integer range 0 to 9;

    signal s_key_axis_en            : std_logic;
begin

    counter_limit   <=  9 when KEY_NBITS_G= 128 else
                        7 when KEY_NBITS_G= 192 else
                        6 when KEY_NBITS_G= 256 else 0;

    s_key_axis_en <= s_key_axis_tvalid;
    s_key_axis_tready <= '1';

    s_round_axis_tvalid <= key_array_valid_reg(to_integer(counter)) when counter < counter_limit+1 else '0';
    s_round_axis_tdata  <= key_array_reg(to_integer(counter));
    aes_key_expansion_round_e : entity LIB_AES.aes_key_expansion_round
        generic map (
            KEY_NBITS_G         => KEY_NBITS_G
        )
        port map (
            clk                 => clk,
            rst                 => rst,
            round_number        => counter,
            s_key_axis_tvalid   => s_round_axis_tvalid,
            s_key_axis_tdata    => s_round_axis_tdata,
            m_key_axis_tvalid   => m_round_axis_tvalid,
            m_key_axis_tdata    => m_round_axis_tdata
        );

    process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                counter <= (others => '0');
                key_array_valid_reg <= (others => '0');
            else
                if (m_round_axis_tvalid = '1') then
                    if (counter <= counter_limit) then
                        counter <= counter + 1;
                    end if;
                    key_array_reg(to_integer(counter) + 1) <= m_round_axis_tdata;
                    key_array_valid_reg(to_integer(counter) + 1) <= m_round_axis_tvalid;
                end if;

                if (s_key_axis_en = '1') then
                    key_array_valid_reg <= (others => '0');
                    counter <= (others => '0');

                    key_array_reg(0)        <= s_key_axis_tdata;
                    key_array_valid_reg(0)  <= '1';
                end if;
            end if;
        end if;
    end process;

    process(key_array_valid_reg, key_array_reg)
    begin
        if (KEY_NBITS_G= 128) then
            keys_out_valid <= key_array_valid_reg;
            for i in 0 to Nr loop
                keys_out(128*i+127 downto 128*i) <= key_array_reg(i);
            end loop;
        elsif (KEY_NBITS_G= 192) then
            for i in 0 to 4 loop
                keys_out_valid(3*i)                 <= key_array_valid_reg(2*i);
                keys_out(384*i+127 downto 384*i)    <= key_array_reg(2*i)(191 downto 64);

                if (i /= 4) then
                    keys_out_valid(3*i+1)                   <= key_array_valid_reg(2*i+1) and key_array_valid_reg(2*i);
                    keys_out_valid(3*i+2)                   <= key_array_valid_reg(2*i+1);
                    keys_out(384*i+255 downto 384*i+128)    <= key_array_reg(2*i)(63 downto 0) & key_array_reg(2*i+1)(191 downto 128);
                    keys_out(384*i+383 downto 384*i+256)    <= key_array_reg(2*i+1)(127 downto 0);
                end if;
            end loop;
        elsif (KEY_NBITS_G= 256) then
            for i in 0 to Nr/2 loop
                keys_out_valid(2*i)                 <= key_array_valid_reg(i);
                keys_out(256*i+127 downto 256*i)    <= key_array_reg(i)(255 downto 128);
                if (i /= Nr/2) then
                    keys_out_valid(2*i + 1)                 <= key_array_valid_reg(i);
                    keys_out(256*i+255 downto 256*i+128)    <= key_array_reg(i)(127 downto 0);
                end if;
            end loop;
        end if;
    end process;

end architecture;