-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_rotate_state is
    generic (
        ROTATE_LEFT_G       : boolean := true
    );
    port (
        enable              : in    std_logic;
        in_data             : in    std_logic_vector(127 downto 0);
        out_data            : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of aes_rotate_state is
begin
    
    rotate_words_gen : for i in 0 to 3 generate
        signal row          : std_logic_vector(31 downto 0);
    begin
        row <= fips197_get_row(in_data, i);
        aes_rotation_e : entity LIB_AES.aes_rotation
            generic map (
                INPUT_WIDTH_G           => 32,
                ROTATION_DISTANCE_G     => 8*i,
                ROTATE_LEFT_G           => ROTATE_LEFT_G
            )
            port map (
                enable                  => enable,
                in_data                 => row,
                out_data(31 downto 24)  => out_data(127-8*i downto 120-8*i),
                out_data(23 downto 16)  => out_data( 95-8*i downto  88-8*i),
                out_data(15 downto  8)  => out_data( 63-8*i downto  56-8*i),
                out_data( 7 downto  0)  => out_data( 31-8*i downto  24-8*i)
            );
            
    end generate;
end architecture;