-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package aes_block_cipher_pkg is
    type sbox_table_t is array (0 to 255) of std_logic_vector(7 downto 0);

    constant SBOX_TABLE_C : sbox_table_t := (
        X"63", X"7C", X"77", X"7B", X"F2", X"6B", X"6F", X"C5",
        X"30", X"01", X"67", X"2B", X"FE", X"D7", X"AB", X"76",
        X"CA", X"82", X"C9", X"7D", X"FA", X"59", X"47", X"F0",
        X"AD", X"D4", X"A2", X"AF", X"9C", X"A4", X"72", X"C0",
        X"B7", X"FD", X"93", X"26", X"36", X"3F", X"F7", X"CC",
        X"34", X"A5", X"E5", X"F1", X"71", X"D8", X"31", X"15",
        X"04", X"C7", X"23", X"C3", X"18", X"96", X"05", X"9A",
        X"07", X"12", X"80", X"E2", X"EB", X"27", X"B2", X"75",
        X"09", X"83", X"2C", X"1A", X"1B", X"6E", X"5A", X"A0",
        X"52", X"3B", X"D6", X"B3", X"29", X"E3", X"2F", X"84",
        X"53", X"D1", X"00", X"ED", X"20", X"FC", X"B1", X"5B",
        X"6A", X"CB", X"BE", X"39", X"4A", X"4C", X"58", X"CF",
        X"D0", X"EF", X"AA", X"FB", X"43", X"4D", X"33", X"85",
        X"45", X"F9", X"02", X"7F", X"50", X"3C", X"9F", X"A8",
        X"51", X"A3", X"40", X"8F", X"92", X"9D", X"38", X"F5",
        X"BC", X"B6", X"DA", X"21", X"10", X"FF", X"F3", X"D2",
        X"CD", X"0C", X"13", X"EC", X"5F", X"97", X"44", X"17",
        X"C4", X"A7", X"7E", X"3D", X"64", X"5D", X"19", X"73",
        X"60", X"81", X"4F", X"DC", X"22", X"2A", X"90", X"88",
        X"46", X"EE", X"B8", X"14", X"DE", X"5E", X"0B", X"DB",
        X"E0", X"32", X"3A", X"0A", X"49", X"06", X"24", X"5C",
        X"C2", X"D3", X"AC", X"62", X"91", X"95", X"E4", X"79",
        X"E7", X"C8", X"37", X"6D", X"8D", X"D5", X"4E", X"A9",
        X"6C", X"56", X"F4", X"EA", X"65", X"7A", X"AE", X"08",
        X"BA", X"78", X"25", X"2E", X"1C", X"A6", X"B4", X"C6",
        X"E8", X"DD", X"74", X"1F", X"4B", X"BD", X"8B", X"8A",
        X"70", X"3E", X"B5", X"66", X"48", X"03", X"F6", X"0E",
        X"61", X"35", X"57", X"B9", X"86", X"C1", X"1D", X"9E",
        X"E1", X"F8", X"98", X"11", X"69", X"D9", X"8E", X"94",
        X"9B", X"1E", X"87", X"E9", X"CE", X"55", X"28", X"DF",
        X"8C", X"A1", X"89", X"0D", X"BF", X"E6", X"42", X"68",
        X"41", X"99", X"2D", X"0F", X"B0", X"54", X"BB", X"16"
	);

    constant INV_SBOX_TABLE_C : sbox_table_t := (
        X"52", X"09", X"6A", X"D5", X"30", X"36", X"A5", X"38",
        X"BF", X"40", X"A3", X"9E", X"81", X"F3", X"D7", X"FB",
        X"7C", X"E3", X"39", X"82", X"9B", X"2F", X"FF", X"87",
        X"34", X"8E", X"43", X"44", X"C4", X"DE", X"E9", X"CB",
        X"54", X"7B", X"94", X"32", X"A6", X"C2", X"23", X"3D",
        X"EE", X"4C", X"95", X"0B", X"42", X"FA", X"C3", X"4E",
        X"08", X"2E", X"A1", X"66", X"28", X"D9", X"24", X"B2",
        X"76", X"5B", X"A2", X"49", X"6D", X"8B", X"D1", X"25",
        X"72", X"F8", X"F6", X"64", X"86", X"68", X"98", X"16",
        X"D4", X"A4", X"5C", X"CC", X"5D", X"65", X"B6", X"92",
        X"6C", X"70", X"48", X"50", X"FD", X"ED", X"B9", X"DA",
        X"5E", X"15", X"46", X"57", X"A7", X"8D", X"9D", X"84",
        X"90", X"D8", X"AB", X"00", X"8C", X"BC", X"D3", X"0A",
        X"F7", X"E4", X"58", X"05", X"B8", X"B3", X"45", X"06",
        X"D0", X"2C", X"1E", X"8F", X"CA", X"3F", X"0F", X"02",
        X"C1", X"AF", X"BD", X"03", X"01", X"13", X"8A", X"6B",
        X"3A", X"91", X"11", X"41", X"4F", X"67", X"DC", X"EA",
        X"97", X"F2", X"CF", X"CE", X"F0", X"B4", X"E6", X"73",
        X"96", X"AC", X"74", X"22", X"E7", X"AD", X"35", X"85",
        X"E2", X"F9", X"37", X"E8", X"1C", X"75", X"DF", X"6E",
        X"47", X"F1", X"1A", X"71", X"1D", X"29", X"C5", X"89",
        X"6F", X"B7", X"62", X"0E", X"AA", X"18", X"BE", X"1B",
        X"FC", X"56", X"3E", X"4B", X"C6", X"D2", X"79", X"20",
        X"9A", X"DB", X"C0", X"FE", X"78", X"CD", X"5A", X"F4",
        X"1F", X"DD", X"A8", X"33", X"88", X"07", X"C7", X"31",
        X"B1", X"12", X"10", X"59", X"27", X"80", X"EC", X"5F",
        X"60", X"51", X"7F", X"A9", X"19", X"B5", X"4A", X"0D",
        X"2D", X"E5", X"7A", X"9F", X"93", X"C9", X"9C", X"EF",
        X"A0", X"E0", X"3B", X"4D", X"AE", X"2A", X"F5", X"B0",
        X"C8", X"EB", X"BB", X"3C", X"83", X"53", X"99", X"61",
        X"17", X"2B", X"04", X"7E", X"BA", X"77", X"D6", X"26",
        X"E1", X"69", X"14", X"63", X"55", X"21", X"0C", X"7D"
	);

    -- @brief                   Gets all of the bytes in a column from a state matrix as per FIPS197
    -- @param   in_data         Input state (128-bits)
    -- @param   index           which column is requested (0-3)
    -- @return                  The 32-bit column
    pure function fips197_get_column(
            in_data : std_logic_vector(127 downto 0);
            index   : integer)
        return std_logic_vector;

    -- @brief                   Gets all of the bytes in a row from a state matrix as per FIPS197
    -- @param   in_data         Input state (128-bits)
    -- @param   index           which row is requested (0-3)
    -- @return                  The 32-bit row
    pure function fips197_get_row(
            in_data : std_logic_vector(127 downto 0);
            index   : integer)
        return std_logic_vector;

    -- @brief                   Passes a byte through the S-Box look-up table
    -- @param   in_data         Input byte
    -- @return                  The corresponding S-Box byte
    pure function sbox_lookup(in_data : std_logic_vector(7 downto 0))
        return std_logic_vector;

    -- @brief                   Passes a byte through the inverse S-Box look-up table
    -- @param   in_data         Input byte
    -- @return                  The corresponding inverted S-Box byte
    pure function inv_sbox_lookup(in_data : std_logic_vector(7 downto 0))
        return std_logic_vector;

    -- @brief                   Left-rotates a bit vector by a specified amount
    -- @param   in_data         Input vector
    -- @param   rot_amount      Rotation amount
    -- @return                  The rotated bit vector
    pure function rotl(in_data : std_logic_vector; rot_amount : integer)
        return std_logic_vector;

    -- @brief                   Right-rotates a bit vector by a specified amount
    -- @param   in_data         Input vector
    -- @param   rot_amount      Rotation amount
    -- @return                  The rotated bit vector
    pure function rotr(in_data : std_logic_vector; rot_amount : integer)
        return std_logic_vector;

    -- @brief                   Computes xtime function as specified in FIPS-197
    --
    -- @details                 This performs a left shift and if the MSB of
    --                          the original vector was '1' then it will XOR
    --                          the result with 0x1B (AES polynominal)
    --
    -- @param   in_data         Input vector
    -- @return                  The result.
    pure function xtime(in_data : std_logic_vector(7 downto 0))
        return std_logic_vector;

    -- @brief                   Implements mix_byte for mix columns operation
    --
    -- @details                 Calculates mix_byte (the result for the mix
    --                          columns operation) as documented in
    --                          https://eprint.iacr.org/2016/789.pdf
    --
    -- @param   a               Input 0
    -- @param   b               Input 1
    -- @param   c               Input 2
    -- @param   d               Input 3
    --
    -- @return                  The result.
    pure function byte_mixc(a,b,c,d : std_logic_vector(7 downto 0))
        return std_logic_vector;

    -- @brief                   Mixes the columns as required for AES
    --
    -- @details                 This multiples 32-bit vector by [2, 3, 1, 1].
    --                          The vector is split into 4 8-bit words and
    --                          which are used to perform the calculation
    --
    -- @param   in_data         Input vector
    -- @return                  The result.
    pure function mix_columns(in_data : std_logic_vector(31 downto 0))
        return std_logic_vector;

    -- @brief                   Inversely mixes the columns as required for AES
    --
    -- @details                 This multiples 32-bit vector by [0E, 0B, 0D, 09].
    --                          The vector is split into 4 8-bit words and
    --                          which are used to perform the calculation
    --
    -- @param   in_data         Input vector
    -- @return                  The result.
    pure function inv_mix_columns(in_data : std_logic_vector(31 downto 0))
        return std_logic_vector;

end package;

package body aes_block_cipher_pkg is

    pure function fips197_get_column(
            in_data : std_logic_vector(127 downto 0);
            index   : integer)
        return std_logic_vector is
            variable col            : std_logic_vector(31 downto 0);
    begin
        col := in_data(127-32*index downto 96-32*index);

        return col;
    end function;

    pure function fips197_get_row(
            in_data : std_logic_vector(127 downto 0);
            index   : integer)
        return std_logic_vector is
            variable row            : std_logic_vector(31 downto 0);
    begin
        row(31 downto 24) := in_data(127-8*index downto 120-8*index);
        row(23 downto 16) := in_data( 95-8*index downto  88-8*index);
        row(15 downto  8) := in_data( 63-8*index downto  56-8*index);
        row( 7 downto  0) := in_data( 31-8*index downto  24-8*index);
        return row;
    end function;

    pure function sbox_lookup(in_data : std_logic_vector(7 downto 0))
        return std_logic_vector is
    begin
        return SBOX_TABLE_C(to_integer(unsigned(in_data)));
    end function;

    pure function inv_sbox_lookup(in_data : std_logic_vector(7 downto 0))
        return std_logic_vector is
    begin
        return INV_SBOX_TABLE_C(to_integer(unsigned(in_data)));
    end function;

    pure function rotl(in_data : std_logic_vector; rot_amount : integer)
        return std_logic_vector is
            variable tmp_vector0    : std_logic_vector(in_data'length-1 downto 0);
    begin
        if (rot_amount = 0 or rot_amount = in_data'length) then
            return in_data;
        end if;

        tmp_vector0 := in_data(in_data'high-rot_amount downto in_data'low)
                     & in_data(in_data'high downto in_data'high-rot_amount+1);

        return tmp_vector0;
    end function;

    pure function rotr(in_data : std_logic_vector; rot_amount : integer)
        return std_logic_vector is
            variable tmp_vector     : std_logic_vector(in_data'length-1 downto 0);
            variable new_rot_amount : integer;
    begin
        -- Since right rotation is the same as left rotation just with a
        -- different shift amount just use the left rotate function
        new_rot_amount := in_data'length - rot_amount;
        tmp_vector := rotl(in_data, new_rot_amount);

        return tmp_vector;
    end function;

    pure function xtime(in_data : std_logic_vector(7 downto 0))
        return std_logic_vector is
            variable result         : std_logic_vector(7 downto 0);
    begin
        result := in_data(6 downto 0) & '0';
        if (in_data(7) = '1') then
            result := result xor X"1B";
        end if;

        return result;
    end function;

    pure function byte_mixc(a,b,c,d : std_logic_vector(7 downto 0))
        return std_logic_vector is
            variable a_xor_b        : std_logic_vector(7 downto 0);
            variable b_xor_c_xor_d  : std_logic_vector(7 downto 0);
            variable xtime_a_xor_b  : std_logic_vector(7 downto 0);
            variable result         : std_logic_vector(7 downto 0);
    begin
        a_xor_b         := a xor b;
        xtime_a_xor_b   := xtime(a_xor_b);
        b_xor_c_xor_d   := b xor c xor d;

        result          := xtime_a_xor_b xor b_xor_c_xor_d;

        return result;
    end function;

    pure function mix_columns(in_data : std_logic_vector(31 downto 0))
        return std_logic_vector is
            variable in_word0       : std_logic_vector(7 downto 0);
            variable in_word1       : std_logic_vector(7 downto 0);
            variable in_word2       : std_logic_vector(7 downto 0);
            variable in_word3       : std_logic_vector(7 downto 0);
            variable out_word0      : std_logic_vector(7 downto 0);
            variable out_word1      : std_logic_vector(7 downto 0);
            variable out_word2      : std_logic_vector(7 downto 0);
            variable out_word3      : std_logic_vector(7 downto 0);
    begin
        in_word3 := in_data(in_data'low +  7 downto in_data'low);
        in_word2 := in_data(in_data'low + 15 downto in_data'low + 8);
        in_word1 := in_data(in_data'low + 23 downto in_data'low + 16);
        in_word0 := in_data(in_data'low + 31 downto in_data'low + 24);



        out_word0 := byte_mixc(in_word0, in_word1, in_word2, in_word3);
        out_word1 := byte_mixc(in_word1, in_word2, in_word3, in_word0);
        out_word2 := byte_mixc(in_word2, in_word3, in_word0, in_word1);
        out_word3 := byte_mixc(in_word3, in_word0, in_word1, in_word2);

        return out_word0 & out_word1 & out_word2 & out_word3;
    end function;

    pure function inv_mix_columns(in_data : std_logic_vector(31 downto 0))
        return std_logic_vector is
            variable in_word0       : std_logic_vector(7 downto 0);
            variable in_word1       : std_logic_vector(7 downto 0);
            variable in_word2       : std_logic_vector(7 downto 0);
            variable in_word3       : std_logic_vector(7 downto 0);
            variable out_word0      : std_logic_vector(7 downto 0);
            variable out_word1      : std_logic_vector(7 downto 0);
            variable out_word2      : std_logic_vector(7 downto 0);
            variable out_word3      : std_logic_vector(7 downto 0);

            variable word0_xtime_2  : std_logic_vector(7 downto 0);
            variable word1_xtime_2  : std_logic_vector(7 downto 0);
            variable word2_xtime_2  : std_logic_vector(7 downto 0);
            variable word3_xtime_2  : std_logic_vector(7 downto 0);
            variable word0_xtime_4  : std_logic_vector(7 downto 0);
            variable word1_xtime_4  : std_logic_vector(7 downto 0);
            variable word2_xtime_4  : std_logic_vector(7 downto 0);
            variable word3_xtime_4  : std_logic_vector(7 downto 0);
            variable word0_xtime_8  : std_logic_vector(7 downto 0);
            variable word1_xtime_8  : std_logic_vector(7 downto 0);
            variable word2_xtime_8  : std_logic_vector(7 downto 0);
            variable word3_xtime_8  : std_logic_vector(7 downto 0);
    begin
        in_word3 := in_data(in_data'low +  7 downto in_data'low);
        in_word2 := in_data(in_data'low + 15 downto in_data'low + 8);
        in_word1 := in_data(in_data'low + 23 downto in_data'low + 16);
        in_word0 := in_data(in_data'low + 31 downto in_data'low + 24);

        word0_xtime_2 := xtime(in_word0);
        word1_xtime_2 := xtime(in_word1);
        word2_xtime_2 := xtime(in_word2);
        word3_xtime_2 := xtime(in_word3);

        word0_xtime_4 := xtime(word0_xtime_2);
        word1_xtime_4 := xtime(word1_xtime_2);
        word2_xtime_4 := xtime(word2_xtime_2);
        word3_xtime_4 := xtime(word3_xtime_2);

        word0_xtime_8 := xtime(word0_xtime_4);
        word1_xtime_8 := xtime(word1_xtime_4);
        word2_xtime_8 := xtime(word2_xtime_4);
        word3_xtime_8 := xtime(word3_xtime_4);

        out_word0 :=    (word0_xtime_8 xor word0_xtime_4 xor word0_xtime_2) xor
                        (word1_xtime_8 xor word1_xtime_2 xor in_word1)      xor
                        (word2_xtime_8 xor word2_xtime_4 xor in_word2)      xor
                        (word3_xtime_8 xor in_word3);

        out_word1 :=    (word0_xtime_8 xor in_word0)                        xor
                        (word1_xtime_8 xor word1_xtime_4 xor word1_xtime_2) xor
                        (word2_xtime_8 xor word2_xtime_2 xor in_word2)      xor
                        (word3_xtime_8 xor word3_xtime_4 xor in_word3);

        out_word2 :=    (word0_xtime_8 xor word0_xtime_4 xor in_word0)      xor
                        (word1_xtime_8 xor in_word1)                        xor
                        (word2_xtime_8 xor word2_xtime_4 xor word2_xtime_2) xor
                        (word3_xtime_8 xor word3_xtime_2 xor in_word3);

        out_word3 :=    (word0_xtime_8 xor word0_xtime_2 xor in_word0)      xor
                        (word1_xtime_8 xor word1_xtime_4 xor in_word1)      xor
                        (word2_xtime_8 xor in_word2)                        xor
                        (word3_xtime_8 xor word3_xtime_4 xor word3_xtime_2);

        return out_word0 & out_word1 & out_word2 & out_word3;
    end function;

end package body;