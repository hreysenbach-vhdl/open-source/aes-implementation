-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_encrypt_block is
    generic (
        KEY_NBITS_G             : integer := 256;
        HIGH_THROUGHPUT_G       : boolean := true
    );
    port (
        clk                     : in    std_logic;
        rst                     : in    std_logic;

        flush_pipeline          : in    std_logic := '0';

        s_key_axis_tvalid       : in    std_logic;
        s_key_axis_tready       : out   std_logic;
        s_key_axis_tdata        : in    std_logic_vector(KEY_NBITS_G-1 downto 0);
        
        m_curr_key_axis_tdata   : out   std_logic_vector(KEY_NBITS_G-1 downto 0);
        m_curr_key_axis_tvalid  : out   std_logic;

        s_axis_tvalid           : in    std_logic;
        s_axis_tready           : out   std_logic;
        s_axis_tdata            : in    std_logic_vector(127 downto 0);

        m_axis_tvalid           : out   std_logic;
        m_axis_tready           : in    std_logic;
        m_axis_tdata            : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of aes_encrypt_block is
    type aes_array_t is array (natural range <>) of std_logic_vector(127 downto 0);
    type state_t is (IDLE_S, PROCESSING_S);

    constant Nr                 : integer := KEY_NBITS_G/ 32 + 6;

    -- Key signals and other common signals
    signal keys_out_valid       : std_logic_vector(Nr downto 0);
    signal keys_out             : std_logic_vector(128*(Nr+1)-1 downto 0);
    signal key_array            : aes_array_t(Nr downto 0);

    signal first_result         : std_logic_vector(127 downto 0);

    -- High throughput signals
    signal axis_en              : std_logic_vector(Nr downto 0);
    signal axis_tvalid          : std_logic_vector(Nr downto 0);
    signal axis_tready          : std_logic_vector(Nr downto 0);
    signal axis_tdata           : aes_array_t(Nr downto 0);

    signal axis_reg_en          : std_logic_vector(Nr downto 0);
    signal axis_reg_tvalid      : std_logic_vector(Nr downto 0);
    signal axis_reg_tready      : std_logic_vector(Nr downto 0);
    signal axis_reg_tdata       : aes_array_t(Nr downto 0);

    -- Low throughput signals
    signal state                : state_t;

    signal m_axis_en            : std_logic;
    signal m_axis_tvalid_sig    : std_logic;

    signal final_round          : std_logic;
    signal old_count            : unsigned(3 downto 0);
    signal round_count          : unsigned(3 downto 0);

    signal round_key_input      : std_logic_vector(127 downto 0);
    signal round_key_valid      : std_logic;

    signal reg_axis_tdata       : std_logic_vector(127 downto 0);

    signal comb_axis_tdata      : std_logic_vector(127 downto 0);

    signal s_key_axis_en        : std_logic;
    signal s_key_axis_tready_sig: std_logic;

begin

    s_key_axis_en <= s_key_axis_tready_sig and s_key_axis_tvalid;
    s_key_axis_tready <= s_key_axis_tready_sig;

        e_key_expansion : entity LIB_AES.aes_key_expansion
            generic map(
                KEY_NBITS_G         => KEY_NBITS_G
            )
            port map (
                clk                 => clk,
                rst                 => rst,
                s_key_axis_tvalid   => s_key_axis_tvalid,
                s_key_axis_tready   => s_key_axis_tready_sig,
                s_key_axis_tdata    => s_key_axis_tdata,
                keys_out_valid      => keys_out_valid,
                keys_out            => keys_out
            );

        key_array_gen : for i in 0 to Nr generate
            key_array(i) <= keys_out(128*i+127 downto 128*i);
        end generate;

    m_curr_key_axis_tdata  <= key_array(0) & key_array(1);
    m_curr_key_axis_tvalid <= keys_out_valid(0) and keys_out_valid(1);

    first_result <= s_axis_tdata xor key_array(0);

    high_throughput_gen : if HIGH_THROUGHPUT_G = true generate
        s_axis_tready <= axis_tready(0);

        axis_en     <= axis_tready and axis_tvalid;
        axis_reg_en <= axis_reg_tready and axis_reg_tvalid;


        axis_tready(0) <=   '0' when rst = '1' else
                            '0' when keys_out_valid(0) = '0' else
                            '0' when axis_reg_tvalid(0) = '1' and axis_reg_tready(0) = '0' else '1';


        axis_tvalid(0) <= s_axis_tvalid;
        axis_tdata(0) <= first_result;

        first_round_proc : process (clk)
        begin
            if (rising_edge(clk)) then
                if (rst = '1' or flush_pipeline = '1') then
                    axis_reg_tvalid(0) <= '0';
                    axis_reg_tdata(0) <= (others => '0');
                else
                    if (axis_reg_tvalid(0) = '0') then
                        if (keys_out_valid(0) = '1' and axis_en(0) = '1') then
                            axis_reg_tvalid(0) <= '1';
                            axis_reg_tdata(0) <= axis_tdata(0);
                        end if;
                    else
                        if (axis_reg_en(0) = '1') then
                            axis_reg_tvalid(0) <= '0';

                            if (keys_out_valid(0) = '1' and axis_en(0) = '1') then
                                axis_reg_tvalid(0) <= '1';
                                axis_reg_tdata(0) <= axis_tdata(0);
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end process;

        rounds_gen : for i in 1 to Nr generate
            signal local_final_round    : std_logic;
        begin
            local_final_round <= '1' when i = Nr else '0';

            e_encrypt_round : entity LIB_AES.aes_encrypt_round
                port map (
                    clk             => clk,
                    rst             => rst,
                    final_round     => local_final_round,
                    key_tvalid      => keys_out_valid(i),
                    key_tdata       => key_array(i),
                    s_axis_tvalid   => axis_reg_tvalid(i-1),
                    s_axis_tready   => axis_reg_tready(i-1),
                    s_axis_tdata    => axis_reg_tdata(i-1),
                    m_axis_tvalid   => axis_tvalid(i),
                    m_axis_tready   => axis_tready(i),
                    m_axis_tdata    => axis_tdata(i)
                );

            axis_tready(i) <=   '0' when rst = '1' else
                                '0' when keys_out_valid(i) = '0' else
                                '0' when axis_reg_tvalid(i) = '1' and axis_reg_tready(i) = '0' else '1';

            register_proc : process(clk)
            begin
                if (rising_edge(clk)) then
                    if (rst = '1' or flush_pipeline = '1') then
                        axis_reg_tvalid(i) <= '0';
                        axis_reg_tdata(i) <= (others => '0');
                    else
                        if (axis_reg_tvalid(i) = '0') then
                            if (axis_en(i) = '1') then
                                axis_reg_tvalid(i) <= '1';
                                axis_reg_tdata(i) <= axis_tdata(i);
                            end if;
                        else
                            if (axis_reg_en(i) = '1') then
                                axis_reg_tvalid(i) <= '0';

                                if (axis_en(i) = '1') then
                                    axis_reg_tvalid(i) <= '1';
                                    axis_reg_tdata(i) <= axis_tdata(i);
                                end if;
                            end if;
                        end if;
                    end if;
                end if;
            end process;
        end generate;




        m_axis_tvalid       <= axis_reg_tvalid(Nr);
        axis_reg_tready(Nr) <= m_axis_tready;
        m_axis_tdata        <= axis_reg_tdata(Nr);
    end generate;

    low_throughput_gen : if HIGH_THROUGHPUT_G = false generate

        s_axis_tready <= keys_out_valid(0) when state = IDLE_S and (rst /= '1' or flush_pipeline /= '1') and s_key_axis_en /= '1' else '0';

        final_round <= '1' when to_integer(round_count) = Nr else '0';

        round_key_valid <= keys_out_valid(to_integer(round_count));
        round_key_input <= key_array(to_integer(round_count));

        e_encrypt_round : entity LIB_AES.aes_encrypt_round
            port map (
                clk             => clk,
                rst             => rst,
                final_round     => final_round,
                key_tvalid      => round_key_valid,
                key_tdata       => round_key_input,
                s_axis_tvalid   => '1',
                s_axis_tdata    => reg_axis_tdata,
                m_axis_tready   => '1',
                m_axis_tdata    => comb_axis_tdata
            );

        process(clk)
        begin
            if (rising_edge(clk)) then
                if (rst = '1' or flush_pipeline = '1') then
                    state <= IDLE_S;
                    m_axis_tvalid_sig <= '0';
                    round_count <= (others => '0');
                else
                    case (state) is
                    when IDLE_S =>
                        round_count <= (others => '0');
                        old_count <= (others => '1');

                        if (s_axis_tvalid = '1' and keys_out_valid(0) = '1') then
                            round_count <= X"1";
                            old_count <= (others => '1');
                            reg_axis_tdata <= first_result;
                            state <= PROCESSING_S;
                        end if;
                    when PROCESSING_S =>
                        if (keys_out_valid(to_integer(round_count)) = '1') then
                            if (round_count < Nr) then
                                round_count <= round_count + 1;
                            else
                                m_axis_tvalid_sig <= '1';
                                if (m_axis_en = '1') then
                                    m_axis_tvalid_sig <= '0';
                                    round_count <= (others => '0');
                                    state <= IDLE_S;
                                end if;
                            end if;
                            if (round_count /= old_count) then
                                reg_axis_tdata <= comb_axis_tdata;
                            end if;
                            old_count <= round_count;
                        end if;

                    when others =>
                        state <= IDLE_S;
                    end case;
                end if;
            end if;
        end process;

        m_axis_en       <= m_axis_tvalid_sig and m_axis_tready and not flush_pipeline;
        m_axis_tvalid   <= m_axis_tvalid_sig and not flush_pipeline;
        m_axis_tdata    <= reg_axis_tdata;
    end generate;


end architecture;