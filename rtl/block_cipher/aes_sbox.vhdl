-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_sbox is
    generic (
        INPUT_WIDTH_G       : natural := 8;
        INVERSE_G           : boolean := false
    );
    port (
        enable              : in    std_logic;
        in_data             : in    std_logic_vector(INPUT_WIDTH_G-1 downto 0);
        out_data            : out   std_logic_vector(INPUT_WIDTH_G-1 downto 0)
    );
end entity;

architecture rtl of aes_sbox is
    constant number_of_bytes    : natural := INPUT_WIDTH_G/8;
begin

    non_inverse_gen : if INVERSE_G = false generate
        s_box_gen : for i in 0 to number_of_bytes-1 generate
            out_data(8*i+7 downto 8*i) <= sbox_lookup(in_data(8*i+7 downto 8*i)) when enable = '1' else (others => '0');
        end generate;
    end generate;

    inverse_gen : if INVERSE_G = true generate
        s_box_gen : for i in 0 to number_of_bytes-1 generate
            out_data(8*i+7 downto 8*i) <= inv_sbox_lookup(in_data(8*i+7 downto 8*i)) when enable = '1' else (others => '0');
        end generate;
    end generate;
end architecture;