-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_rotation is
    generic (
        INPUT_WIDTH_G       : natural := 8;
        ROTATION_DISTANCE_G : natural := 8;
        ROTATE_LEFT_G       : boolean := true
    );
    port (
        enable              : in    std_logic;
        in_data             : in    std_logic_vector(INPUT_WIDTH_G-1 downto 0);
        out_data            : out   std_logic_vector(INPUT_WIDTH_G-1 downto 0)
    );
end entity;

architecture rtl of aes_rotation is
    constant number_of_bytes    : natural := INPUT_WIDTH_G/8;
begin
    rotl_gen : if ROTATE_LEFT_G = true generate
        out_data <= rotl(in_data, ROTATION_DISTANCE_G) when enable = '1' else (others => '0');
    end generate;

    rotr_gen : if ROTATE_LEFT_G = false generate
        out_data <= rotr(in_data, ROTATION_DISTANCE_G) when enable = '1' else (others => '0');
    end generate;

end architecture;