-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_key_expansion_round is
    generic (
        KEY_NBITS_G                 : natural := 128
    );
    port (
        clk                         : in    std_logic;
        rst                         : in    std_logic;

        round_number                : in    unsigned(3 downto 0);

        s_key_axis_tvalid           : in    std_logic := '1';
        s_key_axis_tready           : out   std_logic;
        s_key_axis_tdata            : in    std_logic_vector(KEY_NBITS_G-1 downto 0);

        m_key_axis_tvalid           : out   std_logic;
        m_key_axis_tready           : in    std_logic := '1';
        m_key_axis_tdata            : out   std_logic_vector(KEY_NBITS_G-1 downto 0)
    );
end entity;

architecture rtl of aes_key_expansion_round is
    type word_array_t is array (natural range <>) of std_logic_vector(31 downto 0);

    constant rcon                   : word_array_t(0 to 9) := (
        X"01000000", X"02000000", X"04000000", X"08000000",
        X"10000000", X"20000000", X"40000000", X"80000000",
        X"1B000000", X"36000000");

    constant Nk                     : natural := KEY_NBITS_G/32;
    constant Nr                     : natural := Nk + 6;

    signal word_array               : word_array_t(0 to Nk-1);
    signal result_array             : word_array_t(0 to Nk-1);

    signal s_key_axis_tready_sig    : std_logic;
    signal s_axis_en                : std_logic;
    signal m_key_axis_tvalid_sig    : std_logic;
    signal m_axis_en                : std_logic;

    signal started                  : std_logic;
    signal rot_sig_reg_valid        : std_logic;
    signal rot_sig                  : std_logic_vector(31 downto 0);
    signal rot_sig_reg              : std_logic_vector(31 downto 0);
    signal s_box_sig_reg_valid      : std_logic;
    signal s_box_sig                : std_logic_vector(31 downto 0);
    signal s_box_sig_reg            : std_logic_vector(31 downto 0);
    signal rcon_sig_reg_valid       : std_logic;
    signal rcon_sig                 : std_logic_vector(31 downto 0);
    signal rcon_sig_reg             : std_logic_vector(31 downto 0);
    signal s_box_256_sig_reg_valid  : std_logic;
    signal s_box_256_sig            : std_logic_vector(31 downto 0);
    signal s_box_256_sig_reg        : std_logic_vector(31 downto 0);

begin

    s_axis_en               <= s_key_axis_tvalid and s_key_axis_tready_sig;
    s_key_axis_tready       <= s_key_axis_tready_sig when rst = '0' else '0';
    s_key_axis_tready_sig   <= '1' when started = '0' else '0';

    m_axis_en           <=  m_key_axis_tvalid_sig and m_key_axis_tready;
    m_key_axis_tvalid   <= m_key_axis_tvalid_sig;
    m_key_axis_tvalid_sig <=    s_box_256_sig_reg_valid and started when KEY_NBITS_G = 256 else
                                rcon_sig_reg_valid      and started when KEY_NBITS_G /= 256 else '0';


    split_input_gen : for i in 0 to Nk-1 generate
        word_array(Nk-1-i) <= s_key_axis_tdata(32*i+31 downto 32*i);
    end generate;

    rotation_e : entity LIB_AES.aes_rotation
        generic map (
            INPUT_WIDTH_G       => 32,
            ROTATION_DISTANCE_G => 8
        )
        port map (
            enable                  => '1',
            in_data                 => word_array(Nk-1),
            out_data                => rot_sig
        );

    aes_sbox_e : entity LIB_AES.aes_sbox
        generic map (
            INPUT_WIDTH_G           => 32,
            INVERSE_G               => false
        )
        port map (
            enable                  => '1',
            in_data                 => rot_sig_reg,
            out_data                => s_box_sig
        );

    key_256_sbox_gen : if KEY_NBITS_G= 256 generate
        aes256_sbox_e : entity LIB_AES.aes_sbox
            generic map (
                INPUT_WIDTH_G           => 32
            )
            port map (
                enable                  => '1',
                in_data                 => result_array(3),
                out_data                => s_box_256_sig
            );
    end generate;

    rcon_sig <= s_box_sig_reg xor rcon(to_integer(round_number));

    result_array(0) <= word_array(0) xor rcon_sig;

    result_array(1) <= word_array(0) xor word_array(1) xor rcon_sig;

    result_array(2) <= word_array(0) xor word_array(1) xor word_array(2)
                   xor rcon_sig;

    result_array(3) <= word_array(0) xor word_array(1) xor word_array(2)
                   xor word_array(3) xor rcon_sig;

    key_192_gen : if KEY_NBITS_G= 192 generate
        result_array(4) <= word_array(0) xor word_array(1) xor word_array(2)
                       xor word_array(3) xor word_array(4) xor rcon_sig;

        result_array(5) <= word_array(0) xor word_array(1) xor word_array(2)
                       xor word_array(3) xor word_array(4) xor word_array(5)
                       xor rcon_sig;
    end generate;

    key_256_gen : if KEY_NBITS_G= 256 generate
        result_array(4) <= s_box_256_sig xor word_array(4);

        result_array(5) <= s_box_256_sig xor word_array(4) xor word_array(5);

        result_array(6) <= s_box_256_sig xor word_array(4) xor word_array(5)
                        xor word_array(6);

        result_array(7) <= s_box_256_sig xor word_array(4) xor word_array(5)
                        xor word_array(6) xor word_array(7);

    end generate;

    output_combine_gen : for i in 0 to Nk-1 generate
        m_key_axis_tdata(32*i+31 downto 32*i) <= result_array(Nk-1-i);
    end generate;

    process(clk)
    begin
        if (rising_edge(clk)) then

            rot_sig_reg_valid <= s_key_axis_tvalid;
            rot_sig_reg <= rot_sig;
            s_box_sig_reg_valid <= rot_sig_reg_valid;
            s_box_sig_reg <= s_box_sig;
            rcon_sig_reg_valid <= s_box_sig_reg_valid;
            rcon_sig_reg <= rcon_sig;
            s_box_256_sig_reg_valid <= rcon_sig_reg_valid;
            s_box_256_sig_reg <= s_box_256_sig;

            if (s_axis_en = '1') then
                started <= '1';
                rot_sig_reg_valid       <= '0';
                s_box_sig_reg_valid     <= '0';
                rcon_sig_reg_valid      <= '0';
                s_box_256_sig_reg_valid <= '0';
            end if;

            if (m_axis_en = '1') then
                started <= '0';
            end if;

            if (rst = '1') then
                started <= '0';
            end if;
        end if;
    end process;

end architecture;