-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 - 
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below: 
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity aes_decrypt_block is
    generic (
        KEY_NBITS_G             : integer := 256;
        HIGH_THROUGHPUT_G       : boolean := true
    );
    port (
        clk                     : in    std_logic;
        rst                     : in    std_logic;

        flush_pipeline          : in    std_logic := '0';

        s_key_axis_tvalid       : in    std_logic;
        s_key_axis_tready       : out   std_logic;
        s_key_axis_tdata        : in    std_logic_vector(KEY_NBITS_G-1 downto 0);

        m_curr_key_axis_tdata   : out   std_logic_vector(KEY_NBITS_G-1 downto 0);
        m_curr_key_axis_tvalid  : out   std_logic;

        s_axis_tvalid           : in    std_logic;
        s_axis_tready           : out   std_logic;
        s_axis_tdata            : in    std_logic_vector(127 downto 0);

        m_axis_tvalid           : out   std_logic;
        m_axis_tready           : in    std_logic;
        m_axis_tdata            : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of aes_decrypt_block is
    type word_array_t is array (0 to 16) of std_logic_vector(31 downto 0);
    type aes_array_t is array (natural range <>) of std_logic_vector(127 downto 0);

    constant Nr             : integer := KEY_NBITS_G/ 32 + 6;

    signal keys_out_valid   : std_logic_vector(Nr downto 0);
    signal keys_out         : std_logic_vector(128*(Nr+1)-1 downto 0);

    signal axis_tvalid      : std_logic_vector(Nr downto 0);
    signal axis_tready      : std_logic_vector(Nr+1 downto 0);

    signal key_cols         : word_array_t;
    signal mix_col_key      : std_logic_vector(127 downto 0);
    signal key_array        : aes_array_t(Nr downto 0);

    signal results_array    : aes_array_t(Nr downto 0);

    signal first_result     : std_logic_vector(127 downto 0);
begin

    assert HIGH_THROUGHPUT_G = TRUE report "Low Throughput mode not supported for decryption" severity failure;

    m_curr_key_axis_tdata  <= key_array(0) & key_array(1);
    m_curr_key_axis_tvalid <= keys_out_valid(0) and keys_out_valid(1);

    e_key_expansion : entity LIB_AES.aes_key_expansion
        generic map(
            KEY_NBITS_G         => KEY_NBITS_G
        )
        port map (
            clk                 => clk,
            rst                 => rst,
            s_key_axis_tvalid   => s_key_axis_tvalid,
            s_key_axis_tready   => s_key_axis_tready,
            s_key_axis_tdata    => s_key_axis_tdata,
            keys_out_valid      => keys_out_valid,
            keys_out            => keys_out
        );

    key_array_gen : for i in 0 to Nr generate
        key_array(i) <= keys_out(128*i+127 downto 128*i);
    end generate;

    first_result <= s_axis_tdata xor key_array(Nr);
    

    high_throughput_gen : if HIGH_THROUGHPUT_G = TRUE generate

        s_axis_tready <= axis_tready(0);

        axis_tready(0) <=   '0' when rst = '1' else
                            '0' when keys_out_valid(Nr) = '0' else
                            '0' when axis_tvalid(0) = '1' and axis_tready(1) = '0' else '1';

        first_round_proc : process (clk)
        begin
            if (rising_edge(clk)) then
                if (rst = '1' or flush_pipeline = '1') then
                    axis_tvalid(0) <= '0';
                    results_array(0) <= (others => '0');
                else
                    if (axis_tvalid(0) = '0') then
                        if (keys_out_valid(Nr) = '1' and s_axis_tvalid = '1') then
                            axis_tvalid(0) <= '1';
                            results_array(0) <= first_result;
                        end if;
                    else
                        if (axis_tready(1) = '1') then
                            axis_tvalid(0) <= '0';

                            if (keys_out_valid(Nr) = '1' and s_axis_tvalid = '1') then
                                axis_tvalid(0) <= '1';
                                results_array(0) <= first_result;
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end process;

        rounds_gen : for i in 1 to Nr generate
            signal local_final_round    : std_logic;
        begin
            local_final_round <= '1' when i = Nr else '0';
            e_encrypt_round : entity LIB_AES.aes_decrypt_round
                port map (
                    clk             => clk,
                    rst             => rst,
                    final_round     => local_final_round,
                    s_axis_tvalid   => axis_tvalid(i-1),
                    s_axis_tready   => axis_tready(i),
                    s_axis_tdata    => results_array(i-1),
                    key_tvalid      => keys_out_valid(Nr-i),
                    key_tdata       => key_array(Nr-i),
                    m_axis_tvalid   => axis_tvalid(i),
                    m_axis_tready   => axis_tready(i+1),
                    m_axis_tdata    => results_array(i)
                );
                
        end generate;

        m_axis_tvalid       <= axis_tvalid(Nr);
        axis_tready(Nr+1)   <= m_axis_tready;
        m_axis_tdata        <= results_array(Nr);

    end generate;  

end architecture;