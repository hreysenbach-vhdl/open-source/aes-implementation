#
# Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

stages:
  - docker_build
  - simulations

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_DEPTH: 1
  GIT_SUBMODULE_UPDATE_FLAGS: --jobs 4

docker_build:
  image: docker:latest
  stage: docker_build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - >
      if [[ ${CI_PIPELINE_SOURCE} == "merge_request_event" ]]; then
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on MR '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME': tag = $tag"
      elif [[ -n ${CI_COMMIT_TAG} ]]; then
        tag=":$CI_COMMIT_TAG"
        echo "Running tag pipeline '$CI_COMMIT_TAG': tag = $tag"
      else
        echo "Unsupported pipeline source!"
        exit 1
      fi
    - docker build --pull -t $CI_REGISTRY_IMAGE/aes_sim${tag} .
    - | 
      if [[ -n ${CI_COMMIT_TAG} ]]; then
        docker push "$CI_REGISTRY_IMAGE/aes_sim$tag"
      fi
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - Dockerfile
        - requirements.txt
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /.*docker-.*/
      exists:
        - Dockerfile
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never

block_cipher_sim:
  stage: simulations
  image: $CI_REGISTRY_IMAGE/aes_sim:docker-v2.0.0
  script:
    - make -C sim/block_cipher
  artifacts:
    reports:
      junit: sim/block_cipher/results.xml
    expire_in: 1 week
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "schedule"

aes_gcm_sim:
  stage: simulations
  image: $CI_REGISTRY_IMAGE/aes_sim:docker-v2.0.0
  script:
    - make -C sim/modes_of_operation/aes_gcm
  artifacts:
    reports:
      junit: sim/modes_of_operation/aes_gcm/results.xml
    expire_in: 1 week
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "schedule"
