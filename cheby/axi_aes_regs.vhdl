-- Do not edit.  Generated by cheby 1.6.dev0 using these options:
--  --gen-hdl=axi_aes_regs.vhdl --input reg_map.cheby
-- Generated on Mon May 29 16:13:42 2023 by cocotb


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axi_aes_regs is
  port (
    aclk                 : in    std_logic;
    areset_n             : in    std_logic;
    awvalid              : in    std_logic;
    awready              : out   std_logic;
    awaddr               : in    std_logic_vector(7 downto 0);
    awprot               : in    std_logic_vector(2 downto 0);
    wvalid               : in    std_logic;
    wready               : out   std_logic;
    wdata                : in    std_logic_vector(31 downto 0);
    wstrb                : in    std_logic_vector(3 downto 0);
    bvalid               : out   std_logic;
    bready               : in    std_logic;
    bresp                : out   std_logic_vector(1 downto 0);
    arvalid              : in    std_logic;
    arready              : out   std_logic;
    araddr               : in    std_logic_vector(7 downto 0);
    arprot               : in    std_logic_vector(2 downto 0);
    rvalid               : out   std_logic;
    rready               : in    std_logic;
    rdata                : out   std_logic_vector(31 downto 0);
    rresp                : out   std_logic_vector(1 downto 0);

    -- Control register
    -- Set to reset the module
    ctrl_reset_o         : out   std_logic;

    -- Bits 0 to 31 of the key
    key_0_31_o           : out   std_logic_vector(31 downto 0);
    key_0_31_wr_o        : out   std_logic;

    -- Bits 32 to 63 of the key
    key_32_63_o          : out   std_logic_vector(31 downto 0);

    -- Bits 64 to 95 of the key
    key_64_95_o          : out   std_logic_vector(31 downto 0);

    -- Bits 96 to 127 of the key
    key_96_127_o         : out   std_logic_vector(31 downto 0);

    -- Bits 128 to 159 of the key
    key_128_159_o        : out   std_logic_vector(31 downto 0);

    -- Bits 160 to 191 of the key
    key_160_191_o        : out   std_logic_vector(31 downto 0);

    -- Bits 192 to 223 of the key
    key_192_223_o        : out   std_logic_vector(31 downto 0);

    -- Bits 224 to 255 of the key
    key_224_255_o        : out   std_logic_vector(31 downto 0);

    -- Bits 0 to 31 of the data to encrypt
    data_in_0_31_o       : out   std_logic_vector(31 downto 0);
    data_in_0_31_wr_o    : out   std_logic;

    -- Bits 32 to 63 of the data to encrypt
    data_in_32_63_o      : out   std_logic_vector(31 downto 0);

    -- Bits 64 to 95 of the data to encrypt
    data_in_64_95_o      : out   std_logic_vector(31 downto 0);

    -- Bits 96 to 127 of the data to encrypt
    data_in_96_127_o     : out   std_logic_vector(31 downto 0);

    -- Bits 0 to 31 of the encrypted data
    data_out_0_31_i      : in    std_logic_vector(31 downto 0);
    data_out_0_31_rd_o   : out   std_logic;

    -- Bits 32 to 63 of the encrypted data
    data_out_32_63_i     : in    std_logic_vector(31 downto 0);

    -- Bits 64 to 95 of the encrypted data
    data_out_64_95_i     : in    std_logic_vector(31 downto 0);

    -- Bits 96 to 127 of the encrypted data
    data_out_96_127_i    : in    std_logic_vector(31 downto 0);

    -- version reg
    version_i            : in    std_logic_vector(31 downto 0);

    -- ID reg
    identification_i     : in    std_logic_vector(31 downto 0)
  );
end axi_aes_regs;

architecture syn of axi_aes_regs is
  signal wr_req                         : std_logic;
  signal wr_ack                         : std_logic;
  signal wr_addr                        : std_logic_vector(7 downto 2);
  signal wr_data                        : std_logic_vector(31 downto 0);
  signal axi_awset                      : std_logic;
  signal axi_wset                       : std_logic;
  signal axi_wdone                      : std_logic;
  signal rd_req                         : std_logic;
  signal rd_ack                         : std_logic;
  signal rd_addr                        : std_logic_vector(7 downto 2);
  signal rd_data                        : std_logic_vector(31 downto 0);
  signal axi_arset                      : std_logic;
  signal axi_rdone                      : std_logic;
  signal ctrl_reset_reg                 : std_logic;
  signal ctrl_wreq                      : std_logic;
  signal ctrl_wack                      : std_logic;
  signal key_0_31_reg                   : std_logic_vector(31 downto 0);
  signal key_0_31_wreq                  : std_logic;
  signal key_0_31_wack                  : std_logic;
  signal key_32_63_reg                  : std_logic_vector(31 downto 0);
  signal key_32_63_wreq                 : std_logic;
  signal key_32_63_wack                 : std_logic;
  signal key_64_95_reg                  : std_logic_vector(31 downto 0);
  signal key_64_95_wreq                 : std_logic;
  signal key_64_95_wack                 : std_logic;
  signal key_96_127_reg                 : std_logic_vector(31 downto 0);
  signal key_96_127_wreq                : std_logic;
  signal key_96_127_wack                : std_logic;
  signal key_128_159_reg                : std_logic_vector(31 downto 0);
  signal key_128_159_wreq               : std_logic;
  signal key_128_159_wack               : std_logic;
  signal key_160_191_reg                : std_logic_vector(31 downto 0);
  signal key_160_191_wreq               : std_logic;
  signal key_160_191_wack               : std_logic;
  signal key_192_223_reg                : std_logic_vector(31 downto 0);
  signal key_192_223_wreq               : std_logic;
  signal key_192_223_wack               : std_logic;
  signal key_224_255_reg                : std_logic_vector(31 downto 0);
  signal key_224_255_wreq               : std_logic;
  signal key_224_255_wack               : std_logic;
  signal data_in_0_31_reg               : std_logic_vector(31 downto 0);
  signal data_in_0_31_wreq              : std_logic;
  signal data_in_0_31_wack              : std_logic;
  signal data_in_32_63_reg              : std_logic_vector(31 downto 0);
  signal data_in_32_63_wreq             : std_logic;
  signal data_in_32_63_wack             : std_logic;
  signal data_in_64_95_reg              : std_logic_vector(31 downto 0);
  signal data_in_64_95_wreq             : std_logic;
  signal data_in_64_95_wack             : std_logic;
  signal data_in_96_127_reg             : std_logic_vector(31 downto 0);
  signal data_in_96_127_wreq            : std_logic;
  signal data_in_96_127_wack            : std_logic;
  signal rd_ack_d0                      : std_logic;
  signal rd_dat_d0                      : std_logic_vector(31 downto 0);
  signal wr_req_d0                      : std_logic;
  signal wr_adr_d0                      : std_logic_vector(7 downto 2);
  signal wr_dat_d0                      : std_logic_vector(31 downto 0);
begin

  -- AW, W and B channels
  awready <= not axi_awset;
  wready <= not axi_wset;
  bvalid <= axi_wdone;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        wr_req <= '0';
        axi_awset <= '0';
        axi_wset <= '0';
        axi_wdone <= '0';
      else
        wr_req <= '0';
        if awvalid = '1' and axi_awset = '0' then
          wr_addr <= awaddr(7 downto 2);
          axi_awset <= '1';
          wr_req <= axi_wset;
        end if;
        if wvalid = '1' and axi_wset = '0' then
          wr_data <= wdata;
          axi_wset <= '1';
          wr_req <= axi_awset or awvalid;
        end if;
        if (axi_wdone and bready) = '1' then
          axi_wset <= '0';
          axi_awset <= '0';
          axi_wdone <= '0';
        end if;
        if wr_ack = '1' then
          axi_wdone <= '1';
        end if;
      end if;
    end if;
  end process;
  bresp <= "00";

  -- AR and R channels
  arready <= not axi_arset;
  rvalid <= axi_rdone;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        rd_req <= '0';
        axi_arset <= '0';
        axi_rdone <= '0';
        rdata <= (others => '0');
      else
        rd_req <= '0';
        if arvalid = '1' and axi_arset = '0' then
          rd_addr <= araddr(7 downto 2);
          axi_arset <= '1';
          rd_req <= '1';
        end if;
        if (axi_rdone and rready) = '1' then
          axi_arset <= '0';
          axi_rdone <= '0';
        end if;
        if rd_ack = '1' then
          axi_rdone <= '1';
          rdata <= rd_data;
        end if;
      end if;
    end if;
  end process;
  rresp <= "00";

  -- pipelining for wr-in+rd-out
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        rd_ack <= '0';
        wr_req_d0 <= '0';
      else
        rd_ack <= rd_ack_d0;
        rd_data <= rd_dat_d0;
        wr_req_d0 <= wr_req;
        wr_adr_d0 <= wr_addr;
        wr_dat_d0 <= wr_data;
      end if;
    end if;
  end process;

  -- Register ctrl
  ctrl_reset_o <= ctrl_reset_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        ctrl_reset_reg <= '0';
        ctrl_wack <= '0';
      else
        if ctrl_wreq = '1' then
          ctrl_reset_reg <= wr_dat_d0(0);
        else
          ctrl_reset_reg <= '0';
        end if;
        ctrl_wack <= ctrl_wreq;
      end if;
    end if;
  end process;

  -- Register key_0_31
  key_0_31_o <= key_0_31_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_0_31_reg <= "00000000000000000000000000000000";
        key_0_31_wack <= '0';
      else
        if key_0_31_wreq = '1' then
          key_0_31_reg <= wr_dat_d0;
        end if;
        key_0_31_wack <= key_0_31_wreq;
      end if;
    end if;
  end process;
  key_0_31_wr_o <= key_0_31_wack;

  -- Register key_32_63
  key_32_63_o <= key_32_63_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_32_63_reg <= "00000000000000000000000000000000";
        key_32_63_wack <= '0';
      else
        if key_32_63_wreq = '1' then
          key_32_63_reg <= wr_dat_d0;
        end if;
        key_32_63_wack <= key_32_63_wreq;
      end if;
    end if;
  end process;

  -- Register key_64_95
  key_64_95_o <= key_64_95_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_64_95_reg <= "00000000000000000000000000000000";
        key_64_95_wack <= '0';
      else
        if key_64_95_wreq = '1' then
          key_64_95_reg <= wr_dat_d0;
        end if;
        key_64_95_wack <= key_64_95_wreq;
      end if;
    end if;
  end process;

  -- Register key_96_127
  key_96_127_o <= key_96_127_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_96_127_reg <= "00000000000000000000000000000000";
        key_96_127_wack <= '0';
      else
        if key_96_127_wreq = '1' then
          key_96_127_reg <= wr_dat_d0;
        end if;
        key_96_127_wack <= key_96_127_wreq;
      end if;
    end if;
  end process;

  -- Register key_128_159
  key_128_159_o <= key_128_159_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_128_159_reg <= "00000000000000000000000000000000";
        key_128_159_wack <= '0';
      else
        if key_128_159_wreq = '1' then
          key_128_159_reg <= wr_dat_d0;
        end if;
        key_128_159_wack <= key_128_159_wreq;
      end if;
    end if;
  end process;

  -- Register key_160_191
  key_160_191_o <= key_160_191_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_160_191_reg <= "00000000000000000000000000000000";
        key_160_191_wack <= '0';
      else
        if key_160_191_wreq = '1' then
          key_160_191_reg <= wr_dat_d0;
        end if;
        key_160_191_wack <= key_160_191_wreq;
      end if;
    end if;
  end process;

  -- Register key_192_223
  key_192_223_o <= key_192_223_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_192_223_reg <= "00000000000000000000000000000000";
        key_192_223_wack <= '0';
      else
        if key_192_223_wreq = '1' then
          key_192_223_reg <= wr_dat_d0;
        end if;
        key_192_223_wack <= key_192_223_wreq;
      end if;
    end if;
  end process;

  -- Register key_224_255
  key_224_255_o <= key_224_255_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        key_224_255_reg <= "00000000000000000000000000000000";
        key_224_255_wack <= '0';
      else
        if key_224_255_wreq = '1' then
          key_224_255_reg <= wr_dat_d0;
        end if;
        key_224_255_wack <= key_224_255_wreq;
      end if;
    end if;
  end process;

  -- Register data_in_0_31
  data_in_0_31_o <= data_in_0_31_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        data_in_0_31_reg <= "00000000000000000000000000000000";
        data_in_0_31_wack <= '0';
      else
        if data_in_0_31_wreq = '1' then
          data_in_0_31_reg <= wr_dat_d0;
        end if;
        data_in_0_31_wack <= data_in_0_31_wreq;
      end if;
    end if;
  end process;
  data_in_0_31_wr_o <= data_in_0_31_wack;

  -- Register data_in_32_63
  data_in_32_63_o <= data_in_32_63_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        data_in_32_63_reg <= "00000000000000000000000000000000";
        data_in_32_63_wack <= '0';
      else
        if data_in_32_63_wreq = '1' then
          data_in_32_63_reg <= wr_dat_d0;
        end if;
        data_in_32_63_wack <= data_in_32_63_wreq;
      end if;
    end if;
  end process;

  -- Register data_in_64_95
  data_in_64_95_o <= data_in_64_95_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        data_in_64_95_reg <= "00000000000000000000000000000000";
        data_in_64_95_wack <= '0';
      else
        if data_in_64_95_wreq = '1' then
          data_in_64_95_reg <= wr_dat_d0;
        end if;
        data_in_64_95_wack <= data_in_64_95_wreq;
      end if;
    end if;
  end process;

  -- Register data_in_96_127
  data_in_96_127_o <= data_in_96_127_reg;
  process (aclk) begin
    if rising_edge(aclk) then
      if areset_n = '0' then
        data_in_96_127_reg <= "00000000000000000000000000000000";
        data_in_96_127_wack <= '0';
      else
        if data_in_96_127_wreq = '1' then
          data_in_96_127_reg <= wr_dat_d0;
        end if;
        data_in_96_127_wack <= data_in_96_127_wreq;
      end if;
    end if;
  end process;

  -- Register data_out_0_31

  -- Register data_out_32_63

  -- Register data_out_64_95

  -- Register data_out_96_127

  -- Register version

  -- Register identification

  -- Process for write requests.
  process (wr_adr_d0, wr_req_d0, ctrl_wack, key_0_31_wack, key_32_63_wack,
           key_64_95_wack, key_96_127_wack, key_128_159_wack, key_160_191_wack,
           key_192_223_wack, key_224_255_wack, data_in_0_31_wack,
           data_in_32_63_wack, data_in_64_95_wack, data_in_96_127_wack) begin
    ctrl_wreq <= '0';
    key_0_31_wreq <= '0';
    key_32_63_wreq <= '0';
    key_64_95_wreq <= '0';
    key_96_127_wreq <= '0';
    key_128_159_wreq <= '0';
    key_160_191_wreq <= '0';
    key_192_223_wreq <= '0';
    key_224_255_wreq <= '0';
    data_in_0_31_wreq <= '0';
    data_in_32_63_wreq <= '0';
    data_in_64_95_wreq <= '0';
    data_in_96_127_wreq <= '0';
    case wr_adr_d0(7 downto 2) is
    when "000000" =>
      -- Reg ctrl
      ctrl_wreq <= wr_req_d0;
      wr_ack <= ctrl_wack;
    when "000100" =>
      -- Reg key_0_31
      key_0_31_wreq <= wr_req_d0;
      wr_ack <= key_0_31_wack;
    when "000101" =>
      -- Reg key_32_63
      key_32_63_wreq <= wr_req_d0;
      wr_ack <= key_32_63_wack;
    when "000110" =>
      -- Reg key_64_95
      key_64_95_wreq <= wr_req_d0;
      wr_ack <= key_64_95_wack;
    when "000111" =>
      -- Reg key_96_127
      key_96_127_wreq <= wr_req_d0;
      wr_ack <= key_96_127_wack;
    when "001000" =>
      -- Reg key_128_159
      key_128_159_wreq <= wr_req_d0;
      wr_ack <= key_128_159_wack;
    when "001001" =>
      -- Reg key_160_191
      key_160_191_wreq <= wr_req_d0;
      wr_ack <= key_160_191_wack;
    when "001010" =>
      -- Reg key_192_223
      key_192_223_wreq <= wr_req_d0;
      wr_ack <= key_192_223_wack;
    when "001011" =>
      -- Reg key_224_255
      key_224_255_wreq <= wr_req_d0;
      wr_ack <= key_224_255_wack;
    when "001100" =>
      -- Reg data_in_0_31
      data_in_0_31_wreq <= wr_req_d0;
      wr_ack <= data_in_0_31_wack;
    when "001101" =>
      -- Reg data_in_32_63
      data_in_32_63_wreq <= wr_req_d0;
      wr_ack <= data_in_32_63_wack;
    when "001110" =>
      -- Reg data_in_64_95
      data_in_64_95_wreq <= wr_req_d0;
      wr_ack <= data_in_64_95_wack;
    when "001111" =>
      -- Reg data_in_96_127
      data_in_96_127_wreq <= wr_req_d0;
      wr_ack <= data_in_96_127_wack;
    when "010000" =>
      -- Reg data_out_0_31
      wr_ack <= wr_req_d0;
    when "010001" =>
      -- Reg data_out_32_63
      wr_ack <= wr_req_d0;
    when "010010" =>
      -- Reg data_out_64_95
      wr_ack <= wr_req_d0;
    when "010011" =>
      -- Reg data_out_96_127
      wr_ack <= wr_req_d0;
    when "111110" =>
      -- Reg version
      wr_ack <= wr_req_d0;
    when "111111" =>
      -- Reg identification
      wr_ack <= wr_req_d0;
    when others =>
      wr_ack <= wr_req_d0;
    end case;
  end process;

  -- Process for read requests.
  process (rd_addr, rd_req, key_0_31_reg, key_32_63_reg, key_64_95_reg,
           key_96_127_reg, key_128_159_reg, key_160_191_reg, key_192_223_reg,
           key_224_255_reg, data_in_0_31_reg, data_in_32_63_reg,
           data_in_64_95_reg, data_in_96_127_reg, data_out_0_31_i,
           data_out_32_63_i, data_out_64_95_i, data_out_96_127_i, version_i,
           identification_i) begin
    -- By default ack read requests
    rd_dat_d0 <= (others => 'X');
    data_out_0_31_rd_o <= '0';
    case rd_addr(7 downto 2) is
    when "000000" =>
      -- Reg ctrl
      rd_ack_d0 <= rd_req;
      rd_dat_d0(0) <= '0';
      rd_dat_d0(31 downto 1) <= (others => '0');
    when "000100" =>
      -- Reg key_0_31
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_0_31_reg;
    when "000101" =>
      -- Reg key_32_63
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_32_63_reg;
    when "000110" =>
      -- Reg key_64_95
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_64_95_reg;
    when "000111" =>
      -- Reg key_96_127
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_96_127_reg;
    when "001000" =>
      -- Reg key_128_159
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_128_159_reg;
    when "001001" =>
      -- Reg key_160_191
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_160_191_reg;
    when "001010" =>
      -- Reg key_192_223
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_192_223_reg;
    when "001011" =>
      -- Reg key_224_255
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= key_224_255_reg;
    when "001100" =>
      -- Reg data_in_0_31
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_in_0_31_reg;
    when "001101" =>
      -- Reg data_in_32_63
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_in_32_63_reg;
    when "001110" =>
      -- Reg data_in_64_95
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_in_64_95_reg;
    when "001111" =>
      -- Reg data_in_96_127
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_in_96_127_reg;
    when "010000" =>
      -- Reg data_out_0_31
      data_out_0_31_rd_o <= rd_req;
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_out_0_31_i;
    when "010001" =>
      -- Reg data_out_32_63
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_out_32_63_i;
    when "010010" =>
      -- Reg data_out_64_95
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_out_64_95_i;
    when "010011" =>
      -- Reg data_out_96_127
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= data_out_96_127_i;
    when "111110" =>
      -- Reg version
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= version_i;
    when "111111" =>
      -- Reg identification
      rd_ack_d0 <= rd_req;
      rd_dat_d0 <= identification_i;
    when others =>
      rd_ack_d0 <= rd_req;
    end case;
  end process;
end syn;
