memory-map:
  bus: axi4-lite-32
  size: 256
  name: axi_aes_regs
  description: AES Encrypt AXI Lite Registers
  x-hdl:
      bus-granularity: byte
  children:
    - reg:
        name: ctrl
        comment: Control register
        width: 32
        access: rw
        address: 0x00
        children:
          - field:
              name: reset
              comment: Set to reset the module
              description: >
                Performs a full reset on the module
              range: 0
              x-hdl:
                type: autoclear
    - reg:
        name: key_0_31
        comment: Bits 0 to 31 of the key
        width: 32
        access: rw
        address: 0x10
        x-hdl:
          write-strobe: True
    - reg:
        name: key_32_63
        comment: Bits 32 to 63 of the key
        width: 32
        access: rw
        address: 0x14
    - reg:
        name: key_64_95
        comment: Bits 64 to 95 of the key
        width: 32
        access: rw
        address: 0x18
    - reg:
        name: key_96_127
        comment: Bits 96 to 127 of the key
        width: 32
        access: rw
        address: 0x1C
    - reg:
        name: key_128_159
        comment: Bits 128 to 159 of the key
        width: 32
        access: rw
        address: 0x20
    - reg:
        name: key_160_191
        comment: Bits 160 to 191 of the key
        width: 32
        access: rw
        address: 0x24
    - reg:
        name: key_192_223
        comment: Bits 192 to 223 of the key
        width: 32
        access: rw
        address: 0x28
    - reg:
        name: key_224_255
        comment: Bits 224 to 255 of the key
        width: 32
        access: rw
        address: 0x2C

    - reg:
        name: data_in_0_31
        comment: Bits 0 to 31 of the data to encrypt
        width: 32
        access: rw
        address: 0x30
        x-hdl:
          write-strobe: True
    - reg:
        name: data_in_32_63
        comment: Bits 32 to 63 of the data to encrypt
        width: 32
        access: rw
        address: 0x34
    - reg:
        name: data_in_64_95
        comment: Bits 64 to 95 of the data to encrypt
        width: 32
        access: rw
        address: 0x38
    - reg:
        name: data_in_96_127
        comment: Bits 96 to 127 of the data to encrypt
        width: 32
        access: rw
        address: 0x3C

    - reg:
        name: data_out_0_31
        comment: Bits 0 to 31 of the encrypted data
        width: 32
        access: ro
        address: 0x40
        x-hdl:
          read-strobe: True
    - reg:
        name: data_out_32_63
        comment: Bits 32 to 63 of the encrypted data
        width: 32
        access: ro
        address: 0x44
    - reg:
        name: data_out_64_95
        comment: Bits 64 to 95 of the encrypted data
        width: 32
        access: ro
        address: 0x48
    - reg:
        name: data_out_96_127
        comment: Bits 96 to 127 of the encrypted data
        width: 32
        access: ro
        address: 0x4C

    - reg:
        name: version
        comment: version reg
        width: 32
        access: ro
        address: 0xF8
        # 8 bits of major ID and 16 bits of minor and 8 bits for patch
        preset: 0x00000001
    - reg:
        name: identification
        comment: ID reg
        width: 32
        access: ro
        address: 0xFC
        # 8 bits of library ID and 16 bits of IP ID
        preset: 0x01000001
