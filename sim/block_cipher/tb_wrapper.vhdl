-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;

library LIB_AES;
use LIB_AES.aes_block_cipher_pkg.all;

entity tb_wrapper is
    generic (
        DEBUG_G                         : boolean := true
    );
    port (
        clk                                 : in    std_logic;
        rst                                 : in    std_logic;

        -- Encrypt Round
        aes_enc_rnd_sink_tvalid             : in    std_logic;
        aes_enc_rnd_sink_tready             : out   std_logic;
        aes_enc_rnd_sink_tdata              : in    std_logic_vector(127 downto 0);

        aes_enc_rnd_key_tvalid              : in    std_logic;
        aes_enc_rnd_key_tdata               : in    std_logic_vector(127 downto 0);

        aes_enc_rnd_src_tvalid              : out   std_logic;
        aes_enc_rnd_src_tready              : in    std_logic;
        aes_enc_rnd_src_tdata               : out   std_logic_vector(127 downto 0);

        -- Decrypt Round
        aes_dec_rnd_sink_tvalid             : in    std_logic;
        aes_dec_rnd_sink_tready             : out   std_logic;
        aes_dec_rnd_sink_tdata              : in    std_logic_vector(127 downto 0);

        aes_dec_rnd_key_tvalid              : in    std_logic;
        aes_dec_rnd_key_tdata               : in    std_logic_vector(127 downto 0);

        aes_dec_rnd_src_tvalid              : out   std_logic;
        aes_dec_rnd_src_tready              : in    std_logic;
        aes_dec_rnd_src_tdata               : out   std_logic_vector(127 downto 0);

        -- Key Expansion
        aes_key_exp_128_in                  : in    std_logic_vector(127 downto 0);
        aes_key_exp_128_valid               : in    std_logic;
        aes_key_exp_128_out                 : out   std_logic_vector(128*(128/32+7)-1 downto 0);
        aes_key_exp_128_out_valid           : out   std_logic_vector(10 downto 0);

        aes_key_exp_192_in                  : in    std_logic_vector(191 downto 0);
        aes_key_exp_192_valid               : in    std_logic;
        aes_key_exp_192_out                 : out   std_logic_vector(128*(192/32+7)-1 downto 0);
        aes_key_exp_192_out_valid           : out   std_logic_vector(12 downto 0);

        aes_key_exp_256_in                  : in    std_logic_vector(255 downto 0);
        aes_key_exp_256_valid               : in    std_logic;
        aes_key_exp_256_out                 : out   std_logic_vector(128*(256/32+7)-1 downto 0);
        aes_key_exp_256_out_valid           : out   std_logic_vector(14 downto 0);

        -- Encrypt
        aes128_block_enc_key_tvalid         : in    std_logic;
        aes128_block_enc_key_tdata          : in    std_logic_vector(127 downto 0);

        aes128_block_enc_s_axis_tvalid      : in    std_logic;
        aes128_block_enc_s_axis_tready      : out   std_logic;
        aes128_block_enc_s_axis_tdata       : in    std_logic_vector(127 downto 0);

        aes128_block_enc_m_axis_tvalid      : out   std_logic;
        aes128_block_enc_m_axis_tready      : in    std_logic;
        aes128_block_enc_m_axis_tdata       : out   std_logic_vector(127 downto 0);

        aes192_block_enc_key_tvalid         : in    std_logic;
        aes192_block_enc_key_tdata          : in    std_logic_vector(191 downto 0);

        aes192_block_enc_s_axis_tvalid      : in    std_logic;
        aes192_block_enc_s_axis_tready      : out   std_logic;
        aes192_block_enc_s_axis_tdata       : in    std_logic_vector(127 downto 0);

        aes192_block_enc_m_axis_tvalid      : out   std_logic;
        aes192_block_enc_m_axis_tready      : in    std_logic;
        aes192_block_enc_m_axis_tdata       : out   std_logic_vector(127 downto 0);

        aes256_block_enc_key_tvalid         : in    std_logic;
        aes256_block_enc_key_tdata          : in    std_logic_vector(255 downto 0);

        aes256_block_enc_s_axis_tvalid      : in    std_logic;
        aes256_block_enc_s_axis_tready      : out   std_logic;
        aes256_block_enc_s_axis_tdata       : in    std_logic_vector(127 downto 0);

        aes256_block_enc_m_axis_tvalid      : out   std_logic;
        aes256_block_enc_m_axis_tready      : in    std_logic;
        aes256_block_enc_m_axis_tdata       : out   std_logic_vector(127 downto 0);

        -- Low throughput
        aes128_low_block_enc_key_tvalid     : in    std_logic;
        aes128_low_block_enc_key_tdata      : in    std_logic_vector(127 downto 0);

        aes128_low_block_enc_s_axis_tvalid  : in    std_logic;
        aes128_low_block_enc_s_axis_tready  : out   std_logic;
        aes128_low_block_enc_s_axis_tdata   : in    std_logic_vector(127 downto 0);

        aes128_low_block_enc_m_axis_tvalid  : out   std_logic;
        aes128_low_block_enc_m_axis_tready  : in    std_logic;
        aes128_low_block_enc_m_axis_tdata   : out   std_logic_vector(127 downto 0);

        aes192_low_block_enc_key_tvalid     : in    std_logic;
        aes192_low_block_enc_key_tdata      : in    std_logic_vector(191 downto 0);

        aes192_low_block_enc_s_axis_tvalid  : in    std_logic;
        aes192_low_block_enc_s_axis_tready  : out   std_logic;
        aes192_low_block_enc_s_axis_tdata   : in    std_logic_vector(127 downto 0);

        aes192_low_block_enc_m_axis_tvalid  : out   std_logic;
        aes192_low_block_enc_m_axis_tready  : in    std_logic;
        aes192_low_block_enc_m_axis_tdata   : out   std_logic_vector(127 downto 0);

        aes256_low_block_enc_key_tvalid     : in    std_logic;
        aes256_low_block_enc_key_tdata      : in    std_logic_vector(255 downto 0);

        aes256_low_block_enc_s_axis_tvalid  : in    std_logic;
        aes256_low_block_enc_s_axis_tready  : out   std_logic;
        aes256_low_block_enc_s_axis_tdata   : in    std_logic_vector(127 downto 0);

        aes256_low_block_enc_m_axis_tvalid  : out   std_logic;
        aes256_low_block_enc_m_axis_tready  : in    std_logic;
        aes256_low_block_enc_m_axis_tdata   : out   std_logic_vector(127 downto 0);

        -- Decrypt
        aes128_block_dec_key_tvalid         : in    std_logic;
        aes128_block_dec_key_tdata          : in    std_logic_vector(127 downto 0);

        aes128_block_dec_s_axis_tvalid      : in    std_logic;
        aes128_block_dec_s_axis_tready      : out   std_logic;
        aes128_block_dec_s_axis_tdata       : in    std_logic_vector(127 downto 0);

        aes128_block_dec_m_axis_tvalid      : out   std_logic;
        aes128_block_dec_m_axis_tready      : in    std_logic;
        aes128_block_dec_m_axis_tdata       : out   std_logic_vector(127 downto 0);

        aes192_block_dec_key_tvalid         : in    std_logic;
        aes192_block_dec_key_tdata          : in    std_logic_vector(191 downto 0);

        aes192_block_dec_s_axis_tvalid      : in    std_logic;
        aes192_block_dec_s_axis_tready      : out   std_logic;
        aes192_block_dec_s_axis_tdata       : in    std_logic_vector(127 downto 0);

        aes192_block_dec_m_axis_tvalid      : out   std_logic;
        aes192_block_dec_m_axis_tready      : in    std_logic;
        aes192_block_dec_m_axis_tdata       : out   std_logic_vector(127 downto 0);

        aes256_block_dec_key_tvalid         : in    std_logic;
        aes256_block_dec_key_tdata          : in    std_logic_vector(255 downto 0);

        aes256_block_dec_s_axis_tvalid      : in    std_logic;
        aes256_block_dec_s_axis_tready      : out   std_logic;
        aes256_block_dec_s_axis_tdata       : in    std_logic_vector(127 downto 0);

        aes256_block_dec_m_axis_tvalid      : out   std_logic;
        aes256_block_dec_m_axis_tready      : in    std_logic;
        aes256_block_dec_m_axis_tdata       : out   std_logic_vector(127 downto 0);

        -- Low Throughput
        aes128_low_block_dec_key_tvalid     : in    std_logic;
        aes128_low_block_dec_key_tdata      : in    std_logic_vector(127 downto 0);

        aes128_low_block_dec_s_axis_tvalid  : in    std_logic;
        aes128_low_block_dec_s_axis_tready  : out   std_logic;
        aes128_low_block_dec_s_axis_tdata   : in    std_logic_vector(127 downto 0);

        aes128_low_block_dec_m_axis_tvalid  : out   std_logic;
        aes128_low_block_dec_m_axis_tready  : in    std_logic;
        aes128_low_block_dec_m_axis_tdata   : out   std_logic_vector(127 downto 0);

        aes192_low_block_dec_key_tvalid     : in    std_logic;
        aes192_low_block_dec_key_tdata      : in    std_logic_vector(191 downto 0);

        aes192_low_block_dec_s_axis_tvalid  : in    std_logic;
        aes192_low_block_dec_s_axis_tready  : out   std_logic;
        aes192_low_block_dec_s_axis_tdata   : in    std_logic_vector(127 downto 0);

        aes192_low_block_dec_m_axis_tvalid  : out   std_logic;
        aes192_low_block_dec_m_axis_tready  : in    std_logic;
        aes192_low_block_dec_m_axis_tdata   : out   std_logic_vector(127 downto 0);

        aes256_low_block_dec_key_tvalid     : in    std_logic;
        aes256_low_block_dec_key_tdata      : in    std_logic_vector(255 downto 0);

        aes256_low_block_dec_s_axis_tvalid  : in    std_logic;
        aes256_low_block_dec_s_axis_tready  : out   std_logic;
        aes256_low_block_dec_s_axis_tdata   : in    std_logic_vector(127 downto 0);

        aes256_low_block_dec_m_axis_tvalid  : out   std_logic;
        aes256_low_block_dec_m_axis_tready  : in    std_logic;
        aes256_low_block_dec_m_axis_tdata   : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of tb_wrapper is
    signal s_box_vector         : std_logic_vector(127 downto 0);
    signal rot_vector           : std_logic_vector(127 downto 0);
    signal mix_col_vector       : std_logic_vector(127 downto 0);

begin

    debug_gen : if DEBUG_G = true generate
        e_aes_encrypt_round : entity LIB_AES.aes_encrypt_round(rtl)
            port map (
                clk                     => clk,
                rst                     => rst,

                final_round             => '0',

                s_axis_tvalid           => aes_enc_rnd_sink_tvalid,
                s_axis_tready           => aes_enc_rnd_sink_tready,
                s_axis_tdata            => aes_enc_rnd_sink_tdata,

                key_tvalid              => aes_enc_rnd_key_tvalid,
                key_tdata               => aes_enc_rnd_key_tdata,

                m_axis_tvalid           => aes_enc_rnd_src_tvalid,
                m_axis_tready           => aes_enc_rnd_src_tready,
                m_axis_tdata            => aes_enc_rnd_src_tdata
            );

        e_aes_decrypt_round : entity LIB_AES.aes_decrypt_round(rtl)
            port map (
                clk                     => clk,
                rst                     => rst,

                final_round             => '0',

                s_axis_tvalid           => aes_dec_rnd_sink_tvalid,
                s_axis_tready           => aes_dec_rnd_sink_tready,
                s_axis_tdata            => aes_dec_rnd_sink_tdata,

                key_tvalid              => aes_dec_rnd_key_tvalid,
                key_tdata               => aes_dec_rnd_key_tdata,

                m_axis_tvalid           => aes_dec_rnd_src_tvalid,
                m_axis_tready           => aes_dec_rnd_src_tready,
                m_axis_tdata            => aes_dec_rnd_src_tdata
            );

        e_key128_expansion : entity LIB_AES.aes_key_expansion(rtl)
            generic map(
                KEY_NBITS_G         => 128
            )
            port map (
                clk                 => clk,
                rst                 => rst,
                s_key_axis_tvalid   => aes_key_exp_128_valid,
                s_key_axis_tdata    => aes_key_exp_128_in,
                keys_out_valid      => aes_key_exp_128_out_valid,
                keys_out            => aes_key_exp_128_out
            );

        e_key192_expansion : entity LIB_AES.aes_key_expansion(rtl)
            generic map(
                KEY_NBITS_G         => 192
            )
            port map (
                clk                 => clk,
                rst                 => rst,
                s_key_axis_tvalid   => aes_key_exp_192_valid,
                s_key_axis_tdata    => aes_key_exp_192_in,
                keys_out_valid      => aes_key_exp_192_out_valid,
                keys_out            => aes_key_exp_192_out
            );

        e_key256_expansion : entity LIB_AES.aes_key_expansion(rtl)
            generic map(
                KEY_NBITS_G         => 256
            )
            port map (
                clk                 => clk,
                rst                 => rst,
                s_key_axis_tvalid   => aes_key_exp_256_valid,
                s_key_axis_tdata    => aes_key_exp_256_in,
                keys_out_valid      => aes_key_exp_256_out_valid,
                keys_out            => aes_key_exp_256_out
            );
    end generate;

    -- e_aes128_encrypt_block : entity LIB_AES.aes_encrypt_block
    --     generic map (
    --         KEY_NBITS_G     => 128
    --     )
    --     port map (
    --         clk                 => clk,
    --         rst                 => rst,

    --         s_key_axis_tvalid   => aes128_block_enc_key_tvalid,
    --         s_key_axis_tdata    => aes128_block_enc_key_tdata,

    --         s_axis_tvalid       => aes128_block_enc_s_axis_tvalid,
    --         s_axis_tready       => aes128_block_enc_s_axis_tready,
    --         s_axis_tdata        => aes128_block_enc_s_axis_tdata,

    --         m_axis_tvalid       => aes128_block_enc_m_axis_tvalid,
    --         m_axis_tready       => aes128_block_enc_m_axis_tready,
    --         m_axis_tdata        => aes128_block_enc_m_axis_tdata
    --     );

    -- e_aes192_encrypt_block : entity LIB_AES.aes_encrypt_block
    --     generic map (
    --         KEY_NBITS_G     => 192
    --     )
    --     port map (
    --         clk                 => clk,
    --         rst                 => rst,

    --         s_key_axis_tvalid   => aes192_block_enc_key_tvalid,
    --         s_key_axis_tdata    => aes192_block_enc_key_tdata,

    --         s_axis_tvalid       => aes192_block_enc_s_axis_tvalid,
    --         s_axis_tready       => aes192_block_enc_s_axis_tready,
    --         s_axis_tdata        => aes192_block_enc_s_axis_tdata,

    --         m_axis_tvalid       => aes192_block_enc_m_axis_tvalid,
    --         m_axis_tready       => aes192_block_enc_m_axis_tready,
    --         m_axis_tdata        => aes192_block_enc_m_axis_tdata
    --     );

    e_aes256_encrypt_block : entity LIB_AES.aes_encrypt_block
        generic map (
            KEY_NBITS_G     => 256
        )
        port map (
            clk                 => clk,
            rst                 => rst,

            s_key_axis_tvalid   => aes256_block_enc_key_tvalid,
            s_key_axis_tdata    => aes256_block_enc_key_tdata,

            s_axis_tvalid       => aes256_block_enc_s_axis_tvalid,
            s_axis_tready       => aes256_block_enc_s_axis_tready,
            s_axis_tdata        => aes256_block_enc_s_axis_tdata,

            m_axis_tvalid       => aes256_block_enc_m_axis_tvalid,
            m_axis_tready       => aes256_block_enc_m_axis_tready,
            m_axis_tdata        => aes256_block_enc_m_axis_tdata
        );

    -- e_aes128_low_encrypt_block : entity LIB_AES.aes_encrypt_block
    --     generic map (
    --         KEY_NBITS_G         => 128,
    --         HIGH_THROUGHPUT_G   => false
    --     )
    --     port map (
    --         clk                 => clk,
    --         rst                 => rst,

    --         s_key_axis_tvalid   => aes128_low_block_enc_key_tvalid,
    --         s_key_axis_tdata    => aes128_low_block_enc_key_tdata,

    --         s_axis_tvalid       => aes128_low_block_enc_s_axis_tvalid,
    --         s_axis_tready       => aes128_low_block_enc_s_axis_tready,
    --         s_axis_tdata        => aes128_low_block_enc_s_axis_tdata,

    --         m_axis_tvalid       => aes128_low_block_enc_m_axis_tvalid,
    --         m_axis_tready       => aes128_low_block_enc_m_axis_tready,
    --         m_axis_tdata        => aes128_low_block_enc_m_axis_tdata
    --     );

    -- e_aes192_low_encrypt_block : entity LIB_AES.aes_encrypt_block
    --     generic map (
    --         KEY_NBITS_G         => 192,
    --         HIGH_THROUGHPUT_G   => false
    --     )
    --     port map (
    --         clk                 => clk,
    --         rst                 => rst,

    --         s_key_axis_tvalid   => aes192_low_block_enc_key_tvalid,
    --         s_key_axis_tdata    => aes192_low_block_enc_key_tdata,

    --         s_axis_tvalid       => aes192_low_block_enc_s_axis_tvalid,
    --         s_axis_tready       => aes192_low_block_enc_s_axis_tready,
    --         s_axis_tdata        => aes192_low_block_enc_s_axis_tdata,

    --         m_axis_tvalid       => aes192_low_block_enc_m_axis_tvalid,
    --         m_axis_tready       => aes192_low_block_enc_m_axis_tready,
    --         m_axis_tdata        => aes192_low_block_enc_m_axis_tdata
    --     );

    e_aes256_low_encrypt_block : entity LIB_AES.aes_encrypt_block
        generic map (
            KEY_NBITS_G         => 256,
            HIGH_THROUGHPUT_G   => false
        )
        port map (
            clk                 => clk,
            rst                 => rst,

            s_key_axis_tvalid   => aes256_low_block_enc_key_tvalid,
            s_key_axis_tdata    => aes256_low_block_enc_key_tdata,

            s_axis_tvalid       => aes256_low_block_enc_s_axis_tvalid,
            s_axis_tready       => aes256_low_block_enc_s_axis_tready,
            s_axis_tdata        => aes256_low_block_enc_s_axis_tdata,

            m_axis_tvalid       => aes256_low_block_enc_m_axis_tvalid,
            m_axis_tready       => aes256_low_block_enc_m_axis_tready,
            m_axis_tdata        => aes256_low_block_enc_m_axis_tdata
        );

    -- e_aes128_decrypt_block : entity LIB_AES.aes_decrypt_block
    --     generic map (
    --         KEY_NBITS_G     => 128
    --     )
    --     port map (
    --         clk                 => clk,
    --         rst                 => rst,

    --         s_key_axis_tvalid   => aes128_block_dec_key_tvalid,
    --         s_key_axis_tdata    => aes128_block_dec_key_tdata,

    --         s_axis_tvalid       => aes128_block_dec_s_axis_tvalid,
    --         s_axis_tready       => aes128_block_dec_s_axis_tready,
    --         s_axis_tdata        => aes128_block_dec_s_axis_tdata,

    --         m_axis_tvalid       => aes128_block_dec_m_axis_tvalid,
    --         m_axis_tready       => aes128_block_dec_m_axis_tready,
    --         m_axis_tdata        => aes128_block_dec_m_axis_tdata
    --     );

    -- e_aes192_decrypt_block : entity LIB_AES.aes_decrypt_block
    --     generic map (
    --         KEY_NBITS_G     => 192
    --     )
    --     port map (
    --         clk                 => clk,
    --         rst                 => rst,

    --         s_key_axis_tvalid   => aes192_block_dec_key_tvalid,
    --         s_key_axis_tdata    => aes192_block_dec_key_tdata,

    --         s_axis_tvalid       => aes192_block_dec_s_axis_tvalid,
    --         s_axis_tready       => aes192_block_dec_s_axis_tready,
    --         s_axis_tdata        => aes192_block_dec_s_axis_tdata,

    --         m_axis_tvalid       => aes192_block_dec_m_axis_tvalid,
    --         m_axis_tready       => aes192_block_dec_m_axis_tready,
    --         m_axis_tdata        => aes192_block_dec_m_axis_tdata
    --     );

    e_aes256_decrypt_block : entity LIB_AES.aes_decrypt_block
        generic map (
            KEY_NBITS_G     => 256
        )
        port map (
            clk                 => clk,
            rst                 => rst,

            s_key_axis_tvalid   => aes256_block_dec_key_tvalid,
            s_key_axis_tdata    => aes256_block_dec_key_tdata,

            s_axis_tvalid       => aes256_block_dec_s_axis_tvalid,
            s_axis_tready       => aes256_block_dec_s_axis_tready,
            s_axis_tdata        => aes256_block_dec_s_axis_tdata,

            m_axis_tvalid       => aes256_block_dec_m_axis_tvalid,
            m_axis_tready       => aes256_block_dec_m_axis_tready,
            m_axis_tdata        => aes256_block_dec_m_axis_tdata
        );

end architecture;