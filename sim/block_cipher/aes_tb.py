#
# Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python Libraries
import logging
from random import randint

# Third party
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

# Cocotb base
import cocotb
from cocotb.binary import BinaryValue
from cocotb.clock import Clock
from cocotb.triggers import Timer, ClockCycles, RisingEdge
from cocotb.utils import get_sim_time

# Cocotb extensions
from cocotbext.axi import AxiStreamSource, AxiStreamSink, AxiStreamBus, AxiStreamFrame



__author__ = "Haydn Reysenbach"
__copyright__ = "Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)"
__credits__ = ["Haydn Reysenbach"]
__license__ = "Apache 2.0"
__version__ = "1.0.0"
__maintainer__ = "Haydn Reysenbach"
__email__ = "haydn.reysenbach@gmail.com"
__status__ = "Development"

C_100MHZ_CLK_PERIOD = 10

# Golden data for a single round of AES from FIPS197
GOLDEN_ENCRYPT_ROUND_DATA = [
    {"data": 0x193DE3BEA0F4E22B9AC68D2AE9F84808, "key": 0xA0FAFE1788542CB123A339392A6C7605, "output": 0xA49C7FF2689F352B6B5BEA43026A5049},
    {"data": 0xA49C7FF2689F352B6B5BEA43026A5049, "key": 0xF2C295F27A96B9435935807A7359F67F, "output": 0xAA8F5F0361DDE3EF82D24AD26832469A},
    {"data": 0xAA8F5F0361DDE3EF82D24AD26832469A, "key": 0x3D80477D4716FE3E1E237E446D7A883B, "output": 0x486C4EEE671D9D0D4DE3B138D65F58E7},
    {"data": 0x486C4EEE671D9D0D4DE3B138D65F58E7, "key": 0xEF44A541A8525B7FB671253BDB0BAD00, "output": 0xE0927FE8C86363C0D9B1355085B8BE01},
    {"data": 0xE0927FE8C86363C0D9B1355085B8BE01, "key": 0xD4D1C6F87C839D87CAF2B8BC11F915BC, "output": 0xF1006F55C1924CEF7CC88B325DB5D50C},
    {"data": 0xF1006F55C1924CEF7CC88B325DB5D50C, "key": 0x6D88A37A110B3EFDDBF98641CA0093FD, "output": 0x260E2E173D41B77DE86472A9FDD28B25},
    {"data": 0x260E2E173D41B77DE86472A9FDD28B25, "key": 0x4E54F70E5F5FC9F384A64FB24EA6DC4F, "output": 0x5A4142B11949DC1FA3E019657A8C040C},
    {"data": 0x5A4142B11949DC1FA3E019657A8C040C, "key": 0xEAD27321B58DBAD2312BF5607F8D292F, "output": 0xEA835CF00445332D655D98AD8596B0C5},
    {"data": 0xEA835CF00445332D655D98AD8596B0C5, "key": 0xAC7766F319FADC2128D12941575C006E, "output": 0xEB40F21E592E38848BA113E71BC342D2},

]

# Golden data for a single round of AES from FIPS197
GOLDEN_DECRYPT_ROUND_DATA = [
    {"data": 0x7AD5FDA789EF4E272BCA100B3D9FF59F, "key": 0x549932D1F08557681093ED9CBE2C974E, "output": 0x54D990A16BA09AB596BBF40EA111702F},
    {"data": 0x54D990A16BA09AB596BBF40EA111702F, "key": 0x47438735A41C65B9E016BAF4AEBF7AD2, "output": 0x3E1C22C0B6FCBF768DA85067F6170495},
    {"data": 0x3E1C22C0B6FCBF768DA85067F6170495, "key": 0x14F9701AE35FE28C440ADF4D4EA9C026, "output": 0xB458124C68B68A014B99F82E5F15554C},
    {"data": 0xB458124C68B68A014B99F82E5F15554C, "key": 0x5E390F7DF7A69296A7553DC10AA31F6B, "output": 0xE8DAB6901477D4653FF7F5E2E747DD4F},
    {"data": 0xE8DAB6901477D4653FF7F5E2E747DD4F, "key": 0x3CAAA3E8A99F9DEB50F3AF57ADF622AA, "output": 0x36339D50F9B539269F2C092DC4406D23},
    {"data": 0x36339D50F9B539269F2C092DC4406D23, "key": 0x47F7F7BC95353E03F96C32BCFD058DFD, "output": 0x2D6D7EF03F33E334093602DD5BFB12C7},
    {"data": 0x2D6D7EF03F33E334093602DD5BFB12C7, "key": 0xB6FF744ED2C2C9BF6C590CBF0469BF41, "output": 0x3BD92268FC74FB735767CBE0C0590E2D},
    {"data": 0x3BD92268FC74FB735767CBE0C0590E2D, "key": 0xB692CF0B643DBDF1BE9BC5006830B3FE, "output": 0xA7BE1A6997AD739BD8C9CA451F618B61},
    {"data": 0xA7BE1A6997AD739BD8C9CA451F618B61, "key": 0xD6AA74FDD2AF72FADAA678F1D6AB76FE, "output": 0x6353E08C0960E104CD70B751BACAD0E7},
]

# Golden data for key expansion from FIPS197
GOLDEN_KEY_EXP = {
    "key_128": {
        "key": 0x2b7e151628aed2a6abf7158809cf4f3c,
        "exp": [
            0x2b7e151628aed2a6abf7158809cf4f3c,
            0xa0fafe1788542cb123a339392a6c7605,
            0xf2c295f27a96b9435935807a7359f67f,
            0x3d80477d4716fe3e1e237e446d7a883b,
            0xef44a541a8525b7fb671253bdb0bad00,
            0xd4d1c6f87c839d87caf2b8bc11f915bc,
            0x6d88a37a110b3efddbf98641ca0093fd,
            0x4e54f70e5f5fc9f384a64fb24ea6dc4f,
            0xead27321b58dbad2312bf5607f8d292f,
            0xac7766f319fadc2128d12941575c006e,
            0xd014f9a8c9ee2589e13f0cc8b6630ca6
        ]
    },
    "key_192": {
        "key": 0x8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b,
        "exp": [
            0x8e73b0f7da0e6452c810f32b809079e5,
            0x62f8ead2522c6b7bfe0c91f72402f5a5,
            0xec12068e6c827f6b0e7a95b95c56fec2,
            0x4db7b4bd69b5411885a74796e92538fd,
            0xe75fad44bb095386485af05721efb14f,
            0xa448f6d94d6dce24aa326360113b30e6,
            0xa25e7ed583b1cf9a27f939436a94f767,
            0xc0a69407d19da4e1ec1786eb6fa64971,
            0x485f703222cb8755e26d135233f0b7b3,
            0x40beeb282f18a2596747d26b458c553e,
            0xa7e1466c9411f1df821f750aad07d753,
            0xca4005388fcc5006282d166abc3ce7b5,
            0xe98ba06f448c773c8ecc720401002202
        ]
    },
    "key_256": {
        "key": 0x603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4,
        "exp": [
            0x603deb1015ca71be2b73aef0857d7781,
            0x1f352c073b6108d72d9810a30914dff4,
            0x9ba354118e6925afa51a8b5f2067fcde,
            0xa8b09c1a93d194cdbe49846eb75d5b9a,
            0xd59aecb85bf3c917fee94248de8ebe96,
            0xb5a9328a2678a647983122292f6c79b3,
            0x812c81addadf48ba24360af2fab8b464,
            0x98c5bfc9bebd198e268c3ba709e04214,
            0x68007bacb2df331696e939e46c518d80,
            0xc814e20476a9fb8a5025c02d59c58239,
            0xde1369676ccc5a71fa2563959674ee15,
            0x5886ca5d2e2f31d77e0af1fa27cf73c3,
            0x749c47ab18501ddae2757e4f7401905a,
            0xcafaaae3e4d59b349adf6acebd10190d,
            0xfe4890d1e6188d0b046df344706c631e
        ]
    },
}

# Golden data for block encryption from FIPS197
GOLDEN_SIMPLE_ENCRYPT_BLOCK_DATA = [
    # {
    #     "key"       : 0x000102030405060708090A0B0C0D0E0F,
    #     "plaintext" : [0x00112233445566778899AABBCCDDEEFF],
    #     "output"    : [0x69C4E0D86A7B0430D8CDB78070B4C55A]
    # },
    {
        "key"       : 0x11754cd72aec309bf52f7687212e8957,
        "plaintext" : [0x3c819d9a9bed087615030b6500000001],
        "output"    : [0x69C4E0D86A7B0430D8CDB78070B4C55A]
    },
    {
        "key"       : 0x000102030405060708090A0B0C0D0E0F1011121314151617,
        "plaintext" : [0x00112233445566778899AABBCCDDEEFF],
        "output"    : [0xDDA97CA4864CDFE06EAF70A0EC0D7191]
    },
    {
        "key"       : 0x000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F,
        "plaintext" : [0x00112233445566778899AABBCCDDEEFF],
        "output"    : [0x8EA2B7CA516745BFEAFC49904B496089]
    },
]

GOLDEN_SIMPLE_DECRYPT_BLOCK_DATA = [
    {
        "key"           : 0x000102030405060708090A0B0C0D0E0F,
        "ciphertext"    : [0x69C4E0D86A7B0430D8CDB78070B4C55A],
        "plaintext"     : [0x00112233445566778899AABBCCDDEEFF],
    },
    {
        "key"           : 0x000102030405060708090A0B0C0D0E0F1011121314151617,
        "ciphertext"    : [0xDDA97CA4864CDFE06EAF70A0EC0D7191],
        "plaintext"     : [0x00112233445566778899AABBCCDDEEFF],
    },
    {
        "key"           : 0x000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F,
        "ciphertext"    : [0x8EA2B7CA516745BFEAFC49904B496089],
        "plaintext"     : [0x00112233445566778899AABBCCDDEEFF],
    },
]

# Golden data for block encryption from NIST AES Vectors
GOLDEN_ENCRYPT_BLOCK_DATA = [
    {
        "key"       : 0xEBEA9C6A82213A00AC1D22FAEA22116F,
        "plaintext" : [
            0x451F45663B44FD005F3C288AE57B3838,
            0x83F02D9AD3DC1715F9E3D6948564257B,
            0x9B06D7DD51935FEE580A96BBDFEFB918,
            0xB4E6B1DAAC809847465578CB8B5356ED,
            0x38556F801FF7C11ECBA9CDD263039C15,
            0xD05900FC228E1CAF302D261D7FB56CEE,
            0x663595B96F192A78FF4455393A5FE816,
            0x2170A066FDAEAC35019469F22B347068,
            0x6BCED2F007A1A2E43E01B4562CAAA502,
            0xED541B8205874EC1FFB1C8B255766942
        ],
        "output"    : [
            0x01043053F832EF9B911ED387BA577451,
            0xE30D51D4B6B11F319D4CD539D067B7F4,
            0xF9B4F41F7F3D4E920C57CBE2B5E1885A,
            0xA66203AE493E93A1DF63793A9563C176,
            0xBC6775DD09CC9161E278A01BEB8FD8A1,
            0x9200326BD95ABC5F716768E34F90B505,
            0x23D30FDABB103A3BC020AFBBB0CB3BD2,
            0xAD512A6FEA79F8D64CEF347458DEC48B,
            0xE89451CB0B807D73593F273D9FC521B7,
            0x89A77524404F43E00F20B3B77B938B1A
        ]
    },
    {
        "key"       : 0x4F41FA4D4A25100B586551828373BCCA5540C68E9BF84562,
        "plaintext" : [
            0x7C727BD3E7048E7A8995B7B1169AE4B5,
            0xA55E854BB4F7A9576D7863AB2868731D,
            0x307322DCCA606E047343676F6AF4D9CF,
            0x6EBF2BF9C95D87848D233C931E7A60EF,
            0xF08FB959924CDE1EEC8699EBC57890E3,
            0x887024EF47C89A550018788D1FAA3250,
            0x452E06F148AF25F07BC613CD2F0E501A,
            0x79D738D4361F28F34DBEE24034E03367,
            0xB6B8D34DF3738CA3A86B9EBCB09E639B,
            0xCB5E2F519F4A7A86FC7C41556404A95D
        ],
        "output"    : [
            0x922812AD5FEACDF11FE7FDAE96300149,
            0x419E31CFF54061B3C5ED27FDB8B50C9C,
            0x0932B522A6C04E482499B011EF3C3E9D,
            0xC56A1A61CFEB78B34032D26DBDC3CAC5,
            0x1A3279BC934B9BCE2D9C19BF85823561,
            0x3BA784E48E292D22C6B5A28E1D1BB860,
            0x524FB7B5F9B3D9A5F4DA66E340585BD2,
            0x496FE6D6942DB8D05D716FEC03B17D19,
            0xABB58B33332E24BEAEC7995D69525364,
            0xFE139AA1FD62054668C58F23F1F94CFD
        ]
    },
    {
        "key"       : 0x44A2B5A7453E49F38261904F21AC797641D1BCD8DDEDD293F319449FE63B2948,
        "plaintext" : [
            0xC91B8A7B9C511784B6A37F73B290516B,
            0xB9EF1E8DF68D89BF49169EAC4039650C,
            0x4307B6260E9C4E93650223440252F5C7,
            0xD31C26C56209CBD095BF035B9705880A,
            0x1628832DAF9DA587A6E77353DBBCE189,
            0xF963235DF160C008A753E8CCEA1E0732,
            0xAA469A97659C42E6E31C16A723153E39,
            0x958ABE5B8AD88FF2E89AF40622CA0B0D,
            0x6729A26C1AE04D3B8367B548C4A6335F,
            0x0E5A9EC914BB6113C05CD0112552BC21
        ],
        "output"    : [
            0x05D51AF0E2B61E2C06CB1E843FEE3172,
            0x825E63B5D1CE8183B7E1DB6268DB5AA7,
            0x26521F46E948028AA443AF9EBD8B7C6B,
            0xAF958067AB0D4A8AC530ECBB68CDFC3E,
            0xB93034A428EB7E8F6A3813CEA6189068,
            0xDFECFA268B7ECD5987F8CB2732C6882B,
            0xBEC8F716BAC254D72269230AEC5DC7F5,
            0xA6B866FD305242552D400F5B0404F19C,
            0xBFE7291FAB690ECFE6018C4309FC639D,
            0x1B65FCB65E643EDB0AD1F09CFE9CEE4A
        ]
    },
]

GOLDEN_DECRYPT_BLOCK_DATA = [
    {
        "key"           : 0x44F0EE626D0446E0A3924CFB078944BB,
        "ciphertext"    : [
            0x931B2F5F3A5820D53A6BEAAA6431083A,
            0x3488F4EB03B0F5B57EF838E157962310,
            0x3BD6E6800377538B2E51EF708F3C4956,
            0x432E8A8EE6A34E190642B26AD8BDAE6C,
            0x2AF9A6C7996F3B6004D2671E41F1C9F4,
            0x0EE03D1C4A52B0A0654A331F15F34DCE,
            0x4ACB96BD6507815CA4347A3DE11A311B,
            0x7DE5351C9787C4538158E28974FFA83D,
            0x8296DFE9CD09CD87F7BF4F54D97D28D4,
            0x788799163408323943B3E72F5EAB66C1
        ],
        "plaintext"     : [
            0x9C29EECB2DE04254FAFB896A994102D1,
            0xDA30DDB49D82728EB23DBD029901E9B7,
            0x5B3D0AEE03F7A05F6C852D8FADA0B5C2,
            0x8E8C9AED334FAD11829DF3DFADC5C2E4,
            0x71EB41AF9E48A8A465E03D5EBDB02169,
            0x15081F3B5A0EBB2308DFC2D28E5A8BA3,
            0xF32ADAE4C3575921BC657B63D46BA5A6,
            0x18880EE9AD8AF3FBA5643A5026FACD7D,
            0x667CE599327F936CDDA7E1BB742A33A0,
            0x19990B76BE648A6EC725DAED540ED9E7,
        ],
    },
    {
        "key"           : 0x9CC24EA1F1959D9A972E7182EF3B4E22A97A87D0DA7FF64B,
        "ciphertext"    : [
            0x952F4546A8BF7166964917ECE01BDA3C,
            0x6857E427CEF5DA0FF90B0E4BF44CF7CC,
            0xFCCFDF01D713DCF9673F01C87EAED52B,
            0xF4AA046FF778558EA396DC9CD2407161,
            0x36386148A5C76378B3FFCD40864407B8,
            0xE60B40A594E0619EDDAE3F6D6E3B15B8,
            0x6AF231E1BAE5ED2AA512E11DA0E5572B,
            0x67FFFF934C36E585CFDD9F877045CB19,
            0xC183B994BF74645862FFA726739AADCB,
            0x9E10AAFFC881C88CA3AA65B37F667BCB
        ],
        "plaintext"     : [
            0xB8BB5CE53A15AA6DFDF2CB61BC8E3617,
            0xD1D0FEFE9BA5D175550470E32397F6F3,
            0xB3E65B43BDED2B21E5C181D3C4C4C526,
            0xC41CEAB044289508458048B63352DFC3,
            0x79DE373FD19A2C900C43524B75949E67,
            0x7CCEDA866F7F2BCC4844EF2E5DAC5B80,
            0x4B4045E657C8156D1DCDB43CBF2F5E00,
            0xA4F9255E3BE2439436C4D0449A8D2C4C,
            0x1A56BECE98EA0FD68ABAF12398039994,
            0xAEBFFC692B9000E580479B4F4B28B5FE
        ],
    },
    {
        "key"           : 0xC4A71E055A7254DDA360693FE1BE49F10FAA6731C36DBAA6590B05974E185C5B,
        "ciphertext"    : [
            0x2C487FA96F4090C56AA1B5BE81918A93,
            0x4C9492878FB0CD686DCF8D17D8648545,
            0x4C51237BBD09205DCEF1552F430DD098,
            0xB9D827A694730C133A0222C77F540F9D,
            0x5FC2D36AF359583C9E3B49DF884228A6,
            0x4DE79B67F66207C8281360B99B214042,
            0xCE61367FF97960E944453CD63679BB44,
            0x708897D29BC5E70F9FC8F1F715143FBB,
            0x00F7F5C1B7B161EC26D8D41D36FAB0FA,
            0x8A85C3EE6CE4D37007EB7A89D6753590
        ],
        "plaintext"     : [
            0x31FD5A307E279B2F34581E2C432379DF,
            0x8ECCBAF79532938916711CD377540B90,
            0x45373E47F2214B8F876040AF733F6C9D,
            0x8F03A7C58F8714D2FBB4C14AF59C75B4,
            0x83ADC718946EE907A18286CC4EFD2067,
            0x89064B6F1B195F0D0D234468E4F00E6F,
            0x1CAD5CD3B9C0A643B3C0DD09280FF2E2,
            0xA5929183409384DD72DC94E39687EA2B,
            0x623D5D776700BD8B36E6130FFDE966F1,
            0x34C4B1F35F29C5CC4A03297E1CCC9539
        ],
    },
]

GOLDEN_ENCRYPT_BLOCK_DATA_SELECT = {
    "simple": GOLDEN_SIMPLE_ENCRYPT_BLOCK_DATA,
    "normal": GOLDEN_ENCRYPT_BLOCK_DATA,
}

GOLDEN_DECRYPT_BLOCK_DATA_SELECT = {
    "simple": GOLDEN_SIMPLE_DECRYPT_BLOCK_DATA,
    "normal": GOLDEN_DECRYPT_BLOCK_DATA,
}

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes_encrypt_round(dut):
    """
    Test to validate the encryption of a single round. This is tested against golden data from FIPS197
    Skipped as it is tested in the aes_enc_block testcase
    """
    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    data_source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "aes_enc_rnd_sink"), dut.clk, dut.rst)
    key_source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "aes_enc_rnd_key"), dut.clk, dut.rst)
    data_sink = AxiStreamSink(AxiStreamBus.from_prefix(dut, "aes_enc_rnd_src"), dut.clk, dut.rst)

    dut.rst.value = 1
    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    for golden_input in GOLDEN_ENCRYPT_ROUND_DATA:
        data = golden_input["data"]
        key = golden_input["key"]

        key_frame = AxiStreamFrame(tdata=key.to_bytes(16,"little"))
        data_frame = AxiStreamFrame(tdata=data.to_bytes(16,"little"))

        await key_source.send(key_frame)
        await data_source.send(data_frame)

    for golden_output in GOLDEN_ENCRYPT_ROUND_DATA:
        res = await data_sink.recv(compact=False)

        output = int.from_bytes(res.tdata, byteorder='little')


        assert output == golden_output["output"], f"Expected 0x{golden_output['output']:32X}, got 0x{output:32X}"

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes_decrypt_round(dut):
    """
    Test to validate the decryption of a single round. This is tested against golden data from FIPS197
    Skipped as it is tested in the aes_dec_block testcase
    """
    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    data_source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "aes_dec_rnd_sink"), dut.clk, dut.rst)
    key_source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "aes_dec_rnd_key"), dut.clk, dut.rst)
    data_sink = AxiStreamSink(AxiStreamBus.from_prefix(dut, "aes_dec_rnd_src"), dut.clk, dut.rst)
    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    for golden_input in GOLDEN_DECRYPT_ROUND_DATA:
        data = golden_input["data"]
        key = golden_input["key"]

        data_frame = AxiStreamFrame(tdata=data.to_bytes(16, 'little'))
        key_frame = AxiStreamFrame(tdata=key.to_bytes(16, 'little'))

        await data_source.send(data_frame)
        await key_source.send(key_frame)

    for golden_output in GOLDEN_DECRYPT_ROUND_DATA:
        res = await data_sink.recv(compact=False)

        output = int.from_bytes(res.tdata, byteorder='little')

        assert output == golden_output["output"], f"Expected 0x{golden_output['output']:32X}, got 0x{output:32X}"

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes_key_expansion(dut):
    """
    Test to validate key expansion. This is tested against golden data from FIPS197
    Skipped as it is tested in the aes_enc_block testcase
    """
    logger:logging.Logger = dut._log

    logger.setLevel(logging.INFO)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())
    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    dut.aes_key_exp_128_in.value = GOLDEN_KEY_EXP[f"key_128"]["key"]
    dut.aes_key_exp_128_valid.value = 1

    dut.aes_key_exp_192_in.value = GOLDEN_KEY_EXP["key_192"]["key"]
    dut.aes_key_exp_192_valid.value = 1

    dut.aes_key_exp_256_in.value = GOLDEN_KEY_EXP["key_256"]["key"]
    dut.aes_key_exp_256_valid.value = 1

    valid_bits_128 = 0
    valid_bits_192 = 0
    valid_bits_256 = 0

    for i in range(0, 20):
        await RisingEdge(dut.clk)

        aes_key_exp_128_out_valid = dut.aes_key_exp_128_out_valid.value
        aes_key_exp_192_out_valid = dut.aes_key_exp_192_out_valid.value
        aes_key_exp_256_out_valid = dut.aes_key_exp_256_out_valid.value

        for j in range(0, 15):
            pass
            if (((aes_key_exp_128_out_valid >> j) & 0x1) == 1):
                valid_bits_128 |= 1 << j
                expected = GOLDEN_KEY_EXP["key_128"]["exp"][j]
                received_string = dut.aes_key_exp_128_out.value.binstr
                received_string = received_string[::-1]
                tmp_str = received_string[j*128:128*j+128]
                tmp_str = tmp_str[::-1]
                received = int(tmp_str, 2)

                assert received == expected, f"Expected 0x{expected:08X}, got 0x{received:08X}"

            if (((aes_key_exp_192_out_valid >> j) & 0x1) == 1):
                expected = GOLDEN_KEY_EXP["key_192"]["exp"][j]
                received_string = dut.aes_key_exp_192_out.value.binstr
                received_string = received_string[::-1]
                tmp_str = received_string[j*128:128*j+128]
                tmp_str = tmp_str[::-1]
                received = int(tmp_str, 2)

                assert received == expected, f"Expected 0x{expected:08X}, got 0x{received:08X}"

            if (((aes_key_exp_256_out_valid >> j) & 0x1) == 1):
                expected = GOLDEN_KEY_EXP["key_256"]["exp"][j]
                received_string = dut.aes_key_exp_256_out.value.binstr
                received_string = received_string[::-1]
                tmp_str = received_string[j*128:128*j+128]
                tmp_str = tmp_str[::-1]
                received = int(tmp_str, 2)

                assert received == expected, f"Expected 0x{expected:08X}, got 0x{received:08X}"

        # Check to make sure the valid bits are changing
        if (valid_bits_128 > 0 and valid_bits_128 < 0x7FF):
            assert valid_bits_128 > old_valid_bits_128
        old_valid_bits_128 = valid_bits_128

        if (valid_bits_192 > 0 and valid_bits_192 < 0x1FFF):
            assert valid_bits_192 > old_valid_bits_192
        old_valid_bits_192 = valid_bits_192

        if (valid_bits_256 > 0 and valid_bits_192 < 0x7FFF):
            assert valid_bits_256 > old_valid_bits_256
        old_valid_bits_256 = valid_bits_256

async def enc_block(dut, key_size, high_throughput=True, simple=False):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    aes_prefix = f"aes{key_size}"
    if (high_throughput == False):
        aes_prefix += "_low"

    aes_enc_s_axis  = AxiStreamSource(  AxiStreamBus.from_prefix(dut, aes_prefix + "_block_enc_s_axis"),   dut.clk, dut.rst)
    aes_enc_key     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, aes_prefix + "_block_enc_key"),      dut.clk, dut.rst)
    aes_enc_m_axis  = AxiStreamSink(    AxiStreamBus.from_prefix(dut, aes_prefix + "_block_enc_m_axis"),   dut.clk, dut.rst)

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    if simple == True:
        golden_values = GOLDEN_ENCRYPT_BLOCK_DATA_SELECT["simple"]
    else:
        golden_values = GOLDEN_ENCRYPT_BLOCK_DATA_SELECT["normal"]
    golden_input    = golden_values[key_size//64-2]
    data:list       = golden_input["plaintext"]
    key:int         = golden_input["key"]
    output:list     = golden_input["output"]

    key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, "little"))
    await aes_enc_key.send(key_frame)

    for golden in data:
        data_frame = AxiStreamFrame(tdata=golden.to_bytes(16, "little"))
        await aes_enc_s_axis.send(data_frame)

    for golden in output:
        res = await aes_enc_m_axis.recv(compact=False)
        result = int.from_bytes(res.tdata, "little")

        assert golden == result, f"Expected 0x{golden:32X}, got 0x{result:32X}"

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes128_enc_block(dut):
    await enc_block(dut, 128)

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes192_enc_block(dut):
    await enc_block(dut, 192)

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes256_enc_block(dut):
    await enc_block(dut, 256)

@cocotb.test(skip = True, timeout_time=2, timeout_unit="us")
async def fips197_aes128_slow_enc_block(dut):
    await enc_block(dut, 128, False,  simple=True)

@cocotb.test(skip = True, timeout_time=2, timeout_unit="us")
async def fips197_aes192_slow_enc_block(dut):
    await enc_block(dut, 192, False)

@cocotb.test(skip = True, timeout_time=2, timeout_unit="us")
async def fips197_aes256_slow_enc_block(dut):
    await enc_block(dut, 256, False)

async def dec_block(dut, key_size):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())
    aes_dec_s_axis   = AxiStreamSource(  AxiStreamBus.from_prefix(dut, f"aes{key_size}_block_dec_s_axis"),   dut.clk, dut.rst)
    aes_dec_key      = AxiStreamSource(  AxiStreamBus.from_prefix(dut, f"aes{key_size}_block_dec_key"),      dut.clk, dut.rst)
    aes_dec_m_axis   = AxiStreamSink(    AxiStreamBus.from_prefix(dut, f"aes{key_size}_block_dec_m_axis"),   dut.clk, dut.rst)

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    golden_input = GOLDEN_DECRYPT_BLOCK_DATA[key_size//64-2]
    key:int     = golden_input["key"]
    data:list   = golden_input["ciphertext"]
    output:list   = golden_input["plaintext"]

    key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
    await aes_dec_key.send(key_frame)

    for golden in data:
        data_frame = AxiStreamFrame(tdata=golden.to_bytes(16,'little'))
        await aes_dec_s_axis.send(data_frame)

    for golden in output:
        res = await aes_dec_m_axis.recv(compact=False)

        result = int.from_bytes(res.tdata, byteorder='little')

        assert golden == result, f"Expected 0x{golden:32X}, got 0x{result:32X}"

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes128_dec_block(dut):
    await dec_block(dut, 128)

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes192_dec_block(dut):
    await dec_block(dut, 192)

@cocotb.test(skip = True, timeout_time=1, timeout_unit="us")
async def fips197_aes256_dec_block(dut):
    await dec_block(dut, 256)

NUMBER_OF_BLOCKS = 200 # randint(150,200)
MIN_BACKPRESSURE_CYCLES = 10
MAX_BACKPRESSURE_CYCLES = 100
def backpressure_generator():
    target = randint(MIN_BACKPRESSURE_CYCLES, MAX_BACKPRESSURE_CYCLES)
    count = 0
    cur_state = False
    while True:
        if (count < target):
            count += 1
        else:
            target = randint(MIN_BACKPRESSURE_CYCLES, MAX_BACKPRESSURE_CYCLES)
            count = 0
            cur_state = not cur_state

        yield cur_state

async def aes_enc(dut, key_size, high_throughput=True):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())
    aes_prefix = f"aes{key_size}"
    if (high_throughput == False):
        aes_prefix += "_low"

    aes_enc_key     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, aes_prefix + "_block_enc_key"),      dut.clk, dut.rst)
    aes_enc_s_axis  = AxiStreamSource(  AxiStreamBus.from_prefix(dut, aes_prefix + "_block_enc_s_axis"),   dut.clk, dut.rst)
    aes_enc_m_axis  = AxiStreamSink(    AxiStreamBus.from_prefix(dut, aes_prefix + "_block_enc_m_axis"),   dut.clk, dut.rst)

    aes_enc_s_axis.set_pause_generator(backpressure_generator())
    aes_enc_m_axis.set_pause_generator(backpressure_generator())

    aes_enc_key.log.setLevel(logging.WARN)
    aes_enc_s_axis.log.setLevel(logging.WARN)
    aes_enc_m_axis.log.setLevel(logging.WARN)
    logger.setLevel(logging.INFO)

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    key = randint(0, 2**key_size - 1)

    cipher = Cipher(algorithms.AES(key.to_bytes(key_size//8, 'big')), modes.ECB())
    encryptor = cipher.encryptor()
    key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
    await aes_enc_key.send(key_frame)

    data = []
    frames = []
    for i in range(0, NUMBER_OF_BLOCKS):
        dataword = randint(0, 2**128-1)
        data.append(dataword)
        frames.append(AxiStreamFrame(tdata=dataword.to_bytes(16, 'little')))

        await aes_enc_s_axis.send(frames[i])

    for i in range(0, NUMBER_OF_BLOCKS):
        res:AxiStreamFrame = await aes_enc_m_axis.recv(compact=False)

        res = int.from_bytes(res, 'little')
        expected = int.from_bytes(encryptor.update(data[i].to_bytes(16,'big')), 'big')

        assert res == expected, f"AES{key_size}: Expected 0x{expected:X}, got 0x{res:X}"

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes128_enc(dut):
    await aes_enc(dut, 128)

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes192_enc(dut):
    await aes_enc(dut, 192)

@cocotb.test(skip = False, timeout_time=100, timeout_unit="us")
async def aes256_enc(dut):
    await aes_enc(dut, 256)

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes128_low_throughput_enc(dut):
    await aes_enc(dut, 128, False)

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes192_low_throughput_enc(dut):
    await aes_enc(dut, 192, False)

@cocotb.test(skip = False, timeout_time=100, timeout_unit="us")
async def aes256_low_throughput_enc(dut):
    await aes_enc(dut, 256, False)

async def aes_dec(dut, key_size):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())
    aes_dec_key      = AxiStreamSource(  AxiStreamBus.from_prefix(dut, f"aes{key_size}_block_dec_key"),      dut.clk, dut.rst)
    aes_dec_s_axis   = AxiStreamSource(  AxiStreamBus.from_prefix(dut, f"aes{key_size}_block_dec_s_axis"),   dut.clk, dut.rst)
    aes_dec_m_axis   = AxiStreamSink(    AxiStreamBus.from_prefix(dut, f"aes{key_size}_block_dec_m_axis"),   dut.clk, dut.rst)

    aes_dec_s_axis.set_pause_generator(backpressure_generator())
    aes_dec_m_axis.set_pause_generator(backpressure_generator())

    aes_dec_key.log.setLevel(logging.WARN)
    aes_dec_s_axis.log.setLevel(logging.WARN)
    aes_dec_m_axis.log.setLevel(logging.WARN)
    logger.setLevel(logging.INFO)

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    key = randint(0, 2**key_size - 1)

    
    cipher = Cipher(algorithms.AES(key.to_bytes(key_size//8, 'big')), modes.ECB())
    decryptor = cipher.decryptor()
    key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
    await aes_dec_key.send(key_frame)

    data = []
    frames = []
    for i in range(0, NUMBER_OF_BLOCKS):
        dataword = randint(0, 2**128-1)
        data.append(dataword)
        frames.append(AxiStreamFrame(tdata=dataword.to_bytes(16, 'little')))

        await aes_dec_s_axis.send(frames[i])

    for i in range(0, NUMBER_OF_BLOCKS):
        res = await aes_dec_m_axis.recv(compact=False)

        res = int.from_bytes(res, 'little')
        expected = int.from_bytes(decryptor.update(data[i].to_bytes(16,'big')), 'big')

        assert res == expected, f"AES{key_size}: Expected 0x{expected:X}, got 0x{res:X}"

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes128_dec(dut):
    await aes_dec(dut, 128)

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes192_dec(dut):
    await aes_dec(dut, 192)

@cocotb.test(skip = False, timeout_time=100, timeout_unit="us")
async def aes256_dec(dut):
    await aes_dec(dut, 256)