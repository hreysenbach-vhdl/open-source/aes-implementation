#
# Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python Libraries
import logging
from random import randint

# Third party
from cryptography.hazmat.primitives.ciphers import aead

# Cocotb base
import cocotb
from cocotb.binary import BinaryValue
from cocotb.clock import Clock
from cocotb.triggers import Timer, ClockCycles, RisingEdge
from cocotb.utils import get_sim_time

# Cocotb extensions
from cocotbext.axi import AxiStreamSource, AxiStreamSink, AxiStreamBus, AxiStreamFrame



__author__ = "Haydn Reysenbach"
__copyright__ = "Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)"
__credits__ = ["Haydn Reysenbach"]
__license__ = "Apache 2.0"
__version__ = "1.0.0"
__maintainer__ = "Haydn Reysenbach"
__email__ = "haydn.reysenbach@gmail.com"
__status__ = "Development"

C_100MHZ_CLK_PERIOD = 10

NUMBER_OF_BLOCKS = 200 # randint(150,200)
MIN_BACKPRESSURE_CYCLES = 10
MAX_BACKPRESSURE_CYCLES = 100

def backpressure_generator():
    target = randint(MIN_BACKPRESSURE_CYCLES, MAX_BACKPRESSURE_CYCLES)
    count = 0
    cur_state = False
    while True:
        if (count < target):
            count += 1
        else:
            target = randint(MIN_BACKPRESSURE_CYCLES, MAX_BACKPRESSURE_CYCLES)
            count = 0
            cur_state = not cur_state

        yield cur_state

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes_golden_enc(dut, key_size=128, high_throughput=True):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    aes_gcm_key         = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_key_axis"),    dut.clk, dut.rst)
    aes_gcm_iv          = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_iv_axis"),     dut.clk, dut.rst)
    aes_gcm_in_auth     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_in_aad      = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_in_data     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_axis"),        dut.clk, dut.rst)
    aes_gcm_out_auth    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "enc_m_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_out_aad     = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "enc_m_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_out_data    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "enc_m_axis"),        dut.clk, dut.rst)

    aes_gcm_in_auth.set_pause_generator(backpressure_generator())
    aes_gcm_in_aad.set_pause_generator(backpressure_generator())
    aes_gcm_in_data.set_pause_generator(backpressure_generator())
    aes_gcm_out_auth.set_pause_generator(backpressure_generator())
    aes_gcm_out_aad.set_pause_generator(backpressure_generator())
    aes_gcm_out_data.set_pause_generator(backpressure_generator())

    aes_gcm_key.log.setLevel(logging.WARN)
    aes_gcm_iv.log.setLevel(logging.WARN)
    aes_gcm_in_auth.log.setLevel(logging.WARN)
    aes_gcm_in_aad.log.setLevel(logging.WARN)
    aes_gcm_in_data.log.setLevel(logging.WARN)
    aes_gcm_out_auth.log.setLevel(logging.WARN)
    aes_gcm_out_aad.log.setLevel(logging.WARN)
    aes_gcm_out_data.log.setLevel(logging.WARN)
    logger.setLevel(logging.INFO)

    if (aes_gcm_key.width != 128):
        assert False, f"Key size does not match! Expected 128 bits, but key width is {aes_gcm_key.width}"

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    key = randint(0, 2**key_size - 1)
    key = 0xc939cc13397c1d37de6ae0e1cb7c423c
    iv = randint(0, 2**96-1)
    iv = 0xb3d8cc017cbb89b39e0f67e2

    key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
    await aes_gcm_key.send(key_frame)

    iv_frame = AxiStreamFrame(tdata=iv.to_bytes(12, 'little'))
    await aes_gcm_iv.send(iv_frame)

    dataword = 0x24825602bd12a984e0092d3e448eda5f
    byte_array = bytearray(dataword.to_bytes(16, 'little'))
    data_frame = AxiStreamFrame(tdata=byte_array)
    await aes_gcm_in_aad.send(data_frame)

    data = 0xc3b3c41f113a31b73d9a5cd432103069
    byte_array = bytearray(data.to_bytes(16, 'little'))
    data_frame = AxiStreamFrame(tdata=byte_array)
    await aes_gcm_in_data.send(data_frame)

    auth_tag = await aes_gcm_out_auth.recv(compact=False)
    auth_tag = int.from_bytes(auth_tag, 'little')
    assert auth_tag == 0x0032a1dc85f1c9786925a2e71d8272dd, f"Invalid auth tag received!, Expected 0x0032A1DC85F1C9786925A2E71D8272DD, got 0x{auth_tag:032X}"

    ciphertext = await aes_gcm_out_data.recv(compact=False)
    ciphertext = int.from_bytes(ciphertext, 'little')
    assert ciphertext == 0x93fe7d9e9bfd10348a5606e5cafa7354, f"Invalid ciphertext received!, Expected 0x93FE7D9E9BFD10348A5606E5CAFA7354, got 0x{ciphertext:032X}"

@cocotb.test(skip = True, timeout_time=100, timeout_unit="us")
async def aes_golden_dec(dut, key_size=128, high_throughput=True):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    aes_gcm_key         = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_key_axis"),    dut.clk, dut.rst)
    aes_gcm_iv          = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_iv_axis"),     dut.clk, dut.rst)
    aes_gcm_in_auth     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_in_aad      = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_in_data     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_axis"),        dut.clk, dut.rst)
    aes_gcm_out_auth    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "dec_m_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_out_aad     = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "dec_m_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_out_data    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "dec_m_axis"),        dut.clk, dut.rst)

    aes_gcm_in_auth.set_pause_generator(backpressure_generator())
    aes_gcm_in_aad.set_pause_generator(backpressure_generator())
    aes_gcm_in_data.set_pause_generator(backpressure_generator())
    aes_gcm_out_auth.set_pause_generator(backpressure_generator())
    aes_gcm_out_aad.set_pause_generator(backpressure_generator())
    aes_gcm_out_data.set_pause_generator(backpressure_generator())

    aes_gcm_key.log.setLevel(logging.WARN)
    aes_gcm_iv.log.setLevel(logging.WARN)
    aes_gcm_in_auth.log.setLevel(logging.WARN)
    aes_gcm_in_aad.log.setLevel(logging.WARN)
    aes_gcm_in_data.log.setLevel(logging.WARN)
    aes_gcm_out_auth.log.setLevel(logging.WARN)
    aes_gcm_out_aad.log.setLevel(logging.WARN)
    aes_gcm_out_data.log.setLevel(logging.WARN)
    logger.setLevel(logging.DEBUG)

    if (aes_gcm_key.width != 128):
        assert False, f"Key size does not match! Expected 128 bits, but key width is {aes_gcm_key.width}"

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    key = randint(0, 2**key_size - 1)
    key = 0xc939cc13397c1d37de6ae0e1cb7c423c
    iv = randint(0, 2**96-1)
    iv = 0xb3d8cc017cbb89b39e0f67e2

    key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
    await aes_gcm_key.send(key_frame)

    iv_frame = AxiStreamFrame(tdata=iv.to_bytes(12, 'little'))
    await aes_gcm_iv.send(iv_frame)

    aad = 0x24825602bd12a984e0092d3e448eda5f
    byte_array = bytearray(aad.to_bytes(16, 'little'))
    aad_frame = AxiStreamFrame(tdata=byte_array)
    await aes_gcm_in_aad.send(aad_frame)

    data = 0x93fe7d9e9bfd10348a5606e5cafa7354
    byte_array = bytearray(data.to_bytes(16, 'little'))
    data_frame = AxiStreamFrame(tdata=byte_array)
    await aes_gcm_in_data.send(data_frame)

    auth_tag = await aes_gcm_out_auth.recv(compact=False)
    auth_tag = int.from_bytes(auth_tag, 'little')
    assert auth_tag == 0x0032a1dc85f1c9786925a2e71d8272dd, f"Invalid auth tag received!, Expected 0x0032A1DC85F1C9786925A2E71D8272DD, got 0x{auth_tag:032X}"

    ciphertext = await aes_gcm_out_data.recv(compact=False)
    ciphertext = int.from_bytes(ciphertext, 'little')
    assert ciphertext == 0xc3b3c41f113a31b73d9a5cd432103069, f"Invalid ciphertext received!, Expected 0xC3B3C41F113A31B73D9A5CD432103069, got 0x{ciphertext:032X}"



NBR_OF_KEYS     = 10
NBR_OF_IVS      = 10
NBR_OF_FRAMES   = 10
MIN_FRAME_SIZE  = 10
MAX_FRAME_SIZE  = 50

def print_list_in_hex(data :list) -> str:
    string = "\n"
    for i in range(len(data)):
        string += f"0x{data[i]:02X}, "
        if (i == len(data)-1):
            string = string[:-2]
        if ((i+1) % 16 == 0 and i != len(data)-1):
            string += "\n"

    return string


@cocotb.test(skip = False, timeout_time=10, timeout_unit="ms")
async def aes_enc(dut, key_size=256, high_throughput=True):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    aes_gcm_key         = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_key_axis"),    dut.clk, dut.rst)
    aes_gcm_iv          = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_iv_axis"),     dut.clk, dut.rst)
    aes_gcm_in_auth     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_in_aad      = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_in_data     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "enc_s_axis"),        dut.clk, dut.rst)
    aes_gcm_out_auth    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "enc_m_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_out_aad     = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "enc_m_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_out_data    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "enc_m_axis"),        dut.clk, dut.rst)

    aes_gcm_in_auth.set_pause_generator(backpressure_generator())
    aes_gcm_in_aad.set_pause_generator(backpressure_generator())
    aes_gcm_in_data.set_pause_generator(backpressure_generator())
    aes_gcm_out_auth.set_pause_generator(backpressure_generator())
    aes_gcm_out_aad.set_pause_generator(backpressure_generator())
    aes_gcm_out_data.set_pause_generator(backpressure_generator())


    aes_gcm_key.log.setLevel(logging.WARN)
    aes_gcm_iv.log.setLevel(logging.WARN)
    aes_gcm_in_auth.log.setLevel(logging.WARN)
    aes_gcm_in_aad.log.setLevel(logging.WARN)
    aes_gcm_in_data.log.setLevel(logging.WARN)
    aes_gcm_out_auth.log.setLevel(logging.WARN)
    aes_gcm_out_aad.log.setLevel(logging.WARN)
    aes_gcm_out_data.log.setLevel(logging.WARN)
    logger.setLevel(logging.INFO)

    if (aes_gcm_key.width != key_size):
        assert False, f"Key size does not match! Expected {key_size} bits, but key width is {aes_gcm_key.width}"

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    for key_loop in range(0, NBR_OF_KEYS):
        key = randint(0, 2**key_size - 1)
        logger.info(f"Starting key {key_loop} with key 0x{key:032X}!")

        cipher = aead.AESGCM(key.to_bytes(key_size//8, 'big'))
        key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
        await aes_gcm_key.send(key_frame)
        await Timer(randint(0,100), 'us')

        for iv_loop in range(0, NBR_OF_IVS):
            iv = randint(0, 2**96-1)
            logger.info(f"Starting IV {iv_loop} with IV 0x{iv:024X}!")

            iv_frame = AxiStreamFrame(tdata=iv.to_bytes(12, 'little'))
            await aes_gcm_iv.send(iv_frame)
            await Timer(randint(0,100), 'us')

            aad_frame_len = randint(MIN_FRAME_SIZE, MAX_FRAME_SIZE)
            data_frame_len = randint(MIN_FRAME_SIZE, MAX_FRAME_SIZE)
            logger.info(f"Starting frame with AAD frame length {aad_frame_len} and data frame length {data_frame_len}!")
            data_to_encrypt = []
            data_to_auth = []

            # Generate the random data
            for i in range(0, aad_frame_len):
                rand_data = randint(0, 255)
                data_to_auth.append(rand_data)
            for i in range(0, data_frame_len):
                rand_data = randint(0, 255)
                data_to_encrypt.append(rand_data)

            # These are inputs to the cipher to validate against. They have
            # the correct format (endianness) required
            cipher_auth_data = bytes(data_to_auth)
            cipher_plaintext_data = bytes(data_to_encrypt)

            logger.debug(f"AAD       = {print_list_in_hex(data_to_auth)}")
            logger.debug(f"AAD       = 0x{int.from_bytes(bytes(data_to_auth), 'big'):032X}")
            logger.debug(f"Plaintext = {print_list_in_hex(data_to_encrypt)}")
            logger.debug(f"Plaintext = 0x{int.from_bytes(bytes(data_to_encrypt), 'big'):032X}")

            axis_aad_frame = AxiStreamFrame(tdata=data_to_auth)
            logger.debug(f"AAD Frame = {print_list_in_hex(data_to_auth)}")
            await aes_gcm_in_aad.send(axis_aad_frame)
            await aes_gcm_in_aad.wait()

            axis_data_frame = AxiStreamFrame(tdata=data_to_encrypt)
            logger.debug(f"Data Frame = {print_list_in_hex(data_to_encrypt)}")
            await aes_gcm_in_data.send(axis_data_frame)
            await aes_gcm_in_data.wait()

            ref = cipher.encrypt(iv.to_bytes(12,'big'), cipher_plaintext_data, cipher_auth_data)
            ref_ciphertext = list(ref[:-16])
            ref_auth_tag = list(ref[-16:])

            aad_out_frame = await aes_gcm_out_aad.recv(compact=False)
            aad_out = aad_out_frame.tdata
            for i in range(0, len(aad_out_frame.tkeep)):
                if (aad_out_frame.tkeep[i] == 1):
                    assert aad_out[i] == data_to_auth[i], f"Invalid AAD received at byte {i}! Expected {print_list_in_hex(data_to_auth)}, received {print_list_in_hex(aad_out)}"

            ciphertext_frame = await aes_gcm_out_data.recv(compact=False)
            logger.debug(f"ciphertext_frame = {ciphertext_frame}")

            ciphertext = list(ciphertext_frame.tdata)
            logger.debug(f"ciphertext     = {print_list_in_hex(ciphertext)}")
            logger.debug(f"ref_ciphertext = {print_list_in_hex(ref_ciphertext)}")


            for i in range(0, len(ciphertext_frame.tkeep)):
                if (ciphertext_frame.tkeep[i] == 1):
                    assert ciphertext[i] == ref_ciphertext[i], f"Invalid ciphertext received at byte {i}! Expected {print_list_in_hex(ref_ciphertext)}, received {print_list_in_hex(ciphertext)}"

            logger.info(f"Ciphertext OK!")

            auth_tag_frame = await aes_gcm_out_auth.recv(compact=False)
            logger.debug(f"auth_tag      = {print_list_in_hex(auth_tag_frame.tdata)}")
            logger.debug(f"ref_auth_tag  = {print_list_in_hex(ref_auth_tag)}")
            auth_tag = auth_tag_frame.tdata
            for i in range(0, len(ref_auth_tag)):
                assert auth_tag[i] == ref_auth_tag[i], f"Invalid auth tag received at byte {i}!, Expected {print_list_in_hex(ref_auth_tag)}, got {print_list_in_hex(auth_tag)}"

            logger.info(f"Frame OK!")

@cocotb.test(skip = False, timeout_time=10, timeout_unit="ms")
async def aes_dec(dut, key_size=256, high_throughput=True):
    logger:logging.Logger = dut._log

    logger.setLevel(logging.WARN)

    cocotb.start_soon(Clock(dut.clk, C_100MHZ_CLK_PERIOD, units="ns").start())

    aes_gcm_key         = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_key_axis"),    dut.clk, dut.rst)
    aes_gcm_iv          = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_iv_axis"),     dut.clk, dut.rst)
    aes_gcm_in_auth     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_in_aad      = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_in_data     = AxiStreamSource(  AxiStreamBus.from_prefix(dut, "dec_s_axis"),        dut.clk, dut.rst)
    aes_gcm_out_auth    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "dec_m_auth_axis"),   dut.clk, dut.rst)
    aes_gcm_out_aad     = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "dec_m_aad_axis"),    dut.clk, dut.rst)
    aes_gcm_out_data    = AxiStreamSink(    AxiStreamBus.from_prefix(dut, "dec_m_axis"),        dut.clk, dut.rst)

    aes_gcm_in_auth.set_pause_generator(backpressure_generator())
    aes_gcm_in_aad.set_pause_generator(backpressure_generator())
    aes_gcm_in_data.set_pause_generator(backpressure_generator())
    aes_gcm_out_auth.set_pause_generator(backpressure_generator())
    aes_gcm_out_aad.set_pause_generator(backpressure_generator())
    aes_gcm_out_data.set_pause_generator(backpressure_generator())


    aes_gcm_key.log.setLevel(logging.WARN)
    aes_gcm_iv.log.setLevel(logging.WARN)
    aes_gcm_in_auth.log.setLevel(logging.WARN)
    aes_gcm_in_aad.log.setLevel(logging.WARN)
    aes_gcm_in_data.log.setLevel(logging.WARN)
    aes_gcm_out_auth.log.setLevel(logging.WARN)
    aes_gcm_out_aad.log.setLevel(logging.WARN)
    aes_gcm_out_data.log.setLevel(logging.WARN)
    logger.setLevel(logging.INFO)

    dut.rst.value = 1

    await ClockCycles(dut.clk, 10)
    await RisingEdge(dut.clk)
    dut.rst.value = 0

    for key_loop in range(0, NBR_OF_KEYS):
        key = randint(0, 2**key_size - 1)
        logger.info(f"Starting key {key_loop} with key 0x{key:032X}!")

        cipher = aead.AESGCM(key.to_bytes(key_size//8, 'big'))
        key_frame = AxiStreamFrame(tdata=key.to_bytes(key_size//8, 'little'))
        await aes_gcm_key.send(key_frame)
        await Timer(randint(0,100), 'us')

        for iv_loop in range(0, NBR_OF_IVS):
            corrupt_auth_tag = True if randint(0,1) == 1 else False
            iv = randint(0, 2**96-1)
            logger.info(f"Starting IV {iv_loop} with IV 0x{iv:024X}!")

            iv_frame = AxiStreamFrame(tdata=iv.to_bytes(12, 'little'))
            await aes_gcm_iv.send(iv_frame)
            await Timer(randint(0,100), 'us')

            aad_frame_len = randint(MIN_FRAME_SIZE, MAX_FRAME_SIZE)
            data_frame_len = randint(MIN_FRAME_SIZE, MAX_FRAME_SIZE)
            logger.info(f"Starting frame with AAD frame length {aad_frame_len} and data frame length {data_frame_len}! Auth tag is {'not ' if not corrupt_auth_tag else ''}corrupted")
            ref_plaintext = []
            data_to_auth = []

            # Generate the random data
            for i in range(0, aad_frame_len):
                rand_data = randint(0, 255)
                data_to_auth.append(rand_data)
            for i in range(0, data_frame_len):
                rand_data = randint(0, 255)
                ref_plaintext.append(rand_data)

            # These are inputs to the cipher to validate against. They have
            # the correct format (endianness) required
            cipher_auth_data = bytes(data_to_auth)
            cipher_plaintext_data = bytes(ref_plaintext)

            logger.debug(f"AAD       = {print_list_in_hex(data_to_auth)}")
            logger.debug(f"AAD       = 0x{int.from_bytes(bytes(data_to_auth), 'big'):032X}")
            logger.debug(f"Plaintext = {print_list_in_hex(ref_plaintext)}")
            logger.debug(f"Plaintext = 0x{int.from_bytes(bytes(ref_plaintext), 'big'):032X}")

            # For simulation, I have to convert from big endian to little
            # endian but it has to be on 16 byte chunks

            ref = cipher.encrypt(iv.to_bytes(12,'big'), cipher_plaintext_data, cipher_auth_data)
            ref_ciphertext = list(ref[:-16])
            ref_auth_tag = list(ref[-16:])

            logger.debug(f"Ciphertext = {print_list_in_hex(ref_ciphertext)}")
            logger.debug(f"Ciphertext = 0x{int.from_bytes(bytes(ref_ciphertext), 'big'):032X}")

            axis_aad_frame = AxiStreamFrame(tdata=data_to_auth)
            logger.debug(f"AAD Frame = {print_list_in_hex(data_to_auth)}")
            await aes_gcm_in_aad.send(axis_aad_frame)
            await aes_gcm_in_aad.wait()

            axis_data_frame = AxiStreamFrame(tdata=ref_ciphertext)
            logger.debug(f"Data Frame = {print_list_in_hex(ref_ciphertext)}")
            await aes_gcm_in_data.send(axis_data_frame)
            await aes_gcm_in_data.wait()

            sent_auth_tag = ref_auth_tag[:]
            corruption_index = randint(0, 15)
            sent_auth_tag[corruption_index] = sent_auth_tag[corruption_index] ^ randint(1,255) if corrupt_auth_tag else sent_auth_tag[corruption_index]
            axis_auth_frame = AxiStreamFrame(tdata=sent_auth_tag)
            logger.debug(f"Sent Auth  = {print_list_in_hex(sent_auth_tag)}")
            await aes_gcm_in_auth.send(axis_auth_frame)
            await aes_gcm_in_auth.wait()

            aad_out_frame = await aes_gcm_out_aad.recv(compact=False)
            aad_out = aad_out_frame.tdata
            for i in range(0, len(aad_out_frame.tkeep)):
                if (aad_out_frame.tkeep[i] == 1):
                    assert aad_out[i] == data_to_auth[i], f"Invalid AAD received at byte {i}! Expected {print_list_in_hex(data_to_auth)}, received {print_list_in_hex(aad_out)}"
            plaintext_frame = await aes_gcm_out_data.recv(compact=False)
            logger.debug(f"plaintext_frame = {plaintext_frame}")

            plaintext = list(plaintext_frame.tdata)
            logger.debug(f"plaintext     = {print_list_in_hex(plaintext)}")
            logger.debug(f"ref_plaintext = {print_list_in_hex(ref_plaintext)}")


            for i in range(0, len(plaintext_frame.tkeep)):
                if (plaintext_frame.tkeep[i] == 1):
                    assert plaintext[i] == ref_plaintext[i], f"Invalid plaintext received at byte {i}!, Expected {print_list_in_hex(ref_plaintext)}, received {print_list_in_hex(plaintext)}"

            logger.info(f"Ciphertext OK!")

            auth_tag_frame = await aes_gcm_out_auth.recv(compact=False)
            logger.debug(f"auth_tag      = {print_list_in_hex(auth_tag_frame.tdata)}")
            logger.debug(f"ref_auth_tag  = {print_list_in_hex(ref_auth_tag)}")
            auth_tag = auth_tag_frame.tdata
            for i in range(0, len(ref_auth_tag)):
                assert auth_tag[i] == ref_auth_tag[i], f"Invalid auth tag received at byte {i}!, Expected {print_list_in_hex(ref_auth_tag)}, got {print_list_in_hex(auth_tag)}"

            fail_flag = int(dut.dec_fail_flag.value)
            if (corrupt_auth_tag):
                assert fail_flag == 1, f"Fail flag has incorrect value! Expected '1' got {fail_flag}"
            else:
                assert fail_flag == 0, f"Fail flag has incorrect value! Expected '0' got {fail_flag}"



            logger.info(f"Frame OK!")