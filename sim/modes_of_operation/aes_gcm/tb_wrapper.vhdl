-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;

library LIB_AES;

entity tb_wrapper is
    generic (
        IV_NBITS_G              : integer := 96;
        KEY_NBITS_G             : integer := 256;
        HIGH_THROUGHPUT_G       : boolean := false
    );
    port (
        clk                     : in    std_logic;
        rst                     : in    std_logic;

        enc_fail_flag           : out   std_logic;

        -- Key AXIS interface
        enc_s_key_axis_tvalid   : in    std_logic;
        enc_s_key_axis_tready   : out   std_logic;
        enc_s_key_axis_tdata    : in    std_logic_vector(KEY_NBITS_G-1 downto 0);

        -- IV AXIS interface
        enc_s_iv_axis_tvalid    : in    std_logic;
        enc_s_iv_axis_tready    : out   std_logic;
        enc_s_iv_axis_tdata     : in    std_logic_vector(IV_NBITS_G-1 downto 0);

        -- Auth tag AXIS interface, used in decrypt mode but not encrypt
        enc_s_auth_axis_tvalid  : in    std_logic;
        enc_s_auth_axis_tready  : out   std_logic;
        enc_s_auth_axis_tdata   : in    std_logic_vector(127 downto 0);

        -- Additional Authenticated Data AXIS input interface, pass-through to
        -- m_aad_axis
        enc_s_aad_axis_tvalid   : in    std_logic;
        enc_s_aad_axis_tready   : out   std_logic;
        enc_s_aad_axis_tlast    : in    std_logic;
        enc_s_aad_axis_tkeep    : in    std_logic_vector(15 downto 0);
        enc_s_aad_axis_tdata    : in    std_logic_vector(127 downto 0);

        -- Data input AXIS interface, plaintext in encrypt mode, ciphertext in
        -- decrypt mode
        enc_s_axis_tvalid       : in    std_logic;
        enc_s_axis_tready       : out   std_logic;
        enc_s_axis_tlast        : in    std_logic;
        enc_s_axis_tkeep        : in    std_logic_vector(15 downto 0);
        enc_s_axis_tdata        : in    std_logic_vector(127 downto 0);

        -- Auth tag AXIS interface,
        --      outputs auth tag in encrypt mode
        --      outputs 0 if authentication check passes otherwise all 1s if it
        --          fails in decrypt mode
        enc_m_auth_axis_tvalid  : out   std_logic;
        enc_m_auth_axis_tready  : in    std_logic;
        enc_m_auth_axis_tdata   : out   std_logic_vector(127 downto 0);

        -- Additional Authenticated Data AXIS output interface, pass-through
        -- from s_aad_axis
        enc_m_aad_axis_tvalid   : out   std_logic;
        enc_m_aad_axis_tready   : in    std_logic;
        enc_m_aad_axis_tlast    : out   std_logic;
        enc_m_aad_axis_tkeep    : out   std_logic_vector(15 downto 0);
        enc_m_aad_axis_tdata    : out   std_logic_vector(127 downto 0);

        -- Data output AXIS interface, ciphertext in encrypt mode, plaintext in
        -- decrypt mode
        enc_m_axis_tvalid       : out   std_logic;
        enc_m_axis_tready       : in    std_logic;
        enc_m_axis_tlast        : out   std_logic;
        enc_m_axis_tkeep        : out   std_logic_vector(15 downto 0);
        enc_m_axis_tdata        : out   std_logic_vector(127 downto 0);

        dec_fail_flag       : out   std_logic;

        -- Key AXIS interface
        dec_s_key_axis_tvalid   : in    std_logic;
        dec_s_key_axis_tready   : out   std_logic;
        dec_s_key_axis_tdata    : in    std_logic_vector(KEY_NBITS_G-1 downto 0);

        -- IV AXIS interface
        dec_s_iv_axis_tvalid    : in    std_logic;
        dec_s_iv_axis_tready    : out   std_logic;
        dec_s_iv_axis_tdata     : in    std_logic_vector(IV_NBITS_G-1 downto 0);

        -- Auth tag AXIS interface, used in decrypt mode but not encrypt
        dec_s_auth_axis_tvalid  : in    std_logic;
        dec_s_auth_axis_tready  : out   std_logic;
        dec_s_auth_axis_tdata   : in    std_logic_vector(127 downto 0);

        -- Additional Authenticated Data AXIS input interface, pass-through to
        -- m_aad_axis
        dec_s_aad_axis_tvalid   : in    std_logic;
        dec_s_aad_axis_tready   : out   std_logic;
        dec_s_aad_axis_tlast    : in    std_logic;
        dec_s_aad_axis_tkeep    : in    std_logic_vector(15 downto 0);
        dec_s_aad_axis_tdata    : in    std_logic_vector(127 downto 0);

        -- Data input AXIS interface, plaintext in encrypt mode, ciphertext in
        -- decrypt mode
        dec_s_axis_tvalid       : in    std_logic;
        dec_s_axis_tready       : out   std_logic;
        dec_s_axis_tlast        : in    std_logic;
        dec_s_axis_tkeep        : in    std_logic_vector(15 downto 0);
        dec_s_axis_tdata        : in    std_logic_vector(127 downto 0);

        -- Auth tag AXIS interface,
        --      outputs auth tag in encrypt mode
        --      outputs 0 if authentication check passes otherwise all 1s if it
        --          fails in decrypt mode
        dec_m_auth_axis_tvalid  : out   std_logic;
        dec_m_auth_axis_tready  : in    std_logic;
        dec_m_auth_axis_tdata   : out   std_logic_vector(127 downto 0);

        -- Additional Authenticated Data AXIS output interface, pass-through
        -- from s_aad_axis
        dec_m_aad_axis_tvalid   : out   std_logic;
        dec_m_aad_axis_tready   : in    std_logic;
        dec_m_aad_axis_tlast    : out   std_logic;
        dec_m_aad_axis_tkeep    : out   std_logic_vector(15 downto 0);
        dec_m_aad_axis_tdata    : out   std_logic_vector(127 downto 0);

        -- Data output AXIS interface, ciphertext in encrypt mode, plaintext in
        -- decrypt mode
        dec_m_axis_tvalid       : out   std_logic;
        dec_m_axis_tready       : in    std_logic;
        dec_m_axis_tlast        : out   std_logic;
        dec_m_axis_tkeep        : out   std_logic_vector(15 downto 0);
        dec_m_axis_tdata        : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of tb_wrapper is
begin
    encrypt_aes_gcm_e : entity LIB_AES.aes_gcm
        generic map (
            IV_NBITS_G          => IV_NBITS_G,
            KEY_NBITS_G         => KEY_NBITS_G,
            DECRYPT_G           => false,
            HIGH_THROUGHPUT_G   => HIGH_THROUGHPUT_G
        )
        port map (
            clk                 => clk,
            rst                 => rst,

            fail_flag           => enc_fail_flag,

            -- Key AXIS interface
            s_key_axis_tvalid   => enc_s_key_axis_tvalid,
            s_key_axis_tready   => enc_s_key_axis_tready,
            s_key_axis_tdata    => enc_s_key_axis_tdata,

            -- IV AXIS interface
            s_iv_axis_tvalid    => enc_s_iv_axis_tvalid,
            s_iv_axis_tready    => enc_s_iv_axis_tready,
            s_iv_axis_tdata     => enc_s_iv_axis_tdata,

            -- Auth tag AXIS interface, used in decrypt mode but not encrypt
            s_auth_axis_tvalid  => enc_s_auth_axis_tvalid,
            s_auth_axis_tready  => enc_s_auth_axis_tready,
            s_auth_axis_tdata   => enc_s_auth_axis_tdata,

            -- Additional Authenticated Data AXIS input interface, pass-through to
            -- m_aad_axis
            s_aad_axis_tvalid   => enc_s_aad_axis_tvalid,
            s_aad_axis_tready   => enc_s_aad_axis_tready,
            s_aad_axis_tlast    => enc_s_aad_axis_tlast,
            s_aad_axis_tkeep    => enc_s_aad_axis_tkeep,
            s_aad_axis_tdata    => enc_s_aad_axis_tdata,

            -- Data input AXIS interface, plaintext in encrypt mode, ciphertext in
            -- decrypt mode
            s_axis_tvalid       => enc_s_axis_tvalid,
            s_axis_tready       => enc_s_axis_tready,
            s_axis_tlast        => enc_s_axis_tlast,
            s_axis_tkeep        => enc_s_axis_tkeep,
            s_axis_tdata        => enc_s_axis_tdata,

            -- Auth tag AXIS interface,
            --      outputs auth tag in encrypt mode
            --      outputs 0 if authentication check passes otherwise all 1s if it
            --          fails in decrypt mode
            m_auth_axis_tvalid  => enc_m_auth_axis_tvalid,
            m_auth_axis_tready  => enc_m_auth_axis_tready,
            m_auth_axis_tdata   => enc_m_auth_axis_tdata,

            -- Additional Authenticated Data AXIS output interface, pass-through
            -- from s_aad_axis
            m_aad_axis_tvalid   => enc_m_aad_axis_tvalid,
            m_aad_axis_tready   => enc_m_aad_axis_tready,
            m_aad_axis_tlast    => enc_m_aad_axis_tlast,
            m_aad_axis_tkeep    => enc_m_aad_axis_tkeep,
            m_aad_axis_tdata    => enc_m_aad_axis_tdata,

            -- Data output AXIS interface, ciphertext in encrypt mode, plaintext in
            -- decrypt mode
            m_axis_tvalid       => enc_m_axis_tvalid,
            m_axis_tready       => enc_m_axis_tready,
            m_axis_tlast        => enc_m_axis_tlast,
            m_axis_tkeep        => enc_m_axis_tkeep,
            m_axis_tdata        => enc_m_axis_tdata
        );

    decrypt_aes_gcm_e : entity LIB_AES.aes_gcm
        generic map (
            IV_NBITS_G          => IV_NBITS_G,
            KEY_NBITS_G         => KEY_NBITS_G,
            DECRYPT_G           => true,
            HIGH_THROUGHPUT_G   => HIGH_THROUGHPUT_G
        )
        port map (
            clk                 => clk,
            rst                 => rst,

            fail_flag           => dec_fail_flag,

            -- Key AXIS interface
            s_key_axis_tvalid   => dec_s_key_axis_tvalid,
            s_key_axis_tready   => dec_s_key_axis_tready,
            s_key_axis_tdata    => dec_s_key_axis_tdata,

            -- IV AXIS interface
            s_iv_axis_tvalid    => dec_s_iv_axis_tvalid,
            s_iv_axis_tready    => dec_s_iv_axis_tready,
            s_iv_axis_tdata     => dec_s_iv_axis_tdata,

            -- Auth tag AXIS interface, used in decrypt mode but not encrypt
            s_auth_axis_tvalid  => dec_s_auth_axis_tvalid,
            s_auth_axis_tready  => dec_s_auth_axis_tready,
            s_auth_axis_tdata   => dec_s_auth_axis_tdata,

            -- Additional Authenticated Data AXIS input interface, pass-through to
            -- m_aad_axis
            s_aad_axis_tvalid   => dec_s_aad_axis_tvalid,
            s_aad_axis_tready   => dec_s_aad_axis_tready,
            s_aad_axis_tlast    => dec_s_aad_axis_tlast,
            s_aad_axis_tkeep    => dec_s_aad_axis_tkeep,
            s_aad_axis_tdata    => dec_s_aad_axis_tdata,

            -- Data input AXIS interface, plaintext in encrypt mode, ciphertext in
            -- decrypt mode
            s_axis_tvalid       => dec_s_axis_tvalid,
            s_axis_tready       => dec_s_axis_tready,
            s_axis_tlast        => dec_s_axis_tlast,
            s_axis_tkeep        => dec_s_axis_tkeep,
            s_axis_tdata        => dec_s_axis_tdata,

            -- Auth tag AXIS interface,
            --      outputs auth tag in encrypt mode
            --      outputs 0 if authentication check passes otherwise all 1s if it
            --          fails in decrypt mode
            m_auth_axis_tvalid  => dec_m_auth_axis_tvalid,
            m_auth_axis_tready  => dec_m_auth_axis_tready,
            m_auth_axis_tdata   => dec_m_auth_axis_tdata,

            -- Additional Authenticated Data AXIS output interface, pass-through
            -- from s_aad_axis
            m_aad_axis_tvalid   => dec_m_aad_axis_tvalid,
            m_aad_axis_tready   => dec_m_aad_axis_tready,
            m_aad_axis_tlast    => dec_m_aad_axis_tlast,
            m_aad_axis_tkeep    => dec_m_aad_axis_tkeep,
            m_aad_axis_tdata    => dec_m_aad_axis_tdata,

            -- Data output AXIS interface, ciphertext in encrypt mode, plaintext in
            -- decrypt mode
            m_axis_tvalid       => dec_m_axis_tvalid,
            m_axis_tready       => dec_m_axis_tready,
            m_axis_tlast        => dec_m_axis_tlast,
            m_axis_tkeep        => dec_m_axis_tkeep,
            m_axis_tdata        => dec_m_axis_tdata
        );
end architecture;