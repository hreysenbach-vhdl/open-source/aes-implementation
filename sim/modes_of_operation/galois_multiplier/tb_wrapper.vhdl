-- Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
--
-- This work is licensed under the CERN Open Hardware Licence Version 2 -
-- Weakly Reciprocal License. You should have received a copy of this license
-- with this code. If not a copy can be found at the address below:
-- https://ohwr.org/project/cernohl/blob/master/licence_texts/cern_ohl_w_v2.txt

library ieee;
use ieee.std_logic_1164.all;

library LIB_AES;

entity tb_wrapper is
    port (
        a       : in    std_logic_vector(127 downto 0);
        b       : in    std_logic_vector(127 downto 0);
        c       : out   std_logic_vector(127 downto 0)
    );
end entity;

architecture rtl of tb_wrapper is
begin
    e_galois_multipler : entity LIB_AES.galois_multiplier
        port map (
            a   => a,
            b   => b,
            c   => c
        );

end architecture;