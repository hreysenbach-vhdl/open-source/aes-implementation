#
# Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python Libraries
import logging
from random import randint

# Third party
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

# Cocotb base
import cocotb
from cocotb.binary import BinaryValue
from cocotb.clock import Clock
from cocotb.triggers import Timer, ClockCycles, RisingEdge
from cocotb.utils import get_sim_time

# Cocotb extensions
from cocotbext.axi import AxiStreamSource, AxiStreamSink, AxiStreamBus, AxiStreamFrame



__author__ = "Haydn Reysenbach"
__copyright__ = "Copyright (C) 2023 Haydn Reysenbach (haydn.reysenbach@gmail.com)"
__credits__ = ["Haydn Reysenbach"]
__license__ = "Apache 2.0"
__version__ = "1.0.0"
__maintainer__ = "Haydn Reysenbach"
__email__ = "haydn.reysenbach@gmail.com"
__status__ = "Development"

def get_bit(x, bit_num):
    bit = (x >> bit_num) & 0x1

    return bit

def set_bit(x, bit_num, value):
    x = x & (~(1 << bit_num))
    x = x | (value << bit_num)

    return x

def reflect(x):
    res = 0
    for i in range(0, 128):
        bit = get_bit(x, i)
        res = set_bit(res, 127-i, bit)

    return res


def galois_mult(x, y):
    z = 0
    v = y

    for i in range(0,128):
        if get_bit(x, 127-i) == 1:
            z = z ^ v
        else:
            z = z

        if get_bit(v, 0) == 0:
            v = v >> 1
        else:
            v = (v >> 1) ^ 0xE1 << 120
    return z

@cocotb.test(skip = False, timeout_time=1, timeout_unit="us")
async def galois_multiplication(dut):
    """
    Test to validate the galois multipler
    """
    logger:logging.Logger = dut._log


    for i in range(0, 10):
        a = randint(0, 2**128-1)
        b = randint(0, 2**128-1)

        ref = galois_mult(a,b)
        logger.info(f"Expected result for {i} = 0x{reflect(ref):032X}")

        dut.a.value = reflect(a)
        dut.b.value = reflect(b)

        await Timer(1, 'ns')
        res = reflect(dut.c.value)

        assert ref == res, f"Unexpected result! Expected 0x{res:032X}, got 0x{ref:032X}"

    

